-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 18, 2020 at 06:29 PM
-- Server version: 5.7.28-0ubuntu0.18.04.4
-- PHP Version: 7.2.24-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wallart`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp_actionscheduler_actions`
--

CREATE TABLE `wp_actionscheduler_actions` (
  `action_id` bigint(20) UNSIGNED NOT NULL,
  `hook` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `scheduled_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `scheduled_date_local` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `args` varchar(191) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `schedule` longtext COLLATE utf8mb4_unicode_520_ci,
  `group_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `attempts` int(11) NOT NULL DEFAULT '0',
  `last_attempt_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_attempt_local` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `claim_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `extended_args` varchar(8000) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_actionscheduler_actions`
--

INSERT INTO `wp_actionscheduler_actions` (`action_id`, `hook`, `status`, `scheduled_date_gmt`, `scheduled_date_local`, `args`, `schedule`, `group_id`, `attempts`, `last_attempt_gmt`, `last_attempt_local`, `claim_id`, `extended_args`) VALUES
(6, 'action_scheduler/migration_hook', 'complete', '2020-09-07 06:10:57', '2020-09-07 06:10:57', '[]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1599459057;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1599459057;}', 1, 1, '2020-09-07 06:11:12', '2020-09-07 06:11:12', 0, NULL),
(7, 'woocommerce_update_marketplace_suggestions', 'complete', '2020-09-07 06:32:39', '2020-09-07 06:32:39', '[]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1599460359;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1599460359;}', 0, 1, '2020-09-07 06:33:50', '2020-09-07 06:33:50', 0, NULL),
(8, 'woocommerce_cleanup_draft_orders', 'complete', '2020-09-07 06:44:23', '2020-09-07 06:44:23', '[]', 'O:32:\"ActionScheduler_IntervalSchedule\":5:{s:22:\"\0*\0scheduled_timestamp\";i:1599461063;s:18:\"\0*\0first_timestamp\";i:1599461063;s:13:\"\0*\0recurrence\";i:86400;s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1599461063;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:86400;}', 0, 1, '2020-09-07 06:44:25', '2020-09-07 06:44:25', 0, NULL),
(9, 'woocommerce_cleanup_draft_orders', 'complete', '2020-09-07 06:44:25', '2020-09-07 06:44:25', '[]', 'O:32:\"ActionScheduler_IntervalSchedule\":5:{s:22:\"\0*\0scheduled_timestamp\";i:1599461065;s:18:\"\0*\0first_timestamp\";i:1599461065;s:13:\"\0*\0recurrence\";i:86400;s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1599461065;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:86400;}', 0, 1, '2020-09-07 06:44:26', '2020-09-07 06:44:26', 0, NULL),
(10, 'woocommerce_cleanup_draft_orders', 'complete', '2020-09-08 06:44:25', '2020-09-08 06:44:25', '[]', 'O:32:\"ActionScheduler_IntervalSchedule\":5:{s:22:\"\0*\0scheduled_timestamp\";i:1599547465;s:18:\"\0*\0first_timestamp\";i:1599461063;s:13:\"\0*\0recurrence\";i:86400;s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1599547465;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:86400;}', 0, 1, '2020-09-08 06:45:29', '2020-09-08 06:45:29', 0, NULL),
(11, 'woocommerce_cleanup_draft_orders', 'complete', '2020-09-08 06:44:26', '2020-09-08 06:44:26', '[]', 'O:32:\"ActionScheduler_IntervalSchedule\":5:{s:22:\"\0*\0scheduled_timestamp\";i:1599547466;s:18:\"\0*\0first_timestamp\";i:1599461065;s:13:\"\0*\0recurrence\";i:86400;s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1599547466;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:86400;}', 0, 1, '2020-09-08 06:45:29', '2020-09-08 06:45:29', 0, NULL),
(12, 'woocommerce_cleanup_draft_orders', 'complete', '2020-09-09 06:45:29', '2020-09-09 06:45:29', '[]', 'O:32:\"ActionScheduler_IntervalSchedule\":5:{s:22:\"\0*\0scheduled_timestamp\";i:1599633929;s:18:\"\0*\0first_timestamp\";i:1599461063;s:13:\"\0*\0recurrence\";i:86400;s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1599633929;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:86400;}', 0, 1, '2020-09-09 07:02:14', '2020-09-09 07:02:14', 0, NULL),
(13, 'woocommerce_cleanup_draft_orders', 'complete', '2020-09-09 06:45:30', '2020-09-09 06:45:30', '[]', 'O:32:\"ActionScheduler_IntervalSchedule\":5:{s:22:\"\0*\0scheduled_timestamp\";i:1599633930;s:18:\"\0*\0first_timestamp\";i:1599461065;s:13:\"\0*\0recurrence\";i:86400;s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1599633930;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:86400;}', 0, 1, '2020-09-09 07:02:14', '2020-09-09 07:02:14', 0, NULL),
(14, 'woocommerce_cleanup_draft_orders', 'complete', '2020-09-10 07:02:14', '2020-09-10 07:02:14', '[]', 'O:32:\"ActionScheduler_IntervalSchedule\":5:{s:22:\"\0*\0scheduled_timestamp\";i:1599721334;s:18:\"\0*\0first_timestamp\";i:1599461063;s:13:\"\0*\0recurrence\";i:86400;s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1599721334;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:86400;}', 0, 1, '2020-09-10 07:03:10', '2020-09-10 07:03:10', 0, NULL),
(15, 'woocommerce_cleanup_draft_orders', 'complete', '2020-09-10 07:02:14', '2020-09-10 07:02:14', '[]', 'O:32:\"ActionScheduler_IntervalSchedule\":5:{s:22:\"\0*\0scheduled_timestamp\";i:1599721334;s:18:\"\0*\0first_timestamp\";i:1599461065;s:13:\"\0*\0recurrence\";i:86400;s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1599721334;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:86400;}', 0, 1, '2020-09-10 07:03:10', '2020-09-10 07:03:10', 0, NULL),
(16, 'woocommerce_cleanup_draft_orders', 'complete', '2020-09-11 07:03:10', '2020-09-11 07:03:10', '[]', 'O:32:\"ActionScheduler_IntervalSchedule\":5:{s:22:\"\0*\0scheduled_timestamp\";i:1599807790;s:18:\"\0*\0first_timestamp\";i:1599461063;s:13:\"\0*\0recurrence\";i:86400;s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1599807790;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:86400;}', 0, 1, '2020-09-11 07:03:16', '2020-09-11 07:03:16', 0, NULL),
(17, 'woocommerce_cleanup_draft_orders', 'complete', '2020-09-11 07:03:10', '2020-09-11 07:03:10', '[]', 'O:32:\"ActionScheduler_IntervalSchedule\":5:{s:22:\"\0*\0scheduled_timestamp\";i:1599807790;s:18:\"\0*\0first_timestamp\";i:1599461065;s:13:\"\0*\0recurrence\";i:86400;s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1599807790;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:86400;}', 0, 1, '2020-09-11 07:03:16', '2020-09-11 07:03:16', 0, NULL),
(18, 'woocommerce_cleanup_draft_orders', 'complete', '2020-09-12 07:03:16', '2020-09-12 07:03:16', '[]', 'O:32:\"ActionScheduler_IntervalSchedule\":5:{s:22:\"\0*\0scheduled_timestamp\";i:1599894196;s:18:\"\0*\0first_timestamp\";i:1599461063;s:13:\"\0*\0recurrence\";i:86400;s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1599894196;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:86400;}', 0, 1, '2020-09-14 05:19:28', '2020-09-14 05:19:28', 0, NULL),
(19, 'woocommerce_cleanup_draft_orders', 'complete', '2020-09-12 07:03:16', '2020-09-12 07:03:16', '[]', 'O:32:\"ActionScheduler_IntervalSchedule\":5:{s:22:\"\0*\0scheduled_timestamp\";i:1599894196;s:18:\"\0*\0first_timestamp\";i:1599461065;s:13:\"\0*\0recurrence\";i:86400;s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1599894196;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:86400;}', 0, 1, '2020-09-14 05:19:51', '2020-09-14 05:19:51', 0, NULL),
(20, 'woocommerce_cleanup_draft_orders', 'complete', '2020-09-15 05:19:28', '2020-09-15 05:19:28', '[]', 'O:32:\"ActionScheduler_IntervalSchedule\":5:{s:22:\"\0*\0scheduled_timestamp\";i:1600147168;s:18:\"\0*\0first_timestamp\";i:1599461063;s:13:\"\0*\0recurrence\";i:86400;s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1600147168;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:86400;}', 0, 1, '2020-09-15 05:20:20', '2020-09-15 05:20:20', 0, NULL),
(21, 'woocommerce_cleanup_draft_orders', 'complete', '2020-09-15 05:19:51', '2020-09-15 05:19:51', '[]', 'O:32:\"ActionScheduler_IntervalSchedule\":5:{s:22:\"\0*\0scheduled_timestamp\";i:1600147191;s:18:\"\0*\0first_timestamp\";i:1599461065;s:13:\"\0*\0recurrence\";i:86400;s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1600147191;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:86400;}', 0, 1, '2020-09-15 05:20:21', '2020-09-15 05:20:21', 0, NULL),
(22, 'woocommerce_cleanup_draft_orders', 'complete', '2020-09-16 05:20:20', '2020-09-16 05:20:20', '[]', 'O:32:\"ActionScheduler_IntervalSchedule\":5:{s:22:\"\0*\0scheduled_timestamp\";i:1600233620;s:18:\"\0*\0first_timestamp\";i:1599461063;s:13:\"\0*\0recurrence\";i:86400;s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1600233620;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:86400;}', 0, 1, '2020-09-16 05:21:13', '2020-09-16 05:21:13', 0, NULL),
(23, 'woocommerce_cleanup_draft_orders', 'complete', '2020-09-16 05:20:21', '2020-09-16 05:20:21', '[]', 'O:32:\"ActionScheduler_IntervalSchedule\":5:{s:22:\"\0*\0scheduled_timestamp\";i:1600233621;s:18:\"\0*\0first_timestamp\";i:1599461065;s:13:\"\0*\0recurrence\";i:86400;s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1600233621;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:86400;}', 0, 1, '2020-09-16 05:21:13', '2020-09-16 05:21:13', 0, NULL),
(24, 'woocommerce_update_marketplace_suggestions', 'complete', '2020-09-15 13:03:05', '2020-09-15 13:03:05', '[]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1600174985;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1600174985;}', 0, 1, '2020-09-15 13:03:58', '2020-09-15 13:03:58', 0, NULL),
(25, 'action_scheduler/migration_hook', 'complete', '2020-09-15 16:47:59', '2020-09-15 16:47:59', '[]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1600188479;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1600188479;}', 1, 1, '2020-09-15 16:48:51', '2020-09-15 16:48:51', 0, NULL),
(26, 'woocommerce_cleanup_draft_orders', 'complete', '2020-09-17 05:21:13', '2020-09-17 05:21:13', '[]', 'O:32:\"ActionScheduler_IntervalSchedule\":5:{s:22:\"\0*\0scheduled_timestamp\";i:1600320073;s:18:\"\0*\0first_timestamp\";i:1599461063;s:13:\"\0*\0recurrence\";i:86400;s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1600320073;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:86400;}', 0, 1, '2020-09-17 06:06:32', '2020-09-17 06:06:32', 0, NULL),
(27, 'woocommerce_cleanup_draft_orders', 'complete', '2020-09-17 05:21:13', '2020-09-17 05:21:13', '[]', 'O:32:\"ActionScheduler_IntervalSchedule\":5:{s:22:\"\0*\0scheduled_timestamp\";i:1600320073;s:18:\"\0*\0first_timestamp\";i:1599461065;s:13:\"\0*\0recurrence\";i:86400;s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1600320073;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:86400;}', 0, 1, '2020-09-17 06:06:32', '2020-09-17 06:06:32', 0, NULL),
(28, 'action_scheduler/migration_hook', 'complete', '2020-09-16 08:06:01', '2020-09-16 08:06:01', '[]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1600243561;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1600243561;}', 1, 1, '2020-09-16 08:06:33', '2020-09-16 08:06:33', 0, NULL),
(29, 'action_scheduler/migration_hook', 'complete', '2020-09-16 08:07:33', '2020-09-16 08:07:33', '[]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1600243653;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1600243653;}', 1, 1, '2020-09-16 08:07:46', '2020-09-16 08:07:46', 0, NULL),
(30, 'action_scheduler/migration_hook', 'complete', '2020-09-16 09:16:16', '2020-09-16 09:16:16', '[]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1600247776;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1600247776;}', 1, 1, '2020-09-16 09:17:27', '2020-09-16 09:17:27', 0, NULL),
(31, 'action_scheduler/migration_hook', 'complete', '2020-09-16 09:18:28', '2020-09-16 09:18:28', '[]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1600247908;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1600247908;}', 1, 1, '2020-09-16 09:19:04', '2020-09-16 09:19:04', 0, NULL),
(32, 'action_scheduler/migration_hook', 'complete', '2020-09-16 09:29:11', '2020-09-16 09:29:11', '[]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1600248551;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1600248551;}', 1, 1, '2020-09-16 09:29:22', '2020-09-16 09:29:22', 0, NULL),
(33, 'action_scheduler/migration_hook', 'complete', '2020-09-16 09:30:22', '2020-09-16 09:30:22', '[]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1600248622;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1600248622;}', 1, 1, '2020-09-16 09:30:24', '2020-09-16 09:30:24', 0, NULL),
(34, 'action_scheduler/migration_hook', 'complete', '2020-09-16 09:32:38', '2020-09-16 09:32:38', '[]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1600248758;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1600248758;}', 1, 1, '2020-09-16 09:32:57', '2020-09-16 09:32:57', 0, NULL),
(35, 'action_scheduler/migration_hook', 'complete', '2020-09-16 09:33:57', '2020-09-16 09:33:57', '[]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1600248837;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1600248837;}', 1, 1, '2020-09-16 09:33:58', '2020-09-16 09:33:58', 0, NULL),
(36, 'action_scheduler/migration_hook', 'complete', '2020-09-16 09:35:32', '2020-09-16 09:35:32', '[]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1600248932;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1600248932;}', 1, 1, '2020-09-16 09:35:42', '2020-09-16 09:35:42', 0, NULL),
(37, 'woocommerce_cleanup_draft_orders', 'complete', '2020-09-18 06:06:32', '2020-09-18 06:06:32', '[]', 'O:32:\"ActionScheduler_IntervalSchedule\":5:{s:22:\"\0*\0scheduled_timestamp\";i:1600409192;s:18:\"\0*\0first_timestamp\";i:1599461063;s:13:\"\0*\0recurrence\";i:86400;s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1600409192;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:86400;}', 0, 1, '2020-09-18 09:02:21', '2020-09-18 09:02:21', 0, NULL),
(38, 'woocommerce_cleanup_draft_orders', 'complete', '2020-09-18 06:06:33', '2020-09-18 06:06:33', '[]', 'O:32:\"ActionScheduler_IntervalSchedule\":5:{s:22:\"\0*\0scheduled_timestamp\";i:1600409193;s:18:\"\0*\0first_timestamp\";i:1599461065;s:13:\"\0*\0recurrence\";i:86400;s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1600409193;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:86400;}', 0, 1, '2020-09-18 09:02:21', '2020-09-18 09:02:21', 0, NULL),
(39, 'woocommerce_cleanup_draft_orders', 'pending', '2020-09-19 09:02:21', '2020-09-19 09:02:21', '[]', 'O:32:\"ActionScheduler_IntervalSchedule\":5:{s:22:\"\0*\0scheduled_timestamp\";i:1600506141;s:18:\"\0*\0first_timestamp\";i:1599461063;s:13:\"\0*\0recurrence\";i:86400;s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1600506141;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:86400;}', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL),
(40, 'woocommerce_cleanup_draft_orders', 'pending', '2020-09-19 09:02:21', '2020-09-19 09:02:21', '[]', 'O:32:\"ActionScheduler_IntervalSchedule\":5:{s:22:\"\0*\0scheduled_timestamp\";i:1600506141;s:18:\"\0*\0first_timestamp\";i:1599461065;s:13:\"\0*\0recurrence\";i:86400;s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1600506141;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:86400;}', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wp_actionscheduler_claims`
--

CREATE TABLE `wp_actionscheduler_claims` (
  `claim_id` bigint(20) UNSIGNED NOT NULL,
  `date_created_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_actionscheduler_groups`
--

CREATE TABLE `wp_actionscheduler_groups` (
  `group_id` bigint(20) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_actionscheduler_groups`
--

INSERT INTO `wp_actionscheduler_groups` (`group_id`, `slug`) VALUES
(1, 'action-scheduler-migration');

-- --------------------------------------------------------

--
-- Table structure for table `wp_actionscheduler_logs`
--

CREATE TABLE `wp_actionscheduler_logs` (
  `log_id` bigint(20) UNSIGNED NOT NULL,
  `action_id` bigint(20) UNSIGNED NOT NULL,
  `message` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `log_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `log_date_local` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_actionscheduler_logs`
--

INSERT INTO `wp_actionscheduler_logs` (`log_id`, `action_id`, `message`, `log_date_gmt`, `log_date_local`) VALUES
(1, 6, 'action created', '2020-09-07 06:09:58', '2020-09-07 06:09:58'),
(2, 6, 'action started via Async Request', '2020-09-07 06:11:12', '2020-09-07 06:11:12'),
(3, 6, 'action complete via Async Request', '2020-09-07 06:11:12', '2020-09-07 06:11:12'),
(4, 7, 'action created', '2020-09-07 06:32:40', '2020-09-07 06:32:40'),
(5, 7, 'action started via WP Cron', '2020-09-07 06:33:49', '2020-09-07 06:33:49'),
(6, 7, 'action complete via WP Cron', '2020-09-07 06:33:50', '2020-09-07 06:33:50'),
(7, 8, 'action created', '2020-09-07 06:44:23', '2020-09-07 06:44:23'),
(8, 8, 'action started via Async Request', '2020-09-07 06:44:23', '2020-09-07 06:44:23'),
(9, 8, 'action complete via Async Request', '2020-09-07 06:44:24', '2020-09-07 06:44:24'),
(10, 9, 'action created', '2020-09-07 06:44:25', '2020-09-07 06:44:25'),
(11, 10, 'action created', '2020-09-07 06:44:25', '2020-09-07 06:44:25'),
(12, 9, 'action started via Async Request', '2020-09-07 06:44:25', '2020-09-07 06:44:25'),
(13, 9, 'action complete via Async Request', '2020-09-07 06:44:25', '2020-09-07 06:44:25'),
(14, 11, 'action created', '2020-09-07 06:44:26', '2020-09-07 06:44:26'),
(15, 10, 'action started via WP Cron', '2020-09-08 06:45:29', '2020-09-08 06:45:29'),
(16, 10, 'action complete via WP Cron', '2020-09-08 06:45:29', '2020-09-08 06:45:29'),
(17, 12, 'action created', '2020-09-08 06:45:29', '2020-09-08 06:45:29'),
(18, 11, 'action started via WP Cron', '2020-09-08 06:45:29', '2020-09-08 06:45:29'),
(19, 11, 'action complete via WP Cron', '2020-09-08 06:45:29', '2020-09-08 06:45:29'),
(20, 13, 'action created', '2020-09-08 06:45:30', '2020-09-08 06:45:30'),
(21, 12, 'action started via WP Cron', '2020-09-09 07:02:13', '2020-09-09 07:02:13'),
(22, 12, 'action complete via WP Cron', '2020-09-09 07:02:14', '2020-09-09 07:02:14'),
(23, 14, 'action created', '2020-09-09 07:02:14', '2020-09-09 07:02:14'),
(24, 13, 'action started via WP Cron', '2020-09-09 07:02:14', '2020-09-09 07:02:14'),
(25, 13, 'action complete via WP Cron', '2020-09-09 07:02:14', '2020-09-09 07:02:14'),
(26, 15, 'action created', '2020-09-09 07:02:14', '2020-09-09 07:02:14'),
(27, 14, 'action started via WP Cron', '2020-09-10 07:03:10', '2020-09-10 07:03:10'),
(28, 14, 'action complete via WP Cron', '2020-09-10 07:03:10', '2020-09-10 07:03:10'),
(29, 16, 'action created', '2020-09-10 07:03:10', '2020-09-10 07:03:10'),
(30, 15, 'action started via WP Cron', '2020-09-10 07:03:10', '2020-09-10 07:03:10'),
(31, 15, 'action complete via WP Cron', '2020-09-10 07:03:10', '2020-09-10 07:03:10'),
(32, 17, 'action created', '2020-09-10 07:03:10', '2020-09-10 07:03:10'),
(33, 16, 'action started via WP Cron', '2020-09-11 07:03:15', '2020-09-11 07:03:15'),
(34, 16, 'action complete via WP Cron', '2020-09-11 07:03:16', '2020-09-11 07:03:16'),
(35, 18, 'action created', '2020-09-11 07:03:16', '2020-09-11 07:03:16'),
(36, 17, 'action started via WP Cron', '2020-09-11 07:03:16', '2020-09-11 07:03:16'),
(37, 17, 'action complete via WP Cron', '2020-09-11 07:03:16', '2020-09-11 07:03:16'),
(38, 19, 'action created', '2020-09-11 07:03:16', '2020-09-11 07:03:16'),
(39, 18, 'action started via WP Cron', '2020-09-14 05:19:26', '2020-09-14 05:19:26'),
(40, 18, 'action complete via WP Cron', '2020-09-14 05:19:27', '2020-09-14 05:19:27'),
(41, 20, 'action created', '2020-09-14 05:19:28', '2020-09-14 05:19:28'),
(42, 19, 'action started via Async Request', '2020-09-14 05:19:50', '2020-09-14 05:19:50'),
(43, 19, 'action complete via Async Request', '2020-09-14 05:19:50', '2020-09-14 05:19:50'),
(44, 21, 'action created', '2020-09-14 05:19:51', '2020-09-14 05:19:51'),
(45, 20, 'action started via WP Cron', '2020-09-15 05:20:20', '2020-09-15 05:20:20'),
(46, 20, 'action complete via WP Cron', '2020-09-15 05:20:20', '2020-09-15 05:20:20'),
(47, 22, 'action created', '2020-09-15 05:20:20', '2020-09-15 05:20:20'),
(48, 21, 'action started via WP Cron', '2020-09-15 05:20:20', '2020-09-15 05:20:20'),
(49, 21, 'action complete via WP Cron', '2020-09-15 05:20:21', '2020-09-15 05:20:21'),
(50, 23, 'action created', '2020-09-15 05:20:21', '2020-09-15 05:20:21'),
(51, 24, 'action created', '2020-09-15 13:03:05', '2020-09-15 13:03:05'),
(52, 24, 'action started via WP Cron', '2020-09-15 13:03:57', '2020-09-15 13:03:57'),
(53, 24, 'action complete via WP Cron', '2020-09-15 13:03:58', '2020-09-15 13:03:58'),
(54, 25, 'action created', '2020-09-15 16:46:59', '2020-09-15 16:46:59'),
(55, 25, 'action started via Async Request', '2020-09-15 16:48:51', '2020-09-15 16:48:51'),
(56, 25, 'action complete via Async Request', '2020-09-15 16:48:51', '2020-09-15 16:48:51'),
(57, 22, 'action started via WP Cron', '2020-09-16 05:21:12', '2020-09-16 05:21:12'),
(58, 22, 'action complete via WP Cron', '2020-09-16 05:21:13', '2020-09-16 05:21:13'),
(59, 26, 'action created', '2020-09-16 05:21:13', '2020-09-16 05:21:13'),
(60, 23, 'action started via WP Cron', '2020-09-16 05:21:13', '2020-09-16 05:21:13'),
(61, 23, 'action complete via WP Cron', '2020-09-16 05:21:13', '2020-09-16 05:21:13'),
(62, 27, 'action created', '2020-09-16 05:21:13', '2020-09-16 05:21:13'),
(63, 28, 'action created', '2020-09-16 08:05:01', '2020-09-16 08:05:01'),
(64, 28, 'action started via WP Cron', '2020-09-16 08:06:32', '2020-09-16 08:06:32'),
(65, 28, 'action complete via WP Cron', '2020-09-16 08:06:33', '2020-09-16 08:06:33'),
(66, 29, 'action created', '2020-09-16 08:06:33', '2020-09-16 08:06:33'),
(67, 29, 'action started via Async Request', '2020-09-16 08:07:45', '2020-09-16 08:07:45'),
(68, 29, 'action complete via Async Request', '2020-09-16 08:07:46', '2020-09-16 08:07:46'),
(69, 30, 'action created', '2020-09-16 09:15:16', '2020-09-16 09:15:16'),
(70, 30, 'action started via WP Cron', '2020-09-16 09:17:27', '2020-09-16 09:17:27'),
(71, 30, 'action complete via WP Cron', '2020-09-16 09:17:27', '2020-09-16 09:17:27'),
(72, 31, 'action created', '2020-09-16 09:17:28', '2020-09-16 09:17:28'),
(73, 31, 'action started via WP Cron', '2020-09-16 09:19:04', '2020-09-16 09:19:04'),
(74, 31, 'action complete via WP Cron', '2020-09-16 09:19:04', '2020-09-16 09:19:04'),
(75, 32, 'action created', '2020-09-16 09:28:11', '2020-09-16 09:28:11'),
(76, 32, 'action started via WP Cron', '2020-09-16 09:29:21', '2020-09-16 09:29:21'),
(77, 32, 'action complete via WP Cron', '2020-09-16 09:29:22', '2020-09-16 09:29:22'),
(78, 33, 'action created', '2020-09-16 09:29:23', '2020-09-16 09:29:23'),
(79, 33, 'action started via WP Cron', '2020-09-16 09:30:24', '2020-09-16 09:30:24'),
(80, 33, 'action complete via WP Cron', '2020-09-16 09:30:24', '2020-09-16 09:30:24'),
(81, 34, 'action created', '2020-09-16 09:31:38', '2020-09-16 09:31:38'),
(82, 34, 'action started via WP Cron', '2020-09-16 09:32:56', '2020-09-16 09:32:56'),
(83, 34, 'action complete via WP Cron', '2020-09-16 09:32:57', '2020-09-16 09:32:57'),
(84, 35, 'action created', '2020-09-16 09:32:57', '2020-09-16 09:32:57'),
(85, 35, 'action started via WP Cron', '2020-09-16 09:33:57', '2020-09-16 09:33:57'),
(86, 35, 'action complete via WP Cron', '2020-09-16 09:33:58', '2020-09-16 09:33:58'),
(87, 36, 'action created', '2020-09-16 09:34:32', '2020-09-16 09:34:32'),
(88, 36, 'action started via Async Request', '2020-09-16 09:35:41', '2020-09-16 09:35:41'),
(89, 36, 'action complete via Async Request', '2020-09-16 09:35:42', '2020-09-16 09:35:42'),
(90, 26, 'action started via WP Cron', '2020-09-17 06:06:32', '2020-09-17 06:06:32'),
(91, 26, 'action complete via WP Cron', '2020-09-17 06:06:32', '2020-09-17 06:06:32'),
(92, 37, 'action created', '2020-09-17 06:06:32', '2020-09-17 06:06:32'),
(93, 27, 'action started via WP Cron', '2020-09-17 06:06:32', '2020-09-17 06:06:32'),
(94, 27, 'action complete via WP Cron', '2020-09-17 06:06:32', '2020-09-17 06:06:32'),
(95, 38, 'action created', '2020-09-17 06:06:33', '2020-09-17 06:06:33'),
(96, 37, 'action started via WP Cron', '2020-09-18 09:02:20', '2020-09-18 09:02:20'),
(97, 37, 'action complete via WP Cron', '2020-09-18 09:02:21', '2020-09-18 09:02:21'),
(98, 39, 'action created', '2020-09-18 09:02:21', '2020-09-18 09:02:21'),
(99, 38, 'action started via WP Cron', '2020-09-18 09:02:21', '2020-09-18 09:02:21'),
(100, 38, 'action complete via WP Cron', '2020-09-18 09:02:21', '2020-09-18 09:02:21'),
(101, 40, 'action created', '2020-09-18 09:02:21', '2020-09-18 09:02:21');

-- --------------------------------------------------------

--
-- Table structure for table `wp_commentmeta`
--

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_commentmeta`
--

INSERT INTO `wp_commentmeta` (`meta_id`, `comment_id`, `meta_key`, `meta_value`) VALUES
(1, 2, 'verified', '0');

-- --------------------------------------------------------

--
-- Table structure for table `wp_comments`
--

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2020-09-06 07:02:24', '2020-09-06 07:02:24', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href=\"https://gravatar.com\">Gravatar</a>.', 0, '1', '', '', 0, 0),
(2, 84, 'admin', 'pramodini.das@thoughtspheres.com', 'http://localhost/wallart', '127.0.0.1', '2020-09-15 16:15:58', '2020-09-15 16:15:58', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 0, '1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/70.0', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `wp_hugeit_slider_slide`
--

CREATE TABLE `wp_hugeit_slider_slide` (
  `id` int(11) UNSIGNED NOT NULL,
  `slider_id` int(11) UNSIGNED NOT NULL,
  `title` varchar(512) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `description` varchar(2048) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `url` varchar(2048) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `attachment_id` bigint(20) UNSIGNED DEFAULT NULL,
  `in_new_tab` int(1) UNSIGNED NOT NULL DEFAULT '1',
  `type` enum('image','video','post') COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `order` int(5) UNSIGNED NOT NULL,
  `post_term_id` bigint(20) UNSIGNED DEFAULT NULL,
  `post_show_title` int(1) UNSIGNED DEFAULT NULL,
  `post_show_description` int(1) UNSIGNED DEFAULT NULL,
  `post_go_to_post` int(1) UNSIGNED DEFAULT NULL,
  `post_max_post_count` int(4) UNSIGNED DEFAULT NULL,
  `video_quality` int(5) UNSIGNED DEFAULT NULL,
  `video_volume` int(3) UNSIGNED DEFAULT NULL,
  `video_show_controls` int(1) UNSIGNED DEFAULT NULL,
  `video_show_info` int(1) UNSIGNED DEFAULT NULL,
  `video_control_color` int(8) UNSIGNED DEFAULT NULL,
  `draft` int(1) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_hugeit_slider_slide`
--

INSERT INTO `wp_hugeit_slider_slide` (`id`, `slider_id`, `title`, `description`, `url`, `attachment_id`, `in_new_tab`, `type`, `order`, `post_term_id`, `post_show_title`, `post_show_description`, `post_go_to_post`, `post_max_post_count`, `video_quality`, `video_volume`, `video_show_controls`, `video_show_info`, `video_control_color`, `draft`) VALUES
(1, 1, 'SPEND MORE...', '<h1>SAVE MORE...</h1><br />\n<p>$100 OFF ON EVERY $950 SPENT</p>', '', 60, 0, 'image', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wp_hugeit_slider_slider`
--

CREATE TABLE `wp_hugeit_slider_slider` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'My New Slider',
  `width` int(4) UNSIGNED NOT NULL DEFAULT '600',
  `height` int(4) UNSIGNED NOT NULL DEFAULT '375',
  `effect` enum('none','cube_h','cube_v','fade','slice_h','slice_v','slide_h','slide_v','scale_out','scale_in','block_scale','kaleidoscope','fan','blind_h','blind_v','random') COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'none',
  `pause_time` int(5) UNSIGNED NOT NULL DEFAULT '4000',
  `change_speed` int(5) UNSIGNED NOT NULL DEFAULT '1000',
  `position` enum('left','right','center') COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'center',
  `show_loading_icon` int(1) UNSIGNED DEFAULT '0',
  `navigate_by` enum('dot','thumbnail','none') COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'none',
  `pause_on_hover` int(1) UNSIGNED NOT NULL DEFAULT '1',
  `video_autoplay` int(1) UNSIGNED DEFAULT '0',
  `random` int(1) UNSIGNED DEFAULT '0',
  `lightbox` int(1) UNSIGNED DEFAULT '0',
  `slide_effect` enum('effect_1','effect_2','effect_3','effect_4','effect_5','effect_6','effect_7','effect_8','effect_9','effect_10') COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'effect_1',
  `open_close_effect` enum('none','unfold','unfold_r','blowup','blowup_r','roadrunner','roadrunner_r','runner','runner_r','rotate','rotate_r') COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'none',
  `arrows_style` enum('arrows_1','arrows_2','arrows_3','arrows_4','arrows_5','arrows_6') COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'arrows_1',
  `itemscount` int(2) UNSIGNED NOT NULL DEFAULT '5',
  `view` enum('none','carousel1','thumb_view') COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'none',
  `controls` int(1) UNSIGNED DEFAULT '1',
  `fullscreen` int(1) UNSIGNED DEFAULT '1',
  `vertical` int(1) UNSIGNED DEFAULT '0',
  `thumbposition` int(1) UNSIGNED DEFAULT '0',
  `thumbcontrols` int(1) UNSIGNED DEFAULT '0',
  `dragdrop` int(1) UNSIGNED DEFAULT '0',
  `swipe` int(1) UNSIGNED DEFAULT '1',
  `thumbdragdrop` int(1) UNSIGNED DEFAULT '0',
  `thumbswipe` int(1) UNSIGNED DEFAULT '0',
  `titleonoff` int(1) UNSIGNED DEFAULT '1',
  `desconoff` int(1) UNSIGNED DEFAULT '1',
  `titlesymbollimit` int(3) UNSIGNED NOT NULL DEFAULT '20',
  `descsymbollimit` int(3) UNSIGNED NOT NULL DEFAULT '70',
  `pager` int(1) UNSIGNED DEFAULT '1',
  `mode` enum('slide','fade') COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'slide',
  `vthumbwidth` int(3) UNSIGNED NOT NULL DEFAULT '100',
  `hthumbheight` int(3) UNSIGNED NOT NULL DEFAULT '80',
  `thumbitem` int(3) UNSIGNED NOT NULL DEFAULT '10',
  `thumbmargin` int(2) UNSIGNED NOT NULL DEFAULT '5'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_hugeit_slider_slider`
--

INSERT INTO `wp_hugeit_slider_slider` (`id`, `name`, `width`, `height`, `effect`, `pause_time`, `change_speed`, `position`, `show_loading_icon`, `navigate_by`, `pause_on_hover`, `video_autoplay`, `random`, `lightbox`, `slide_effect`, `open_close_effect`, `arrows_style`, `itemscount`, `view`, `controls`, `fullscreen`, `vertical`, `thumbposition`, `thumbcontrols`, `dragdrop`, `swipe`, `thumbdragdrop`, `thumbswipe`, `titleonoff`, `desconoff`, `titlesymbollimit`, `descsymbollimit`, `pager`, `mode`, `vthumbwidth`, `hthumbheight`, `thumbitem`, `thumbmargin`) VALUES
(1, 'My First Slider', 1700, 700, 'slide_h', 4500, 700, 'left', 1, 'dot', 1, 0, 0, 0, 'effect_1', 'none', 'arrows_1', 5, 'none', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 20, 70, 1, 'slide', 100, 80, 10, 5);

-- --------------------------------------------------------

--
-- Table structure for table `wp_links`
--

CREATE TABLE `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_mltlngg_terms_translate`
--

CREATE TABLE `wp_mltlngg_terms_translate` (
  `ID` int(6) UNSIGNED NOT NULL,
  `term_ID` int(6) NOT NULL,
  `name` text NOT NULL,
  `language` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wp_mltlngg_translate`
--

CREATE TABLE `wp_mltlngg_translate` (
  `ID` int(6) UNSIGNED NOT NULL,
  `post_ID` int(6) NOT NULL,
  `post_content` longtext NOT NULL,
  `post_excerpt` text NOT NULL,
  `post_title` text NOT NULL,
  `language` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_mltlngg_translate`
--

INSERT INTO `wp_mltlngg_translate` (`ID`, `post_ID`, `post_content`, `post_excerpt`, `post_title`, `language`) VALUES
(1, 26, '<!-- wp:shortcode -->\n[huge_it_slider id=\"1\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:heading {\"customTextColor\":\"#009cdd\",\"className\":\"has-vivid-cyan-blue-color has-text-align-center margin-null\"} -->\n<h2 class=\"has-text-color has-vivid-cyan-blue-color has-text-align-center margin-null\" style=\"color:#009cdd\"><strong>SHOP BY CATEGORY</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"align\":\"center\",\"className\":\"margin-null\"} -->\n<p class=\"has-text-align-center margin-null\"><strong>test purpose only test purpose only test purpose only</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:shortcode -->\n[woo_category_slider id=\'63\']\n<!-- /wp:shortcode -->\n\n<!-- wp:spacer {\"height\":20} -->\n<div style=\"height:20px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:shortcode -->\n[woo_product_slider id=\"75\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:spacer {\"height\":20} -->\n<div style=\"height:20px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:heading {\"customTextColor\":\"#009cdd\",\"className\":\"has-vivid-cyan-blue-color has-text-align-center margin-null\"} -->\n<h2 class=\"has-text-color has-vivid-cyan-blue-color has-text-align-center margin-null\" style=\"color:#009cdd\"><strong>BEST SALES</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:spacer {\"height\":47} -->\n<div style=\"height:47px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:woocommerce/product-best-sellers {\"columns\":4,\"rows\":1,\"alignButtons\":true} /-->\n\n<!-- wp:columns {\"customBackgroundColor\":\"#f3f3f3\",\"className\":\"quadpay-section-home\"} -->\n<div class=\"wp-block-columns has-background quadpay-section-home\" style=\"background-color:#f3f3f3\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:image {\"id\":102,\"sizeSlug\":\"large\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"http://localhost/wallart/wp-content/uploads/2020/09/quad-pay-banner.jpg\" alt=\"\" class=\"wp-image-102\"/></figure>\n<!-- /wp:image --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:spacer {\"height\":52} -->\n<div style=\"height:52px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:heading {\"align\":\"center\",\"level\":1,\"customTextColor\":\"#009cdd\"} -->\n<h1 class=\"has-text-color has-text-align-center\" style=\"color:#009cdd\"><strong>BUY NOW. PAY LATER WITH</strong></h1>\n<!-- /wp:heading -->\n\n<!-- wp:heading {\"align\":\"center\",\"level\":1} -->\n<h1 class=\"has-text-align-center\"><strong>QUAD PAY</strong></h1>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:columns -->\n<div class=\"wp-block-columns\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:heading {\"align\":\"center\"} -->\n<h2 class=\"has-text-align-center\"><strong>499</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"align\":\"center\",\"textColor\":\"vivid-cyan-blue\"} -->\n<p class=\"has-text-color has-text-align-center has-vivid-cyan-blue-color\"><strong>Already Sold</strong></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:heading {\"align\":\"center\"} -->\n<h2 class=\"has-text-align-center\"><strong>125</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"align\":\"center\",\"textColor\":\"vivid-cyan-blue\"} -->\n<p class=\"has-text-color has-text-align-center has-vivid-cyan-blue-color\"><strong>Pre Orders</strong></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:heading {\"align\":\"center\"} -->\n<h2 class=\"has-text-align-center\"><strong>100</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"align\":\"center\",\"textColor\":\"vivid-cyan-blue\"} -->\n<p class=\"has-text-color has-text-align-center has-vivid-cyan-blue-color\"><strong>Only for Order Over</strong></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->', '', 'Welcome', 'en_US');

-- --------------------------------------------------------

--
-- Table structure for table `wp_options`
--

CREATE TABLE `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost/wallart', 'yes'),
(2, 'home', 'http://localhost/wallart', 'yes'),
(3, 'blogname', 'wallart', 'yes'),
(4, 'blogdescription', 'Just another WordPress site', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'pramodini.das@thoughtspheres.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:214:{s:24:\"^wc-auth/v([1]{1})/(.*)?\";s:63:\"index.php?wc-auth-version=$matches[1]&wc-auth-route=$matches[2]\";s:22:\"^wc-api/v([1-3]{1})/?$\";s:51:\"index.php?wc-api-version=$matches[1]&wc-api-route=/\";s:24:\"^wc-api/v([1-3]{1})(.*)?\";s:61:\"index.php?wc-api-version=$matches[1]&wc-api-route=$matches[2]\";s:7:\"shop/?$\";s:27:\"index.php?post_type=product\";s:37:\"shop/feed/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=product&feed=$matches[1]\";s:32:\"shop/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=product&feed=$matches[1]\";s:24:\"shop/page/([0-9]{1,})/?$\";s:45:\"index.php?post_type=product&paged=$matches[1]\";s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:32:\"category/(.+?)/wc-api(/(.*))?/?$\";s:54:\"index.php?category_name=$matches[1]&wc-api=$matches[3]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:29:\"tag/([^/]+)/wc-api(/(.*))?/?$\";s:44:\"index.php?tag=$matches[1]&wc-api=$matches[3]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:46:\"wc_category_slider/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:56:\"wc_category_slider/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:76:\"wc_category_slider/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:71:\"wc_category_slider/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:71:\"wc_category_slider/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:52:\"wc_category_slider/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:35:\"wc_category_slider/([^/]+)/embed/?$\";s:51:\"index.php?wc_category_slider=$matches[1]&embed=true\";s:39:\"wc_category_slider/([^/]+)/trackback/?$\";s:45:\"index.php?wc_category_slider=$matches[1]&tb=1\";s:47:\"wc_category_slider/([^/]+)/page/?([0-9]{1,})/?$\";s:58:\"index.php?wc_category_slider=$matches[1]&paged=$matches[2]\";s:54:\"wc_category_slider/([^/]+)/comment-page-([0-9]{1,})/?$\";s:58:\"index.php?wc_category_slider=$matches[1]&cpage=$matches[2]\";s:44:\"wc_category_slider/([^/]+)/wc-api(/(.*))?/?$\";s:59:\"index.php?wc_category_slider=$matches[1]&wc-api=$matches[3]\";s:50:\"wc_category_slider/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:61:\"wc_category_slider/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:43:\"wc_category_slider/([^/]+)(?:/([0-9]+))?/?$\";s:57:\"index.php?wc_category_slider=$matches[1]&page=$matches[2]\";s:35:\"wc_category_slider/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:45:\"wc_category_slider/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:65:\"wc_category_slider/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:60:\"wc_category_slider/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:60:\"wc_category_slider/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:41:\"wc_category_slider/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:55:\"product-category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_cat=$matches[1]&feed=$matches[2]\";s:50:\"product-category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_cat=$matches[1]&feed=$matches[2]\";s:31:\"product-category/(.+?)/embed/?$\";s:44:\"index.php?product_cat=$matches[1]&embed=true\";s:43:\"product-category/(.+?)/page/?([0-9]{1,})/?$\";s:51:\"index.php?product_cat=$matches[1]&paged=$matches[2]\";s:25:\"product-category/(.+?)/?$\";s:33:\"index.php?product_cat=$matches[1]\";s:52:\"product-tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_tag=$matches[1]&feed=$matches[2]\";s:47:\"product-tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_tag=$matches[1]&feed=$matches[2]\";s:28:\"product-tag/([^/]+)/embed/?$\";s:44:\"index.php?product_tag=$matches[1]&embed=true\";s:40:\"product-tag/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?product_tag=$matches[1]&paged=$matches[2]\";s:22:\"product-tag/([^/]+)/?$\";s:33:\"index.php?product_tag=$matches[1]\";s:35:\"product/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:45:\"product/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:65:\"product/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:60:\"product/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:60:\"product/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:41:\"product/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:24:\"product/([^/]+)/embed/?$\";s:40:\"index.php?product=$matches[1]&embed=true\";s:28:\"product/([^/]+)/trackback/?$\";s:34:\"index.php?product=$matches[1]&tb=1\";s:48:\"product/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?product=$matches[1]&feed=$matches[2]\";s:43:\"product/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?product=$matches[1]&feed=$matches[2]\";s:36:\"product/([^/]+)/page/?([0-9]{1,})/?$\";s:47:\"index.php?product=$matches[1]&paged=$matches[2]\";s:43:\"product/([^/]+)/comment-page-([0-9]{1,})/?$\";s:47:\"index.php?product=$matches[1]&cpage=$matches[2]\";s:33:\"product/([^/]+)/wc-api(/(.*))?/?$\";s:48:\"index.php?product=$matches[1]&wc-api=$matches[3]\";s:39:\"product/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:50:\"product/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:32:\"product/([^/]+)(?:/([0-9]+))?/?$\";s:46:\"index.php?product=$matches[1]&page=$matches[2]\";s:24:\"product/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:34:\"product/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:54:\"product/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:49:\"product/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:49:\"product/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:30:\"product/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:45:\"sp_wps_shortcodes/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:55:\"sp_wps_shortcodes/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:75:\"sp_wps_shortcodes/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:70:\"sp_wps_shortcodes/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:70:\"sp_wps_shortcodes/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:51:\"sp_wps_shortcodes/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:34:\"sp_wps_shortcodes/([^/]+)/embed/?$\";s:65:\"index.php?post_type=sp_wps_shortcodes&name=$matches[1]&embed=true\";s:38:\"sp_wps_shortcodes/([^/]+)/trackback/?$\";s:59:\"index.php?post_type=sp_wps_shortcodes&name=$matches[1]&tb=1\";s:46:\"sp_wps_shortcodes/([^/]+)/page/?([0-9]{1,})/?$\";s:72:\"index.php?post_type=sp_wps_shortcodes&name=$matches[1]&paged=$matches[2]\";s:53:\"sp_wps_shortcodes/([^/]+)/comment-page-([0-9]{1,})/?$\";s:72:\"index.php?post_type=sp_wps_shortcodes&name=$matches[1]&cpage=$matches[2]\";s:43:\"sp_wps_shortcodes/([^/]+)/wc-api(/(.*))?/?$\";s:73:\"index.php?post_type=sp_wps_shortcodes&name=$matches[1]&wc-api=$matches[3]\";s:49:\"sp_wps_shortcodes/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:60:\"sp_wps_shortcodes/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:42:\"sp_wps_shortcodes/([^/]+)(?:/([0-9]+))?/?$\";s:71:\"index.php?post_type=sp_wps_shortcodes&name=$matches[1]&page=$matches[2]\";s:34:\"sp_wps_shortcodes/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:44:\"sp_wps_shortcodes/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:64:\"sp_wps_shortcodes/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:59:\"sp_wps_shortcodes/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:59:\"sp_wps_shortcodes/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:40:\"sp_wps_shortcodes/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:45:\"language_switcher/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:55:\"language_switcher/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:75:\"language_switcher/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:70:\"language_switcher/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:70:\"language_switcher/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:51:\"language_switcher/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:34:\"language_switcher/([^/]+)/embed/?$\";s:50:\"index.php?language_switcher=$matches[1]&embed=true\";s:38:\"language_switcher/([^/]+)/trackback/?$\";s:44:\"index.php?language_switcher=$matches[1]&tb=1\";s:46:\"language_switcher/([^/]+)/page/?([0-9]{1,})/?$\";s:57:\"index.php?language_switcher=$matches[1]&paged=$matches[2]\";s:53:\"language_switcher/([^/]+)/comment-page-([0-9]{1,})/?$\";s:57:\"index.php?language_switcher=$matches[1]&cpage=$matches[2]\";s:43:\"language_switcher/([^/]+)/wc-api(/(.*))?/?$\";s:58:\"index.php?language_switcher=$matches[1]&wc-api=$matches[3]\";s:49:\"language_switcher/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:60:\"language_switcher/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:42:\"language_switcher/([^/]+)(?:/([0-9]+))?/?$\";s:56:\"index.php?language_switcher=$matches[1]&page=$matches[2]\";s:34:\"language_switcher/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:44:\"language_switcher/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:64:\"language_switcher/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:59:\"language_switcher/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:59:\"language_switcher/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:40:\"language_switcher/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:39:\"index.php?&page_id=26&cpage=$matches[1]\";s:17:\"wc-api(/(.*))?/?$\";s:29:\"index.php?&wc-api=$matches[2]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:26:\"comments/wc-api(/(.*))?/?$\";s:29:\"index.php?&wc-api=$matches[2]\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:29:\"search/(.+)/wc-api(/(.*))?/?$\";s:42:\"index.php?s=$matches[1]&wc-api=$matches[3]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:32:\"author/([^/]+)/wc-api(/(.*))?/?$\";s:52:\"index.php?author_name=$matches[1]&wc-api=$matches[3]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:54:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/wc-api(/(.*))?/?$\";s:82:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&wc-api=$matches[5]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:41:\"([0-9]{4})/([0-9]{1,2})/wc-api(/(.*))?/?$\";s:66:\"index.php?year=$matches[1]&monthnum=$matches[2]&wc-api=$matches[4]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:28:\"([0-9]{4})/wc-api(/(.*))?/?$\";s:45:\"index.php?year=$matches[1]&wc-api=$matches[3]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:25:\"(.?.+?)/wc-api(/(.*))?/?$\";s:49:\"index.php?pagename=$matches[1]&wc-api=$matches[3]\";s:28:\"(.?.+?)/order-pay(/(.*))?/?$\";s:52:\"index.php?pagename=$matches[1]&order-pay=$matches[3]\";s:33:\"(.?.+?)/order-received(/(.*))?/?$\";s:57:\"index.php?pagename=$matches[1]&order-received=$matches[3]\";s:25:\"(.?.+?)/orders(/(.*))?/?$\";s:49:\"index.php?pagename=$matches[1]&orders=$matches[3]\";s:29:\"(.?.+?)/view-order(/(.*))?/?$\";s:53:\"index.php?pagename=$matches[1]&view-order=$matches[3]\";s:28:\"(.?.+?)/downloads(/(.*))?/?$\";s:52:\"index.php?pagename=$matches[1]&downloads=$matches[3]\";s:31:\"(.?.+?)/edit-account(/(.*))?/?$\";s:55:\"index.php?pagename=$matches[1]&edit-account=$matches[3]\";s:31:\"(.?.+?)/edit-address(/(.*))?/?$\";s:55:\"index.php?pagename=$matches[1]&edit-address=$matches[3]\";s:34:\"(.?.+?)/payment-methods(/(.*))?/?$\";s:58:\"index.php?pagename=$matches[1]&payment-methods=$matches[3]\";s:32:\"(.?.+?)/lost-password(/(.*))?/?$\";s:56:\"index.php?pagename=$matches[1]&lost-password=$matches[3]\";s:34:\"(.?.+?)/customer-logout(/(.*))?/?$\";s:58:\"index.php?pagename=$matches[1]&customer-logout=$matches[3]\";s:37:\"(.?.+?)/add-payment-method(/(.*))?/?$\";s:61:\"index.php?pagename=$matches[1]&add-payment-method=$matches[3]\";s:40:\"(.?.+?)/delete-payment-method(/(.*))?/?$\";s:64:\"index.php?pagename=$matches[1]&delete-payment-method=$matches[3]\";s:45:\"(.?.+?)/set-default-payment-method(/(.*))?/?$\";s:69:\"index.php?pagename=$matches[1]&set-default-payment-method=$matches[3]\";s:31:\".?.+?/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:42:\".?.+?/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:25:\"([^/]+)/wc-api(/(.*))?/?$\";s:45:\"index.php?name=$matches[1]&wc-api=$matches[3]\";s:31:\"[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:42:\"[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:7:{i:0;s:23:\"slider-image/slider.php\";i:1;s:37:\"translatepress-multilingual/index.php\";i:2;s:36:\"woo-best-selling-products/woobsp.php\";i:3;s:57:\"woo-category-slider-by-pluginever/woo-category-slider.php\";i:4;s:27:\"woo-product-slider/main.php\";i:5;s:67:\"woocommerce-simple-registration/woocommerce-simple-registration.php\";i:6;s:27:\"woocommerce/woocommerce.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', 'a:4:{i:0;s:75:\"/var/www/html/wallart/wp-content/uploads/bws-custom-code/bws-custom-code.js\";i:1;s:76:\"/var/www/html/wallart/wp-content/uploads/bws-custom-code/bws-custom-code.php\";i:2;s:76:\"/var/www/html/wallart/wp-content/uploads/bws-custom-code/bws-custom-code.css\";i:4;s:0:\"\";}', 'no'),
(40, 'template', 'storefront', 'yes'),
(41, 'stylesheet', 'storefront', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '47018', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '1', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:3:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;i:3;a:4:{s:5:\"title\";s:10:\"Contact Us\";s:4:\"text\";s:79:\"Email: support@framehood.com\r\nAddress: \r\n3 Lylia Dr.\r\nWest New York, \r\nNJ 07093\";s:6:\"filter\";b:1;s:6:\"visual\";b:1;}}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:2:{s:31:\"multilanguage/multilanguage.php\";s:22:\"mltlngg_delete_options\";s:17:\"weglot/weglot.php\";s:23:\"weglot_plugin_uninstall\";}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '27', 'yes'),
(84, 'page_on_front', '26', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '1', 'yes'),
(93, 'admin_email_lifespan', '1614927737', 'yes'),
(94, 'initial_db_version', '47018', 'yes'),
(95, 'wp_user_roles', 'a:7:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:114:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:18:\"manage_woocommerce\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}s:8:\"customer\";a:2:{s:4:\"name\";s:8:\"Customer\";s:12:\"capabilities\";a:1:{s:4:\"read\";b:1;}}s:12:\"shop_manager\";a:2:{s:4:\"name\";s:12:\"Shop manager\";s:12:\"capabilities\";a:92:{s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:4:\"read\";b:1;s:18:\"read_private_pages\";b:1;s:18:\"read_private_posts\";b:1;s:10:\"edit_posts\";b:1;s:10:\"edit_pages\";b:1;s:20:\"edit_published_posts\";b:1;s:20:\"edit_published_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"edit_private_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:17:\"edit_others_pages\";b:1;s:13:\"publish_posts\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_posts\";b:1;s:12:\"delete_pages\";b:1;s:20:\"delete_private_pages\";b:1;s:20:\"delete_private_posts\";b:1;s:22:\"delete_published_pages\";b:1;s:22:\"delete_published_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:19:\"delete_others_pages\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:17:\"moderate_comments\";b:1;s:12:\"upload_files\";b:1;s:6:\"export\";b:1;s:6:\"import\";b:1;s:10:\"list_users\";b:1;s:18:\"edit_theme_options\";b:1;s:18:\"manage_woocommerce\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;}}}', 'yes'),
(96, 'fresh_site', '0', 'yes'),
(97, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'sidebars_widgets', 'a:8:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:7:{i:0;s:23:\"multi_language_widget-2\";i:1;s:8:\"search-2\";i:2;s:14:\"recent-posts-2\";i:3;s:17:\"recent-comments-2\";i:4;s:10:\"archives-2\";i:5;s:12:\"categories-2\";i:6;s:6:\"meta-2\";}s:8:\"header-1\";a:0:{}s:8:\"footer-1\";a:1:{i:0;s:10:\"nav_menu-3\";}s:8:\"footer-2\";a:1:{i:0;s:6:\"text-3\";}s:8:\"footer-3\";a:0:{}s:8:\"footer-4\";a:0:{}s:13:\"array_version\";i:3;}', 'yes'),
(103, 'cron', 'a:20:{i:1600419746;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1600419772;a:1:{s:26:\"action_scheduler_run_queue\";a:1:{s:32:\"0d04ed39571b55704c122d726248bbac\";a:3:{s:8:\"schedule\";s:12:\"every_minute\";s:4:\"args\";a:1:{i:0;s:7:\"WP Cron\";}s:8:\"interval\";i:60;}}}i:1600420183;a:1:{s:24:\"woocommerce_cleanup_logs\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1600420191;a:1:{s:33:\"wc_admin_process_orders_milestone\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1600420276;a:1:{s:29:\"wc_admin_unsnooze_admin_notes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1600423345;a:1:{s:32:\"woocommerce_cancel_unpaid_orders\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:2:{s:8:\"schedule\";b:0;s:4:\"args\";a:0:{}}}}i:1600430983;a:1:{s:28:\"woocommerce_cleanup_sessions\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1600455746;a:2:{s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1600455747;a:1:{s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1600473600;a:1:{s:27:\"woocommerce_scheduled_sales\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1600495793;a:2:{s:33:\"woocommerce_cleanup_personal_data\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:30:\"woocommerce_tracker_send_event\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1600495796;a:1:{s:14:\"wc_admin_daily\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1600498946;a:1:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1600498982;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1600498986;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1600671746;a:1:{s:30:\"wp_site_health_scheduled_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"weekly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:604800;}}}i:1600755043;a:1:{s:25:\"woocommerce_geoip_updater\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:11:\"fifteendays\";s:4:\"args\";a:0:{}s:8:\"interval\";i:1296000;}}}i:1600769777;a:1:{s:25:\"hugeit_slider_opt_in_cron\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:20:\"hugeit-slider-weekly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:604800;}}}i:1602839753;a:1:{s:25:\"otgs_send_components_data\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:2:{s:8:\"schedule\";b:0;s:4:\"args\";a:0:{}}}}s:7:\"version\";i:2;}', 'yes'),
(104, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_nav_menu', 'a:2:{s:12:\"_multiwidget\";i:1;i:3;a:2:{s:5:\"title\";s:5:\"Links\";s:8:\"nav_menu\";i:24;}}', 'yes'),
(112, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(114, 'recovery_keys', 'a:0:{}', 'yes'),
(116, 'theme_mods_twentytwenty', 'a:3:{s:18:\"custom_css_post_id\";i:-1;s:16:\"background_color\";s:3:\"fff\";s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1599653843;s:4:\"data\";a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:3:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";}s:9:\"sidebar-2\";a:3:{i:0;s:10:\"archives-2\";i:1;s:12:\"categories-2\";i:2;s:6:\"meta-2\";}}}}', 'yes'),
(129, 'can_compress_scripts', '0', 'no'),
(153, 'recently_activated', 'a:4:{s:17:\"weglot/weglot.php\";i:1600248872;s:45:\"woocommerce-multilingual/wpml-woocommerce.php\";i:1600248490;s:31:\"multilanguage/multilanguage.php\";i:1600243500;s:39:\"wpb-woocommerce-product-slider/main.php\";i:1600188419;}', 'yes'),
(162, 'action_scheduler_hybrid_store_demarkation', '5', 'yes'),
(163, 'schema-ActionScheduler_StoreSchema', '3.0.1599458954', 'yes'),
(164, 'schema-ActionScheduler_LoggerSchema', '2.0.1599458955', 'yes'),
(167, 'woocommerce_schema_version', '430', 'yes'),
(168, 'woocommerce_store_address', 'Niladri Vihar', 'yes'),
(169, 'woocommerce_store_address_2', '', 'yes'),
(170, 'woocommerce_store_city', 'BBSR', 'yes'),
(171, 'woocommerce_default_country', 'IN:OR', 'yes'),
(172, 'woocommerce_store_postcode', '751016', 'yes'),
(173, 'woocommerce_allowed_countries', 'all', 'yes'),
(174, 'woocommerce_all_except_countries', 'a:0:{}', 'yes'),
(175, 'woocommerce_specific_allowed_countries', 'a:0:{}', 'yes'),
(176, 'woocommerce_ship_to_countries', '', 'yes'),
(177, 'woocommerce_specific_ship_to_countries', 'a:0:{}', 'yes'),
(178, 'woocommerce_default_customer_address', 'base', 'yes'),
(179, 'woocommerce_calc_taxes', 'yes', 'yes'),
(180, 'woocommerce_enable_coupons', 'yes', 'yes'),
(181, 'woocommerce_calc_discounts_sequentially', 'no', 'no'),
(182, 'woocommerce_currency', 'AUD', 'yes'),
(183, 'woocommerce_currency_pos', 'left', 'yes'),
(184, 'woocommerce_price_thousand_sep', ',', 'yes'),
(185, 'woocommerce_price_decimal_sep', '.', 'yes'),
(186, 'woocommerce_price_num_decimals', '2', 'yes'),
(187, 'woocommerce_shop_page_id', '6', 'yes'),
(188, 'woocommerce_cart_redirect_after_add', 'no', 'yes'),
(189, 'woocommerce_enable_ajax_add_to_cart', 'yes', 'yes'),
(190, 'woocommerce_placeholder_image', '5', 'yes'),
(191, 'woocommerce_weight_unit', 'kg', 'yes'),
(192, 'woocommerce_dimension_unit', 'cm', 'yes'),
(193, 'woocommerce_enable_reviews', 'yes', 'yes'),
(194, 'woocommerce_review_rating_verification_label', 'yes', 'no'),
(195, 'woocommerce_review_rating_verification_required', 'no', 'no'),
(196, 'woocommerce_enable_review_rating', 'yes', 'yes'),
(197, 'woocommerce_review_rating_required', 'yes', 'no'),
(198, 'woocommerce_manage_stock', 'yes', 'yes'),
(199, 'woocommerce_hold_stock_minutes', '60', 'no'),
(200, 'woocommerce_notify_low_stock', 'yes', 'no'),
(201, 'woocommerce_notify_no_stock', 'yes', 'no'),
(202, 'woocommerce_stock_email_recipient', 'pramodini.das@thoughtspheres.com', 'no'),
(203, 'woocommerce_notify_low_stock_amount', '2', 'no'),
(204, 'woocommerce_notify_no_stock_amount', '0', 'yes'),
(205, 'woocommerce_hide_out_of_stock_items', 'no', 'yes'),
(206, 'woocommerce_stock_format', '', 'yes'),
(207, 'woocommerce_file_download_method', 'force', 'no'),
(208, 'woocommerce_downloads_require_login', 'no', 'no'),
(209, 'woocommerce_downloads_grant_access_after_payment', 'yes', 'no'),
(210, 'woocommerce_downloads_add_hash_to_filename', 'yes', 'yes'),
(211, 'woocommerce_prices_include_tax', 'no', 'yes'),
(212, 'woocommerce_tax_based_on', 'shipping', 'yes'),
(213, 'woocommerce_shipping_tax_class', 'inherit', 'yes'),
(214, 'woocommerce_tax_round_at_subtotal', 'no', 'yes'),
(215, 'woocommerce_tax_classes', '', 'yes'),
(216, 'woocommerce_tax_display_shop', 'excl', 'yes'),
(217, 'woocommerce_tax_display_cart', 'excl', 'yes'),
(218, 'woocommerce_price_display_suffix', '', 'yes'),
(219, 'woocommerce_tax_total_display', 'itemized', 'no'),
(220, 'woocommerce_enable_shipping_calc', 'yes', 'no'),
(221, 'woocommerce_shipping_cost_requires_address', 'no', 'yes'),
(222, 'woocommerce_ship_to_destination', 'billing', 'no'),
(223, 'woocommerce_shipping_debug_mode', 'no', 'yes'),
(224, 'woocommerce_enable_guest_checkout', 'yes', 'no'),
(225, 'woocommerce_enable_checkout_login_reminder', 'no', 'no'),
(226, 'woocommerce_enable_signup_and_login_from_checkout', 'yes', 'no'),
(227, 'woocommerce_enable_myaccount_registration', 'yes', 'no'),
(228, 'woocommerce_registration_generate_username', 'yes', 'no'),
(229, 'woocommerce_registration_generate_password', 'yes', 'no'),
(230, 'woocommerce_erasure_request_removes_order_data', 'no', 'no'),
(231, 'woocommerce_erasure_request_removes_download_data', 'no', 'no'),
(232, 'woocommerce_allow_bulk_remove_personal_data', 'no', 'no'),
(233, 'woocommerce_registration_privacy_policy_text', 'Your personal data will be used to support your experience throughout this website, to manage access to your account, and for other purposes described in our [privacy_policy].', 'yes'),
(234, 'woocommerce_checkout_privacy_policy_text', 'Your personal data will be used to process your order, support your experience throughout this website, and for other purposes described in our [privacy_policy].', 'yes'),
(235, 'woocommerce_delete_inactive_accounts', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:6:\"months\";}', 'no'),
(236, 'woocommerce_trash_pending_orders', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:4:\"days\";}', 'no'),
(237, 'woocommerce_trash_failed_orders', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:4:\"days\";}', 'no'),
(238, 'woocommerce_trash_cancelled_orders', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:4:\"days\";}', 'no'),
(239, 'woocommerce_anonymize_completed_orders', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:6:\"months\";}', 'no'),
(240, 'woocommerce_email_from_name', 'wallart', 'no'),
(241, 'woocommerce_email_from_address', 'pramodini.das@thoughtspheres.com', 'no'),
(242, 'woocommerce_email_header_image', '', 'no'),
(243, 'woocommerce_email_footer_text', '{site_title} &mdash; Built with {WooCommerce}', 'no'),
(244, 'woocommerce_email_base_color', '#96588a', 'no'),
(245, 'woocommerce_email_background_color', '#f7f7f7', 'no'),
(246, 'woocommerce_email_body_background_color', '#ffffff', 'no'),
(247, 'woocommerce_email_text_color', '#3c3c3c', 'no'),
(248, 'woocommerce_cart_page_id', '7', 'no'),
(249, 'woocommerce_checkout_page_id', '8', 'no'),
(250, 'woocommerce_myaccount_page_id', '9', 'no'),
(251, 'woocommerce_terms_page_id', '', 'no'),
(252, 'woocommerce_force_ssl_checkout', 'no', 'yes'),
(253, 'woocommerce_unforce_ssl_checkout', 'no', 'yes'),
(254, 'woocommerce_checkout_pay_endpoint', 'order-pay', 'yes'),
(255, 'woocommerce_checkout_order_received_endpoint', 'order-received', 'yes'),
(256, 'woocommerce_myaccount_add_payment_method_endpoint', 'add-payment-method', 'yes'),
(257, 'woocommerce_myaccount_delete_payment_method_endpoint', 'delete-payment-method', 'yes'),
(258, 'woocommerce_myaccount_set_default_payment_method_endpoint', 'set-default-payment-method', 'yes'),
(259, 'woocommerce_myaccount_orders_endpoint', 'orders', 'yes'),
(260, 'woocommerce_myaccount_view_order_endpoint', 'view-order', 'yes'),
(261, 'woocommerce_myaccount_downloads_endpoint', 'downloads', 'yes'),
(262, 'woocommerce_myaccount_edit_account_endpoint', 'edit-account', 'yes'),
(263, 'woocommerce_myaccount_edit_address_endpoint', 'edit-address', 'yes'),
(264, 'woocommerce_myaccount_payment_methods_endpoint', 'payment-methods', 'yes'),
(265, 'woocommerce_myaccount_lost_password_endpoint', 'lost-password', 'yes'),
(266, 'woocommerce_logout_endpoint', 'customer-logout', 'yes'),
(267, 'woocommerce_api_enabled', 'no', 'yes'),
(268, 'woocommerce_allow_tracking', 'yes', 'no'),
(269, 'woocommerce_show_marketplace_suggestions', 'yes', 'no'),
(270, 'woocommerce_single_image_width', '600', 'yes'),
(271, 'woocommerce_thumbnail_image_width', '300', 'yes'),
(272, 'woocommerce_checkout_highlight_required_fields', 'yes', 'yes'),
(273, 'woocommerce_demo_store', 'no', 'no'),
(275, 'woocommerce_permalinks', 'a:5:{s:12:\"product_base\";s:7:\"product\";s:13:\"category_base\";s:16:\"product-category\";s:8:\"tag_base\";s:11:\"product-tag\";s:14:\"attribute_base\";s:0:\"\";s:22:\"use_verbose_page_rules\";b:0;}', 'yes'),
(276, 'current_theme_supports_woocommerce', 'yes', 'yes'),
(277, 'woocommerce_queue_flush_rewrite_rules', 'no', 'yes'),
(278, '_transient_wc_attribute_taxonomies', 'a:0:{}', 'yes'),
(280, 'default_product_cat', '15', 'yes');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(281, 'woocommerce_admin_notices', 'a:1:{i:0;s:7:\"install\";}', 'yes'),
(284, 'woocommerce_version', '4.4.1', 'yes'),
(285, 'woocommerce_db_version', '4.4.1', 'yes'),
(286, 'woocommerce_homescreen_enabled', 'yes', 'yes'),
(289, 'action_scheduler_lock_async-request-runner', '1600419806', 'yes'),
(290, 'woocommerce_maxmind_geolocation_settings', 'a:1:{s:15:\"database_prefix\";s:32:\"pKxf9709VIh3uoD7AoozuOGfNlHrUISC\";}', 'yes'),
(291, '_transient_woocommerce_webhook_ids_status_active', 'a:0:{}', 'yes'),
(292, 'widget_woocommerce_widget_cart', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(293, 'widget_woocommerce_layered_nav_filters', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(294, 'widget_woocommerce_layered_nav', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(295, 'widget_woocommerce_price_filter', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(296, 'widget_woocommerce_product_categories', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(297, 'widget_woocommerce_product_search', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(298, 'widget_woocommerce_product_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(299, 'widget_woocommerce_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(300, 'widget_woocommerce_recently_viewed_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(301, 'widget_woocommerce_top_rated_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(302, 'widget_woocommerce_recent_reviews', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(303, 'widget_woocommerce_rating_filter', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(306, 'woocommerce_admin_version', '1.4.0', 'yes'),
(307, 'woocommerce_admin_install_timestamp', '1599458997', 'yes'),
(311, 'wc_admin_note_home_screen_feedback_homescreen_accessed', '1599458997', 'yes'),
(314, 'woocommerce_meta_box_errors', 'a:0:{}', 'yes'),
(315, 'wc_blocks_db_schema_version', '260', 'yes'),
(319, 'woocommerce_onboarding_profile', 'a:9:{s:9:\"completed\";b:1;s:7:\"skipped\";b:1;s:12:\"setup_client\";b:0;s:8:\"industry\";a:1:{i:0;a:1:{s:4:\"slug\";s:21:\"art-music-photography\";}}s:13:\"product_types\";a:1:{i:0;s:8:\"physical\";}s:13:\"product_count\";s:1:\"0\";s:14:\"selling_venues\";s:2:\"no\";s:19:\"business_extensions\";a:0:{}s:7:\"plugins\";s:7:\"skipped\";}', 'yes'),
(321, 'woocommerce_onboarding_opt_in', 'yes', 'yes'),
(324, '_transient_woocommerce_reports-transient-version', '1599546732', 'yes'),
(339, 'woocommerce_task_list_welcome_modal_dismissed', '1', 'yes'),
(343, '_transient_shipping-transient-version', '1599459296', 'yes'),
(344, '_transient_timeout_wc_shipping_method_count', '1602051296', 'no'),
(345, '_transient_wc_shipping_method_count', 'a:2:{s:7:\"version\";s:10:\"1599459296\";s:5:\"value\";i:0;}', 'no'),
(372, 'woocommerce_marketplace_suggestions', 'a:2:{s:11:\"suggestions\";a:26:{i:0;a:4:{s:4:\"slug\";s:28:\"product-edit-meta-tab-header\";s:7:\"context\";s:28:\"product-edit-meta-tab-header\";s:5:\"title\";s:22:\"Recommended extensions\";s:13:\"allow-dismiss\";b:0;}i:1;a:6:{s:4:\"slug\";s:39:\"product-edit-meta-tab-footer-browse-all\";s:7:\"context\";s:28:\"product-edit-meta-tab-footer\";s:9:\"link-text\";s:21:\"Browse all extensions\";s:3:\"url\";s:64:\"https://woocommerce.com/product-category/woocommerce-extensions/\";s:8:\"promoted\";s:31:\"category-woocommerce-extensions\";s:13:\"allow-dismiss\";b:0;}i:2;a:9:{s:4:\"slug\";s:46:\"product-edit-mailchimp-woocommerce-memberships\";s:7:\"product\";s:33:\"woocommerce-memberships-mailchimp\";s:14:\"show-if-active\";a:1:{i:0;s:23:\"woocommerce-memberships\";}s:7:\"context\";a:1:{i:0;s:26:\"product-edit-meta-tab-body\";}s:4:\"icon\";s:116:\"https://woocommerce.com/wp-content/plugins/wccom-plugins/marketplace-suggestions/icons/mailchimp-for-memberships.svg\";s:5:\"title\";s:25:\"Mailchimp for Memberships\";s:4:\"copy\";s:79:\"Completely automate your email lists by syncing membership changes to Mailchimp\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:67:\"https://woocommerce.com/products/mailchimp-woocommerce-memberships/\";}i:3;a:9:{s:4:\"slug\";s:19:\"product-edit-addons\";s:7:\"product\";s:26:\"woocommerce-product-addons\";s:14:\"show-if-active\";a:2:{i:0;s:25:\"woocommerce-subscriptions\";i:1;s:20:\"woocommerce-bookings\";}s:7:\"context\";a:1:{i:0;s:26:\"product-edit-meta-tab-body\";}s:4:\"icon\";s:106:\"https://woocommerce.com/wp-content/plugins/wccom-plugins/marketplace-suggestions/icons/product-add-ons.svg\";s:5:\"title\";s:15:\"Product Add-Ons\";s:4:\"copy\";s:93:\"Offer add-ons like gift wrapping, special messages or other special options for your products\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:49:\"https://woocommerce.com/products/product-add-ons/\";}i:4;a:9:{s:4:\"slug\";s:46:\"product-edit-woocommerce-subscriptions-gifting\";s:7:\"product\";s:33:\"woocommerce-subscriptions-gifting\";s:14:\"show-if-active\";a:1:{i:0;s:25:\"woocommerce-subscriptions\";}s:7:\"context\";a:1:{i:0;s:26:\"product-edit-meta-tab-body\";}s:4:\"icon\";s:116:\"https://woocommerce.com/wp-content/plugins/wccom-plugins/marketplace-suggestions/icons/gifting-for-subscriptions.svg\";s:5:\"title\";s:25:\"Gifting for Subscriptions\";s:4:\"copy\";s:70:\"Let customers buy subscriptions for others - they\'re the ultimate gift\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:67:\"https://woocommerce.com/products/woocommerce-subscriptions-gifting/\";}i:5;a:9:{s:4:\"slug\";s:42:\"product-edit-teams-woocommerce-memberships\";s:7:\"product\";s:33:\"woocommerce-memberships-for-teams\";s:14:\"show-if-active\";a:1:{i:0;s:23:\"woocommerce-memberships\";}s:7:\"context\";a:1:{i:0;s:26:\"product-edit-meta-tab-body\";}s:4:\"icon\";s:112:\"https://woocommerce.com/wp-content/plugins/wccom-plugins/marketplace-suggestions/icons/teams-for-memberships.svg\";s:5:\"title\";s:21:\"Teams for Memberships\";s:4:\"copy\";s:123:\"Adds B2B functionality to WooCommerce Memberships, allowing sites to sell team, group, corporate, or family member accounts\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:63:\"https://woocommerce.com/products/teams-woocommerce-memberships/\";}i:6;a:8:{s:4:\"slug\";s:29:\"product-edit-variation-images\";s:7:\"product\";s:39:\"woocommerce-additional-variation-images\";s:7:\"context\";a:1:{i:0;s:26:\"product-edit-meta-tab-body\";}s:4:\"icon\";s:118:\"https://woocommerce.com/wp-content/plugins/wccom-plugins/marketplace-suggestions/icons/additional-variation-images.svg\";s:5:\"title\";s:27:\"Additional Variation Images\";s:4:\"copy\";s:72:\"Showcase your products in the best light with a image for each variation\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:73:\"https://woocommerce.com/products/woocommerce-additional-variation-images/\";}i:7;a:9:{s:4:\"slug\";s:47:\"product-edit-woocommerce-subscription-downloads\";s:7:\"product\";s:34:\"woocommerce-subscription-downloads\";s:14:\"show-if-active\";a:1:{i:0;s:25:\"woocommerce-subscriptions\";}s:7:\"context\";a:1:{i:0;s:26:\"product-edit-meta-tab-body\";}s:4:\"icon\";s:113:\"https://woocommerce.com/wp-content/plugins/wccom-plugins/marketplace-suggestions/icons/subscription-downloads.svg\";s:5:\"title\";s:22:\"Subscription Downloads\";s:4:\"copy\";s:57:\"Give customers special downloads with their subscriptions\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:68:\"https://woocommerce.com/products/woocommerce-subscription-downloads/\";}i:8;a:8:{s:4:\"slug\";s:31:\"product-edit-min-max-quantities\";s:7:\"product\";s:30:\"woocommerce-min-max-quantities\";s:7:\"context\";a:1:{i:0;s:26:\"product-edit-meta-tab-body\";}s:4:\"icon\";s:109:\"https://woocommerce.com/wp-content/plugins/wccom-plugins/marketplace-suggestions/icons/min-max-quantities.svg\";s:5:\"title\";s:18:\"Min/Max Quantities\";s:4:\"copy\";s:81:\"Specify minimum and maximum allowed product quantities for orders to be completed\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:52:\"https://woocommerce.com/products/min-max-quantities/\";}i:9;a:8:{s:4:\"slug\";s:28:\"product-edit-name-your-price\";s:7:\"product\";s:27:\"woocommerce-name-your-price\";s:7:\"context\";a:1:{i:0;s:26:\"product-edit-meta-tab-body\";}s:4:\"icon\";s:106:\"https://woocommerce.com/wp-content/plugins/wccom-plugins/marketplace-suggestions/icons/name-your-price.svg\";s:5:\"title\";s:15:\"Name Your Price\";s:4:\"copy\";s:70:\"Let customers pay what they want - useful for donations, tips and more\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:49:\"https://woocommerce.com/products/name-your-price/\";}i:10;a:8:{s:4:\"slug\";s:42:\"product-edit-woocommerce-one-page-checkout\";s:7:\"product\";s:29:\"woocommerce-one-page-checkout\";s:7:\"context\";a:1:{i:0;s:26:\"product-edit-meta-tab-body\";}s:4:\"icon\";s:108:\"https://woocommerce.com/wp-content/plugins/wccom-plugins/marketplace-suggestions/icons/one-page-checkout.svg\";s:5:\"title\";s:17:\"One Page Checkout\";s:4:\"copy\";s:92:\"Don\'t make customers click around - let them choose products, checkout & pay all on one page\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:63:\"https://woocommerce.com/products/woocommerce-one-page-checkout/\";}i:11;a:4:{s:4:\"slug\";s:19:\"orders-empty-header\";s:7:\"context\";s:24:\"orders-list-empty-header\";s:5:\"title\";s:20:\"Tools for your store\";s:13:\"allow-dismiss\";b:0;}i:12;a:6:{s:4:\"slug\";s:30:\"orders-empty-footer-browse-all\";s:7:\"context\";s:24:\"orders-list-empty-footer\";s:9:\"link-text\";s:21:\"Browse all extensions\";s:3:\"url\";s:64:\"https://woocommerce.com/product-category/woocommerce-extensions/\";s:8:\"promoted\";s:31:\"category-woocommerce-extensions\";s:13:\"allow-dismiss\";b:0;}i:13;a:8:{s:4:\"slug\";s:19:\"orders-empty-zapier\";s:7:\"context\";s:22:\"orders-list-empty-body\";s:7:\"product\";s:18:\"woocommerce-zapier\";s:4:\"icon\";s:97:\"https://woocommerce.com/wp-content/plugins/wccom-plugins/marketplace-suggestions/icons/zapier.svg\";s:5:\"title\";s:6:\"Zapier\";s:4:\"copy\";s:88:\"Save time and increase productivity by connecting your store to more than 1000+ services\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:52:\"https://woocommerce.com/products/woocommerce-zapier/\";}i:14;a:8:{s:4:\"slug\";s:30:\"orders-empty-shipment-tracking\";s:7:\"context\";s:22:\"orders-list-empty-body\";s:7:\"product\";s:29:\"woocommerce-shipment-tracking\";s:4:\"icon\";s:108:\"https://woocommerce.com/wp-content/plugins/wccom-plugins/marketplace-suggestions/icons/shipment-tracking.svg\";s:5:\"title\";s:17:\"Shipment Tracking\";s:4:\"copy\";s:86:\"Let customers know when their orders will arrive by adding shipment tracking to emails\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:51:\"https://woocommerce.com/products/shipment-tracking/\";}i:15;a:8:{s:4:\"slug\";s:32:\"orders-empty-table-rate-shipping\";s:7:\"context\";s:22:\"orders-list-empty-body\";s:7:\"product\";s:31:\"woocommerce-table-rate-shipping\";s:4:\"icon\";s:110:\"https://woocommerce.com/wp-content/plugins/wccom-plugins/marketplace-suggestions/icons/table-rate-shipping.svg\";s:5:\"title\";s:19:\"Table Rate Shipping\";s:4:\"copy\";s:122:\"Advanced, flexible shipping. Define multiple shipping rates based on location, price, weight, shipping class or item count\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:53:\"https://woocommerce.com/products/table-rate-shipping/\";}i:16;a:8:{s:4:\"slug\";s:40:\"orders-empty-shipping-carrier-extensions\";s:7:\"context\";s:22:\"orders-list-empty-body\";s:4:\"icon\";s:118:\"https://woocommerce.com/wp-content/plugins/wccom-plugins/marketplace-suggestions/icons/shipping-carrier-extensions.svg\";s:5:\"title\";s:27:\"Shipping Carrier Extensions\";s:4:\"copy\";s:116:\"Show live rates from FedEx, UPS, USPS and more directly on your store - never under or overcharge for shipping again\";s:11:\"button-text\";s:13:\"Find Carriers\";s:8:\"promoted\";s:26:\"category-shipping-carriers\";s:3:\"url\";s:99:\"https://woocommerce.com/product-category/woocommerce-extensions/shipping-methods/shipping-carriers/\";}i:17;a:8:{s:4:\"slug\";s:32:\"orders-empty-google-product-feed\";s:7:\"context\";s:22:\"orders-list-empty-body\";s:7:\"product\";s:25:\"woocommerce-product-feeds\";s:4:\"icon\";s:110:\"https://woocommerce.com/wp-content/plugins/wccom-plugins/marketplace-suggestions/icons/google-product-feed.svg\";s:5:\"title\";s:19:\"Google Product Feed\";s:4:\"copy\";s:76:\"Increase sales by letting customers find you when they\'re shopping on Google\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:53:\"https://woocommerce.com/products/google-product-feed/\";}i:18;a:4:{s:4:\"slug\";s:35:\"products-empty-header-product-types\";s:7:\"context\";s:26:\"products-list-empty-header\";s:5:\"title\";s:23:\"Other types of products\";s:13:\"allow-dismiss\";b:0;}i:19;a:6:{s:4:\"slug\";s:32:\"products-empty-footer-browse-all\";s:7:\"context\";s:26:\"products-list-empty-footer\";s:9:\"link-text\";s:21:\"Browse all extensions\";s:3:\"url\";s:64:\"https://woocommerce.com/product-category/woocommerce-extensions/\";s:8:\"promoted\";s:31:\"category-woocommerce-extensions\";s:13:\"allow-dismiss\";b:0;}i:20;a:8:{s:4:\"slug\";s:30:\"products-empty-product-vendors\";s:7:\"context\";s:24:\"products-list-empty-body\";s:7:\"product\";s:27:\"woocommerce-product-vendors\";s:4:\"icon\";s:106:\"https://woocommerce.com/wp-content/plugins/wccom-plugins/marketplace-suggestions/icons/product-vendors.svg\";s:5:\"title\";s:15:\"Product Vendors\";s:4:\"copy\";s:47:\"Turn your store into a multi-vendor marketplace\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:49:\"https://woocommerce.com/products/product-vendors/\";}i:21;a:8:{s:4:\"slug\";s:26:\"products-empty-memberships\";s:7:\"context\";s:24:\"products-list-empty-body\";s:7:\"product\";s:23:\"woocommerce-memberships\";s:4:\"icon\";s:102:\"https://woocommerce.com/wp-content/plugins/wccom-plugins/marketplace-suggestions/icons/memberships.svg\";s:5:\"title\";s:11:\"Memberships\";s:4:\"copy\";s:76:\"Give members access to restricted content or products, for a fee or for free\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:57:\"https://woocommerce.com/products/woocommerce-memberships/\";}i:22;a:9:{s:4:\"slug\";s:35:\"products-empty-woocommerce-deposits\";s:7:\"context\";s:24:\"products-list-empty-body\";s:7:\"product\";s:20:\"woocommerce-deposits\";s:14:\"show-if-active\";a:1:{i:0;s:20:\"woocommerce-bookings\";}s:4:\"icon\";s:99:\"https://woocommerce.com/wp-content/plugins/wccom-plugins/marketplace-suggestions/icons/deposits.svg\";s:5:\"title\";s:8:\"Deposits\";s:4:\"copy\";s:75:\"Make it easier for customers to pay by offering a deposit or a payment plan\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:54:\"https://woocommerce.com/products/woocommerce-deposits/\";}i:23;a:8:{s:4:\"slug\";s:40:\"products-empty-woocommerce-subscriptions\";s:7:\"context\";s:24:\"products-list-empty-body\";s:7:\"product\";s:25:\"woocommerce-subscriptions\";s:4:\"icon\";s:104:\"https://woocommerce.com/wp-content/plugins/wccom-plugins/marketplace-suggestions/icons/subscriptions.svg\";s:5:\"title\";s:13:\"Subscriptions\";s:4:\"copy\";s:97:\"Let customers subscribe to your products or services and pay on a weekly, monthly or annual basis\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:59:\"https://woocommerce.com/products/woocommerce-subscriptions/\";}i:24;a:8:{s:4:\"slug\";s:35:\"products-empty-woocommerce-bookings\";s:7:\"context\";s:24:\"products-list-empty-body\";s:7:\"product\";s:20:\"woocommerce-bookings\";s:4:\"icon\";s:99:\"https://woocommerce.com/wp-content/plugins/wccom-plugins/marketplace-suggestions/icons/bookings.svg\";s:5:\"title\";s:8:\"Bookings\";s:4:\"copy\";s:99:\"Allow customers to book appointments, make reservations or rent equipment without leaving your site\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:54:\"https://woocommerce.com/products/woocommerce-bookings/\";}i:25;a:8:{s:4:\"slug\";s:30:\"products-empty-product-bundles\";s:7:\"context\";s:24:\"products-list-empty-body\";s:7:\"product\";s:27:\"woocommerce-product-bundles\";s:4:\"icon\";s:106:\"https://woocommerce.com/wp-content/plugins/wccom-plugins/marketplace-suggestions/icons/product-bundles.svg\";s:5:\"title\";s:15:\"Product Bundles\";s:4:\"copy\";s:49:\"Offer customizable bundles and assembled products\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:49:\"https://woocommerce.com/products/product-bundles/\";}}s:7:\"updated\";i:1600175037;}', 'no'),
(393, '_transient_timeout_wc_shipping_method_count_legacy', '1602053428', 'no'),
(394, '_transient_wc_shipping_method_count_legacy', 'a:2:{s:7:\"version\";s:10:\"1599459296\";s:5:\"value\";i:0;}', 'no'),
(406, '_transient_health-check-site-status-result', '{\"good\":\"6\",\"recommended\":\"11\",\"critical\":\"0\"}', 'yes'),
(485, 'woocommerce_task_list_tracked_completed_tasks', 'a:2:{i:0;s:13:\"store_details\";i:1;s:8:\"products\";}', 'yes'),
(690, 'woocommerce_tracker_ua', 'a:1:{i:0;s:76:\"Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/70.0\";}', 'yes'),
(702, 'woocommerce_tracker_last_send', '1600335461', 'yes'),
(764, 'current_theme', 'Storefront', 'yes'),
(765, 'theme_mods_storefront', 'a:12:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:7:\"primary\";i:19;}s:17:\"storefront_styles\";s:5060:\"\n			.main-navigation ul li a,\n			.site-title a,\n			ul.menu li a,\n			.site-branding h1 a,\n			.site-footer .storefront-handheld-footer-bar a:not(.button),\n			button.menu-toggle,\n			button.menu-toggle:hover {\n				color: #333333;\n			}\n\n			button.menu-toggle,\n			button.menu-toggle:hover {\n				border-color: #333333;\n			}\n\n			.main-navigation ul li a:hover,\n			.main-navigation ul li:hover > a,\n			.site-title a:hover,\n			a.cart-contents:hover,\n			.site-header-cart .widget_shopping_cart a:hover,\n			.site-header-cart:hover > li > a,\n			.site-header ul.menu li.current-menu-item > a {\n				color: #838383;\n			}\n\n			table th {\n				background-color: #f8f8f8;\n			}\n\n			table tbody td {\n				background-color: #fdfdfd;\n			}\n\n			table tbody tr:nth-child(2n) td,\n			fieldset,\n			fieldset legend {\n				background-color: #fbfbfb;\n			}\n\n			.site-header,\n			.secondary-navigation ul ul,\n			.main-navigation ul.menu > li.menu-item-has-children:after,\n			.secondary-navigation ul.menu ul,\n			.storefront-handheld-footer-bar,\n			.storefront-handheld-footer-bar ul li > a,\n			.storefront-handheld-footer-bar ul li.search .site-search,\n			button.menu-toggle,\n			button.menu-toggle:hover {\n				background-color: #ffffff;\n			}\n\n			p.site-description,\n			.site-header,\n			.storefront-handheld-footer-bar {\n				color: #6d6d6d;\n			}\n\n			.storefront-handheld-footer-bar ul li.cart .count,\n			button.menu-toggle:after,\n			button.menu-toggle:before,\n			button.menu-toggle span:before {\n				background-color: #333333;\n			}\n\n			.storefront-handheld-footer-bar ul li.cart .count {\n				color: #ffffff;\n			}\n\n			.storefront-handheld-footer-bar ul li.cart .count {\n				border-color: #ffffff;\n			}\n\n			h1, h2, h3, h4, h5, h6 {\n				color: #000000;\n			}\n\n			.widget h1 {\n				border-bottom-color: #000000;\n			}\n\n			body,\n			.secondary-navigation a,\n			.onsale,\n			.pagination .page-numbers li .page-numbers:not(.current), .woocommerce-pagination .page-numbers li .page-numbers:not(.current) {\n				color: #161616;\n			}\n\n			.widget-area .widget a,\n			.hentry .entry-header .posted-on a,\n			.hentry .entry-header .byline a {\n				color: #484848;\n			}\n\n			a  {\n				color: #96588a;\n			}\n\n			a:focus,\n			.button:focus,\n			.button.alt:focus,\n			.button.added_to_cart:focus,\n			.button.wc-forward:focus,\n			button:focus,\n			input[type=\"button\"]:focus,\n			input[type=\"reset\"]:focus,\n			input[type=\"submit\"]:focus {\n				outline-color: #96588a;\n			}\n\n			button, input[type=\"button\"], input[type=\"reset\"], input[type=\"submit\"], .button, .added_to_cart, .widget a.button, .site-header-cart .widget_shopping_cart a.button {\n				background-color: #333333;\n				border-color: #333333;\n				color: #ffffff;\n			}\n\n			button:hover, input[type=\"button\"]:hover, input[type=\"reset\"]:hover, input[type=\"submit\"]:hover, .button:hover, .added_to_cart:hover, .widget a.button:hover, .site-header-cart .widget_shopping_cart a.button:hover {\n				background-color: #1a1a1a;\n				border-color: #1a1a1a;\n				color: #ffffff;\n			}\n\n			button.alt, input[type=\"button\"].alt, input[type=\"reset\"].alt, input[type=\"submit\"].alt, .button.alt, .added_to_cart.alt, .widget-area .widget a.button.alt, .added_to_cart, .widget a.button.checkout {\n				background-color: #333333;\n				border-color: #333333;\n				color: #ffffff;\n			}\n\n			button.alt:hover, input[type=\"button\"].alt:hover, input[type=\"reset\"].alt:hover, input[type=\"submit\"].alt:hover, .button.alt:hover, .added_to_cart.alt:hover, .widget-area .widget a.button.alt:hover, .added_to_cart:hover, .widget a.button.checkout:hover {\n				background-color: #1a1a1a;\n				border-color: #1a1a1a;\n				color: #ffffff;\n			}\n\n			.pagination .page-numbers li .page-numbers.current, .woocommerce-pagination .page-numbers li .page-numbers.current {\n				background-color: #e6e6e6;\n				color: #161616;\n			}\n\n			#comments .comment-list .comment-content .comment-text {\n				background-color: #f8f8f8;\n			}\n\n			.site-footer {\n				background-color: #f0f0f0;\n				color: #6d6d6d;\n			}\n\n			.site-footer a:not(.button) {\n				color: #333333;\n			}\n\n			.site-footer h1, .site-footer h2, .site-footer h3, .site-footer h4, .site-footer h5, .site-footer h6 {\n				color: #333333;\n			}\n\n			#order_review {\n				background-color: #ffffff;\n			}\n\n			#payment .payment_methods > li .payment_box,\n			#payment .place-order {\n				background-color: #fafafa;\n			}\n\n			#payment .payment_methods > li:not(.woocommerce-notice) {\n				background-color: #f5f5f5;\n			}\n\n			#payment .payment_methods > li:not(.woocommerce-notice):hover {\n				background-color: #f0f0f0;\n			}\n\n			@media screen and ( min-width: 768px ) {\n				.secondary-navigation ul.menu a:hover {\n					color: #868686;\n				}\n\n				.secondary-navigation ul.menu a {\n					color: #6d6d6d;\n				}\n\n				.site-header-cart .widget_shopping_cart,\n				.main-navigation ul.menu ul.sub-menu,\n				.main-navigation ul.nav-menu ul.children {\n					background-color: #f0f0f0;\n				}\n\n				.site-header-cart .widget_shopping_cart .buttons,\n				.site-header-cart .widget_shopping_cart .total {\n					background-color: #f5f5f5;\n				}\n\n				.site-header {\n					border-bottom-color: #f0f0f0;\n				}\n			}\";s:29:\"storefront_woocommerce_styles\";s:2283:\"\n			a.cart-contents,\n			.site-header-cart .widget_shopping_cart a {\n				color: #333333;\n			}\n\n			table.cart td.product-remove,\n			table.cart td.actions {\n				border-top-color: #ffffff;\n			}\n\n			.woocommerce-tabs ul.tabs li.active a,\n			ul.products li.product .price,\n			.onsale,\n			.widget_search form:before,\n			.widget_product_search form:before {\n				color: #161616;\n			}\n\n			.woocommerce-breadcrumb a,\n			a.woocommerce-review-link,\n			.product_meta a {\n				color: #484848;\n			}\n\n			.onsale {\n				border-color: #161616;\n			}\n\n			.star-rating span:before,\n			.quantity .plus, .quantity .minus,\n			p.stars a:hover:after,\n			p.stars a:after,\n			.star-rating span:before,\n			#payment .payment_methods li input[type=radio]:first-child:checked+label:before {\n				color: #96588a;\n			}\n\n			.widget_price_filter .ui-slider .ui-slider-range,\n			.widget_price_filter .ui-slider .ui-slider-handle {\n				background-color: #96588a;\n			}\n\n			.order_details {\n				background-color: #f8f8f8;\n			}\n\n			.order_details > li {\n				border-bottom: 1px dotted #e3e3e3;\n			}\n\n			.order_details:before,\n			.order_details:after {\n				background: -webkit-linear-gradient(transparent 0,transparent 0),-webkit-linear-gradient(135deg,#f8f8f8 33.33%,transparent 33.33%),-webkit-linear-gradient(45deg,#f8f8f8 33.33%,transparent 33.33%)\n			}\n\n			p.stars a:before,\n			p.stars a:hover~a:before,\n			p.stars.selected a.active~a:before {\n				color: #161616;\n			}\n\n			p.stars.selected a.active:before,\n			p.stars:hover a:before,\n			p.stars.selected a:not(.active):before,\n			p.stars.selected a.active:before {\n				color: #96588a;\n			}\n\n			.single-product div.product .woocommerce-product-gallery .woocommerce-product-gallery__trigger {\n				background-color: #333333;\n				color: #ffffff;\n			}\n\n			.single-product div.product .woocommerce-product-gallery .woocommerce-product-gallery__trigger:hover {\n				background-color: #1a1a1a;\n				border-color: #1a1a1a;\n				color: #ffffff;\n			}\n\n			.button.loading {\n				color: #333333;\n			}\n\n			.button.loading:hover {\n				background-color: #333333;\n			}\n\n			.button.loading:after {\n				color: #ffffff;\n			}\n\n			@media screen and ( min-width: 768px ) {\n				.site-header-cart .widget_shopping_cart,\n				.site-header .product_list_widget li .quantity {\n					color: #6d6d6d;\n				}\n			}\";s:39:\"storefront_woocommerce_extension_styles\";s:0:\"\";s:18:\"custom_css_post_id\";i:-1;s:11:\"custom_logo\";i:47;s:24:\"storefront_heading_color\";s:7:\"#000000\";s:21:\"storefront_text_color\";s:7:\"#161616\";s:34:\"storefront_button_background_color\";s:7:\"#333333\";s:28:\"storefront_button_text_color\";s:7:\"#ffffff\";s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1599655794;s:4:\"data\";a:7:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:8:\"header-1\";a:0:{}s:8:\"footer-1\";a:0:{}s:8:\"footer-2\";a:0:{}s:8:\"footer-3\";a:0:{}s:8:\"footer-4\";a:0:{}}}}', 'yes'),
(766, 'theme_switched', '', 'yes'),
(769, 'storefront_nux_fresh_site', '0', 'yes'),
(770, 'woocommerce_maybe_regenerate_images_hash', '27acde77266b4d2a3491118955cb3f66', 'yes'),
(775, 'storefront_nux_dismissed', '1', 'yes'),
(779, '_transient_product_query-transient-version', '1600255309', 'yes'),
(784, '_transient_product-transient-version', '1600186571', 'yes'),
(786, '_transient_timeout_wc_product_loop_cbee12fc2ad25860fe8d4d167adf37f1', '1602246355', 'no'),
(787, '_transient_wc_product_loop_cbee12fc2ad25860fe8d4d167adf37f1', 'a:2:{s:7:\"version\";s:10:\"1599654354\";s:5:\"value\";O:8:\"stdClass\":5:{s:3:\"ids\";a:4:{i:0;i:39;i:1;i:38;i:2;i:37;i:3;i:36;}s:5:\"total\";i:4;s:11:\"total_pages\";i:1;s:8:\"per_page\";i:4;s:12:\"current_page\";i:1;}}', 'no'),
(788, '_transient_timeout_wc_product_loop_36e22b7a514b059f69d19e0f96a12485', '1602246355', 'no'),
(789, '_transient_wc_product_loop_36e22b7a514b059f69d19e0f96a12485', 'a:2:{s:7:\"version\";s:10:\"1599654354\";s:5:\"value\";O:8:\"stdClass\":5:{s:3:\"ids\";a:4:{i:0;i:35;i:1;i:34;i:2;i:33;i:3;i:31;}s:5:\"total\";i:4;s:11:\"total_pages\";i:1;s:8:\"per_page\";i:4;s:12:\"current_page\";i:1;}}', 'no'),
(790, '_transient_timeout_wc_product_loop_c94eed5df6e9407202af27b141f91df5', '1602246355', 'no'),
(791, '_transient_wc_product_loop_c94eed5df6e9407202af27b141f91df5', 'a:2:{s:7:\"version\";s:10:\"1599654354\";s:5:\"value\";O:8:\"stdClass\":5:{s:3:\"ids\";a:4:{i:0;i:28;i:1;i:39;i:2;i:29;i:3;i:30;}s:5:\"total\";i:4;s:11:\"total_pages\";i:1;s:8:\"per_page\";i:4;s:12:\"current_page\";i:1;}}', 'no'),
(794, '_transient_timeout_wc_product_loop_0d710f3c3d099bd3c3fca20dcfd07709', '1602246355', 'no'),
(795, '_transient_wc_product_loop_0d710f3c3d099bd3c3fca20dcfd07709', 'a:2:{s:7:\"version\";s:10:\"1599654354\";s:5:\"value\";O:8:\"stdClass\":5:{s:3:\"ids\";a:4:{i:0;i:28;i:1;i:29;i:2;i:30;i:3;i:31;}s:5:\"total\";i:4;s:11:\"total_pages\";i:1;s:8:\"per_page\";i:4;s:12:\"current_page\";i:1;}}', 'no'),
(796, '_transient_timeout_wc_product_loop_6eb03708ac078c1439844d8d8b61edc5', '1602246355', 'no'),
(797, '_transient_wc_product_loop_6eb03708ac078c1439844d8d8b61edc5', 'a:2:{s:7:\"version\";s:10:\"1599654354\";s:5:\"value\";O:8:\"stdClass\":5:{s:3:\"ids\";a:4:{i:0;i:28;i:1;i:39;i:2;i:29;i:3;i:30;}s:5:\"total\";i:4;s:11:\"total_pages\";i:1;s:8:\"per_page\";i:4;s:12:\"current_page\";i:1;}}', 'no'),
(828, 'storefront_nux_guided_tour', '1', 'yes'),
(832, '_transient_timeout_wc_product_loop_dbaaaa35f2169c63d284795d8e23308c', '1602682838', 'no'),
(833, '_transient_wc_product_loop_dbaaaa35f2169c63d284795d8e23308c', 'a:2:{s:7:\"version\";s:10:\"1600085878\";s:5:\"value\";O:8:\"stdClass\":5:{s:3:\"ids\";a:4:{i:0;i:39;i:1;i:38;i:2;i:37;i:3;i:36;}s:5:\"total\";i:4;s:11:\"total_pages\";i:1;s:8:\"per_page\";i:4;s:12:\"current_page\";i:1;}}', 'no'),
(834, '_transient_timeout_wc_product_loop_93c8b2907cc8ca606083a7a681d1a5b4', '1602682838', 'no'),
(835, '_transient_wc_product_loop_93c8b2907cc8ca606083a7a681d1a5b4', 'a:2:{s:7:\"version\";s:10:\"1600085878\";s:5:\"value\";O:8:\"stdClass\":5:{s:3:\"ids\";a:4:{i:0;i:35;i:1;i:34;i:2;i:33;i:3;i:31;}s:5:\"total\";i:4;s:11:\"total_pages\";i:1;s:8:\"per_page\";i:4;s:12:\"current_page\";i:1;}}', 'no'),
(836, '_transient_timeout_wc_product_loop_cc8a2d32d836d33d4bec83cb31531f7d', '1602682839', 'no'),
(837, '_transient_wc_product_loop_cc8a2d32d836d33d4bec83cb31531f7d', 'a:2:{s:7:\"version\";s:10:\"1600085878\";s:5:\"value\";O:8:\"stdClass\":5:{s:3:\"ids\";a:4:{i:0;i:39;i:1;i:38;i:2;i:37;i:3;i:36;}s:5:\"total\";i:4;s:11:\"total_pages\";i:1;s:8:\"per_page\";i:4;s:12:\"current_page\";i:1;}}', 'no'),
(838, '_transient_timeout_wc_product_loop_e5369b77f07fe5444fb1aae0ef57c8d5', '1602682839', 'no'),
(839, '_transient_wc_product_loop_e5369b77f07fe5444fb1aae0ef57c8d5', 'a:2:{s:7:\"version\";s:10:\"1600085878\";s:5:\"value\";O:8:\"stdClass\":5:{s:3:\"ids\";a:0:{}s:5:\"total\";i:0;s:11:\"total_pages\";i:1;s:8:\"per_page\";i:4;s:12:\"current_page\";i:1;}}', 'no'),
(840, '_transient_timeout_wc_product_loop_b27d6511cd0b0974ef126df50f877a28', '1602682839', 'no'),
(841, '_transient_wc_product_loop_b27d6511cd0b0974ef126df50f877a28', 'a:2:{s:7:\"version\";s:10:\"1600085878\";s:5:\"value\";O:8:\"stdClass\":5:{s:3:\"ids\";a:4:{i:0;i:39;i:1;i:38;i:2;i:37;i:3;i:36;}s:5:\"total\";i:4;s:11:\"total_pages\";i:1;s:8:\"per_page\";i:4;s:12:\"current_page\";i:1;}}', 'no'),
(867, 'theme_mods_wp-store', 'a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1599656870;s:4:\"data\";a:14:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:13:\"right-sidebar\";a:0:{}s:12:\"shop-sidebar\";a:0:{}s:12:\"left-sidebar\";a:0:{}s:15:\"widget-area-one\";a:0:{}s:12:\"product-area\";a:0:{}s:15:\"widget-area-two\";a:0:{}s:17:\"widget-area-three\";a:0:{}s:11:\"widget-icon\";a:0:{}s:10:\"footer-one\";a:0:{}s:10:\"footer-two\";a:0:{}s:12:\"footer-three\";a:0:{}s:11:\"footer-four\";a:0:{}}}}', 'yes'),
(868, 'widget_wp_store_cta_simple', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(869, 'widget_wp_store_promo', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(870, 'widget_wp_store_product', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(871, 'widget_wp_store_cat_product', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(872, 'widget_wp_store_cta_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(873, 'widget_wp_store_offer', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(892, 'woocommerce_catalog_rows', '4', 'yes'),
(893, 'woocommerce_catalog_columns', '3', 'yes'),
(1133, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(1198, '_site_transient_timeout_php_check_57655df84568a745a63faa797dbc117e', '1600685443', 'no'),
(1199, '_site_transient_php_check_57655df84568a745a63faa797dbc117e', 'a:5:{s:19:\"recommended_version\";s:3:\"7.4\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:0;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}', 'no'),
(1208, '_site_transient_timeout_browser_4a94ab2d971cee639ee614b5e469c456', '1600690677', 'no'),
(1209, '_site_transient_browser_4a94ab2d971cee639ee614b5e469c456', 'a:10:{s:4:\"name\";s:7:\"Firefox\";s:7:\"version\";s:4:\"70.0\";s:8:\"platform\";s:5:\"Linux\";s:10:\"update_url\";s:32:\"https://www.mozilla.org/firefox/\";s:7:\"img_src\";s:44:\"http://s.w.org/images/browsers/firefox.png?1\";s:11:\"img_src_ssl\";s:45:\"https://s.w.org/images/browsers/firefox.png?1\";s:15:\"current_version\";s:2:\"56\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(1210, '_transient_timeout_wc_report_sales_by_date', '1600442010', 'no'),
(1211, '_transient_wc_report_sales_by_date', 'a:32:{s:32:\"4e334dff96c136ccdcde143d4a514145\";a:0:{}s:32:\"488a420761fb8d1e649c0c93a7039a67\";a:0:{}s:32:\"aac2c37c8d43ffb600f90536e3e03cfc\";a:0:{}s:32:\"9308d24f4683662538e1d52a6fa9b43e\";N;s:32:\"21a13507c7fecaff5db15e5d445ee72c\";a:0:{}s:32:\"a75d38c769165620ca3ebf7df0bdc279\";a:0:{}s:32:\"7807b3282470cbe513580190afd69c7a\";a:0:{}s:32:\"96d641c4c348aaa57476a526d5e665b6\";a:0:{}s:32:\"6797dfb580de74049cc01f6a5efec1a5\";a:0:{}s:32:\"7c5d0ef51d3ca6a24afec84b3871896d\";a:0:{}s:32:\"de9aa8d71a5704d863d62add3f9cfbfc\";a:0:{}s:32:\"56d9883b2b1bc551eedf666de89f84b9\";N;s:32:\"5f7b5a2095bdd9f7b25b2525ce2d3461\";a:0:{}s:32:\"f869034380a89f8cc4d4e03ae3b215ca\";a:0:{}s:32:\"a62e668c55ba2f466c9de41a9a60f9d8\";a:0:{}s:32:\"779494acb676f3e7212873cdc6233968\";a:0:{}s:32:\"3abca3bf8b5ef12c87e3c986a66f0f11\";a:0:{}s:32:\"fe663690b4aa33f18ae86f98afb6cf33\";a:0:{}s:32:\"b000c5f35db657badeecd495d68fb05a\";a:0:{}s:32:\"818b948b067b7b1650a3040988137eef\";N;s:32:\"797b18856a8515de2641c03be3fcd483\";a:0:{}s:32:\"7640d190495adc9e35c7e380a26a527b\";a:0:{}s:32:\"3ba78c6be3aacd027b96a88e052dc2ab\";a:0:{}s:32:\"12a41a88c562a29fc52df817592a760b\";a:0:{}s:32:\"2a930e17bec92234554b68bea59d3f8a\";a:0:{}s:32:\"72bb0c8f64acbada920403f3bce158b3\";a:0:{}s:32:\"43b9b7d38f38b3a895fd997e9378d887\";a:0:{}s:32:\"eac0326b125721d03a2f25ff8755c8b2\";N;s:32:\"9c4db13561c0b0bfd3230794c29db5bd\";a:0:{}s:32:\"faf7b25931be52921c9f4e70c793f69f\";a:0:{}s:32:\"4c279a6d04d43af5da50ca5ff5cfaa61\";a:0:{}s:32:\"143e49425f6b125613726451269f5beb\";a:0:{}}', 'no'),
(1212, '_transient_timeout_wc_admin_report', '1600442010', 'no'),
(1213, '_transient_wc_admin_report', 'a:1:{s:32:\"d7d78abe6333171b6f9aa75785b0308e\";a:0:{}}', 'no'),
(1327, 'hugeit_slider_title_for_crop_image', 'Slider crop image', 'yes'),
(1328, 'hugeit_slider_crop_image', 'stretch', 'yes'),
(1329, 'hugeit_slider_title_for_title_color', 'Slider title color', 'yes'),
(1330, 'hugeit_slider_title_color', '000000', 'yes'),
(1331, 'hugeit_slider_title_for_title_font_size', 'Slider title font size', 'yes'),
(1332, 'hugeit_slider_title_font_size', '13', 'yes'),
(1333, 'hugeit_slider_title_for_description_color', 'Slider description color', 'yes'),
(1334, 'hugeit_slider_description_color', 'ffffff', 'yes'),
(1335, 'hugeit_slider_title_for_description_font_size', 'Slider description font size', 'yes'),
(1336, 'hugeit_slider_description_font_size', '13', 'yes'),
(1337, 'hugeit_slider_title_for_title_position', 'Slider title position', 'yes'),
(1338, 'hugeit_slider_title_position', '33', 'yes'),
(1339, 'hugeit_slider_title_for_description_position', 'Slider description position', 'yes'),
(1340, 'hugeit_slider_description_position', '31', 'yes'),
(1341, 'hugeit_slider_title_for_title_border_size', 'Slider Title border size', 'yes'),
(1342, 'hugeit_slider_title_border_size', '0', 'yes'),
(1343, 'hugeit_slider_title_for_title_border_color', 'Slider title border color', 'yes'),
(1344, 'hugeit_slider_title_border_color', 'ffffff', 'yes'),
(1345, 'hugeit_slider_title_for_title_border_radius', 'Slider title border radius', 'yes'),
(1346, 'hugeit_slider_title_border_radius', '4', 'yes'),
(1347, 'hugeit_slider_title_for_description_border_size', 'Slider description border size', 'yes'),
(1348, 'hugeit_slider_description_border_size', '0', 'yes'),
(1349, 'hugeit_slider_title_for_description_border_color', 'Slider description border color', 'yes'),
(1350, 'hugeit_slider_description_border_color', 'ffffff', 'yes'),
(1351, 'hugeit_slider_title_for_description_border_radius', 'Slider description border radius', 'yes'),
(1352, 'hugeit_slider_description_border_radius', '0', 'yes'),
(1353, 'hugeit_slider_title_for_slideshow_border_size', 'Slider border size', 'yes'),
(1354, 'hugeit_slider_slideshow_border_size', '0', 'yes'),
(1355, 'hugeit_slider_title_for_slideshow_border_color', 'Slider border color', 'yes'),
(1356, 'hugeit_slider_slideshow_border_color', 'ffffff', 'yes'),
(1357, 'hugeit_slider_title_for_slideshow_border_radius', 'Slider border radius', 'yes'),
(1358, 'hugeit_slider_slideshow_border_radius', '0', 'yes'),
(1359, 'hugeit_slider_title_for_navigation_type', 'Slider navigation type', 'yes'),
(1360, 'hugeit_slider_navigation_type', '1', 'yes'),
(1361, 'hugeit_slider_title_for_navigation_position', 'Slider navigation position', 'yes'),
(1362, 'hugeit_slider_navigation_position', 'top', 'yes'),
(1363, 'hugeit_slider_title_for_title_background_color', 'Slider title background color', 'yes'),
(1364, 'hugeit_slider_title_background_color', 'ffffff', 'yes'),
(1365, 'hugeit_slider_title_for_description_background_color', 'Slider description background color', 'yes'),
(1366, 'hugeit_slider_description_background_color', '000000', 'yes'),
(1367, 'hugeit_slider_title_for_slider_background_color', 'Slider slider background color', 'yes'),
(1368, 'hugeit_slider_slider_background_color', 'ffffff', 'yes'),
(1369, 'hugeit_slider_title_for_slider_background_color_transparency', 'Slider slider background color transparency', 'yes'),
(1370, 'hugeit_slider_slider_background_color_transparency', '1', 'yes'),
(1371, 'hugeit_slider_title_for_active_dot_color', 'Slider active dot color', 'yes'),
(1372, 'hugeit_slider_active_dot_color', 'ffffff', 'yes'),
(1373, 'hugeit_slider_title_for_dot_color', 'Slider dots color', 'yes'),
(1374, 'hugeit_slider_dot_color', '000000', 'yes'),
(1375, 'hugeit_slider_title_for_loading_icon_type', 'Slider Loading Image', 'yes'),
(1376, 'hugeit_slider_loading_icon_type', '1', 'yes'),
(1377, 'hugeit_slider_title_for_description_width', 'Slider description width', 'yes'),
(1378, 'hugeit_slider_description_width', '70', 'yes'),
(1379, 'hugeit_slider_title_for_description_height', 'Slider description height', 'yes'),
(1380, 'hugeit_slider_description_height', '50', 'yes'),
(1381, 'hugeit_slider_title_for_description_background_transparency', 'Slider description background transparency', 'yes'),
(1382, 'hugeit_slider_description_background_transparency', '0.7', 'yes'),
(1383, 'hugeit_slider_title_for_description_text_align', 'Description text-align', 'yes'),
(1384, 'hugeit_slider_description_text_align', 'justify', 'yes'),
(1385, 'hugeit_slider_title_for_title_width', 'Slider title width', 'yes'),
(1386, 'hugeit_slider_title_width', '30', 'yes'),
(1387, 'hugeit_slider_title_for_title_background_transparency', 'Slider title background transparency', 'yes'),
(1388, 'hugeit_slider_title_background_transparency', '0.7', 'yes'),
(1389, 'hugeit_slider_title_for_title_text_align', 'Title text-align', 'yes'),
(1390, 'hugeit_slider_title_text_align', 'right', 'yes'),
(1391, 'hugeit_slider_title_for_title_has_margin', 'Title has margin', 'yes'),
(1392, 'hugeit_slider_title_has_margin', '1', 'yes'),
(1393, 'hugeit_slider_title_for_description_has_margin', 'Description has margin', 'yes'),
(1394, 'hugeit_slider_description_has_margin', '1', 'yes'),
(1395, 'hugeit_slider_title_for_show_arrows', 'Slider show left right arrows', 'yes'),
(1396, 'hugeit_slider_show_arrows', '1', 'yes'),
(1397, 'hugeit_slider_title_for_thumb_count_slides', 'Thumbnail Background Color', 'yes'),
(1398, 'hugeit_slider_thumb_count_slides', '3', 'yes'),
(1399, 'hugeit_slider_thumb_background_color', 'ffffff', 'yes'),
(1400, 'hugeit_slider_title_for_thumb_passive_color', 'Passive Thumbnail Color', 'yes'),
(1401, 'hugeit_slider_thumb_passive_color', 'ffffff', 'yes'),
(1402, 'hugeit_slider_title_for_thumb_height', 'Slider Thumb Height', 'yes'),
(1403, 'hugeit_slider_thumb_height', '100', 'yes'),
(1404, 'hugeit_slider_title_for_share_buttons', 'Share buttons', 'yes'),
(1405, 'hugeit_slider_share_buttons', '1', 'yes'),
(1406, 'hugeit_slider_title_for_share_buttons_facebook', 'Facebook', 'yes'),
(1407, 'hugeit_slider_share_buttons_facebook', '1', 'yes'),
(1408, 'hugeit_slider_title_for_share_buttons_twitter', 'Twitter', 'yes'),
(1409, 'hugeit_slider_share_buttons_twitter', '1', 'yes'),
(1410, 'hugeit_slider_title_for_share_buttons_gp', 'Google Plus', 'yes'),
(1411, 'hugeit_slider_share_buttons_gp', '1', 'yes'),
(1412, 'hugeit_slider_title_for_share_buttons_pinterest', 'Pinterest', 'yes'),
(1413, 'hugeit_slider_share_buttons_pinterest', '1', 'yes'),
(1414, 'hugeit_slider_title_for_share_buttons_linkedin', 'Linkedin', 'yes'),
(1415, 'hugeit_slider_share_buttons_linkedin', '1', 'yes'),
(1416, 'hugeit_slider_title_for_share_buttons_tumblr', 'Tumblr', 'yes'),
(1417, 'hugeit_slider_share_buttons_tumblr', '1', 'yes'),
(1418, 'hugeit_slider_title_for_share_buttons_style', 'Share buttons style', 'yes'),
(1419, 'hugeit_slider_title_for_share_buttons_hover_style', 'Share buttons hover style', 'yes'),
(1420, 'hugeit_slider_version', '4.0.6', 'yes'),
(1421, 'widget_hugeit_slider_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(1569, 'woocommerce_category_slider_install_date', '1600174198', 'yes'),
(1570, 'wc_category_slider_version', '4.0.8', 'yes'),
(1571, 'wc-category-slider-v4_0_9_initial_upsell_promotion', 'hide', 'yes'),
(1778, 'product_cat_children', 'a:0:{}', 'yes'),
(1836, '_transient_orders-transient-version', '1600186560', 'yes'),
(1837, '_transient_timeout_wc_customer_bought_product_36d36b6d62a2d66bfe96a45e180946d5', '1602778560', 'no'),
(1838, '_transient_wc_customer_bought_product_36d36b6d62a2d66bfe96a45e180946d5', 'a:2:{s:7:\"version\";s:10:\"1600186560\";s:5:\"value\";a:0:{}}', 'no'),
(1839, '_transient_wc_count_comments', 'O:8:\"stdClass\":7:{s:14:\"total_comments\";i:2;s:3:\"all\";i:2;s:8:\"approved\";s:1:\"2\";s:9:\"moderated\";i:0;s:4:\"spam\";i:0;s:5:\"trash\";i:0;s:12:\"post-trashed\";i:0;}', 'yes'),
(1842, '_transient_timeout_wc_term_counts', '1602845788', 'no'),
(1843, '_transient_wc_term_counts', 'a:5:{i:22;s:1:\"1\";i:18;s:1:\"2\";i:17;s:1:\"1\";i:16;s:1:\"2\";i:15;s:1:\"0\";}', 'no'),
(1867, 'widget_wpb_woo_product_slider_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(1868, 'wpb_wps_general', '', 'yes'),
(1869, 'wpb_wps_slider_settings', '', 'yes'),
(1870, 'wpb_wps_style', '', 'yes'),
(1891, 'widget_woobsp_best_selling_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(1895, '_transient_timeout_wc_report_orders_stats_6be6fc2f5a5663f95af98eb0d6bbad85', '1600793229', 'no'),
(1896, '_transient_timeout_wc_report_orders_stats_8ff0264f78897ce8d4a2568e6aeb1d60', '1600793229', 'no'),
(1897, '_transient_wc_report_orders_stats_6be6fc2f5a5663f95af98eb0d6bbad85', 'a:2:{s:7:\"version\";s:10:\"1599546732\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":16:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2020-38\";s:10:\"date_start\";s:19:\"2020-09-15 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-15 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-15 22:17:00\";s:12:\"date_end_gmt\";s:19:\"2020-09-15 22:17:00\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(1898, '_transient_wc_report_orders_stats_8ff0264f78897ce8d4a2568e6aeb1d60', 'a:2:{s:7:\"version\";s:10:\"1599546732\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":16:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2020-38\";s:10:\"date_start\";s:19:\"2020-09-14 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-14 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-14 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-14 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(1899, '_transient_timeout_wc_report_orders_stats_1bb1ae3f14b892b99572d553a6c267c4', '1600793229', 'no'),
(1900, '_transient_timeout_wc_report_orders_stats_9c58c467eca0222eeba4d442066a7f9a', '1600793229', 'no'),
(1901, '_transient_wc_report_orders_stats_1bb1ae3f14b892b99572d553a6c267c4', 'a:2:{s:7:\"version\";s:10:\"1599546732\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":16:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2020-38\";s:10:\"date_start\";s:19:\"2020-09-15 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-15 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-15 22:17:00\";s:12:\"date_end_gmt\";s:19:\"2020-09-15 22:17:00\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(1902, '_transient_wc_report_orders_stats_9c58c467eca0222eeba4d442066a7f9a', 'a:2:{s:7:\"version\";s:10:\"1599546732\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":16:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2020-38\";s:10:\"date_start\";s:19:\"2020-09-14 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-14 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-14 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-14 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(1912, '_transient_timeout_wc_product_loop_a14263c1206ca2dd93a6ca94c2b77e2f', '1602780969', 'no'),
(1913, '_transient_wc_product_loop_a14263c1206ca2dd93a6ca94c2b77e2f', 'a:2:{s:7:\"version\";s:10:\"1600186571\";s:5:\"value\";O:8:\"stdClass\":5:{s:3:\"ids\";a:3:{i:0;i:36;i:1;i:37;i:2;i:38;}s:5:\"total\";i:3;s:11:\"total_pages\";i:1;s:8:\"per_page\";i:3;s:12:\"current_page\";i:1;}}', 'no'),
(1916, '_transient_timeout_wc_product_loop_cd4ceb08a6a3e9bac7f4260a4e03a47f', '1602845789', 'no'),
(1917, '_transient_wc_product_loop_cd4ceb08a6a3e9bac7f4260a4e03a47f', 'a:2:{s:7:\"version\";s:10:\"1600193977\";s:5:\"value\";O:8:\"stdClass\":5:{s:3:\"ids\";a:4:{i:0;i:36;i:1;i:37;i:2;i:38;i:3;i:39;}s:5:\"total\";i:4;s:11:\"total_pages\";i:1;s:8:\"per_page\";i:4;s:12:\"current_page\";i:1;}}', 'no'),
(1923, '_transient_timeout_wc_blocks_query_b830d3785c97bb55310e03a8f188db53', '1602781141', 'no'),
(1924, '_transient_wc_blocks_query_b830d3785c97bb55310e03a8f188db53', 'a:2:{s:7:\"version\";s:10:\"1600186571\";s:5:\"value\";a:6:{i:0;i:84;i:1;i:82;i:2;i:39;i:3;i:38;i:4;i:37;i:5;i:36;}}', 'no'),
(1925, '_transient_timeout_wc_blocks_query_196f95fe0f1beda3ed4922f8b0505b6e', '1602781153', 'no'),
(1926, '_transient_wc_blocks_query_196f95fe0f1beda3ed4922f8b0505b6e', 'a:2:{s:7:\"version\";s:10:\"1600186571\";s:5:\"value\";a:3:{i:0;i:84;i:1;i:82;i:2;i:39;}}', 'no'),
(1927, '_transient_timeout_wc_blocks_query_f3c61392c784b9ce0a3a290370e6295a', '1602781157', 'no'),
(1928, '_transient_wc_blocks_query_f3c61392c784b9ce0a3a290370e6295a', 'a:2:{s:7:\"version\";s:10:\"1600186571\";s:5:\"value\";a:5:{i:0;i:84;i:1;i:82;i:2;i:39;i:3;i:38;i:4;i:37;}}', 'no'),
(1929, '_transient_timeout_wc_blocks_query_b8b26cf7a1dc9e3d6f2e8b909b739366', '1602847852', 'no'),
(1930, '_transient_wc_blocks_query_b8b26cf7a1dc9e3d6f2e8b909b739366', 'a:2:{s:7:\"version\";s:10:\"1600255309\";s:5:\"value\";a:4:{i:0;i:84;i:1;i:82;i:2;i:39;i:3;i:38;}}', 'no');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1937, '_transient_timeout_wc_low_stock_count', '1602818036', 'no'),
(1938, '_transient_wc_low_stock_count', '0', 'no'),
(1939, '_transient_timeout_wc_outofstock_count', '1602818037', 'no'),
(1940, '_transient_wc_outofstock_count', '0', 'no'),
(2021, 'mltlngg_options', 'a:19:{s:21:\"plugin_option_version\";s:5:\"1.3.6\";s:10:\"wp_version\";s:5:\"5.4.2\";s:22:\"suggest_feature_banner\";i:1;s:23:\"display_settings_notice\";i:0;s:13:\"first_install\";i:1600237004;s:16:\"default_language\";s:5:\"en_US\";s:17:\"list_of_languages\";a:2:{i:0;a:4:{s:6:\"locale\";s:5:\"en_US\";s:4:\"name\";s:7:\"English\";s:6:\"enable\";b:1;s:8:\"priority\";i:1;}i:1;a:4:{s:6:\"locale\";s:2:\"ar\";s:4:\"name\";s:14:\"العربية\";s:6:\"enable\";b:1;s:8:\"priority\";i:2;}}s:9:\"save_mode\";s:4:\"ajax\";s:15:\"wp_localization\";i:1;s:17:\"language_switcher\";s:16:\"drop-down-titles\";s:6:\"search\";s:6:\"single\";s:15:\"video_providers\";a:64:{i:0;s:79:\"[(https?://((m|www)\\.)?youtube\\.com/watch)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:1;s:82:\"[(https?://((m|www)\\.)?youtube\\.com/playlist)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:2;s:59:\"[(https?://youtu\\.be/)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:3;s:67:\"[(https?://(.+\\.)?vimeo\\.com/)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:4;s:74:\"[(https?://(www\\.)?dailymotion\\.com/)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:5;s:57:\"[(https?://dai\\.ly/)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:6;s:69:\"[(https?://(www\\.)?flickr\\.com/)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:7;s:58:\"[(https?://flic\\.kr/)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:8;s:69:\"[(https?://(.+\\.)?smugmug\\.com/)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:9;s:73:\"[(https?://(www\\.)?hulu\\.com/watch/)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:10;s:84:\"[(https?://(www\\.)?scribd\\.com/(doc|document)/)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:11;s:63:\"[(https?://wordpress\\.tv/)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:12;s:71:\"[(https?://(.+\\.)?polldaddy\\.com/)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:13;s:58:\"[(https?://poll\\.fm/)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:14;s:67:\"[(https?://(.+\\.)?survey\\.fm/)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:15;s:91:\"[(https?://(www\\.)?twitter\\.com/\\w{1,15}/status(es)?/)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:16;s:82:\"[(#https?://(www\\.)?twitter\\.com/\\w{1,15}$#i)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:17;s:88:\"[(#https?://(www\\.)?twitter\\.com/\\w{1,15}/likes$#i)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:18;s:85:\"[(https?://(www\\.)?twitter\\.com/\\w{1,15}/lists/)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:19;s:89:\"[(https?://(www\\.)?twitter\\.com/\\w{1,15}/timelines/)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:20;s:80:\"[(https?://(www\\.)?twitter\\.com/i/moments/)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:21;s:73:\"[(https?://(www\\.)?soundcloud\\.com/)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:22;s:73:\"[(https?://(.+?\\.)?slideshare\\.net/)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:23;s:86:\"[(https?://(www\\.)?instagr(\\.am|am\\.com)/(p|tv)/)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:24;s:75:\"[(https?://(open|play)\\.spotify\\.com/)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:25;s:67:\"[(https?://(.+\\.)?imgur\\.com/)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:26;s:76:\"[(https?://(www\\.)?meetu(\\.ps|p\\.com)/)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:27;s:81:\"[(#https?://(www\\.)?issuu\\.com/.+/docs/.+#i)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:28;s:71:\"[(https?://(www\\.)?mixcloud\\.com/)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:29;s:80:\"[(https?://(www\\.|embed\\.)?ted\\.com/talks/)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:30;s:86:\"[(https?://(www\\.)?(animoto|video214)\\.com/play/)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:31;s:72:\"[(https?://(.+)\\.tumblr\\.com/post/)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:32;s:83:\"[(https?://(www\\.)?kickstarter\\.com/projects/)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:33;s:57:\"[(https?://kck\\.st/)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:34;s:62:\"[(https?://cloudup\\.com/)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:35;s:75:\"[(https?://(www\\.)?reverbnation\\.com/)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:36;s:68:\"[(https?://videopress\\.com/v/.)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:37;s:86:\"[(https?://(www\\.)?reddit\\.com/r/[^/]+/comments/)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:38;s:74:\"[(https?://(www\\.)?speakerdeck\\.com/)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:39;s:77:\"[(https?://www\\.facebook\\.com/.*/posts/)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:40;s:80:\"[(https?://www\\.facebook\\.com/.*/activity/)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:41;s:78:\"[(https?://www\\.facebook\\.com/.*/photos/)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:42;s:83:\"[(https?://www\\.facebook\\.com/photo(s/|\\.php))(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:43;s:82:\"[(https?://www\\.facebook\\.com/permalink\\.php)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:44;s:74:\"[(https?://www\\.facebook\\.com/media/)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:45;s:78:\"[(https?://www\\.facebook\\.com/questions/)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:46;s:74:\"[(https?://www\\.facebook\\.com/notes/)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:47;s:78:\"[(https?://www\\.facebook\\.com/.*/videos/)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:48;s:78:\"[(https?://www\\.facebook\\.com/video\\.php)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:49;s:73:\"[(https?://(www\\.)?screencast\\.com/)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:50;s:97:\"[(https?://([a-z0-9-]+\\.)?amazon\\.(com|com\\.mx|com\\.br|ca)/)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:51;s:102:\"[(https?://([a-z0-9-]+\\.)?amazon\\.(co\\.uk|de|fr|it|es|in|nl|ru)/)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:52;s:89:\"[(https?://([a-z0-9-]+\\.)?amazon\\.(co\\.jp|com\\.au)/)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:53;s:75:\"[(https?://([a-z0-9-]+\\.)?amazon\\.cn/)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:54;s:63:\"[(https?://(www\\.)?a\\.co/)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:55;s:66:\"[(https?://(www\\.)?amzn\\.to/)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:56;s:66:\"[(https?://(www\\.)?amzn\\.eu/)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:57;s:66:\"[(https?://(www\\.)?amzn\\.in/)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:58;s:68:\"[(https?://(www\\.)?amzn\\.asia/)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:59;s:63:\"[(https?://(www\\.)?z\\.cn/)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:60;s:84:\"[(#https?://www\\.someecards\\.com/.+-cards/.+#i)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:61;s:94:\"[(#https?://www\\.someecards\\.com/usercards/viewcard/.+#i)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:62;s:64:\"[(#https?://some\\.ly\\/.+#i)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";i:63;s:78:\"[(https?://(www\\.)?tiktok\\.com/.*/video/)(([^\\s]*)|([^	]*)|([^\n]*)|([^\\<]*))]i\";}s:20:\"translate_open_graph\";i:0;s:24:\"display_alternative_link\";i:0;s:14:\"hide_link_slug\";i:0;s:20:\"hide_premium_options\";a:0:{}s:17:\"plugin_db_version\";s:3:\"0.3\";s:19:\"go_settings_counter\";i:3;s:12:\"deactivation\";s:19:\"2020-09-16 08:05:00\";}', 'yes'),
(2022, 'widget_multi_language_widget', 'a:2:{i:2;a:2:{s:11:\"wiget_title\";s:0:\"\";s:25:\"mltlngg_language_switcher\";s:14:\"drop-down-list\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(2023, 'bstwbsftwppdtplgns_options', 'a:1:{s:8:\"bws_menu\";a:1:{s:7:\"version\";a:1:{s:31:\"multilanguage/multilanguage.php\";s:5:\"2.3.0\";}}}', 'yes'),
(2058, 'category_children', 'a:0:{}', 'yes'),
(2096, 'trp_settings', 'a:12:{s:16:\"default-language\";s:5:\"en_US\";s:17:\"publish-languages\";a:2:{i:0;s:5:\"en_US\";i:1;s:2:\"ar\";}s:21:\"translation-languages\";a:2:{i:0;s:5:\"en_US\";i:1;s:2:\"ar\";}s:9:\"url-slugs\";a:2:{s:5:\"en_US\";s:2:\"en\";s:2:\"ar\";s:2:\"ar\";}s:22:\"native_or_english_name\";s:12:\"english_name\";s:36:\"add-subdirectory-to-default-language\";s:2:\"no\";s:30:\"force-language-to-custom-links\";s:3:\"yes\";s:17:\"shortcode-options\";s:10:\"full-names\";s:12:\"menu-options\";s:10:\"full-names\";s:15:\"floater-options\";s:16:\"flags-full-names\";s:16:\"floater-position\";s:12:\"bottom-right\";s:14:\"trp-ls-floater\";s:2:\"no\";}', 'yes'),
(2097, 'trp_plugin_version', '1.8.2', 'yes'),
(2132, 'trp_machine_translation_settings', 'a:9:{s:19:\"machine-translation\";s:3:\"yes\";s:18:\"translation-engine\";s:19:\"google_translate_v2\";s:14:\"block-crawlers\";s:3:\"yes\";s:32:\"machine_translation_counter_date\";s:10:\"2020-09-18\";s:27:\"machine_translation_counter\";i:0;s:25:\"machine_translation_limit\";s:7:\"1000000\";s:20:\"google-translate-key\";s:0:\"\";s:23:\"machine_translation_log\";s:2:\"no\";s:46:\"machine_translation_trigger_quota_notification\";b:0;}', 'yes');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(2193, 'wp_installer_settings', 'eJzs/etyG0mWJor+3mM27xAHs6ckzRAkwaukylQbk5KyVK0LW6Qyq+zsbaggECSjBCBQEQAp5uw268c4Y7bHrJ+lH6Wf5Kyr+3IPDwBUVWVn99TYzFSKADw8/LKu3/pW/nz/+f9ong/2nvfqYl415aKqy6Lp/Tp/vocfHDzv3c2nE/z3QP49zhc5/fsA/wA/LMe9XzfP95/yV/uLm6J/V9XjeV00TX+6nCzKSTm7XuaT/nyyvC5n+G0YZ5ZPC/rP3ee9H8/evc362cVNkf0IPz3Dn2bvzE+zM/dTnOu8rsbL0aLvxuAhaB7Pe8t6Ql88et67WSzmzfOdHZzZdlVf49/34NvwosXwc3HfDKf5LL8upsVsMZTf7Q/av9vJR6NqOVvs4A+bHZ1Hs5zPq3oxXOTXjaxR+XzXLN7KCe4/SzzoqqqX02ZnUc3LUR/GpU/ggf/4j81zWOJ5PvoM86Wn7ePT9g72j46O9uGfx8F+uJ3rmscAXnNcNKO6nC/Kipd2F56AmzCqpvNJsSgyedx2dlbUV8VokcH0snx2n9l9zXBRtuDP4yyfNFVWzkaTJQwdfunHqjqtptOiHhWZrNs2PhQWoZzCM3T5j1LLfzfvj6rZAnZpZzmfVPm42dnbHRzs7D6l7/Qn1XU16O8Otucz2uPD5z04R0Xd+zXuBwyKK1fDz/UPT90hoqXkswxPLmbj/rIp6n6zvDRrk+N2/4+uc7vY6Ny21xtGkMnO63JU6NwGx3iy/OOHi/s5fTgY7A/wC3t7iS8MF8WXRY9f7RVsxacGXz8xWjm7qugDWPnfF3k9uc/Cl4UHPE09oPjTsrzNJ7SKbvLmOB8nNu7v8vG4v6j6o7xefHt4ONg/2h/IJGE7ijs4MLi6u8//B5/v5fy6zseF+SOMynKDb9gxXzGY4xFf44ScGU0bWi781r6IpWZRw6f9RZ3PmknOLwq3B79yrJLLf9b3YgG/ti+LTl+bFuMyjwc6EMFyV1UjOeXBnPA7h/gd2Gx4w9tycQ9Xadq0vnQkM8LjnoOIw2/1j1tfOxYBRzOalbM/5vRNeu+nIp3os7u5+/szWY/L5Xh83142WrJdPo/56GrKf8BFHBzKYPlk0i+neHXpM1q9gS5LXk5GN/ApTgQeS9/Yl3MG97cpqvbjDvhxeLx4yw7lxMqelaPP9334wWf+9Mi+GewyKIBbEI0kRMbV3Ywkgx6df+RHBycD9nQ2uk/e5z2RjeH9PZEfbHp/B4fPVt7g48Hg6dobjNPumsVf6TbDyx8d7w0eeqNZ+/ztRv/tRv9cN/ppdKPxN3vpCz1IXejTd+eb3+bjlZf56HjwbP1dPorusk7gl6OW+RLv/u0S/+0S/xup5Uuw37tu8V7qFn8HP9j8Gu+tu8YbqOTj6Bq7GfzC7vHhsz/nHj/0Gq+8fHvrD/Z+9zE+WHeKD1cf4qPgDHcdxn/8R3jS4WAXjkDsQeM9br94cDg7hfvffGvvW9M6yyoftVf5OCEL4mXuvHztdT7c42W+zJtylJHkuC3qBj7dyq6rakyrHCwePrD5hS1X4rhS/K2tdddf1Rw14ANMIwyPwTN6G0iB9EnHW8EnvYZTTtPNFst61pggiR1rO3uzyECQNRRI4Y0zoiTD5a6rSSO3YFLdNf7z2XUm26FbOLrJZ9eFnJC9g2cYZvvm5uDFwfbB9t43O/Bf39zsv3hdfika+Nf+i//8n75ZTuD//z++mZQvTsZjnF8xzt7MmgU8rKgfNdmsWpRX5Yins7jJFzRTXJlslM/gU5zDDO/woqIzhydlK7u7KUc32V0OM5/PQRXgZKe4KNUVTBgHmmXlImtuquVkvP3NDjxfpoGzg6Oawwyyoq7hyN7d4C/qexwDHlKDwK3lgTiXuphWt/CTRf6lmlXT+2xR1NPEkPjdGbzHbREscXVFn7wtFzfLfFbCvCY57g2IJxnjmx1YJF3GwYplzKJ1HFXNtACLISubZlmAAPMriy8Fq1HC4vHmXoJIvK6rJfznHX5SF7BsCxgEfjXKmyL7ZlSNixfnpx/fnF0MX7767tP33+zQn2iVm4LXo166acezaappkZ395ix7D1s6AjkK547HHxeXy+ttODbZGKYJw+Bk7vIajiUIXRDasFKXcGQX99kdLFN2PVm60w07geJ3NqbVbLIrmPWyDl82nNGFLD4u0LJZVNPsmr4FG1cuJgU+M3s1IUsXNv+srrLw97ybeH5oVWlG4fcbOHB4+srxNSwLHZ9PH9/C93WZdIczPJ+gjWoQDgs/z2DDd/2G86vpnseXhydVzfmmVFkxyy8nxc64bPB/g2uLBw6WG14Ut74urktwBvg4zs3B45Hx1sPYtyVcOvzxN/Ddanb9AqQxjfHNjvyBfsvrQQ/QlwQRdlV+wWXFVaDlcJ/hX9Ach3vtl0ef8LK8uipQXrvv0+bw9aNsiXt2MOWPciPxeIokGMNxnsF5gjcUndTgzfNSkb8G2gEljmpyWtJ7lBvJ8e+KyQgP9YwONL+5zv0tnIFZdlPd8bWQI5eRteZXDA35cLVpH09+KurLvPwjyIOWNDBfw1WG4fDiXFZf+PnjYgEGG90rXlfcVJZg8G2Mr2fgRIG1u4DF1Ml+V4Ngh0/ctqiM8zNF3Y0jXJV1A5tUTsMJvZmCIsU1mYNFhc+dwYLAk+nS5+NpySeLNoASOTzby/I6my2nl/Bs2A2cXRNcAjzl8J+RtAtOvheuucgIWHBWRCxy3fAFX9IG1qyZT/J7nm3ixLvFNSP6Lfx+CRoEhoRrRBdrlNOVaxIi/2D3QHRIxeu/8LKndet4l5aNXtCvuAV6+/FWL0jA84GGh93VqDLrJQq4/BaOCM1dd3W0rIMnJN4F9hd+MZVTxiuIU51XcB4KWGMS5aScQSTDBFYc3CmITvptDUYfCN5yNi6+0GT0pcvRZDipRuCTDaf5PH3NYV4Z6CsQLjN6OIzx3A3gbadmcT8p/BLxZU28oNuoH8/6p2/foLcwxYUsvhSjJV8jfPVmXozQIjGyA79FNh2891V5veRNTTyCTAUyRVAMZPlikY9u+EyqmqVD4D+QZ5ZT2LA6QyMS5gPnhn7gZ3AFV765bxZFy/DwZ7BR7Yo6tSYLBqUvzFkkIt6GBsbGx47g+TDRxDuMwfRWewwuFW5/44W+tWz+WF3SNC/M3965KBIbczc5mEOXRYFvDyIDBddlcRUL3Jesw/yZykCzL5YN2S96t+gOTWD+ToqPYKKf6ZynpHhwrOUVYKZjMAhAol7jNlXLBQksOtB4UP3zl5fwxRy8Ad6xc/g6POe8vJ71KzQNWMquf6K5SaC96R7hDcH3gzN2BWd2wW+Gynrl1YpFTmS2wmW4LYs7sjdINLLKRUlS51cLemJqwLxtC4sCwYHp2Kuc5xOYEoT2LcHry2efM3BccNvJ887qqloYIaiXmETz/Mbf/2xcFeQT8C1QiQP/b5qaO+xGVcfy9PxzOc/eygr6kcH2oDfCl+HF56PtjBo8DOZ4p17TGYVNxQuMMQUK1qmzcIPrpPtptIE4UnR0l/NxzqYHHJaqnDlPw3zfjZM6YDlL19yYYGpighmX4+Ht8+nFkT++Or/IPhZ/WhbNYtU7scegq/ipKZwKuqddBEGcg2vpHuqXlg56rsvql+AOnS+4+KD34eM7/LKzPJzZAud2ASoYZNM056XENYosxZ3/ipuF4vG/duoKJ+JpWGfOmkWlSYk/5r05FlMNS6lxgQGkcWQOwtkeodM4ya+dSj2pwXoYrbqq/MJuj3D2dB9P6woWKl+pQEBY3qBIBwEx9Wa1Wzy+8peRWbf6Yp6/+uDW8wZMdvyVNS1m0QGmJ5aLRwmzncaEOwZ23iI7w2vkx8FTLWOhlwQysgkOcmT3nVr3r9vz6bTWWOqFTl2Hc251FzvG5AqADc8rTiqE9vfkNh/n2TE4otUliEZ4WRAOSYGQhe6rcRl/PPsu/4xe5xku5nfLcjIuaj5tKKrJKBD/lCwYUTQ5G1xlYw+uiqc8eAc4DPHLWt8XQVXwxGZ5hd4ZCRt21JydvGbNajr33pyJJpS9o79a7V9Zz5r9gcjoPRnfoh0wDn72qv0iKSf8si7yzzrQ6QSl4CgxDj9SBS5llVCNw/zz69hW1dHcksDZqpZ1aOhtOXHfFJOr/g3sD/nL46LK2ICEdZ7BOVnWE/kzGDsgT+yKlbPWGUzsX/Kw8jnN52CX6dK/AudvMoareYLy/pIP19oFtA/HIS8LOvg6sDqRdAB1/UgyVvBj7xZdguH+ma1iEhTyeLxJ9QJ98vRNwVOOz0uHVT5+wNDz6POfltUiePtgrFew1LNx5P1WKBj/tATJOJaoUeOCAKETi8HO8ZJEeTnBy3FTVZ9NwGM6GeJq3A8bMFuH3kof5qSwjJRz8ReKBvJiyG2+KovJGITmPYhw+HL5k/HR88D2x4gIPGgcn8q2vOOryA7vG4yj8/nP3rwkeeUdBQqy63Fum+14bvPJMuWz6Sqc8sb3z8ErAp/yvn9WgZl971/9pshJklUZCpPQ2m3ITO43YiZrsE4NSLw67BdWeE/qQDyE3i8b3x1K41OTdyuMU4oNjIPTTnuSYcKK1suf5d9cgH1LBxpfCGQEXGY0QHK9sN1yxs3NnfFYH/tQMj70FSxN2dxkjz/9/RM6L+r1sR/c9a4nZ29Wh0XgCxwQKNCcxSfJ4Q7O9XWxGKJ5NVxUQxQ8Q7M8q0wqY0CA5eRdy0A+qbcGvqCaVXa9VLJIvILULEXoB7u7u2qJldEKcHx0f3vwbF1igSf6x39Yora9y+sZTLZ5jtEmf6rhxBkvA07WAk4xvpF+Aa5lHRox+gnH/0+Cz9lWnhXFuEhlF2A/J6RLxhijGNG9k/ldLWejLvfiZ30FF8xZLOdweX/K63F6/Z9umNjhfFHLGhLJ4EXU4fahV+Xqp2Xn8K3RDQbyONpG/jDGFdOTOk5OKnEslrPiyxwOLO7KTTH6jIHUy+Imvy1NxEQWnqOKHHo5w3mmn33UFbGXLA38H6v3XGCNzoxTkhnjcxpOx2jErNNM8OYhaTi0yt1IFCIr6cs3+ZgdmUI1pThnkk4RlEIkazrSTV0vQpKekCri/HpjgqNNuNrkMoqnkPtQtNdELamz8nndD4L9CyaDDyYThvAJ4qdLxEifjpo7fuwpSzYbhcKhXpa3pTNxPr1x2xbph1AnxK+S8hJggNegHa7oQC7ucNN+X+UY3oKbu3pYp+JgAiViHlDbu5DdtUQ6RmDWgRFAuVR0IS9hIyZ3+X0j+leNX3XsKYel2UDZmxPYQw1PS0hhbjScESeNGETjzu2s0K4oEXghpoU1BV1cF2yX0WKv83CwsaJaqJ1v5f3/9LmGqRofO5pMYBbr3fOxaThbJjMiErsrDGbuUJbyZsPLxPOX/BPN9TUYstn5opi3M5Ppl7c/flvBXhfZj5yK3OD3wa9PluBX1tl3lFzCEdIC73C9sPVp4HBtc5HimkOrC7ZCJeKlSz4pL79MJzZvdlnn9X16Ogc8nVjSU6IVji4HgeCRmIs+3D5SIQ/zK2pMYbkNpXC7TgHW4xUY5q2DFz57X5biwC3FQWuLP0l8LzK845xAVnIkqqpLkMxwLdAdQEtwbEIvjIPpvlSt2O1VsaDIPi0HoaDimH3XUCAuK5VqIFo9KgOzwCONm1FAWGKpTXooNXrI9GNbhueG3paErnTVyRRFA+m6qu+H6NO5Ddjyhs14jHVg4cewbs6CNmYWPqPZlimlZmcOp3fRaFbgyMEmLXLcGKeu4I9DjRUOy3EqUv0nRPgEPyi7ldvLYsFKRpa5EaHddRPIUcA0c65XCGZHR6NaLuicteTQQSuo1j6jqfNj7qaTIvCc8T3cEzgAigVKrmoACIDXaStKq0e7x2BXTH9KF1pMmN+d5UZN7KCj9viJXyfYuJuqdU3aAhO0/AxWfVHNQHeDzauv2nGW7U/xDWA9LjAZoQG9TfQLvTkJILTCs2ri0wEU3o1zauvGJDuyI/LC1uSMsi28dhrV0UcSZgp03hfcJvNDQYos8T7h4sazUFAEiazLpiKgZQljXkuOWyI6ORpjZOmn3wMmXs5EsJC9hiZF1SwSx9i6+YkjXNUjPXkYaI8CVPQS5ITx9TEmKHyALn7TcoZGYnW1zW8jkq9rBE7V7ME+3UWvoV4pcayiQzecIkbGvmBvHRGoC1RS6K00ohlwc0VN6Aj0x8v7wPJqujT33hpXJYo4wT40ZSrKrjJAYluwXBwiP9rea8HB0mNhDsgoQwmpR1kyzsitHb0Vx/cx+7cyLp7m8yL0tFc7OyuSBYzB+dd/+p8gdEFkX03gPFEayEUuxEepFoQ6CgKBKXAZPuCcfZa37LNIoEx9F3FbcNH+9Z/+X7t29NJXBuKB8Z58MkFIHUccwizdv/7T/0rDFtJgN0YNipQQZBheoSIysh0ygDBm9/rK8+XlBI4Jx4NST6L8mgSTjClE+AYvq3o/3mguTkwZeNaWJOfCieQo8iaTnkJjFKzoMBvoEe/jKlHYSZfCeHmp87HCjt8AI+hxctVswm6p3hzBraqQzpewapg1hbW+9z5t15nhjAuorQqOxIQHpoOCb/3A8cLJF9M53NPvihx+4JNVdqHpOZJN4fdCFSoZsmk1Xk6K9Y+JHsDHLecouAzi3sX66pLTqQuXfSK4BCd0+cwQlKJfYD4m9JktftLZo1PQTy7J6UEU+NMoy9Yanl6Epc3rJZlvsS1gX59zTB5XImEuUkxyaQRHSiopHOPx91WFsJPfwTk+Bzd+ms+bJxkJL5eHNgjLIIOzQ4uDcHNKJmKoD92dfFSDYE4CHfiZf18U8yRmjIdn7CuZ9GrbRCuNUiOMyGCqmjK1HLCBrSOTSM5OERtg3fmrGwKbNgWBD69Co1NXVnU5jLlUrY0rwKASDgV23S8f+GGd7ANK4cElVQDbMUe11eyMqslyOmt2MPy/w6d4zdvQuU+MGVynMEJIaAKseJJ8dSJSCMajLotm8L00V+hR17trrA4GcJKM5oRBH7CsCPEJE1t9yzVTOqvutmDe2V01e4RXGY4C+LE4b7GiCdLjDNgwPkZBTTF7wu/JPvIhWCRnAt+91gy73GqUin0RhoiaQbHp5celF0Yzr/rgMo47Jajawg+0gwNJhMkznBtdFwveNkh9yuY17sLIOevaQYERUK7J+G96TlfcefF18lvRzF6V0QweOaSCYGETGjNOya3Tlhp0E9+/FOhwG4nnjQITTfY2Y4/OZCL8GBrCgzhmxFsIQomCm1iuEKZrHrmgBB6XRyIR0oPvbpChitIAkdgiIx4utoFyM4gsVIUdb/csHRDD+Ndr798/zz7NRvny+maRveJ/n8Lh47TntCAQtYuMgT56/AQ35LKCA54z4vhH1p3P4TwO87rO7+ErLFEbA0DbE3FP37D5XYqdCu40GUPV11mVcOo+UCAQSL4ssVoItTscoppRb43WDj1ix+ACXgpsRIRPnL3JKCpYp6dyvIn/9C4fFyrWOHyNMWkMLoXyvIlMGlVp3anlrBN7L8ayXNlcMJ7Wj45+O6tm/Qj/G1fykCTigCMOimg3kj3nP3wvFsaDPanc4+wEDJb9idOgofskCLOuK46bJukvitKF6KjLJReNwfiTcY3fYut6vUFBeJ7RpMJAycnpa0G2pNVKJ9ALfyhmpBfiuUAOUJDP/VZLvsInQ1O2qr+9M6mkmits5fLe2XeczVcpFcknHugDCOtp+VMRVW1QhoiiELSwiPjDskwZIvn6DGXMx38ENUAOG7pvmXt4aYBDDsipyMr5vGD8vkqAJraO3CGlXRQJgU9A9bpcFGyHpbfFLxP8ukYJOl4SJNh4lwk0kR3k9PzcVempWKY4MOaK7auRd2CxjMF+4TcuKOXICykvEd9wg5B/mTc3lxUGIeHvZ/kMvoopXAbJJC+nw9ERpkOVyJClTlUPxZXwqWHxXhz+unsRRbrrKnJkEn5K8GGSkwzyoCy6jkse9rnESLvHP/3w8dwmKXAhvWMvr0XwL8Xw0eMdHp4zj+HYr1jdc6DMFcWB/u6jAENJQZbjuw8UXMB95HAafOkGjv2kkNOmNRmc2hOLGyUI55mtnKHChayH4xN7AU+rh+IIHW53P6cbeMJSl4wXOKwsDQR7ZOGvH9aWq2uNM2kbrYN21q+v5LHmVhK5d7uczIpa4Wjy25bj9RVhFPEVWWeKb+BCePIce3fchekS0WgJzjAkSGpu5CZ7h6ZIwckx+gO6Sgie42PWhopJPZuPsNPBoKB6627afN+Y0juS0+EgZ4eD205NxBWcFxzfu8oEktekkrS80GdevnfqYqsDIqcaTs14mteL1OH9WLDqWTaS2iHTAS3GKkrQrncE+J0dOMMhdWce055yUgKTbA36JvDPyG0gASZVsmBLiLKggvdxDCWVtDSJtRdOsL3I3sB34H+83qbDgWPIAaMS5wwJPFswvrXItVxi5RPvhLswwYzqaVAwIrqrvERtaCDNTtAn7q0aD9avCCwUegcZzOoVYQ6yySGTGjznDy/aUil44+6rH9k4Wi0ueidZgHSKcpdOK/7iePsgKaa68UkObqZ5jBjMnBgPn2Qz2bqcj//wiatv0L9BotMhF+yxv1Rd/pH8IVAJ3pn8w5NAq17clZIQr8FZwbG630o+cU9P4GWDyBdGvVJvM4tW2aylHfPH0+wDaclzVvZsrKTd3sPNc0s54dHqPHvk1emPMAXc3It3j8zZhgP2HcPL42oD4n7Y3P/4QBHq5WK+XHgzn045ejW+/hvWDDHAh6HQe0lhK5AP2enLk4sTxpjdo82QX/tManyluk5VqKVu0f9xVaYoNshQnsK1Z2NZxCJYFDjoH8A0R9AuSs1y/AdvmJ0Q8QWJ8/6vrhe/zt7h3vvis5siBFCGs2hrA6rTIhNGlBgGGa+8/Vc2Toomz8PBA4gMyBfCYkWLDdLYfH6JWlOf5ScgNifGx8DXVw6QsvHof3Hzs7uqhhMkILFJrKU2KjiXBBubPJVdBhTJcZZNRTVh7bBkouc8CR+f7zk3d33JN/u8vfVVeC7xVYpp3gKEPKzOylhCaptEeJB5NV/OowIC9AvRmuEIJCxX4EiJH3XmfCipDYWBYVlGITh4XwtFkodsf8PYEMdDSQWQuRFXY+BS4gF0ScxwCh/BZkPP4BR2oUTrLttPTmct6c1bwvZzHO+OLlY9pXJ2dzgFGoUuCR7+k9+e/M7Frb5esxrwDfEBzSdFeNaceMgetxwHPHrfM2UfoRObJ8mJGKMzaYNJ1SQZYXfzYQ/eufpcOr4QeixSjPWSo6ftyBVZenLzGiQNcoAzwb2A5wpXjQlu3py+Hb788P5i+PbDycvh25P33386+f7V+fC350p3A3vFX0WyG/ljOu7rNn/NlnzMy0YmbGiawOiaLqcOAwaX4cCbAMGDfHw5W4k4Z3cqxsFI5pmjsqJ3UMA1Yhwx8Ifri3w9c1ww1khC5p58Ywc/2E7iXuJsq5y3jwUKQJLQGmnOccW2xAan899dIMj8IZLwLieTDAN3jIGe9cH4u6Z1tXNOTu6NAqbZZ+TEhgBOBSI2mQh0hOu8mmVdZAmeogprWrTkLEY7BUEtpqchj5TjJ4r49ujqgLYGl6hjXMV+Y+y+GnGSdsRCl26a336OLfYY5lrOUPFwjEU+SAGY11YBzCSOY811F4U21ClRWtKViNdKKyZOB/xtAl79Jji6RVURoQwHptmtCSTZPJ8VE7GeBIKDFdtcE0cRy0leo8Fc/kQiGe2d9AGOHi/0CrhNfHrRBkMoUO1R9HPVdugE8iTouwz7dYQR7HiK72XwDDE0An5AYJrmZvUcac+7EjuePcVldpB45ct0sicJnF6AAA0jtsYvQGa+SbnRWnF+a4ELvVBZkoa7hNWxvEtEVbZcLDD1FNrQwdNilxiuQDWObYYUJh8/2uRwt9bBp0R/e/7hfcabnKMqvQUZ1gE8zRNOmoRoFb4RoYZWAmSjNaaIpf/9heIR/Hp7IqQqxNlZ2JMTWL46VlT2dDLke2QizfIaw7IZSsK1Z4tp40F9CsLFjziXi/GSIZryMK0RWLHugIpVz9KzCfwUcE2WczzXx/+1I50QrT7LWnN7+cVcC5WmqHox9gAhoIUQL2gqTLAIXJ6DPAoBuaOUVnlOmOQ1sTU7srasZ3og+mdjOgAk5PjTHfcpJmhvq5JAw1eTctQFVY5OhyCPMIvhUPcc8a4Z44DitAGDYZPSAhP/0ZgFm18VyKgCRHHFMaWRRtEQR3s9C90JDilgml0ohooO2GygOtsVgvkI87u6YXiRxJ+oqTK5a3nC+FA502EmkuN4NRsTG0sTOHNEocM8g2Ph6FleepdMmLge6qkpVmGl9SQIET27BHKmELdhrIpzBZpJWF2hEWPJKSCbK9IoKK178zJONQeqa9mYP2qwXBeFeVBbLBFyXwhns/bCdIC8KHws1oTYAO2D4mVjZ+A863RiWocxyqGiBrhEH9GBDLC+Tigl7irmKyscMVAPFa+1TXtZsgoxOPv2kWxRhF7aDrwal66+A9cWX5SM84CZarOBzRmi0yD2Wsjfl8Y1rxt/FSOg0UkGDfAYr0/xJUdDfUszVwYp9CR9YJCci6qWZuhWl3Y+dJWjSbF5g7kba8JtCdaYaKvscus02vG1FZfY+77i9DLiSzzhxsHllBiBaCQkAY6Ku26ISgRLvwukO/eZy2mXzqOn4JgI3q4Z+enOKithOKgGIEdqFNRhtUQKf8L6NWioaiE258jAchNtGL6vvUueqiBV8+gVH8EXqPIL7eXhZfVlSDHVLwtnSzhkHNVkayKAok8aRHIpevlt94JMFsq8IxTHPkHvDjyNRtwYjSZ6nbc7quZl4WoWE/lY463vtdFYVwmFmrAw6ZyCUzNczvy1GCKF9pCjVF59hDc8ePpK8FTqqnJ1H944F4NqO0+MCiIDjPMELowhiBaOjDpKAoULWrjoCs+uVboTkkegTINL00PWqp63+kVdFc0on3dWSOpz2FV2kSFXJtkqHc6Jcrge8xa5Kj8iMkQHQahV3rzM7otEkVRTTW478ULq9ZNipDSQAxEzb9aqgIIFa7yXiLnzMr0N6q0VKvRq7qeXFdZkWCpaHdJmrkdw9LGmqMinfX8JJVCPPF8mUMX2qAlqWKaHTV4itD8sZA+BhNd1Pr/JHs+fULrljrjZSDqyHKmygIvqDHtRSLKc6CERKJ8mt1o5C03rkGtHQs8aNY2vsXA19CyTNzt//AwsAYwrWJy35kPCSskkfocW13iKPKwIIgMD2RM3uWEG2coCSAoggtRueCI3G5YD2iJI+RC6cyGg3h9hpiB1H+hncL3LlNWSDJ0W8SzB5CekYOSSDm/yJrC5hnKw/flOQoGkhpdiMDkVtRvR1q4IbqUkV8ZoV9PZ4Qe/KdjrRIlwMgMBjCuCf4S9L3zRZOfp3uARPZcyI2NOsk2G+b5nSv3CMAXRFYT29EMefCIwUGINlOeeM586Gmc9Kdv8c9+PSbK+q770HODpa8f79PFtYhl+IJ63r1qHHiXC3oPFfc0NXTQmNMkvi0n63el4YdeF4+c9EcTSAYN6L/S4+wVSQBVj+QD5awbSdQLFvHbO2dvd2+3vPoP/m+0+w06pB3thO5yDw1Q7HEWTfXu0+/Tpr2QO37qnD+D3V+CF96tZ/25O/WCl3c0efCAFiv3kN/CnyqHXx165pknP0+c9NFxdd421XTOo21S1nMuKYBOfyjUJdc0EcJG4zw62yMF+A0OKFUnnInquJo60G435FtmJ2u0DM0rnby5enX18dX4+/OHVx/M3H967/kWIoRkGbTy0PVD0ZG0MIi8efaoLIJ9iv6ywM8LQNtLwD8GOu7pzw+gXDSc59GncYgqslmqGYDM5bGh/zwru+rvr2qnozw5o5+p8KF8z3Y9wV2tOo/ZhbkXDDYD3sWPI33qEfFWPEOq1tLK/W6IhDbb1a2O24m40qwZNtqLZ35dWNO2x1Q7QfjJW9qF7WV9hqlEz1LyQYPSQySWlnaZvg1Cf3kiAgivJt6cV1wxvY7p4o2l41jkEKwS4HSEuJs3DHXOwH9SkAD10Xy3N7Kl98XWBYFiyUphhQAmmELVajgtfG60MmTDoaxvR4EjdNalzKlR/9YGhg6k2O4Pdo6NDbrOzvz1QxEGcDEfbcTlvxQQtmZqPovmkI60HIaiFhKblU+IzV/WkCakCGbFDVOOW1CQVykP2CQL+erRCmrxOf2x3ti6rWoA5Kwa5Il4Fhy6OCEkSJyaIyAVIUgwX07X1IAo6r4lB3BFPgRQ5PugKO9mGjrBE7EDR1ihnpIgZ+5jv8uZzgvYB9+shLWUMHYceFqKcAbVlkgYnlF89l2nzMmUuo0GuVy13nRxlDnncFCUmJZkjdy7E0ETan1zj5WzJXY3GBS9u8NTgJy6CMarm9zU9RXaVf3NBN9MuF6cI0oMoM221eogWHbsuQOcz2a5l5AWpKVSGDcbXWr0DkjZjmC99XBdPVPioTyjlDSE6Q2srsd1FmiRoQxCco9JqH3PD1tB1kTxQOTjvxPCbwqhakSwakfP6LGeR6VjA6i4oey5pDiLZDjd3hikKhd1Mitk1hiaulMTOofxtQ5JpNaTFHPKXDNsvM/1qTJJSTsSTLmzDvtUFJWnkGUT4nnjPsBJUC1SCmFduqQ4FaAAeNdWXRFS+BByU+G7HLq/F/vG8otYSHOlLVuRxzS2RlWhZCfOLc4icQs3gPO/IH3b+q6l2+a876UtIJh2yFdiFCINo+MD9GmVWvZBCG3lwGGx6CFuwFwJ8WZOHXbqqYHtItg0k80TIGorZhyHo1Gbj0U+MbJaa3BmyanHIgCWX+pyWiwRF7v72rmPEW6egH0JLGz4gDbv8q9Ot0rP3DPptPUfpn0Uyub6MpYW9iYSWgzgJm0Dj6f2cfiekLmmO9Au3atV/Hn5DeraaDqv4DTWxMi1yRAJcLSfcp4fSvlWFaFNXrWvtItQDCNtjqQYXY0SuXmvxN+ev83TUMprsAmOu2nz0wbs+jZf5NxwLvEZACrzUhGttW17gb1+9FJ2LutZ5A8lnbFQ/zpSAkbJHjqeZUw5SpyaKhT1WNCq03DGxiutLs9slADA2NS8dsqAaGkH1/DkWDfPh9nAgDqT2TO2A35KoxZR9tuJqfKZIbs2iZWAYt2XisNTyIzxwaDQuI7/lgRxOvLcmI0qr7ZDEMcbYWD54AzUtAtOqtMXHLDuGzU8diKNNBSkOL2dLIgTe4ntdmnYhsnJYdEANppDHyckcJvtHl3ZER0YKTCjPRaZVkzCuHZFKXX0uZsIyVRHdE0wD22OMq9HSUw5zow+u2rBniuowx/IGfwaWPV2F9dpwVQyn4MWMhmxZDq/no8dPWhVYX1lwtaZ6LoHgtCiYD2enVC2pYNPUkUiz666gu6AcaXwyuR4zcShCczuUAA4XYINclEQZJuxhcfziHl02DboCno+uunUDyN5DBcVW7CWV/GhcQnslUTyFcqnTahaVgQeLuFnx0ccCrlBtVGTejs4nyn7V17/IPr2JjVcDJb8EZ6QwaXRUROKJGUZxTibiDYxcuNAd5CSjWt+UDPtA4t4IBQHKOiaoEIznmzhhPAFJfBAUUiCEEa+yPL0LzbBpmZTF+EvZ7AUV3uPxPxP/iKpggkYoJl2Izo7ET1c5heERiqsD/2DhVcUf/ENCUaiEOlR2lKoxwrO0bwyD8L60O+WpUnIbLD4vfV1wTo+L7evtrHdV7w9ff+w9SV7FCNmb0oaaJmaJ1Kalo7Cf4S/gBqRFrUtErmngkwZv7c3sjYQyznhGbQdQMVdzvyq1pODEbS2GLb4ESb9yezhJp9cF9jHPelh009vKUBdr+hsOPpfVOr84YfI4C3YzVyjcy7CHjq/E8Pr8EIbeDZmrPGJ5PmzQNqLweJBvTqpVvTAUlR9RY0bkYBg3z6nqFsE4MvL29navVbELFn9q2K8xY3TIbzrNlY0rj8Ql6AYEg+SywqxnRJmLgTQ9bXes5G+tOcPB5qfi/5Fmfb4klqmctleiauMpeu6QETFdLecMxRHgulQSmsCnCyB3RF0eWkeTR8aZB/Vg2ftWImiwxSxWaVIZjUmRvJCgtTIkSbMXuUUYXMEFTK/XL6ICR7k74KdOrbJBo89FfBw3dqdFMVkqAld6NWFFo6+L8DXCa2YR2uJnGnYVMdzzOk/Xf2J0Xo9jx9l7YUXUWBLRqOrELguU0BJ0z31eg6XmlsWgIBouME20v0v4Fms7U8w7XxD/sn2zYIw8+6Beb3sc0kit26vJkiqVvtmZ6xPXIqtjTqPeCZy0aRG8HQVVNQuiutwh6SsmH3bKF8N0HM+zjkh6a7vxylfaJUdbRK8qlcndNYKtxIpiPa7waolwH+ONXEScXDMpDr6kaHU18x6e2XFiTaN4rEOn4bxfqggCowuGuSkmc6Y8D6DQNVG4RL4ZlhevQS6/cwFJUwqupfv4nokXRNXsA/68uc7C5QvAfKyO84RJqzEXavKS1vLBhjM0C62ZIreBH5F+he9VsWCkGIOBLk1ieXasUMBIAheTTm3noihtyJk5lwIM7+RIA1CBblh19txautoon87pSq00ajbjLsuueNPF7BiK+vjc/lsh5BPyaHV8lfk3iLhfu5isz95idr6dfXX2zN72wAXkUtasV3OmpXp0XjjUwad6VNTEhR2iRB83y9ENZkuQr/kDCawnxiqmDAe53HdhRoWdBJtSeZJ+g3QEKWVjGCY9e+RhBqS3fLrBtI9k1E2quMM5uoyVdEPD72AgXimOHcIDwshhXdzV8JxhjSSYbQwmo4lR491SNZI5hD5VhlAehvFI5EtIPLMTVlFREZ6NyOpLvPrCWndWgVOilJyGc0NcKGcwk0qvHJQ51PXNOmWf3LxD65+sMqiTVUdciCrKkOJbjBzg5qMhW/dG4eC0SUqPIcql/44xj/+eXbxzL5qHRiUblGVoT7ZtKKo6ltgxXyFPSrCcobKi5xM+gnHNVpUzYyiY4zj9UJ4t0PH8QoIXvvjyO/puaUO08AIcqSNLBTs2ej5yvLSLxGxR7OBvkRSTltmxdjgNpHRw1+4upN7aixSYhkCW8LVQaGvpD3iP6oJQdE0WU5vEe2x8ObnfxES/KViFEMGutMXJRjdVU8ySR1IjZ8mMT6eITIjfbUe1GL6gBBQ3fsn0zdmU+KW9GiO8O5M4hNTddSflVVdtLkLjcv0Wfp69xVioj7kh365e1YDHvaF+F1WdirwlGcK1/hM9dW0NukILRkj5OCJLyy8tfKvuQTol2CrGm+TSBZ0WWPmgEEFfR4Mre4eH0kVn1ToID0c7LkCShHWDI7bMp9o+J28EGBN8Ica7rVz53i0cTFq4adlQ6JCyEtzIq+cTss7g2TIJMVldkE2neTrmSOu6Km6VKvFjjeg6yZNi2nCa7KpvMMNVlRqJAjbN6vkEKfkpitjoKgl6WTHHOca5YMR6TBLdYBkV7KnFGGj5qpJGfZ9dgtL7HDa2Dhd3FUG7UBCH5AdURdIsCJRtI3rDJsdGqz8V//LPCdKDbpA+ITe/BqQ/2M0Gh4jh3X/6QJD+sz0H0ndP/5lA+qvBxH8hhL7i5DuQ+fpxFwZ/gDBpeOuCHn0EXzthnDAhfm1CiJ0+RptLidhlgYmpRjOfM+lctU0A7f8dcf2DXVjKnweUzCD4Y3kvc7b6U6eRk0B4PONpBR6D4dcNngbE78IQ1LfqHlPk3HwX8xqjomRXvyTKOezLJFljW79CB2yLf4v0haDuxo0yKZTyt8uCBChm2FaHfyUSxGAyFPfYxmWWX+s8ArAhjExD3jOrFuVhZuhzTIRxi7ij9CfgFW9RswWymiQixFgMpHL5nDkmLgkj4iS4YU6LDCQNeD/ae3rAgPcVmrEjJWUpiG6KsHLkipqAUd+WWmu6mZnAgVYm987JJJAnfWp6hnEizUHAMTIlufG1imc9JybcOMd9eeXakpS0Q3iZliR5TAWxrLnBG9e3JXvNt+WYd24Nbtw8Xw01ngIzbQQQ8G5ithY8AilVPU+qWuKn0l6xu+HFpvBkhYyHQBRCqqhRK2vBFcFXUgXYMEAmgp8LUaFBRb67P/+HtxkW+OKBzx79P//PI36NRx8+Pgp+fX5DPfJuGJ9rqgOQkal1RXlLhME0kcaDK/WoMXWA8eb9w7JYpuoK2husdDshAEDaGuWx9xO+kuPKNPl3DIY5l9VJORIwUyJqoqQFBjM/fXxLcoyYK4ipgb4ahHR90H/FsUqlOefzyX0rNW4qTYzd4eHbvh0CqEGXGX3I0104ZqxluGL+yWlDijs+aabNgJcppso4GiCsJ3Ad6DEWHbwVGJ5XxVDLzlogzpQUdP63j/0J4aMACVxDNi6UCdASxImBCbp5UD6wCrJAxw0mjWvBIwaeN+kWynMQ2wjjcMTJKqtl41rP26OJ8RpcDprFmpe1W0jBQXR2OSDoiJ4kr7xKlrXoNczjdD8u/JIuqnTV0kVL2cUdX5MXPNUdpLuOKKcwgydPwE4m5ClZ+ASHxRU4cX1NuNmvgspz1nJF0wKtfrRZWFJSrrU2pa9uI0IoWfk4qS0iPERROPpLOmeeU8xSgFpoTlg3ViKzeImBpz+vVAIlzNwpOmSpgL3sVm/wxghS5VhFwlJ45kDffxUIPz0g7QP/1SH8+Oxnm8W8vfjTtH8s5fHvesH+v0bU/9/rZP3pxavO40XykCV0m8ZRWuBq3NfUZjGT7n0EYPaqewPV8iDASFhJwg2ZtLOIKXAuEQOcl5PA9PVmwH13EFKEtpLzlXb8+MZ6MbVqxUwDCYFyxgz5Eor3Kqor9PTn1XWEo8lCCgDKNUUbaZJgtVjuqLvEc57mXkqizxb380ofJPT3BmlM1j1H7hpdqQBqLMgXx+WbnM6xj+z7a5co5yAYhi4hsRlcfHzz/vvhxceT9+dvTy7efHg/fPn79yfv3pwOTz+8v3j1uwtTGkcb6EvUEtUc3ZUkqZC9pdW1199eZ0VIJNKUPqMdAloUtRejbcOpTCntPyq0x5kraVZJWEjH60U/qBUO1n1Nh5qwfVXvkhjwkZ++p3C52KhUbc46r2xcpu1rqj6i+2PD3K+EMDrpt9DC0yGkyjcw9HtU9woTJCepZ12FNNw8kGAavbIbDd/63ds3r197cBWVWQo73iajsslJkril/Vdkecz5y7Ob5bUP9ZnkOSdGGI3gGdu0AlVbY0vQC30F0w2o+3mOiUyTYSEj2QM8lT+jG6u5dNThyfVkFVPKc0Jamd5w4+7kJdioO0wMKwgOHhc0XPGvM6aElKYt5pAopsiW3uWN8yoCmuktKsgLYl9cj0bAeuxrMVE2apxN8nFhvkgQbNLPm5zdYPtCcosAsL3lqC1uEYWh/jBF4DG6dlXWzeLh99tRlqPrJzcb/j5bTosaocVU97ygNoUsuueubrAVxUnzfYbShA3wsOmaNAqfxULcRQxzJULTiJUyxmIs8grXFCGFeWmr8jmg2iczQggaMepdLszSwU1ah65H4b3S/0MRq7KJaxaXI9cw708Yf1EbGAbS8nr/i4BmL0q+XlV4XkiLUSYNM7rSmrdN4Y7ORPaIEDWYXx7+7t3bj2enw9coLoZvq+tH5NBdYZ/oXodgdDZTKkiYL2FKs4UGR1WKyrsJrbnADYPmr/FbsbmJKSFxCVGY4DBW3nbQ6sjeLhjLsz1CBpiCKp1hziX4qnXhOr+SHwsObNN2X9e3wHPtngXHJYeTDiSNT6BWTJBM7hlXLlVerY66/BI35fVN9hEuvGJgGcKHBZT/F+3ZxbshmlIWuff8uZUNvaTY9HVMG1VhRG29pCjYNfUKKtbg2N/mYylcI/EgshVTqN9zo2KtXknF3ugSGonQ6qmjDdSx6sw1nRFKVeIUSfa0tOHHYCn2N4qdn+E1cQDGz8W9i1eDy8e8ao8abgJTL0ddprJHNaxoO6Ohh1E1qWp1R7UGa8SFWBswtJA3vlETwz5oWcZGBbKRXCu39v4Yq2OwaVATtJfCHA3v4irZyJHDO1b7woZeIGPnQksI4X4mX3jTQpIAWU1GZius1+O4XpeThow/bEbnKm0lpatR2KCqaoNFkw45Eua+KRz7l9SNsK3AqXE6htsPBpyvmwFtcHTfWvDGGbOU3PjOr3o6qdpQfQYZrBK7BaP03BeVDVC1Odsqo+UzvXOvs2LujwQdSRqiKYkeH6O/gtfnXcZv0G86Smv0ibaxp9Yl91YtnJrnPVefSK3yHG8M/evPiMr8QhrU+IwnRUUjwbwavcbNA30vamkLihVJCAnh7C9uE6j1vuhNqviT+LI7+sgBPS7Hklto5tXsAfU1UZ3njEAy3GTToDlZ8XBDo4YZMbgXOX4Ppf9GrLWM4NR8BXvVnNjKfY7Mug0Lqh0v0lGz67ycqQE7EwcUti17jdcIpSUWbMFnTaFymgjgNTcEJvg0rz8XxNfspF0BRt3GZVmmWCGXyKAJjksuBmm3XKXNKTVG0S/A3JF+NLBOVHSsPjvhqZEqFJ5Zm9yc+kDljVsiV5am2kd9bt2Cm5z9e0kQWcDkwy7FA96X2KYxslYwIhkTLLR2JFyJJr1a1sq3ReIeJByCkzXH8v27i3B6a8uw1tYHhmtpO7D8gzsw2RuC3WJd7U0pemA1RmRNwxpGBZADIKi33Go8O/ZZXX25J3dsOVOHAi31hxd+iEdta05GoG7Zj+HrOvNpbO/CFEkkRCwMAr5zRHVo32fHhu0rA1cYGJeBYZfU7zYDxjnRxl7z94RDZ4XbS90xghTrnQqHQ5FCJFSOXi3I3MohZzRk+xyyQfY0YL9JQiA5LkmiFgze5dRtPIUku6YManvLC1lfW4t8gnSfJG9+eR89JHSygrVUYEqgUAOiIU/MyrKFq9PV4mHB48hgSYoHJPKJExOs1XEyrr/aHiDZQ63CqDdZIW3PBa3sivmFPF65KrXHTod/FGdFbBbhGtknviwikoyO6ZmobycBu7prNvqpvJvafaum9hirWnBduaYn2P/UxnxvilZ+TwONLe4s1Qwl8riOqYHlugeOmeY1hRankwxrBZPPbeLWopny0Ncleh02VRG8gWh1kJ3tRm0dCHINhrleQehogYMK/4OaCNtfTp2txuayjZ48nFyeAMyL6RBGGdIbYMM2XQhWZVGjlaidobTOhk3heIYwktuAxvCMg+HPn/NYQ9CVwTOG6U5Bnj+M7zU32mDB28QXmmOqLWiK6/2c3+USPE2UU6QkM4LptCqOZQglGtLCNyUNjro4eDHFRCAbbCVaCgbslE/R7xgPRzjCYI7R0BuWh7GaVBTI7J6DY5y/UJKXHnXYmTPPPkgZzV3TSbZ3gpGhdezcsba64WYElPbiODhK8VM+uyWcalS+xGDsa45HD6o+41pPLYmPVCMpTlXF76y9CSbZci79f5azRBmX7jcL/sKBngTtV84S3TjoeV2nB/YPVrTURjGojvS2vDnNZ0Jyg9UX15J3DNqbU7G/h+P5UjoUuJ7UIuV4RzM6s8RtPDuy6xAqOu+rq3nd+h0vCjsIqj+d5WMzPc7RSazQzOxPcnyrADsMUDgjj2xP7sKyXR7aTQiux+E360rV2pS+bKYblmNUJTWiw1ZaXWvTduTUU5PDaZQmxHjaqrFlpWmXLCdkBICX4rHUTGjPEXtskpTGcwTjqqc221c9K1j0TmYtu+5O7SknDsn0ukB+oJJuBVuprNpWBWxMlAhcrDCZqsWYIwsU0ecbhQKjUQknc8WnLpT3v7e8f0oy1PoUsaDU1ImigShFR77f1+fpDF/PolKlTFJPcyNmApx+ogljnL9BWGM184hOAebFq7PJIxVJnTy7SY//z52G9H6iIKSHiJO6hPnAFS1YapO8mtxL9ydTNd+BMzDRHYrK426tjk8G8R9WEldwzrF87kHhj/Tbrgab+rQkZzBwCq4/KocgkK3NfeYQ0OHRfMxYBFRRWqqEATxKEVJwl53SSXlLKTXz0zV8ZLgx1B4Vo3uMPvS90yxIxWfRPMpY4TNd+UlB5V5N8jt/BHojE6zpZc28nM0QwTpaG40ar98O0X4dA2AFosXNizrUlt0M/SBKmoUWmb55uQEqxUeAxRbwRbFonDhCRVwvKVO9YgeLJIHfy0u1LgobWk1MwCg8Rrnh05iPrsmuCUomxbeD3d3d9UfA7Xk16UKowBtfsSRIZ5wDPhtSBihiiYxwA1hOtzo1ja4FAPkGOUBm4Fe9+jKfgGFRZ4NBepfgsKIlJZlUVyn06ePbdWIjqSv3045BgjhQHUIHWiEZVAmi2wnQmM5hHRuhGYxaAfpiLGomBla7bz6ceoEHVHcHMdbgQBRceUwHIwm4HSl9cVB31XCu8+FHwVzgu/Inp5rKRYvpzEX1XExZSmTDh7oujk5CaX9Du3Uuxc39BuO8gCmZS+bQOvEp4j6071oCwNa2u+KmERIQw6HEmBfMAwqepGBZOUNm8rOW+9q0Je0Zag/elZlki9Sgi6g/V50yj5XF8BSCpBwxX7DY//LPuPFSkoX7gneK5C9KIC7pIrsjeYkfVIkv6DReTk2gEkydMegYWfq2V84watTHP/VeNNJkEu40/uGFz0Fhho2JTpQkq6Z2Id18ESuSQqgvmxiSYK6K7y/hY97a07IzqmWOWYPcKwmaLF4KJrMj2ugFdbNpO0cpklO0JLqCnWhqyvb7GdOZwkAHC9MEHdUm7/IXuzIa0hCmy56RwL3AKJU1QPNih7SYt6H4LGDAkX/wPOtpjMseg6pOIYiedpecWilOHQiph7rrqYrTUV4QPJAe9JgUOTMb3KdaUVebKg3OCg+esEJM/mwWwO1f22pDv846QXSXFyqJHRq5E6D5Rpt2OLTASmt8Zs/pI0a2he3n4pDsr2aXzfzXqz2+t4jEULYwbv3bYX5KE+6GY0XVLHNtOrmN2HoZ+bK8LaX7mC0YQJhpLjSZdIIx/kKGrEQSgox2o6gjru3uSM1IhPiqKMYIWtXDC3YX168zwA/bLjeJsuXNcEqJbBA2wZDIjq/18cKHFOgXeW0RPgImWNEPYm/7uG28+WbOM0NJgrwpLGwi9vSvcRW77W3Xofwhw5osl3Hy2cVmNuCCJP4dOrvChWXGxXx8QSRpjsZQPdFwydocJ0fPe1ys/1WdSA+wE+nBUbsT6dE6kpMDR3LiH/8zsZysZ4n4JTKdDHYPY6oTJF2z+hn+5Iu7lRnT85yGQEQLMfW/ksDk/8aEKMeJRqe/aM4K5lXZk+WhDjrre4vCur2jXjsX9pshm8qKIZNEKgPcSgIShQ08PWeA56v8nlrEUj7ah7Tdu25nvxcaAgn6a9pwyhkPBGIpKTnVl+Qjv1JJbpLjveOBUpMcbcju8Q6ZhJspQuk4Kc1hVvJLkC7hjYMmtonYHsqLoRHQmlNTIaWjCR2ihYkGD0GG+E/ehlJgCf99lAubtlO2nGbUXjNvXooNsbjvmO2GNdWcmSPPmo4UezG8VbR8LmJ1uJt9of9//iWhyQ9XUqT++bXUhxt0bDmhjYbxaF9dLxlJShkWZf2TNgFA/1hROLQA/qwfbu8nZ+MTNJ2M20zr8Qsg/9U5r2RzbKVwTbuolBm0ADGZK4erEE5yT9Cu1MxmGApT7G1bF2BrMIHs0hrdIHKC12g5vZyRy6A3uiUck6uxtzavmaZC1HOFoXaG5Pscs3oOMTO4cIGTMsuIllc5nDxJZ5N1Fbcero6OdJqzYEbKHsqFjgIatGLnP3wflRoFT15P4mdqkhA1go42YjMl0y2GdGtLupAeB22cB8MuLu89865RO3wkyKnm1LmrJE4OvprWqhMNSnU+4yUdOHPEhLG+jZdPuTDh1cYLzSUyzNZE6FEzdIsQJXiNjcINH8lq5q35ppi+2J5W3+zA/0pzO3ixT59r1Dim8zkIJeI8LkRrYvB20Wi/HTiyONDyM43T4S6G3UORXr219z454hi8LYUPyBX8MUJc4u5OcVwDgyaLBVgQpN4Ta7Vv+QFieyEL/E4WZ7JgJ789+Z3M3CNLpN9hQuNuZCFEZmeuEs1y1IpUMKEXrUNHv8G1p+AeFkSGUY6CZE7w7mkMZfq42/oahj5LYid2hIXqijaxvbfSS4aDnFhkCzKtEybEt/fTx7ca6HOIIVOc3m6YF7zjSmr2TtnIGUl+Phd+zALRGEe0FoUkAM0dXcRHWuB6ZOPhwEx3nk6CEWzUiCvEjkzKxpTj8BhiCYaDCHsrHH00Bz023UwINgFPxhWF4Tngp7wixHq/lX1f57d4RV+D9G+ihLQHuAflyNZIxSliyo7ilma7yI5M7pRYcEJiIbsl/zKnMgwDheLEi1MNbuHsyzGBK+lFEBZPYsJvVGsDnHaiTShnYvva5UM1lgrB6i+TP5pzs7kwsKdeEJc6pGpzTRqK2cG8OY6hfIVgav211pTIMemdY0SxNRmtrUjuxNrGfb4ykEO53AycOlh6cet74LWoGtKNJ1cJIakV7vnxhxg97tFe9qgNao6hkKFDO/QcpZc0KiCZykUKJXPpJ19+47bKOSzhdWRV0rXTEkfHQYAtEaxVIhmqsdxhPatuK60aK5THtvNgeHllZTQH4/UwBHYWNwgIsA8YZbwRJlgOc5v2edJbQN509Qn1IGGkg5aotxwEs07NqC4kD2hAMtY1SNGWu1vlqKY9M0+CMJ0wdSHYZd21igWVGpBVmvdWz8ymKXpMl1mEwKNT1PLUcIHb7YxtB7vpZIgHmzZqKH7F4yePomZk8CKYOOBNSk1uU+8gnhwdLG49fFaXt/nofnjK9mjP0xykJ+NoU6rsgDvWRRL2YHc/672HQU6WixtY2p+QDP0VN8tzTpyzhnkr8IyO+Bn0/AM3bvC6Gxm/yVYeoKq49oU5Rj2omAtT5ej6SFbwVsg5qmJWfc52wb98gcRllAZOJVleUjM9r+4bhsShKGMLyiGnSfba4+sFZYyjEJZWpRRjM5nfjRgBCvLWp0JF4tK6/bzp+/HDQU8lrYXREw91bsWiE+r2Y+Hq6Y14uLw3bIrFHY/WlqtuOro6c9MJCgfh8GTkDibhGl30ieJutc1cU4aODuDy+mYlT+K/r/3kNeExzsmpYOnbkfBq78zopqoaTX2JOcxk1s2NA4pEptBD4bqofTXxF5qhTIpUTstJXnO13LVcYQwLYYBlUcw8bmmClRncijYRYKu4Yule4BMk1TqgSan27TyiC5NrcPy2LO4Shr9jnPGW7KM3TPtyUl8TGPiVfvJIgeR0UVzFtxysvFnhsLTsBzkuC1CiIGYF+chHEnFFhRbDsTpEvBoc/yTL1N4aAyr08v5q75uaWFpLpydm1ubE3TwMXJHobq0OniuKSXqUMC1pahqD0M9xSqrt6kQEC+MCKXKYzYnC0Cgwz3BDvuM4yRaugnQunBY1s8rCrNq2Ucv8Xud1BYsTOl1mqSTx9Jaqre6zkwbpp3I0GASlx7cwqn1gqAovIKZBs7eqYnvJIuiPha+IzadEwU4EacwTSYcntzE1DXNOzY60gw2JtuCusyEiCp1jQYRCKP55i3tizKa2enfdVrcI50/RAAODV/RRMpTUsVdmLK2pJriyOzYldkdkLaA7ODHwTQwTEbuFWvp9suikBVg/eakG23sHG7ntnchKx+s0dRqL3NYP9XU+K0GHTO9lC6XnwRRs0Js+ffG+yOs+t+iUHu6uCykSdcwoe+1AtWymhRwDIbLcXXRyHdxVXo2yOESUxdH27leBLAbZ7tPn+/vP9w8e1Enm+ODYgizk6T8TxmJl7viXCK/AtH+IrhDzJDYMhZNHyprT6ev/bdET+88OBD3xt7z6LyCvztAQ2JK7qhpJwrdvm9ckwSH7cCxshvibZp7PXrwzPwPZjX+KASMrH9TRe2cPDt038xeU6/zVf3m6Nzj6NV4L+s/jXyuSZJp/Loj/3TYfrpczyuneh/14EK4tM2dIDRvW3+TZTV1cfetFZlWPkUOzIbkpPc52zAvs9F6YZfhmJ5dGAXYgI3p7yMHwbW9WMVsk/BiOMP5qW3trH7z4e3ARkpzOYcAcr0YiR49Naku0M7Yy7Ri3lZEkRa4jFc1P9ATA//8qbwIy9szj3IKWtVuWOpaUubPyzWh/XxTzxldQ+e7d4tsSOwuKM0StSqLL/PycOiMJNhcFrHRaYuOBeh2xD13WcfmMy+HiV24pP3LPXYsowCdA2cu6yOkPbr3IGHBy2S4MldI0Lu2BRXyza8yfmmWnXhd87VzNCLdrRTMpdkUPwnutKBA/3CvYoFnDjgdv/RxealoxUTS8UPvZYGk1bKAV7sfbwffstSTyVboQsenGFLm1GWVLCvfhl8/lfLqD6E74zt9hUGVYjr8dDAZHz46etc+4mcp3VYWr3+CZN0udHu14d3d35WgXlI/5SCQFN+UcmzZsNPDRs8HxyoHPl5dOBm0216NnB/srhzwTDM2JmsCbDPr04GDdAihxkR1Qjtv8xVtebmZuJbi0310fMloIH9ts3DoUKJ/+LjsvCqFVEpRoYrpHR8e7z/Zas2VCoavgPKbmkHpwJBg/UVXlGyH7DO/IawyvLoo5Buvof0vzNeIaF5cRExZRb7abar4FSgStlOy6IvRBh/jeCVpS7QifDvYm+SOy+e90abadlZsYXE/YTVKeoEgEujIXKcBxE+7hDV/eEsZhKbdBqatNNIrpC07NcMUcozGCMCf/BqUH1R5seUgpw8dNAAeWRwr6fJBvS8oM2d/F6fdF5t1jMpp9PWmtKGSM/kUuwKyiB8LfuegMRxG9LrekUd1Ocn92b1TPFnZkiRUQnyM0ypeNWTPt2EdweIJJba3bYbgpy2mzs6jm5agPTwqV/cbbaMLAy2l0jl+KLW6OL5k24ofFtyUYF+HZTVttEMvjPict5N0RHUsgWKITowV4sFWCXv51QfvovPtEfkx97Y6ornRj3zL5U6V2pOPatRXzZQ0uRZNY9mBJTt+dy3WhlLBZ6Hdgjk+XU11Y38aeZizT8pvQuehkr8gZI5Cgo/p2I+OemfKXkilZ2YsyfdVbapRzFdHrCFkM3YHBr21+Sn+R2AT7iz1EI4W/6Ngb/4ujlvL4UUwmyedShpGYLsUg2lpxTLkfGNXR5d4Ul71Jga/39p8ePGX09cG6VtQRLRNtBNq6PtP2PRF+0268xOjPTTvb5oJPBw/qQ/mXbaB0kOwj2X7ZJG7VNylSc5O71eb3zFojcE+Oawn12Ib+cQcXuQTv7bafqtwn8k8m0U4kyZczi0OA218SafAVZn6ZY164qDjVtLxEhbq2kIrrBdrWpQwpRag+yK7pZG0krkTBHHhNPc4WplI8IAA0SAK5IZY3al5AVpIuia/LMFqw/YhWrvwdvEw+rs7y60oipoq7CGxUnX1HzRmz3gj58Npmg0F/O/hW+gsL8LGFM3jEPUsJj11gW1l4JOe+BEBUfKHCz8TMOAbitDf31WqhhuMfBoz8Jhumk9a6XDiHni7JT9/ZJa6wNHceclxO3HUCKImPnlaYJlSyMDlWp2s7ocatI5ilx63+iOAaMqch9tcz/QV5sSkNiI33MCATkG9gpCV5zoKb43zV3luiRalGn3umJNZlkam/8VgZPQPMvx072soA4TZz0rjhDkUVFkP427HRLW9y2iXcBGLWZJYGhGy68dy1Q3oVjD3Qb7hon3xkKpIUcHfioVjfdg3fvkOiQsdtJYnwmb0baLzZo8WdnZJQtI0aPXSKsbqA9Ua0MsszR2PUSFyZgP2E2HrQYq4QmfrERtOSZMDpYUFc1cqz5e4WX3Fds+E1Y0CHmNMeYmmtXzzKi1P7SEY+k5OcoEPoBjjPKh/MuSNnRXl2VMzHR458PqlmuRL2JMb8J77hmHhJ36Jtfz+9rCbd8OyuZiUhhVArwS+2A4fnMdatVcIJq8E1mWin6mLaVdrwU7AAiZzjKh+1zkikguiUUWm3688SHxqJuCoDiyM6TM50HY7Kl+YISEd5Nzg2EORbUNNOyUOGDeKAD0uFJikLyhZA9Ct4foX+iM6lF3QoZm1/e6aNYANDWdnRGlK8BlM2r6H2CPl2CAejv19FbcC0FdRfV+1hbQnlJ8y9WjYhF+Ek9gx8evTSFhUejVE+GS0nJhvuW4jnTvJ2kXsoXRZtKCJtyFQIVa92rAKHo19d9Ukj6eXsESaO8KT+olNDOFbDq0+ATeUKO9JG++BIgrEjifIt8IZ0hOBivO1G/Uf0EknbFRVHnhI87mveUT1kWygqH4UYj5K3kFYnE0VUsmQYviLBXwUWd5vxmCwU8p9BlgzhZ9+Lqnz+HI0VXu7FUBToUG/G4ycbrfU1dyiClY56YXattCvgaGw/cy5vEm7rhC6hVqebslOskwl4LNjc0S1U9yK6/qVjVG9SJvu6q8hGNZU/yIH0lzogeChiYBqGPX1bJs+xnObO6X7RsBCKAhtUVEosT8w7gkfLHlraHzsXXpMAEbfhhLLf5rf5OY1MrZ6QO9LBuJ2LJbjm7T82Ia1NcJA9JMcxcNCRNofMc26ckwjiLlMpNfKAbg9BL3XM48BLTwvi67tBiuo7DbHq2S/bOved+77c54SyfbpxHWWwqnCHFvkXz3SiQVjiLSqT3D2pw8rkmtfYKBAthx6sOTEqY1BL8d7imEswnupi+GQw3Sof1zQwlaocsbAVzmJz46kJEQEqdo6RvcTdQkqaukcy/SsdgjLZ8y306CIHJiG28dz84INOc+VcWr9MI2VBPXXEdca8J57xJhUNadeB2eOB8Pi1jzaoQuctEToBkWgvxQTWT3rZuK7mfUSOuLKPMNSfFmedT/e2tSKBWw052Jkh6LI+88yzjj7weQzJlWca+YkkWFQDKpgtXn33Td/9FV0A4U9nD1KolGlX9L8lBoT+LZe3xK7YCkInm2aJ2jpN83vOVdvQJilv/C+UGhuMH9pHXmeqqRHVorXIaYXsS4psCqW1dfWhG0zB+Y7kkntnD9s58Yza2PHNhGwUGHXLpFn6ng6MJl8vvuBVS7X5/EHqnVINexCebxLFsD3gvoC0QyvjLL8/80U+a4gPX92Sx3eaTxCYBY8pR1j6SguVtA8DUYCkwhiWxGKHVO1AQlEcbz/7qnJWpcdczmYFIjfQ0rZtLzDV8IgNzQtu+wn/QnpI6kKb4GTEqXRWTtvZvMPIIzkDRq2MVK1I5w+z4cz5QgGtzVxAy6/roSGU9fBxXC+wRDqhv3opSXZfsLLJ1W+9BWs0QciiR0nVWBzJsE5FWAewMc9aN7FsmnSa0G4z9t7IjmaZmjN5OQZFXfLreHtAW459U5PdRdmbi0UcL4eLqbJyF18lMQh5o0jHyqTABatO9nfwIyUn4QCtbVtjBjlB4+uaMCPkyJF1h/3mObkebncq9ER7zOoDEY2muJHMmECxcDokMQrdZwkae4+W3GofmMJoKXjYE2F/ExOHWkzv3OA7dsT9lc4E1BPVoF1ZIk/x07z6Z5dJaiHwNIMkSRU5zkziiQg4HkmYHwanTMAjuYTCAikr0TFHn0FBvkzrO1HnjnzSp6Zr5qxv2veUx7/LOUSuVNz+3koKlw2NadVi8NhM7fi47fh+lk/LUWCLUYu5FtUAvClWh3pjR/dilcFAaXU4Eh85ceauIh1bs69iOBnBlY58bmY1BfAnEUkgyEESTe6dluNofImV/2iiejUoH3gzwU0HDvf6CTiLgbgxmMdJpC0dfhu3lU4rPkBN4mlOVMQp1LUjDJ5U19cUzjFKyZ6fIKhOXm/ketOcvNyxrRptCVEx5sSTBJ4CQGXiuT9oVCxY0B52m+yxqOPFdU9aUsRYD5asvE8GyazNnQ4072reDit7rd9Nr86uwGnSFaAMDAYIlpS8IDBtSqDH0Yx28ijQt3r6seRNpb17UxMTyb9Us2p6z8OlBGTk6KAFhbpWyKp5nbtcnTi2MwsRmd15Xj5SPSK9rejkuLaURG8BuqcGcX8rkKwwmhk6Jz2vdHDBa7CsFwy16nU+mLO1dPxyuhO6mgbUG0iNVSmlyI+y2XafnFqbmOIv8BCk18ux09+aa+nS473vyDci4vvLCteyRjAPu2Jq0KSXENcgARztUNPoeeLxw1kOS77AmDMLYtf6W7F2fK6ynLWiXEKooBBo7yukbZe12cTEbUXeORZQ7Svjy+xd5ia5w3wq7iW/jM6Es0wYB8ExCczG01K3YutmsD/cjaaTIbN/gZga0tr8QYPKUYuHvCXPpKVyZLhQuyC+yGmTBU3U90KihrtmALjUHBxHfSQgmUfUpoP6VhV8S3L8VmJUF1WmtAhudcN+WkcD79jqs+FqnT9HeKmhnIutnkVHK5DbG3IgtknYg8rXXoMs6Njm7bYcY/2FeznfsItq+YRduFsqaIhuUlznGlLC14lbQro7iL72qsQuh8KwBgip3qa2ogMJbb2vL6I00KtiRrh32N81MZQ/L0tvLQJCTpwzcoLIY95X7p/kJYTeZ+iEYZrTO2KNcr8FLYDxAB9vHzCPUvaH1/77z3298yv+t/J/5BJX9vQfRGnDaunxE7zIs+Vk8ocnDIXhXNzlfbBz85xziHzFG/y4LsiSaueO6RQzwWZZKQtMkz3+w2v7/Gl+XY6Gf1piCdjwej6CidD1VWZOmE7qzdN3iB6g64U7retkvAVKeHOM9NSHabPzO6TlEJzc2U21qFLbs+EzQtzzOuhQO9TJ3UyodLSY+xZvlGVhqdTbQkuKnxYtSXB8012XI8ljpSo2nhDTXEQsOxQaKAwTvPErBqLocJPgTZolRZoEMJagHTY5/1zOPbzwXGu7VePgjX8Pv/xyioCwB8d4Pl689X0EZ3xgztltB+Mg5aWwuSIydcL9IB06iBS3tQTJeclDnlk72inmMKqZtu26z3pngtsEbXJTjYMChl6Cq449yLYdlnyawgKciKyCUMMslu4+KoOMH+5IUDoF7MQxN9GbML9uiJz5am868gKNBmv8vDw2zdLQRvaxbxnSBfBbEZjzusWpespwYphV2xNrR2d3RULdIDBAdQ63aIGVRBG0+7KbSqPti5PtxGVwQ2f83IAFatYCLd6yGKctSB4+ifkK+Rf6yECLc7iFY6ThHvYbLCkMR4xWZnJJvl7qUygCwsp8pYPRvQ7cOGuay2r7LaIApbkX8X4bg9UJWPq83+oeiekQfvEJvlCmde6pM95jpx6PRMqxp9cXSIPxSxHH2ZXy2OhABgn23AI1Td6Gt8dtmC6XXRS0E2ZcnpPeA4V9EIm1Mt7wc0kwOCOW4dWzvg13UB1yvDHy6EcNx3D6xWyTV28HXfDx7n2+W87GFkHkQm1Ww21yuli0ayRTteLYKO3QOI/9PiPrOmm6gwSDUYPSWWWS166JH7c/EXgbqwNpucjcbonHpk2AFUloYw3ITfG+VkCFJIgeHwFhJjO6wS3g14qt9Fgy92ojIvWjXfPzWdxJWFGeHzzX4XPpR3r3CHxCCLw7vtYG0OlEJqGP3b/MYInVPDl7swI5oW3KCJ5I+RlNy3hqACYGCLCYFrFmTnQQ8UAF0Xb/1dY62DRcp+Fv1tqUKxAIGG/tH9AoJ3n0B1cY/QeNm/0BjhozJiUnsZLdvWX9pp2Wt0ga6/iYnmcn3l2ZwgJR1dCMax2FT+//urgrr4fqVLxRKCsKav7onIre9Qt/gRvSxlZ7o0iMwCoK/um5DoOhMVwyNFh8Jq9FY5Rc/g3rojoy1iEtmc/t2fYb/oVcQeJkrDQS+8gRs7aKhR6KK0WA8a7HUxTqYHuwvaupY/jXvh/+61x0fSVPvdENAzfSjsomHFWTC3+5i5qc1OqeDB+Vg4/S1nfzITbuBpuIOlSCqSVUV3fzfrvZG271Kr7N1SndVcX/mmiT9N2K4sBO11iE3dgUP4kH4lQzxXMRH5h8M49Sy2Kvce6Xja4aXu4M6acnVMjBTAWIDkfp4v+eN1JJQygydKgXFUjBcSMFStPqFl4fe0zgeJbJalvrRXEyD0eGe2WGVT9dcXtjmyiSGs0LZvrXWPBGgEx1+RQ0yKA7KtMJe9k5H5EbCbUtvTWwU7YHJEYr8QGbt4Y9g5PFGIlVicZ1C+jsC+3yxhUcE1LzCifEpzjAVdhoGA2yR03bDU0yvnW9LBeVXU2WzY3uSiSOLSxxzWCdRBKhZ9Utlti7lPJnFcxBRlB+6/yuh701rb3KSQ7wdxDiew87ii2svRaymypQdXvmQZAjMNm5O6mdgDrSji7mYW9IYhfrW2IHMQGm68GXqiXSvYBrbf0d9LNIfNhIejDQyhlZmG3QSfCc2BeoJcHCx5uMvZJ9Jb6YMJPqERn+KfEXkUDAhE/WX1yN7RMSAYUnu0HYaMzTSOZEvIT2N6xqyQkj+rJD6ZKeIdblEL286jK1mBTLGSeR1BGNnevQ2ElU4YT1Ow88S9+dnJ47cRXwi6RujxCC9xUyF8TrgjZwnD51K8VFlg8TX+rWGALyKrh3iA98oLohJjHtQyfqQNw0jb+as6bAtTWjKs+u8sxTkyLJ3UpQzVcs+KQ7OHjY40OtVgla5ibmRtEMMlHNaz9wg9k95TdV2RP7/GLTy22qkbo86ZBnnbZhCFtlu1AMQgzMTCZiJSKpk5zoimH4nLJhNwLDEnVD/e+c6KT2NRyydTOa/DJ6g8mipJ0wnKRWb1McpHYNa1b04GoVSFhD10nANijTzid29OO9wdjrrLgbcvqfCfTpug59KFBMJN67ygfM4nyhVFdr+x1hM1NGw6p2ljk9LBUBOOoA7GygFX78/fmbH3//fYCC5h7kCNHB7zfaLEY+S1Y9ROBTwfhmF/fzonmAYFe0GmWbWphaFyH0UHyNBRlkRaOex6iaXpYzUxZBzXtBqFjwpZnNCvusk4/4VAX3RwG/vZndVjgN4a6A1QtjBakSOrrfBu3AZ+w3SBuhxFrjnicxdgrh4TVqOOUhgW6HAkWNsbdBHDWEoPaY905Au4YMMPegoQnomUkvWQoQ2T/tPsp2+cHXAwPFBUqoYZgrzbq8b6VtVh3xM+OwkIpRkOTYGT4xXIuiGRnfQDRopALb07bcCL+WjEXsSxtNxlQj+DProPxF1B+A3DXN9F4uJ58jSoHwLCXDm7bvwEti7si+y2ef+fBcFXUrjrvRayD6qSuFEDgg1Ic1p17CEnna2FXAfleOw1PwurpScFPICLxsV/SlI/km4y7SRBbjEhdjoYuxQSGmH0nsSjLyBDdKF6V7cVJ8+StrAj9LEMEV6//I6AA9vKNJ1fgGwpSwn42ru43XeFQt5+LMkJzitsKUMyTQiMemIWRZsVDUQ77c7PaVC7dpFq0jfXO4VOHsDZb9MDkyC/TiC182PgRNWK+y3rn7QEGn/DovXR8qV6p2n50IRc2L7AeEv31wgPSm5Q0lxeFmUoexe5q816pof8KQljZbzuR+FqbyzqcbYaHmOROXuK+hxb5evoaT8Rg9XHdNTbTUahkYyXHcITD9zi1IYDOHsUM4CxDR+9/pEm4ieUinh4PxE6q6kWdpHo8fQaC+GwTyODvZAoVs6a9A+VGRqLkdpM3hWlPpGFPidWk8ZTugp9D6uaJ15cuQPac6iYcIC5RxwYjUCbcJUQ+pAR/QH5cBB5ggMsyHHnkgaW2Xr4HXIC0rZ+nHU3++BdaIZszL75KaLOeH6V0ZLsshncGha8HqHtIqn/cGt2sWg9BARbzPkwsqj52FTctaMfP4JPwqn85/DZpEz5VoKzBQPuIGcAvnslF2Tj7GHKyAza6o31T7uCYtfIV28r/2g3/ttVlf0J5tUckkCCLR0vlvqefZNridAyaJFYyHiT8Kco7Du9GwJikyHBHKmY4CsndVdb2cm14I8utJVVET3xhJ0rj8pG+GBccg9SbptRG5c6295NmvJDCFRCRteRsc333u/R5kPsS4T6DGNZBBNgz7M5YqJU1QlXpU8Cq75lXW9RYNC7lZhLuAJf4AMzFcWijUOcj9LDwD5GrG5fYma/XfAiGAQbYhgZ1dC3Vcyf/mP1iLtYj2wSYUltzFuYUZYZW9QV29rntYVMCmN6MK+Bi7VRAcdOlKL1EmUIu+sTKetryF1DwSF1B07siGrrcs+Qr5VeY2sbuVfs3EhVRCEVXPW11KDSe9dukUo82uMM3ex01WL715JTEyjfq1aokXNYpyboaAs7Vtsp98zrW3m4CZAnwT8g82f+aTp/d95TkklQPG07wqpSVpKp+sDac5OIFtiF6masJS+d7DzRoOquKkVqUYkqN7h152j1GB2NiaI1I/+qvrlsamZKPu80ajc4l5mA9RnYtxjMu6+gxb24mqX48OqYsreD+KZGoejwMVVBBKB2cVadJvsTj5ykXMXKuVRvpodZQ+gxegLp2E572c9E2VxdlntiQ3TQ2Epoa+KgwOdNW8A4+fTuS6CGYwSgcyOQ+Vr9wKRyS3qNouTQSaMAG5NtWXjGeJujhiSTyBIokQKtkB6qOPPeQUEXa1FzHuk3VkMxyP+tMyny3Qhkp5Dq1KF8dHmCZ9adO7avZhTce7Dso2/MQju7C0a8Vh2ElQNQTVwjNG6Fvi9vHKnUyBZs1Pex9m2W+qydix8AivjNoMyitq5GXvjDt0wJ97sZlAfZQtEcT/OTx/dX7+5sN750qn9eDaneCqrefZB2R882N5JnqsUCnGycIUV3DA17kIut3RNMjZnwo1uGk7YLm8kdOiRN+Z0pYRJ8GKg+ArujT854Iituii5ZHbaKDZZLuRb/Eg/8s/+wxmOfvP/ynlxm+Eq3U2I6woesjIPcXkzSa830thzzXhsf4ASgs9F/eJ+jo7M0xIE4NYULK36mh175SVdT7kb3NgO0mftsGqqakTkY1qNL+LPWLNG/y+xLZFnGDwQj+/TLmHB84FbLePO3reY+L0h/ePe9rf280Gu893B88HT6P+cU9X9o872j/eP951HeT8BL6yhdzggS3k1nSTsk3kDriX2sNbyMEzsfyd2k+uaCWHfdB+PB2evf30/Zv3w9dv3r56SGe5p3FnOd+r4x6Jidttf/Boy4FONB5a2V4O398s24pWc/hN8+gN285F7wZjvC8iRdb1Vlx5G/cUkajo39FLbdjO7nAP981ct76I8f5lsbgrCjx100kflrFP3oN+/FfpeffsSFrerQJy/pm9DLiRGx15IltGj7HZoJMbiIHv+RfZa/zJ5r3cVj8r2cxtYE66JboNZrCdueYlpn8ExyzQjMewWjnjI+SD15Z7FX16yj60+y6CdcHdUEgFlcTt7EgZ6T22KPZuW4BvpzpW7O/zrh68GIB0TWPzV4VwfBrjHPMsOP9TaYqjFhoaD4aH2zZR+IoyO1ZBHAWPST0J63JydnH6mxMGALS0D77kKux/RMdm6IL8rvQ0bNZTHjayMmXzee9Tz/Wg969AxUm5i6PbUtJ+AdeweYrJ/EeNj1MK/hTLo5MzWoXNDidCJhrhEXykJA/Ou2YTGwO6tL6BoySNo9mvpP4lZoST/eydaoOSt8iE0vMhkB4cNDpRtBa9eNx1Po5AI5lR2rssVEsObyf2nF3zCRZ8SJ9eS/VgBsWrl1zqjSlNHXlAxUm3xpGIU3crBQ4SsaHF30nSmUusNKRgvMHErA4eFKxJXQWGWBPqiwE3iqfrvUfB7B3H163buDq+4ntfRMLFddzCD+teuCiOvEWCsS4OH9GGi++HyoPHx2Zj29OKe4xRI1R8kU+fa+yFmtvm3CVVuxXSzxWPOjLMYh01bRkOtPxM4yQWfH97sBL7FIUbXn6XNWDJTHMD5liQCucrFiq8PRg+ekGBnYdKhaOHY8z2PILlm9/rFeALkZ720S+wS/t/fmCfdi4qTmZcKSW5rG/Bv8POhh9myUU4XNnbO6iZoYCbVlgJJydHpLnLgH8d+sHP1KGc3uLBHco5kkKc2ai771X3lGGeo2XKuSf6/B/9U/Tg/MU3l/r4yxdcvxLehHNcDTJmsoIDIS06uoAsc4sOEYJ4gj5/Qj2SNlJAq5AcYdzFk3RUp1WapBUdpeNSkEP0aVZSBgsubA1bnUJA0hoMNj1KPIXL5bW2N20Wvq6LZS5Ppm4T9nygfMK0nMG0cYQrfkxiOrvBDj0L/vXU/Ss93aC7ryVb9F3tcwvVNPRkbAU/l+om3ixdzpx2+E5/YN/rrC5uy2rJgZEZoj159/yPuDesPRyp12ZZTF0LxdcxbVLw7LlFoycF+SvGH9OZ3MZze4L9CbnRbDpCgu/4D8vyJ7mb9CPXkVQt8u3pmNVPGdCKXFUsKVW74utl2pNUdMO2vtWRS7uyKGqqCS44+4nLRdWnxj/19vxmLvKP1iZYGZZ0P5o2jSi2sTXjQL9yEBwSvuLnBr6H/S5517lAEK1TUFxNL3tMlHaChyfV80SH2et47h58ome18ys6NfrCp6bgqjkM8p+xAA7sUe28jr/kH+7J49nesVDEUNEOtp9tf9nG3QPXHv9MR2S7HcwCz5KcqofHsvb6ewcZ+OeHh893D6NY1uHKWNbTp0/3XCTLPf0rA1m7DwxkrXOl/yKRLPgrVSmuCmPBPnz/mnbrAeGrw+Pu8BVroaCpR+jsr4xU7YXrsipUtR/FMb4qWHWwnwpW8TtYX5Uf8e83ILWHq/gfNFrBgbBjCtsuwJTo4/b1j9eHwg5gXU/5N+yYH28eC1v3uHQ07HA3GQ2LJnHFQbE3htdV85o+wiVnFFnP85GPEW5p3Iw/r2bMSnn6+rjt3Ey9WkhGvA6e7WvAazcAjumGBaJf+nTZrXvc0YWWnBjwBWeEaZTc25PAIqNzysEU5bpRxK9ofOdpIiDisvrS8XNp1rqs9SAp7GeeXeZ1XPabYGHAlx+s8f5bEaAaTCtEwuwQE6fwgqWCPWZVWnGfYArrwyJ8g6Tw0lpVagxFxyzRVqZDM39VlmnQ33uWgUAHxXn4sCzT/uDw6OjZM6Oc/7ws00OV8/q7/bMmmmDmP57BBR7+8OojptsfoKiPDh6kqJNyaKW+PoiXapXGPojF7dep7IM1Kjt8xr9jnY2pmv/NBC4fNrgJtM6zcvbHvC+WYCqXBWv+Hr/zwExW+gEdCayjpMo2j/33kb7aOzhmZQ7aLFDmcWOdRI+cbARHc9ZfzluaCQdrAbEd/ScswTh3bIMg6WrfU5GWhagWm7g4zj2WP04+dLf1UO7gq/GJ9WqN1uHham2/P9gj8MT+88NnD1Jrh/sHewdPj51acxP4edRa16H/2VxNHJGuzfDB/ubhg9SYvZvrcBGxBOjGRZhRv05vJXERRm+ZB/z7VVpH+6qzvlqu8JbtyWm9m6+Q/2QaPVD2twbukPt7Sbkvj/t3IvOPjv5Dy/wV+Vbdp7+iGhigdwN3dG/wQDVw9PTZs8N/MzWQOP8/qztzSHf2w8d351/h0BwOHqQJ9BSs0gKyTB2SXz59sLQn03SVtJepPUjSy2R+sUJ7D8a5XI7H93PMmW+ARYMz8h1+nysqNw6+rXpMR+DtKfz+m/mLC58qkk7zl5hcL5XfX6sxvsmzm7q4+tZf5Koe0+PoNosG3fGz2Om98G/yzU7+grh2JPESjGalQg/N1W97s+qq4tLW61HvBUZwcITt7I1QUEgzxhL7XPaJwwfdP8JM0r/MGkpJqpQyZN9VWtpEOgS/TNgQ6jN3CWqCKYXq5Sx4fyVJnL/4Hns9mm+ObiqhJigjzSbKL1hGKdSBifgujpP7LQ5ZXtPQ4aAxXarvVjK5z/ysPrILS0nF5/JXV9Iqyaqj7S+4TNKkkPQ1kc0KiPZX/+Xp3uDo156xEbXpmMgjqDsSfX78axwi/dUKl1S+FdAvsYve2F1xtQZLJKrM9ra/xAHyAxtZPAjeB5auid1mwdQjAN08hkvRHp8gcgI04lb2PUp1sBbO6oqSpbhPDuyC335iJq7WTiUkBKHTT+lpjGZS7WbD5Xc4aGOG+PuimDfh71An4PNm2glpUo4+C5uuMX6o4e/jS1ekGYwBU6IvpGfrxSvnJQJazOTksJ1BHVwbvGZXeAM7evVlj+V4k57cklQyUjqAgdNkxWK0/aS9paQA5Dz8ne7r/MUZZ9Cb5eUUjEAN6di+qoof6BQcoEiW04b/Z6dgpsy+jNQtVTL6PsoWh0nYhvMgLWTguTXx87RuFEpNWy7jF26L481MnVCPeTEJgwfqWS5HLazH9pKcUBM0eCJZyCiXtiTndFeQSGgKxcwxuwKz/LVGsRT5MLE7eXJWfJkXXBmKA+XELRVPCj4RZsQ5IlDizrn/+T8lcyKD3T1NijB+gNbJMInZEN37anlb5EtPXEsP6PgyrerLGpld+d7Kl3dofEFHxs/klAOsQk7l6YwjQP3KLOl+eTZ8Mmu21JMVluuf/U7xMh4u0/rNIDnfXCQUHDgWucpRFfSZnddlRcfyEGEWOn1FgHGpFqJ8AppVrlZKTiaaSjAQFi1R70n/oO6p+u8oJFdCqwJjY45VfJ/fqeRlJFhqWgcd0/Ltkjpn+ENRX6LiZD5F4d6hOjkpEos3nQb6Hfy/UwSDXhOCpv0y1t/tEqlK4tf+hUgTVaXkl+W3VUkFD0tE3qZWYT95Tgykm6beCh5L0ztaYtJ+7bdZzrXcnWQ3KKRbTzjIrc6o3zml58IuGU371F0XM3LDke0j9R7xHdFa1rqkmv8cK8iRB1cKePXM08UOD15dXBdfmIMefs3sE56aHud9OR8u6zL+nfTGlsbNujj8jvF39VPKUuKjalipSkxEBpPjIjGKce2Ptdc6s7LeFgqXpSAAU7TBRkrPi/hoGi+eNxfk+RXKAnPCUwseCxgXiHCXxp4597jvzvzdtKeX4Ncsf+UaR3K0JcLtSIp2NOg88paW83jxuo+z3J95MGkDElahwPwfNDlE31XLxXy5SKxQtD7ePD1zdmj4QcrO9V/7TpF+z/3f3rbeAynFPA0fvFVspZp7KmBRXD+Hc8jlsgr4MaKI8j9+b+iV5NgQ6eKsH5fJit5vMtdL22xJVS2KRGoKEft0usvGVWuREWveJubb5xab6RmK68EbNpVeBefKcvSr68WvYVfQ+dMfn+bqYYCjVFdzMTjbkkqVpt0ndU2d9Y8mV9lIuTnR/Pqvv+NTh2a2Y3VVzaf9yYk1wGgfXBn9Af9eKnBVIDN3BhXACONAsOynEyYsy8GGmHHrcqFraB/jg5ZkVQglnOMBOnsPPaJlY8gjF1UF3uHoxv/gt+e84kSO5zZBj3OKAt9cJbBn75Dn0y2hcExcYhdbIaPAnTZ9VNdMlXn/xQIhtgfawMl9vIXClhVOm6Sx6cYpLYT9b23emDCpOnEQY0U+ZQY9b2AUzP1YLpj/21W4ExcMo8RAiFbVtZGuRAi4nf3I9v1lLVQ81HNLiFTK1o8QqJ39gFTQnXeOfBp+aTx8cKYQr6Z0GPRAjdNPmHtCmsITiFk4M/CX6Cm4NuOEdCY9j+K8WfClKCWgHB3NWAd1H82TycTg7XdMHy8hKqRwjzLX+d+d8adCcRrsbfGFoNQWxt8hbM2c9yWY3jHnQztnfx4pLFDV0QSYeFCPgBOvCmhX6eHYELR3h2vubi7OFCMaWCSFihBDVrP8trzWuq3Zsv0me12r3/Cr7LV/Mmi9vAQ8zSWw7tFaI353xRz8VuBcn7KrfJa9LxYkS1ALxBZF+w4EqSP8N96yupp4740fIde2TG35bteWx5OMf7q7/awlfL/rcPrwu/FarPhuy1jGdi7z5SVcCpfIz9LT3G0P197Xl8hbjHfJheC2tIOKZjVsJz/aeArb0uEe/Dq7KooxS6hiAj8p/j/2qe180j6i5Y42zSbtDTCbNHjWH+z29/azvecHe/Np9v27izCfdLzr80maJGi2UwHpnY6o+DZKoZ/KeWeWafCXzjKtCdH/fJiDXZtheAhybj9ONMUx8qypqGX9jG+yBOn9w1ZDD3bt+qxCHrRe4MGpqMOnqVRUEMLlqh3vEfsn/vtFIlCg7ucJztEawf7lo6tpB9QcaRC0xFfIX16LW71h1suPn05xYU0JVWj96z/9//Ag/es//U9Hs50j5wzYULBCJUd6/UHw0RWlVUMHuhxztaT49EFWNf0iSQTC08O9pxoudb0E4/q3sHYfwzPYJiTi5aLqaeZ+4tAaCCCckws7kLlhv0Slj8jLp52nMVZIHLwcDEnArI/bMHf1vDmsclM6okNXqOcbGmApHEexo/4kNqqgBInhJOhZroAfvVnPdeZftVleakkp1wFXSJnlJ6AmF77pDbV4CFcpeNrFisW9mhRfqIaWWfHClcbaQWzPQp8jGXcw7Ec32agHgUaUiPqPTcL8thjLI6ZUTEhWdr6QrzBTGdemfRFORhRByZ1L8cG2S1rgri9cHXeu5GtChUXl1Jp7mS0nE5cmcRh6/ZT20nzMQTfQTMm5rWEPCNcN18Otv8bUOvYK30C4K4SBUid4Ws3v/fwo+EAECdrgR84ztpChwpJlrWm3BEsWFxknekrUBdU86/1oUEs2mH0BC67E9jMVM4JWdfMoG8Mmz1wzbXIH9Jkjc/LHcPir4MltcYxaBnvxmIX3Fd4np6+z75cgysAdvs6+m1iiVVsTv0FTTSOXMKikbBFvXnK0mAn15NXTS8g/btMBNsEbdvETOnJ2fCchueZ7oFJP6ZI7DgiciuBBrn5eJcmjzp9SPrCZayY4lAK4sfGcUq+Ev4xfynPCMSO6uVY45AeNs+rhdb79qlsgjTbCE7uowDcdWw4SR3Qh68cMMCjWU0oltUch6SJs0WxUzUu9AUoqY5UETzBxP7MPMH2fik0f0TWdUM2KEVtE89xxmtiOd06QVMK85SrJS88BK+d5TSdJI9lCFu5VDITwthzX5QUXe5COkCdJIFAj1e2LQMZpyHLKpCyPQkVhGQov/vb8w/v+pPysnQiTc92IwwVxIhigIoIE3KOghZIqjoaMf2cRURP0kBHhNzkIxpknBMpZxmnSJ+ypbp/RKey3hNIBg9IUtCJTHQTuLJCivouSXFTe1KDJRx6pgTcvU+yFVuCussT0kEeLog2muVlf6gGdxFHc4Zt5Fqd8WnM+HnhgLlGir5uwKIaOaYtJ2Lo8DyDeeWMbqzHhEZMP4SbQ9WcbsHJv4nl7uUkAiazcG5cdk92QaeNHiSdTxwlKQqR+mJIJnBCYq1BusepyRBRlWLgXSkSBvOtpIq+NbtwpJkLMloEuR8VTTYia94aqjJGcCTt+g5tLZi7eA9ORtoNwIjojp69hhfBeI7kDEiGNCFYGgrmhk9DcN0Se7yRlD1elt6U3zgEs5OqZ3l4CaApYXh9wlj5GFrbtK5k2HxyBh0sErGisFh2EyLZkcOE8H7GUlaxne3gfq0bO/DBRHj7hpV8U8ytDhoPFqBi6gTuTeDU5xA7ygwwZePK6yKaNPn6/nBY1jCpyGofhEvWA/sbRjKxotUbP+F7aF7ipN5jVSflD4VqJRSAWISkqXYgy5p41BF58AZT046s32IxYvZpeuoHFZ3zIgA8p4PeCXPCcHt5H+lGXjx4UnMFw+j+yjZjMyVPnb1pPMvm3MmwEeFsY8FnhzhAaXLnrFL2K1c28xRlHTAgkordvOUMrFbF5qROI1q3I9a7OYflikY9ufKbpOjxWuNMlNnOITvjHgky1DiJkfWGKSRKyN8T+tt93v/W2bbW5ynfSWWFLWLl6LQXqPUq1KZHXRtGVj1nWOAhrEgCBv/pg3IoniVdpF7OkrPMGZEE+IbZ4XvIykAnu+K/XsKgpiOtwBsepRLPO9U/IR1fDaX5/WVATpbMP5xePk+RYlkFtOZ2ADtIAiOu6O7n/N1HZooaNzQ2yeeHTxFrIixbGEHZ8SBdyqMCF589pUhx0H4rz8fhJ0GHishBPkbDlSUPw+/efsu/P3mYgJYtZQ50dOyW+lQ72CJKn+LhcOAv+EjPhs0IbpaCJ1KbL24DLjO51NSJYcEAW6fpE7ES9K81hbUWm1MegxpAr7VRO+HMcJRVITEURz6gfnxPGNyhz2fbhZ+UzOD1COHzpGkRmWrzD3x3+5s3LV8PXb169fTk8ef/+w8XJxZsP789pL6sKMU0w52XKJ9xVwiv/vt/BI6g1tzhA5RXtmkPLUJtQ6nCYDCGEZDCxyEn8wu6cxrCzfOxeOznrjuB0aLKDiGS6lapqF8YlaVKkfV3Xq6+iG4kHaZn8OJKE+BWbkRovPZoCmtpxncjHFpnVUzeGA+3kfDfb29uPnH+DIL3EbhgzuRhHrm41a4Xlefy3lbC1MvLvLifn1ZsutbC1JeKENlKvKu2sDiOKv1kixzbXKk4KH4YQ6xcBTTbVdyqZERB5cPFAKMb0B5olsF1EyUdA0gZ7aRP39ST2RmQqsZvtg//90NNN2lLWEHftoL0JnjC7+y1DwKyaKcR8Fp8nni4D9uxZYMvG75m2II27X9AZA2ltZ95y0TbLBfk1wmw0nWb3GeFJ6FP7IJ1wGBiUMJUJ6TI0Ufiowsnb4fjXEutDDd1nCqbGrIOL/VBTE4mEhZhjN9oXH74kIii+Pd4C5rgoR1pKPrmBvSClPAzkdTeXfraD2+B/aWKU7qqHXazszFxwZ5VrFO2/eEWolzs8o+BawAG0hrM86DEjt7e8RbdFpgKKoicULTC7o72y8Bwk+nfNzXqbA+55PHe1A7sVnoHP19yPZm4vc6S2lFdTrLcENPX0cId7mGUPA8A9G7I2MziKn/rG0xkHaZAJGjcGlhSMctilSMqp69lrFh/Pl7nKqQE7hvMWtxUFJpH7PHSVtVzCBwfaWuPLBLxHpvhkzD1ffB/oaf9kmn+hJj+zgjtLUddyqy/dy7bUXvtdW0aMA71zt1d8W+6hhEYCoVvppFH4ilNccN4EG5Q9hn3qU3/CvK4xW+d+mjcKFX+SMGQks6TdfSnAdRf7x4lN6/DugnhG+51bJlCe9A9BLajBLP8Hv8tvuwDDH27dTTUWTlp8zcZLV00xjAlqaVlGHcS6LQBt6+Z1+9YywN5J+v6LmHB8sNw7cS02GMcd1EnkxpbaA3Y7e3NFgCCEv/ojKlWTGLWuRkvXhs2831bGjqN3on739s3r18PzT2dnHz5eiI+EJjWK8Lt5n+jar4l71npCq2jajrf3H05kcNzfPUIC1YPd54cxgepqIoPB7rPjw6eWpk0m8PMQGVg8zM+GJ0NU2Onrh+DIjlYQFgi2BvQv3mI2FzugNd1QMlzUXH7WZ/ugzyOvgJXhj9LP+jqIWZpG1cTK0w/7haPLBoLigvFz9+/w4j3Di/d0e7d/iXfsHzvBaAchl9svB/jkjhYv3mTSR+ugXnQ2D/rxLEMU+xv61ua0O6knJIFsx/ArGTzFJYBHzAU/sXqZmACaLJzWr5jVNF3DO6CuRKgu9iIqmyzASFCKPJ+X2iUATjKsA8G2YRKq2hFPPpCU1JVjwVPvmio+GM5E6kTN/VGOtNuPULQ/Ihv+Edm1j1rGYKIJuM5vs7ntJoZUGzcVVyBSdVpFileQta2/OtaGBOppf2E3x1n40ku0wHbX1rdwQC4eOXuMZyEv+8dPtzL9z8ETrDvXR7VM4LY142e5yK/ZExLcnXtyIhxhXPTkylgjNyR2FYgJo/fkf4fUM9EZssPqauibQaL7gZ0QcbIdzXP1qR3mJpWfcAd1XTqw0rVHpm9NmOxP3LLg97rDXLgqr+kccD0edUyfFHIGCH+fXsr3tsEuGVJ35U/o90gWD4yesrkJLXw9wJ9m5Z9A+v19QYFK+debMaZar0rnRFI+LPUqe+YMp08vIQ/kbga9eMnZpSp5Bp6oeeqxLvKMQccztDuJVgSG4ie/REpsHYMDcJJZWlQLCmnUBcr6BSW20ar3V3Y3ad6RqHqYeTd41t89BAsv232GKm9wFJl3xyvNu2OwD/cGzrpzz/+ZaKrS+sIaekdo96Ds6zD1drtNPeV86rDy9OOv5aSSm+raVM98y0afMf/5OamQRlGttHKFhhVEuDnQvwiWKmND/cL0NJNfDeTEImPC6AYWGGkl4QKkLSnYi3fwxVP84ubMV93PSBcF4EL+5YsC/MRRs/lUtPjISZtrd6BUhAkdxEwivCtXym2lx5KrHhNRMufsy7+6WuUSy9/uQ53jwVPplAvS8+Dg+d7DOMwHBwd7e3vPDMvf7s/pHK88KL9EIUondU3BFXORjHJwAcvrWaP9u6iUqVG8gQIb9WugqeFPY49IKAPSzJ9bBqe7486Ku2bCSdIG5rycW6bAjtv2i5DJh6Fb+9B7zOv/DM9qvymqDRgDYTd+XyE64fzVh82dz64nJIXm0z2RmT2WmP8rYgzknhssM8PUNJFZ4cRUaHrYQZqf9fjYVUftrWwp2w1xwaByPm886tdlN5DugYLl1KoTi4E8J4+Ws3Q1k7dQm7TL0AS/R/U5HtXL6SUTuAYrQxW2mrEC0xvk0W3CNWtSjU1bvxOquHJmnplK6O91peI7uqfq8OZNSklOdzXsS7jm/hE8cVoLdAO1HR/4GxrnQdv/8t5/WWdAP0pWh7S76wn426tgPH1q8Tj+rMFBIgqA89+kLikJ1cYUpvCzgT1U3/tOe2hbLgIMe7v2Zu0Zp0gBqKAvhKu9Cn03Bk3BytG9aq9TOQMdVI4dXYapXsQ1uysUA7/5dQjQWPyzjB4SQq4MPxP1oTP0Hg95RHjDbabHHE6+BpT9DRiTNxpHOJhjaqOOm8rsGAyObt+FDetUHIORBwQ72WUZP3nlUtctFarTm5FoVspvLeRayeEeXA+CxoYQh4m1YXLNnvhCEM7yeg+AgevbcDqR6HpmhhVVaqiIn0XhEf6UJ0FsYbFS60d+07kh8mzcRhik8Ho5yDTUZGUzxZUQfLxyd3oCKeFiCsDX7ZFTMKwNVqmFz2PAuN5zjkXl6woiqMcZBaqwnrDH43UF1FZBvtbJNCMvROVY8sMkQKF1qRlRS11mMUvI5UCc5aCQFjWsHGeP2QKziFN3TNpdX8aOW15+YHmGzAEHxye9AGu6Y9GDspcVArjQpic6u75TGOHVJVYz2r9y8chTDtk5Z1Qmjflj1Zhc6kxUVJfFFbX2HY9jNaRXVq5lMKJkEcaufTTeieVcMnNscIrt+wBdlp0Q7SHeYwMFYBZcgwfMvU4BR2mJxMyFqqxgs7IOGCDPT+KtSCiBscfEPq1qIeZS5Rko3UYAoa2NwWXx5jdV3jUF4oQXxQPaie19TaPPZ/3BQQY+yP4u+C0PcsX3D4+OBsfHJk+993P2+lzhfPz8bcRg276CdR+9lK4kNgLj/ZEgCAyVODBmJ+IDV42/mhsFZurYbXDdVrjh+Lb+6Rxi/yq//MhksBGpPkXeBfLn/PDq1TnPNgjo/sJz2asCq9Ro7JfjdyqdChkJFAY4jMIAFNasqgkmuS/4WzFXykL/nPDw93Zh0c7ht0gHG9s7WxFE2QAs79tBDaKL2s5ezf5Y3UtmPoZkbfHgASzT5yrn1R1mukRxbmkZNCb+rmuGmEpra4wFMZX/Z4x3gRHRHVgYPKOGByDu99upRVe5StlFpMdSz06j3o7UuzXz5nlGkkBYGOB4hXaCM/E8NQH3m1McbbzzzltFOgBUPUnPt9O+h4OFV9Kyk/9YVad47epRIfVYeSOZUi5BxxPzyqACNc/JxClJugT2toicgvFdbXqMC/NyjB7Ww04SVvUnXot8ASbQ5XJRuEJ32twel1u8P3n36nnG//3DydtPr3qEybV/aJlh1HC8FadxG72ygAz1bnI8MWyTvezTJnnnNqmRynZ6VX3mwn1L/XBZXg/JuRo2IHuHixuY3E2FED9PsoJ0nem5Dh44V18fb+xyDCphxZsWEbrTYwGISs6IppsyjsB7X5Yzw/eBlrz0u5DHCKR6pZeWPK0tc4+DirR0ly8WLAWp9iy4p4+ffLNz+YIIkH2pGtzoPy0Lpt+Q0BPJAKrPBPHspOB6Z1KEZMhjfDVBIEZUXRck2HFv8Q8IYaiWmDLSuuOLtrNpAnYigPsM67fLa9DxNH0reolGsi2uk+VeNTpQGC443j6w+zEtr2+ENmGuGOxcfXHxz/1lYk8dvUcMB4zKGsQF2mZddfwhTcasuEMUjYSrQj00LseOv0HaZKu4oU0I2CulNO8hvnuwu94LTr0euzBGcQa6DW1A3KaMlAE7pi7NulxIDoZkfSG1ETk8Gc7gdnaCSyAX3R7v6d502Hwu50Mac+h+zOYgnXRilS34SkYxCTkp7+7P/+EtXYCSKSDQErrFkjftvRH3qYnWalws0EsjgibkKK3n8Jnq0dYdarLHdIDGlefdmN2Ha0Wovdonp6ZPPOEB8h1QMtrXZ1/AqcY1BQlKGmsem07wPyL0zixSPikIz8vrGVnEmN1NEygElY6mFgyc7byG+y5c7nD1JCxWzsJT+4ADeFmDf4GmEKpCdLp+cuAaX82aZzfL4LmWw8BJBZpHnjGrAlkkpgSrY70+NbmGLpOr5XTpnWQTJYjTe4nmA5i/xdWCOwvEh6HpZfDcBYK/V2yjXQrEfi+bFlPAmeI40Qgl9nAt6J+XRUj+VgZlMHhgLdN46djCemcFR3x70U6hjWeYadCwFsiSkvEHseJWHRLBArlzIZlAiLWCTcdGK0QT3hihgtQ0HbvyanaDZ5IQ9R0b82PxiKIWC6Zz7dKLxXhImyF6EY6v4PRBD8L3exRVu7wf1tWk6NFXOLyW+65I82JUXt3jD4SApJoUPiJHqECO33AgMnvsfCP3gF7wcNwIlFdPqKwg+GIwE/t9E+PZyky/5dox/GhvoplaOUiq60KOUnaCgzf6fNpLfYm0fbW7sX2lgX5EauL2Fr5rAaomrY60qHfqyYREFbiBF3flNdjbl6CP75XySNaV6quYORyECOWaVwoutpT+AS2e7F01Y2IIE1HvMATYpUjZsuof4Qg+LqDpYVjwwCn19hzSpeNUfpvf5ufkiaqGNfNMi6tIhFrM0xvmeyiEDm8m6GjQeNwKzoR/rwphXJC5LufgXI4TAW3c7GfhXruYbnK7RabPpL6f8n8zZiTXXim6KI2TPyIGaZGKLwzojYtcvBDg5DAlJ8IcU3pC3POKlvT7VxeexoCe++nj28bpK1dTCJfgasls2jdVY4DPRqFNpCIZfHQiGjuZ5j/B+57vP0nt0F96g0QDsILQtMXdfPGD81640+V82dw4Fihn+fdaJ09VEdb+Zmea5W6pa7R0wSD7Br3XFyToh02OnbbBQ2PWB2ar+WaHvqGWG7emwzpPit+rvyI/dUmQgFpNaW8cqaNSCzpSbrpOIMyJDeGGjZLfXBBGbSymONLO8EDbmbRZwzAX/1QdJ9f5QdhbR9Wyzq9FDOLrdnoJCZnQCKmJerh0Nj7ms8/Zuxz+DPJhixEjX3K0556QLOD8dU6KfoKdgWAxXGI0K2ZgjS7Fxvgjiy+/z9mnN2gvF5tk/EyzwBtp1EOGA4zbIW1U4C7Ic2t1fIAfLmecgJdUyVaGR1IQ/qDduVWjmIyaTpELT3QySaIlK9aSmsCaR+qkJdRKhwQ5rRoQAeWoS4g5Vkxp2qM0KdwST9epdSzdBbIV/mm0CMrVpxvrULnnsNYzUEZNgwsgHqfwUCRK7o35q/R2cIHfw5533G99yriG3SOpiC1dmgSnmUrwl3lzc1nl9TgeSEIiRP5TEMNkRLbJ7WfoIRo673OULJDEcFBsYGr/q5Xf1bKm8GowWvL+po2Atb9LB2u+UszCI3vo/RVNT9OUk5Ktts/FPXoceCbBO1Lki3fIDVOi5xe0T73Q2DVjEzjfKEORWS9G4HZGoDdZQFK44GxgB0+Uoq2Y4b4zMMVAGOtOis7Jea3W3fwf1UbWM8acU6oHHVHKFsOZSPUgapl8rbf5fYU8Spsd0zfePnQiybuiAcslR960aRKeK8vihXyZvFqcaAd9sRRwkjP5zbe///TmpbfOEZ/azLGdlew0C01+WrO8nKPaIL4PjCDVoMOwBw57OWQA5+JFx16kpJf4YKrbUsNu5I5mWb0i9ISG18tyPCzHHKYhQ4e6klLcqqKio7JirgJHfESOpAlL5DNOYoxy5fO6Z+2v109M8sBMZ+Qaemu1M7SicEEgNo8DsXm6gQm4gZxwbNgrA+P78U3947JhPXGvHGfTnJqOEUf5XXRD5GuJAy/6EbW1f32NrbBcZDJRd1qa1Cnc0vegL+MkGGwQ/Ig9VJHE/DjNueB+oqVImfxWz2tMNEV9RmDoYgcri3YGuzu7z3ZK4aWAl+kr81u/uupfltd9tqD75azvs7mH/f0dEHEYPFp82xteTsBQcu1qK0Tv1L0XzSgHm6OPaVTMhWG/WnUER+QHgmHHMT7r/LExto7IUM6LKfZfZxMESNZSIx+Y9pZaxGLcARCKPJi1uRO2UYIgu2/CKOax1+w7JN0x1pTSKcyxLqdMqZJbongLPes7H85m40KTUgnDwtO0ugA7Z0Jg5HuSUGoByiDhtD50yGl6gy2JlLJo8Ck3Jgk3oSWSb3XQbZGsabDsaah6kUyz4JPv4HUoP819BXE/mTY6cEXC6BlTTNtrxyJMaUl4hblT4HZ2AiJ6C6TCGIV5reaNfNzIK1K/Xu2ifonAs5xi0mRKk4oGd3TS52TJVBpokZIO3osxVbKFpD+42SmG2RcCvA6TCE49UfuXgBPa/h4uFkXsqYf8S2degHZCPDN5VHxU2t/nR2iTi/QCd9hcqRuQk84Ce9FGSnJm5q5ooQxdkwsmteo5g8xuoF2OHhLsQMxAfIcMx6g6kO0cThec0vD6sKctURTXbsI73upi+2yyTyPz1/AgEDLm20flFcI7+tTt4pH+ki1W7IXHzULd6lG/aLrJJtyOlheva2DrRFlA7dtge7/y3ZxxXwjeMY2hsBCjc4m/FJMCuz/yv6gAD1xqv5BCQaMv3UTRFhvt4VXoD/R9c5ptr5xhDfLivgciBRuugxyUZV0VI+YhxksOzFfz5SSvpYPFdvab6g4hk1scTID/e1Ne3zChrgsmGJfZ6qkR8oHVLVD8JpqC42W0gTOOrci9BGGFJdYi5q8YOplPECzO3GmUDiH346PSRLDPmvJE1JhTvKRbejRkDRnTijSqhZ2ztmKnjKUrBoGQltUl3fE78JTyJ8wYTDhO0R2i6Ah7EGOv9kGPXDSq7sS/Ixr5Lcpsm4zgjFPg3Mukeq89Boh84x6sej6prteHXqwrkX6RiBC7RUK0Kk7STDArDScOxC5aQ+T0segwdwyHPQ1uaHq0KYYYCADO+j+RXfesudJtfSL4FrQ/6A43ra4Zjtv8pqasPTfLwKavtupAUxMiIpg/dgjXaLYYjgSE+S//7BBY/Jbps3c9oQ7AbKSo4mkFcMrZHH1G7TNEBpznGqerIimA05savSGxllPPVOQ0SOgq7b0cPkS/vKIslpX8IqIEaRWLJ4fsEF05D6lfNWOFboNIA4fubeesyawFy2V2319U/Wmcq07J3Y3lqOUC5hxU16UgZmLwQmlbBKMCc0EPmYHvIs+cEUMvcpWPihWaVVpKEhiGGHRAcRa3RjGFeD1B7BSkNzjkb/sM+RA1MW63sR2J9hk3hYeMUt5QEH+EfBFz+GQ0IimJ2HN3m2RSX6EufLLAhhEvytn9u9NXsRRmxb8UrtugjuWjBEOVBBXL2ZD/NPNmtfrDqOvya0VCSZPSxuJ9JnL8uOBkARdseU0oQVmE1iYSxFpL0a2zDubIczxECCAKGqPzoSxHkyHnyoejqyFOsHB2wSz9JTSDwkRGbK3ZAJLgQcDwnRWOVsB4Rh7L75FQfMXQzqMbJwhG6bFUuU6ExuOKhA7ymlzdi16m5tlEWUalPVT8UQfYFw9qkshgUkSluGdWgfnwIIDqzDDjlB2zIBVoVBNAo6LiKz/2TPFsRCycIFVHUUIxJ1ACTFr1+ElPqTXZDmIGizZa9Hl22jXxILugMRA4szbR0J5s0BT946vzC5IjDkwxLlSThQzD2uqAY+qjarKcCoi37f6CzRaFP1sE0WY+gv4KXVLGJrmrnHt9yh4aRhU13f+YoQDYPoW1b9noMuWiGCX9mQCd5ZOFLzv0PkjjQW9ImZEoM3oucj4RVkjjK2dt3R0KQxa2VGQTWH+hJRQlGsG78GGVLbgh47J6oiEMB1pBlYl3FCZz066KxlvTQtue+2QbFcaAjxoeMwp9dOS0kplFObRlPe5TVAPW92oBNzldHWwMb3LgEIfXB7mInthMU0aJxZc9Jr4Uu/hkACM+CB2HXgA8inCoI+nlUgQWa7ukMWg5on69zVUIOSB5KnFOLYnL7ojwJjes3TPCYOS8DevsCTFFWSF3+SLsGjOXCmZwuN8CRjBzbZ+5bDTATp/hJCfFF/hOMUOlCOeRwHRws6Zo9NKBxb3bMkUmyGRLHwi3rflMub2lzeJWVixG2+2eGYTi6aa6SwY5qcCb8jCPmpXGVrrnmzWtrMFPHlkLIzwhPapi2mAC0hhC4Z5LqLp0q8GcjaucXEDBjy6sDCWNjCEljcmTJ8C6xr5K2vVgNJI4IPQWHfYtpdxayPPEJeg6/+Fz1ZxzbW9w/VmYaQwnIH9X7NgqhCTrBx4KhINpxsQy+ZpiIQnLuaORRhwSsCmPxc1yejkDj7LJnPimTJX+XcOj96kT3TrPxuqPGAsQZtREYchYqYfT/0FM1qUiQy3ArGkFV9Wv5IaAxDVfwee4tUGKiyWGYKTwIhDty7jOr6ncawwSOMyhbokOx6Vxt17adiPH56jYIjnT1SIrupaKbRA4L2kKDFuRg6kwUtKF9JnGllxstcMdyB4jthuE02SyxT+iHAmHuC1/wxONkd8xgzr1J7ERrMaUOsnY+Hpkg5QxChc/cQhIvL1XV/waxJ+M1TZ4vThoE4Op0tDB5nmmOy+FgJLaJdcVzxZn+xwE2eHdFVnOOKUl3WqMD9wrvjx7jEAg+gsD6cnNkg+fbDq7N5qARSODsrFwmvrTYkrV1BK883ltJr0Q4yomaRf2w8hvQPXlomiNijPCyoOej9KZdhfRYnh37zuq9wybBG3qK555Vfs97v1Ix9L0SSR71MgjDbqx462/W5Psl3nNBeuQ2dozVGWPGo9HxKvsVZEcOuW0DJNoDyqOaYn5pJ0bgXUc5cyWz9YE+YZkKqiLULjvpKrRwFtsvG9x9c0Nc5UwGYitX4sN5w2ECyURPOq4K8aj3puKG8/YmWfvQH5PXPqbrHHUYgmtyCGUQCw+LGRs1koLVP60LEefiU4WAzOBcuVzErZSSSu8y+V1lugY5Y2RMdcM5AklKvyik+I6H90n6i+ZuhTFrNhQ7bPAtFi2pfRxGsO0LXRZHReHHZNaSlrRaafm6azNUNAUprJXG93hd+QtOdRbLpSWlZvQ0iUgAyy7j+M9yTNFTmXxhfTl2JYckEbg3qA3XK1QduGekrLBJ1Da6W8+i2ErFbTcgiyZdo/OG5/3pvkkk9+6IST/sS0eHhM6fJGn1vLR2ml3vh2MJBUIjnfouJdAeT3D5oFdUlGsZtUX5iqcl4sCrw3Igw9SCW9KIU0v1I7bLeRCJKj5iBJgNLUYVsHANS9qlzSm+9eFUUVRFYcGGKoNI+SGTSh5PwPYdaIEtBsZS1VIMwpRu7JQfkOwLnMqP6lmdjSKcrF1ZrOZHOP2v5ZfUKQWbjb7XVLG07l20S3B30QZvtbL6ZwXdXl9TdnZ9fA8PjDo4GYeaTSG9Z0kswgsn1i+jRUV3J1UIAQ2f3ndO87YMxO1MWUtYRREx2nkC+Y6kbKMpYQ3sVrBnW5hWZnnNCdNnfNF4MCtb4zpJLyt7pDeMKHdYKrmvGGBbhT1RsJQ2XZ2XhQss//ln1Uix1isaxhgebkNQnzHYcx2rnU+O3ymdwaH+0fHa/BVGYoFrDpEqNVXDI9wLOZBqCh9wTXtXTm58JAqdpxcJROyFNYeD1fv3k6z3hiinkzKxiXATm7zMdOPfCxuq8mSxj6H41/U60+XQT1jqqxPkl76CCeS0Y0VRqmL57zLRKlR+lbFbAQp44DzfB2LwyUy1IaHLqcA+1pGBBeOwUpPjD5dGbVgiXWJfCawHJxOSzjke6l+WYlzEBhzotK4wxG8v1Cr+6pXmzMKYUBtyqxE8JGGoP7sZOdLzoaLSx+QtcEIbDmi0EeLJuKVrT60yUbx45vM5ki5cIYf+PgJB9PTH8unYegtgcLzGUmTtNzO3iyYto6xwK7yX36EmLkG6WatVZXc1FYvBht+vVyWk0UfzrqPw1qfUgH5uatpsuAMdtcbSRxMSvRwk/FjtWLDdpzIfARHY1mozJNjQXIkNc7MZpuIYbIXg+Z7tmqi1zp0K3pFJSdeF9f4/mTFoUWE63ko10ASvh49511Ssm3ZQqDuLuIragtFhFh3ZYd111rd2t6huDFQCwkIrvQBA16X2zLv6qGbjs7HUeW/7taac+XssY+vv5cg100JtheqGlAwHrmuyewVA3pDUYxaGlMdRqqAiFBCGq8IoJC5i5hdaJpXURt4xPAaGT1l02YfTB7euYEcIaCwi/RNVeyS1C1zqI/5DUiXaUSfvKOPgWfTbkoqj35HQhDDqR7ZHlCkBJ6TrompglkXNkw91KVOnWjRmhCnqzhHaaqXteSoLiTdYmw1/vFXBJzWHrO5g1gI1mFSRuRPvMeYc5jEsDM2D8qarVGQxiwOOu9IRyFS8u4fdGZ6vJTWQiMM1C1ikFMwWjvOrvPqqnfS4JzLknsxKpGN9UENHQurMTewLZ29FDW7paGYscSFMgiC4X7gsGUEZgsdu073a1XC9s9GM1qt4cbsCrxTW6IwAEJwc4xgr9rVdj70jSntoZsyV3aIIPsbMT9MSQESWAv58lyjRFeEJJyB2opWauj9BSWzNUghU9YkR8XYpwdd5k0pdRfZdXmr/afXBmMTQTgfahSzODLafWwhvspag3JHPYNcnsntj2b6cW3Tk3IdyVdtp4hvsoaLmMtW+qVT54M255rY8e30anL/u5rDGqVX09xIwk4R/a9mCq8kpbBKR6mm+F76qY86JORZusJFE4LiVRt/TDhG0epFbJxDi3cWjQ26yN9DOq0q8NIjk1pNY6zx0vByG8Zvxu7Nmf94CF5pVfcIfu4Q+tyvDIWgx++bbEDyHdrp+rOIKyeI3rnioDWq1j6jo9G8wUJnPS4W+LbO73oe1u9R4an9NQleHWVZT77F9qY9l1LGVxftDbKu/El/GI544Qt/3E3zy9qjmodl2VNCm6t8QtWqhJHXPsMx2GBduWjgPMJx7O3SbsqrsGGsamJel4yxJMvHLlYwGrOFnMzyyX1D6V+md9Tg7jn8eFL034JfssWt5+W/30zJzvv08e1WEsBPoiDM8HedT3oVsF0NxJa9NHgVqiNaYLyYTyp9QwkcxbOEixOM+lGAixF81bkwsspBuI9kVTDKqalavwoDrri8go7kUKUe7DOKqX3/6U0w0rnB2+JrikBHF42Dheyp1co4spOCEOpSItKOOiarlopenTluQviRooG1Uvmei5TZ0Wh/LblHFkcuf2crhmrifEYAVwPeMTJUBPkh2Tc3H8Ge4Q9cBsbXrbpBE/MhyXd5vwjIm8ib8zEpyouQiwXHshr7+6UarQwvFkPH4a5SdQwtDhxNVul90mDdBYcSpIIX2cnOPr5OzFht+tgis9GsAPzWMvTPXHIk0hVJugf1RBhJxPoCXubN2QWR3dF6S78JKp0BG2e+rAmLYuDOXM5XoIXaRALUV0ovyum6PtD7z3v7X8mufZgNBtgirtUm8Gh1o6v9p4fPdo8cu/b+n8WtPeji1obXlBhZgmLb0v3CF8umD0Z5oU+0LNtIeK2WO8jbuSOMfhjn9l+09dWzg5hdG96yvwE18VbLgUlT9v5btLmCH4rEizmxKqmNTL2escH1nR7Cqn0AL6E82tjubIywyQrRD9oYm5h9+igUiGD7r0KnfYAdjP9Gt/wflW6ZL9Ox9NdrFuXo832fJpzsWDaAO3FOX8re8pfC1mTpgdK05UcH2JmsyOx4SpTyuSjmbFdSwQyvIL0nAs5cBST75doIhhALcJYwPoa/KMZSmAMDEvk93hc6mTw8Xmdw+5i6jx+xnGNiF7dtOyNqC5RhQkGIge2cqicaMP1HWO+EIUu4APXohqUVVbxTcAhXBERA8hHEqFZOKdG8KCb32+nG3nuH2Iie210cbtpPDRdBqnlcCZGQbrn0Tct1wvF9p5os0apGOkef1dWISRAjNl8GLmkd1/kon3miaypQkn98xO1b+M/mOOAYawiTk/IdMXVOB605nXKK0N0z6nzTom4FC5H8M7jNOhfJLQ4X1ZCPq5uWC3i0HsFf9DfanYmmlXaN4+OmcRfKHtUjFGJPvvxgxY7HTBBKIiYB7KiBkAZtXZUcvolzbNicTtzC5KxWtV3hWb3FunSXqcyVXva35+j+wDUcFUK0ISHb4MkeSZp4+oFmgVY8/SPZUVJ1WkxfbE+rb3bgf5kRAqXqp891DiuRmzZd3LT6DrPbwomD7XaQI3FCoXUcaPmZxklOK30545P6EazD+hpxrDI/0pSgEJZhjpFUpMNtWb3H30XAjtJV8a9kD8nRTU5wfR8q8H+ryW0rqGS443RFTy4bRCMUtGG8thTvbFUXuuiriziBdbOspdwxeeUPtvfWVRi3V1VasQnibKTMR7i4Z7i733EyaovaozPJMCj460LIUNrpv9S8NqHkD4vaMQnsJlWiQchQNm+xe3+umhWN49/HmfdPqfHTTLtAN6k5bUJj6yrtTNMmNMq9ILPYf0NMk2yfFni+s9ENBrcx24E++ISKQCk14VEb0hhLSJRBECKejQ9OHIAIBUFIvMZJMaIzjCsIBackvvTFW+8zz4w0Efkc8bDRRs3AbsVqsbtyXMSDC5eAZASlbrcuLFbOdETM+aSTmVGyv9Detv3twTOn1uifT8N/etBJcC+5hQ7YOn0ujd2e38wlLkAPCR/BIIcf4b0acx240k+/c9j5nT33nYNwbixHXvqrRg6W4teEqXVCgTPYLWGXHxf5REFjIHiny6mY9cXYp3T0CXsdk9ozkxp0fmfgvrPb9R39Bm+CZ8jXNuoVUzs6pfkKwYHNjQkZ6Qi8b+cq09iw0w+PO54/2H4K6kK+1LVL+KWOdmOHXxMQ0c7fh0fPDwdRQORwZUDkaPfZruk1dviz9hrr8iZ+GS2/xWn+6wYi+CH/xn2xsYf0X97zYNdzT/Z4NG36s/w27XXCf5++O8/e57flde7iI8bvbI2SdDkHR8/Y5QwHc5TnY/ATZu7PaANNpDyO05j30j0VQ72kTgjmokYaZfajerjc9nVG37ScbGVYFMd8gGjPSUNaUD6XeW0en26FdfjsyPuEq6xhU16qDUjEMsCFgpN4C4/Tw+PNBiaB/Ez1Dcy/whYwtqAkc5A0NWdUxCY0U045DD+f45qyvc0TlrPii2RfHNGXFts1yo0ljzRUzz+eJXtKGffUPGiJmA+z40RUgDoxxF9q1ABjFt2kaklv8MC2JW17wy48Z1m6Lu+5eoUNstGNpUcjDN7IJrIe6O7FIIXKs3gWxuN7fHp+vvPb8ye2s6ufhdoDaZdg1RH6N/T69lYfvV+A27dnuvmGzWzjSaZJfnqOLdFySPaU7sc+MtU9Nn6Iqyirq8+FbXnPVINxb+XTswu2im7yZqjdcZXEPf2+kZf2i/Ee9zZ21X5G9zHwQw5CP+TA+SGd0w7kD839D/8nZT1fUD4YgU9/4Jp9X4Fw5bL8Ak1nRwqjUe3IPZalMNyJ4e1SptYjJwgzxD3y4qQUqF7OZsQlQY6rFFXQKoK2778/+QEd1IZ7ukq8cq4tiOAfK3E95EE+klvnfT+Xzk+uLhv3ZA+IReULfISXelyQYJ4t59t4NM8LQTSryt+ejlmCKRJOmmdWfCL1miK9qqNYF9W17ebhkdMP8ifhddxScAt34X98d//m/OQdSPlr5oqyWEQSR6PJEOwwNDGGrG3oOsdO6oE6lyuc1AP1OFc4qQeutaP+c/DXdVIPVjiXflLPun1UN87T1T7qgTiRSQ/zYKXzeNDtPh5+TT599xm6jwf7zw8PH+o+HgXu4+HP6D4mnIJfhucYNWtu9aNGY9tb1a6dLndjseal8Rx+nrz4L8IdHRwOVvqj/5ZeD2zCP8K89/ZbS6kPxZfbJwf34fn9HM+pPzvgKkeeMq4M6X9jJWgvHxvmDZkfNajCTd1whZ/39uk44X2s3ZHt+sq4WPuVcrH6K7gWi/7l+mdxD+2VX6mXa7+Sr3/QzZqXgmP90w22CVg/oT/mq74it5ZFlXJ+NRtsNv7KUUnqr1qPGKzfyMH6jRys38jBRhs5WL+Rg/UbOVi/kYP1GznYcCMH6zdywBt5uOcRWQQ30Vb3fUHH9NnsmoHeQaygfrzBbu9hECxAsqzc9r312763ftv31m/73kbbvrd+2/fWb/ve+m3fW7/textu+976bd8TeT8AHd4sL52Wb4YIF3W/QqVXfJmXtQWYlbDj+4Nd3Wo0V8b5fTOUysFIbctfh5KRcp/y4+G3FLcQeiejQOETgj4sCMs/Dz8RRdz0pzl9srersyW58g4RKqq06eBO9S/7MKWT8S16euOgALphwLe0qJaDmY+uGJ62BxbT9zWoTji+1OKv9W045df6Zfhvi8dKfPVuxF91ohAMKaSUAqPLLRxduMmkX/o/78m3JQ7cZS3uDXQhbNdt+JGEZkVk1y5bIeWkIT7KZTH4pfbd1+LAQfDlhVpkK0p07Q8WU9qWfbtRfVOyuiOYDvDy0juKR5j2aYdXbHSl37UbuI8mL28ghhfh5JiliZ5hthJtnbuqGslWhr+i55lPze91f/fbG7kT/du9WXrP9/bD/Y3XI7H9B7BGaIJxsx87ZfzWjvtIx4hPxN7TRP4qem54OPYP3C/wcKzYv/Cc7OuTzA/6U3dOOn6LR+YfaZlv8uaGxkF9UewejPKjo8vDg4PR4f7R/uXR7v7Ty6vx/t5ufpDnrOgsglmEHLuOLPT2RZWV5GAeDNy3+9bi9Z2UxPbVQ23UHvicPcVK9bMzY8ualsvhr41k6+swx26U0H9FJ1f9V5ki8oao+ME9Hn4uQCr71RzKT3GzEj/dyUdE90/no9nRCUlodYhsXKHPOgdnF+NLuHDHqBb2D58Njnb5X2YVB0+NkciJqby+j5ZrT2V3n4ijg68lMmIyC1gEAunrqx3tp1/tbt4fMaxth1kcG2yftbeze7DDjKT9SXVdDbbnrL68skRd92tyued5TdtUPt873D0aPJU1EEXkNWbHy+rZsmbwU2MGr3vhAQowxm7/d0dRXxfY6ol2jU8WjSb8DdnvwSm/42LeLBfHMAR9L6SVCcL66D9ND2UppVbGVqxBdeweskbzuhw5fD8FDYwlITGH8vnh0eDo1xwiaX0+XIADHW1/ezXikZHtRrXMuflAsQ8ZmSz3KsraT8X42G0+kWsnZ8nfrf303dr5Owy7LKr+KK8X3+4fDo44qPQUXeZZcZfbAAD8UZsH+z8eO8dZDGVXGiGFE1LusCd/6PPik7R75kMXZkSMh3HHSjvgnpFb3Fuj7hPPjC2oiL5AFtk/wul+Onj29LB1iWHqE27zGF1c+OCt/yBxcvFOviywjDNDxrK6kD7Cnj1odlvMSmmBauhXPctx8qp3SLEVV11eYMVd39/0ruPq42IdDg522xIPI3Syc8Fawd9P3N8TS4W46VO5d3fE8OUYs5B8pqEkvlzIymDD0wv0V5CFuw9fn/1nz1rrA0sCgmgcrQ48iUzrjsU5hhdkfi7KbFFZgQSHPDni2BF9yDtKWyFtROj6AtNAyWUbPHjZ8F1WLNre1yzacWvRYExqXtdetR/0z6lVO3C3z/HBBu1qdZ0wPTMpvnB+Dtw5V8lyV/OCnv3mLH3O9h6yYIMdWDN6j7+EynUrtnu0176GtiAtXLEL/XMSbIPOi3D+N9LeUIqEMc6NxTdU/c/3kDPgxLlA+tghB7ZMaf6Fj1vWnCubFlZDU0dZo6avwWi8KSZzSrQviVJ/usTuaTbTvZz+pfaDlU3nfjz9iu2AjxMqxBQPhirE2LipLTmAr/CtFxVyV1yyFnF8MqYZM4FnU8eXQU5z2J4bzGo6tNMF173RPdpi/35LjKytuGwZN1EU3l9A7B7v7B7p12gDtpvb1vofHMYbsNte+0OyPg+8Xoc5zUYdlueB901O5GurzGxr6+2BPF9h7R0f7j1da+11PL/LzkNf5/dFjsxb9vNN7Tt42OHx/nFo46GPvNbGG+w+fQb20J9j5OHad1h5Zq+YHImWV4xh0inK/NFnPhb8fF/tQ/nlNJ/T7w6SdiNY3zx3OH2iPMrnao27IeiAi9tNDzmW3bCRD/fzp7/m9BbNUD/GD55FZqLYCxjAcGdSIm99Lv53wh5d1mmJVAxFX9VU3w60J8fgqoYjjKj3fqmV/D+bdcwz0A/IOmUimPQde+bP+Bv73U0v2uBg3UU7Xn/RVkzir3rbnn31bdv92237222j24Yb46Jv3C/FpyHi64Zf1pN+Fnx50/t2tO66bRbF6JzEX+m+taIXm9+142d/zl07fvhVS12Z/faVOVh7ZQ5bh//ol3Fk98koQ3jaTVX3w/1MKQnYq7O6yk7o+w8wwlbrhsOjZxsYYe1n/1xHdKMA2/Hg4Onu32yvv2kDb3tteq0wzvbAGzVY7dbAjVpvbYWP/WvaVwe7X3ehBs/2N71QDOpYLc+ONxJnWCGzl8fouk22hV5x8PQ4uZ6RsPtq6YIDu0jNkFIHiqP8x7+JlP+4IoUAKPGDSJK0t9xud6rOz+R6L0zEUVGsNhCZCvvv4v4p4DMkapoUi0eMr0VM5zouKlv8bkriBfoZhci2s5Mg+eL64MIz+ZGaA6RmaQviuKR4WzUrmLB829aCjMtbJlT4tkftfeGMTYuqR22pi297KCX7+aS8nj3PkL6oqH/de/FNSUcga+qR77JBBWf1Nv2cbi62Aql2BsdHB3tPD48HPSx5X9x82zsCOZjdFOX1zeLb3v4R/INGu6TI2bfEF4n4ZCwzYHLtb3vRH2AGOzwF+A94AXyPm4MXpx/enb19dfEqe/nh9NO7V+8vTi7efHi/lZ19+PHVx9ef3mJbga3s/A1+CxkQs9cfPmbvP7zvn354+erjuVSYvLnCVfzXf/qf3LAGq/jqsiDkEfKeSPYfCyL4axgInmMRIdLraM/Lagp/uSlmDcacXUOSnb/DRMawHH97tOc6i1xV+HK9F3IWYI7YJYQqNFyk+aqkmoFFXmLx//WyHDNjaWLkg8Hx7uC4NTrTq27IG6ZHzyVgcEYmMv48LIdLzeLw2cFgvzWLU1e54bIaxLVADJ52cg0+MWzM1fGQwaYPiV89kxqvjR+1e9T9KG0YjcSyoHqJsH/jcZ+lxy0icu3EfKUWjjiPNj4K2HvGHTEjv7LpElkti9HNjCpFm8Xy6gqPAhZzzv7Ow9cvtAio1WnHYlrix4qEpBY4AkLxMotKmwVEsLAyz+QLMDdg28J42bct9//T+cWHd0xHdk6X+/TD+wuQA1vZxcnvPrz/8O732cWrj+/Os5P3L7NP5/7OO6ndEtrau0Bou5vs8bTIufLLT5I+2RLmDyPMNUtHN+3JVrifjWtPIjklShOPsV7t3rLRMtm+zSi7EybvfXJ6+ur8nF7244e39OK8BvJ2nyhnnzggh7sHu08P/osYKvGGcSqFr/59CpVic95KzZVIfaPdRO+K1otkc+7y+y1X4M6tfLBGLWCRNnV8eH2lPRmNy2V3eAbvySzjVmaV8sXyouLxfo1NQ7/keKykJaQ/bXk2RY1XcxsHOGdbrrDeZaOiN2ZqNvMzaS59pb1JUce6beXfN5w6LGv7OxW65i1lMz++evvm5DtQUOefzs4+fLzQE8rpRVsyK/mvORf7/bGSrOOqS7lDCchmp7XX5tbbVCWJCZ/w1CSqfoc7GOIGkX2xrL12hIlhKhQPHq74uJiUSKV3jYJNLjVSA5XSS2xU1TxU6ioHl9Sl/+xS5ESn44sSg86bkbJS1jhmjabO2WjkmEJTK7Llj0wrveGXhQfZC8zHXlT8UDaI6uV2cU+SPz8VuoBNZsTsjaeOCH71bz7m47Li67X6iy8dZ8Q5NR5Y/e3XWDPK6dHVXyRCbrMs0Y7mWV1dLpF8Gb/XZ2JzKbBlrnauAk0v20ssVvRjm1F/e451uEV/Xo4+Fx2L/moKttXq2b+nrsWrv3N2AxJg9VfOP8Nbr/7Kp49vV3/hBMywavVXfkDre/VXXqG9j8VrhMpe/d1T5OXnBVyzAkFnCLMhjBzjLT+jnHi2Aye3JJpm28/EY1+fhGZOu588laGz1F7k2MxsHGnlPhZGcEXwFuoZae02qqumsVVr3E1ods8gD6zC01YcnrJpcs8Mm/Jk1FygG/S9o74X/iaGEu3sw/lFdvH7s1dshIhd8uZVhyUCor3EkFOxSHEaW6O9LIwZIYDRmbZEXshFk6ZvzvQQY0oKKxxJI6o6ehbeP+m5TbXnkSwlAhI1A4ivnb8cEjbwd0+apmIXaRF1JeiifXNd6Ynt3/cJ4idFlKCCi+Ju1GqDtIaUg4T78d2nN29fZqBtyUk8eQt+4UXXLnDTs2yup3bUOrWOjjj1cNoX9hSD/QRR4Rp7UhP6rk6fvF9stcyru6IGy4eVqByud5/eXrx5++b995/gRT6+Onn5+1BnNsLbAI9uRx7MCREkMRJ6wnwbbiKVBbUIaK/cExMuLjhBjK/YCKDNDKwPU6XdsjgQN+zMC14cY2k5+g8TxbDQLFTCPPtJfokWmDgGMZ5IbbG+AxZy+3Rwa4n105+FC7Kbzy9OvoOFvGitH3YwRNMC+SqnuIXw3xJusvNybNTi7Ww5i2gsW1wSJwO3/Gkt1t3dNkztpriumvuGeFZSntSHGXhg31fn/BVexA+ue1DDR4RvyeHu7tbu7m4mIcIyl3OzpR35/Ho1wl1NDtjpu3OkJEbFfocEOmD0aV34Fv7lMxKTEO8rCF4uENyyJEA4vjJ1U9m4kgpQ51YEd4GtuU31U5WZ+TXGHcA8ub6JQihIH0BH4B9OovG0K4i0QWUBJEXcaVKrvQOEoHMTF9vr62+E4//xCMd5lx2VZGqjw+Vjagt3K1JcXPueOHI1w0622Tbp4Wca5ar6zFQ/RC4lI16W10PGK6JJPIQ7UjTYdtKbV8w/kJ7rJmSyMcVzqrl30LzQnR7Ticn32iyvHEEQvPdlOct9qYfrIJJPtD0Oa+/17cji02q5g02fUVq6yxe2MWxwTx+DYXn5IugySzfadXm9vGdumbhD9/quadaiIUnh7vLVJL9uNRdjTq4S9p8k80/EvXOb19SAjoL1pNouLFFbvFUaVepfUv8Tu7xqFqGyanXpgpe2ml0MinRLPRTbyvpt9mOKQXxu0TLXnm650oJ3qGS0QomPqKxBXETtfFb0KOQm8MpqHpt/Y+zCpBPBsicVN7QJtpmqWrnhI5nFA0NCKHbKZkoEwtrNJthdOm6dFgczFBmTNjATsWQRt4ldEmYz1s6f2PKGA0Uk66lPVUnN1ECqjjHvg/a8Eo344z3dmw6bz+V8SGMO3Y+ZdoVOuvgqdCXte3k62nf35//wli5AyaEZ1/8UVRRo36blCoVrxQ3JheXjCxzqOXymerR1h5rsMR2gceX2rdXenjut030maTh9gtRs3NS3NHzf2pEMTjWuKUhQ1/F6O7L71Uf1hkqHIDwvr2dERYOGvOE2LFlr4JO7OtXncOVruO8zChfg1ePObK0mAQ84gNqzTspiiuIny1GGiSpq4nmzDJ4rlyMUujSPri7oHev1SW25jtVyuhRLbIWAnwn9CrImJ8XVgszR1mHAqDPYQehdrdhGuxTwIldL25dTuvspuZo22hTyvFE1L7UTpHRzLy27IB3YcuabrZaNtsPrnWkHz2in0MYzSR/q4VUXWLJQZPVyUvh0QdJN19h5LhKkwVSqaREGZ9wUG1cRQ6fflVezGzyTRBnbsTE/Fo/gZhW2w3pKLxbjIW2G6EU4vksmokUf5fIF1zRc3g/B+S569BVptMk8UkjwPi9G5dU9/oA/wq967kgix2MKPu4TkT12BHvuAb3g4dqh+An4mlfhF4OZ2O9zT0FCcHIsXwlXaRlR0kgEX4ktudebnNYanPIaRRYO3gQdkvUl0vbVJmyLShzK/I8Yu665tYkS0KJqYsVJ6WOXtceZFRqou7grsbHLJejje22pKuvKxUg120XwczyBqwQXW0rMP/uumtHFsdm1rt66I76EbVtW/SMcgTs2nr/6YGuTo15Was8peWLcDrmx89yoAbBrnR50FnS8NUKxRdfKdri7KhYcXZa5Kp4qudnP1lF+2u0Wma7txXECEpbmfna5b/DlYm+yB7xI0m0Rk30dQoBOm6RQzJ50TYjcU17S719dYFQDhI921CYmetVXqj0ewyXAcBMRCVfkhbcVGrLBkHLZYta4k2n+E7zv+f6T1A79pTco6O8q+9+7my9+cN4LFYdtz5fNjYqJ3Fn+vc5G3NSvkBSLtSXc1kor7G/Qe31Bgn7Y5NjzBjw0Wrwhx6a+2aFvqOXmyHeZW9bxefNPWRBEOSwNcjkKNdevHu1ZSqbRdQJhfo9/umGj5DcXRFMwFlMc2ybyQNvZGScNkeSOf6qOE7aK5nmWFFUdVcuaOlQTZy68bqeXkGzq/TjwcOlsfMxnn7N3yFQN8oFZRSVD+4RkAYfGqML8aoJ8mbAY4yXRRmM4cAbW6FJsDKHP9vucfXqD9nKxWONQ4KQa+P/5WFEDauw0ToZDd7txFbgL8tyUGhgEH00bf7iccStRYUXd4hZd+RWuJ2j3JfUME5PRkH7jBL4j55rzgt1iLakJrHmkTlpCrXRIkNOqARFQjrqEmKMI5oYotkdP7depdSyTDT+1jrstV59urEO1xegS42tITwULIB4n902xLU9NvbTrlMtzwwv8Hva8437rU8Y17B5JRaoztS231nVC1oEkJALbPcamRLC/9Gv1+olxV0Wv9i6SxG4giZFs2QSm9r9a+V0ta4JJBKMl72/aCFj7u3Sw5ivFLDxSAaSKS9M8/+fiHj0OPJPgHd2y5DAOuSmYzzOhNAueeqGAHPIyyiuycGQoMuvFCBTUiiwgKVxwNuAno7wp2jHDfWdgioHgutIGbYbW3vwf1UbWM0ZfbeK+QsWWdk2mANKCQ/9S7rzhMX3j7UMnkrwrStFajaFw5E1OrbTs5Q6+ZF2+eSmrxR1uQF8s2TbwnNrm299/evPSW+eIAmngwLoOYCw0+WnN8pIaHBBiECNI2PAX/MCMvRwygHPxomMvUuhQ+WCq21LDbngiafWK0BMaIsRyWI45TEOGDm608GjX9ZLtOQxHMGW4OpImLJHPGIyk2AWCx1Kbdbl+YpIHZrq0J9Cuxfb+J8XmcSA2TzcwATeQE9JRYU1gfD++qdLVAIHDErTxfbLRcghviHwtceBFP6K2Nq2cJbbCcpG20Z+WJnUKt3xniJwCa3Rcox+xhyqSmB+nORfcT7QUceg2qAoxa9uOUYx4qJGIDdkBnu0Mdnd2n+2UmK1m6dcn4Y//UV31L8vrPlvQ/XJmWMkO+/s7IOIweLT4tje8nICh5HJ/FTZXrHsvGmwXMWZsDkFNEJ3HdsmI/EAw7MbSK9I7f2yMiSRwqN4Og8Cw/62zCWxclRCJjMaaOBgZpockAN5lf2yWO2EbJQiyO9CTmsdes++QdCfUbEKnkDmmp0xiQm1RvIWe9Z0PZ7NxoUmphGERtVuXtpPcVf6eJJRagDJIOK0PHXKa3mBLIqUsGnzKjbCVNrRE8q0uhAClqNWaBsuehqoXyTQLPvkOXgevCIUdWML3yWIPXJEwekaZquDasQhj91Yzh2BLEfTzBEQ0wnDGKMxrNW/k40ZesYAjPlfI5CW2V80pJk2mNKloxH32OVlCA6iSDt7rJQ0mW3jlEWoYZl/gca1iDIlTT4SxVZ1EsWn7e2zCihF77BCUuY4lqJ0WE/Go+Ki0v8+PUK6j9AJ32FypG5CTzgJ70UZK8Kvze4EqaBJvWfhgkhN3LjNlM7uBdjl6SLDjvFi07hDGMjURIw5kO4fTbkXkkD8mpwQKRKIomjo2jre62D6b7NPI/DU8CMRA/+2j8grJ3fvFdL64f6S/ZIu1JDzCYr5cuNXD35jWQRxupz4jtK6BrRNlAbXtk5wvczfxEmOPqMaEkUWI0bnEX4pJATaK/At0NXr6j/1CsrXjXrqJoi022sOr0B/o++Y02145u8I2vfc9ECnTkuSgLOuqGDEPIT2s5tV8Ockx4QbKBizl31R3CMLY4mAC/N+b8voGJmuDCcZltnoKhOmoqGdfoykuhBQRNnDGsRW5lyCsmptC0BogPkqszMonBGOiSD+lQ8j9+KioRfZZU56IGnNiOvilp3oilXpxciJIo5oTItqKnTKWrtoz2SXd8TvwlPInzBhMOE7RHaLoCHugQ0BylYzK0EXT7ubUEektymybjOCMU+Dcy6R6rz2EjLnMYNVzbCOzNvRiXYn0ixif1/WwsqzEq+IkzQSz0nDiQOyiNUROH4sOc8dw2NPghqZHEwbsRvV/IrvuW/SAh1tOXDECV+0IAi0p3eHlb2rK2mOfvxm1TctMnaGmJrS8iOY0JOrQ4Ug6l/zLP7uyEn7L9Nm7nmC4S4wUVTytAE45m6PPqMguMuA4Y0MnZ+ZTAKc3NXpDrllN+5kNigFYJpDQVdp7OXyIfnlFWSwr+UVECdIqFk8O2SG6kjttsDNjMlboNog0oMw5v2ecs5Z+u10AzZTc3ViOuolSRA1zUF2XgluCNGxXC0YF5oIeMjeLEnnmjBhPxtmtWefgKINdQWAY6j4CirO4NYrJ2rRbitgpSG9wyN8WK/kQNdUGt7Ed6dZvrmc35Q0FakjIFyUcE/Lcqja3SSb1FerCJwtsGPGinN2/O30VS2FW/EsOH+USK+JV/ijB0FOBlyLcHATKdubNavWHUdfl14qEEixzY/E+Ezl+XEcmQEgUOrIIrU2kvkSZYyP2zjqYI8/xECGAaGx7XPKhxJ5ZnCsfjq6GVOnr7IJZ+ktoBoWJjHYHdh9AEjwIGL7Uswu908AzkpzlZRF1qsw9ZF0QjAQ7J3UrxZzG44qEDpwnvNCsl3O0r6iuOcOuY9x0LMC+eFCTRAaTIqoFEMU+3SvAfHgQQHUSHWF2zIJUoFFNAI3CBx9uH8WIToosMp4Nh+uB+0Pg87EXEyhKKOYESoDroR8/6WmTOQEsM0d9Cy36PDvtmniQXdAYCJxZm2hoT5ZbuutleHV+QXLEgSnGhWqy4Ke/wWIt/AXH1EfVZDmdsanRdn/BZovCny7umZiPoL9Cl5SxSe4q516fsoeGUUVN9z+WRqLospD2LRtdplwUo6Q/E6CzfMIIqEWJvqDzQRoPegNl7810uzGvLRLShhXS+MpZW3eHwpCF7W1VjkPrL7SE4mK57LEPq2zBDRmX1RMNYTjQCqpMvKMwmZt2H1duqBfdmnOfbMNwGngC8/CYUeijI6eVzCzKoS3rcZ+iGrC+Vwu4ya1DQUrOGN7kwCEOr6/9LjVllFh82WNc/2DxyQBGfBA6Dr0AeBThUEkGgX4tAos1mBz1O2QPSWSh+vU2VyFtT8hTiXNqSVx2R4Q3uWG2t7BoSOOXORvW2RNiirJC7vJF2DXGSCnVn0wzrDzPMIJJ3+VRNcBOn3H9wRf4TjFDpQjnkcB0cLOmaPTSgcW92/IWg5TgwgczLRJwn0klpiw+5mMXo+0nyRUYtFZgdZDTN8h81Kw0tpJg1sC0sgY/eWQtjDAVnjoxbTABaQwhAjDTqk70D1s0jHXGC8nGVU4uoOBHF1aGkkbGkJLG5MkTaNfjpV0PRiOJA0Jv0WHfUsqthTxPXIKu8x8+V805uZ28/izMNIajB4QfKtixVQhJ1g88FAgHks4CaSOZfE2xkITl/CQ9xzgkYFMei5vl9HIGHmWTOfFNmSr9u4ZH71MnWs+zsrvclZ+xgrlZ7PZSjgBl27x7hMijJopMxno+fKMfxIr1hT+lNQnjeKu6mtwDNac+s/A57naQ9Ypa3F+CaYHR2aDnQJBW3RK1jqvlBAHfW9DdFRUnUSlWVW9yUxXuIAhfUh4YySKfU5GlpB7pMw03uXBrh4eQPUa4N8iryWSLf0RpE456G4kD50bC5ncFRbZuyHYyQS3Dpq1j4+uRWVLGwFyqI1RQJF7oqyvpUV/NrkoswMEbx3GcGF+VRhM2zzPdeTQtpuI0NezN4tniBKBDJTsIvILNGbq0pIuu5VsEOc8eK62CYOvJ85IPn2w6uzeak0W7gxK0cJr60wL2/97F83yqm/ZT7S2Gn1nPxfZQV/mKGs0F1hqVcASfB9UfZTjtLqIR8e7e89T0DDsNbeornnlV+z3u/UjHkkhB2FwhE9WIKI3DCXO9/G5N/l/mNRf4Q2bL0VC7PWo8RJGLbFU7yaErmgAQn39FvUxL8idN3wi/M1fgyJZP4AQpiGR2KDULNJP7TtAapbzF9vwWF+TcUDts6eJsS9paxBPrhQvlFTwQuSvsow6dihv+Fy+zEN1LRlwafucpRclRlUAsPiyKbNZKa1b+tCxHn7HJFMVqAn0rbbOsgu3IbWHndZq/w1JE9smYywjyhF7F6AIZgdf56D5RkkmWP4lZMavaZ4Hbk+hkGECXhDVpA5KOi8O+iu9qklGGSbQZtyZnwhQUPOyILvDqL+QtOfpbLuSVygXlIOkSkE2W3cchoOSZIj+z+EL6MijuJo1A1TOYDJN4bhoKlZQNPqfSzojzWQzKd8iYCxJnck8QNeNS4TSfZD5cN4Tkf4XRoqUI5Mh5a7lt7Uw83w4Glwoqx/t4C2riUV6DkRrPIdrc3OkLcxXOy0WB1wbkwQdWgIWpjpSfrsh0kGwRJc5HlDCkqcWwCgaueVG7PDLdvy7YKoqqOFrA6G3hVGpBNLoemqoK7QbLFsygh1FrVynKbwjWZU4VKdXMjkaBL7bObIKTw97+1/ILCt7CzWZXTCp7OtcuuiX4myjp13o5nfOiLq+vKWG7HrHHB4YZrRz4aAzrO0kmFlg+sXwbK1C4O89AoGz+8rp3nLGzJmqD6UWtgug4jXzBHIkFy1jKgVPZPdzpFrwVeR4pjAGaOueLwLHc75dg8YLVd+0lvC34AI0OT5+GdoMppPOGBXpWRByA0bPt7LwoWGb/yz+rRI7hWdcwwPKSKK8c7GznWuezw2d6Z3C4f3S8BnKVoVjAQkREX33F8MR3d0XBIcpocJl7V5ouPKQKJydXyUQxhevAI9i7t9OsN0atJ+AAupzYyW0+zmlfPxa31WRJY5/D8S/q9afLAKE9URsJ+CaVn26sMEpdPOddJqqP0rcqJihIGQec+utYHGmYeCtytBSsX8uI4FoyJn3xBvqqQAZLrMsCDgiyqlCGLeGj7yl+M0gvtM9BYMyJSsOYAyWLKRSem0JYm0YKkUHM/oOyxzWFbMcjaYjquhyxnS9pHK43fUAiB4Oy5YiiIS3miFe2INHmH8WPbzKbNuVaGn7g4yccX09/LJ+G0bgEMM8nKU0ekzht8JgJPNiRAciPEEbXYEs2a1UlN/Uo3lQbkSVKnT6cdR+atT6lYvRzV+Zk8RrsrjeSS5CWoamQslqxCnpnU62H8r+cLQuVeXIsSI6kxpnZBFQJ//WlF+Poe7aQotc6dCv6SScnXhfX+P5kxaFFhOt5KNfgxDU1FUCdd0nJtmULAR7tfEW4JJOSa1U6E8a6a4fxrr1DcWPQFxIjXOkDBlQvt2WedTTfTgfs40DzX3drzbly9tjH199LkOumBNsLVc0Ncg8rmF3z2ysG9IaiGLU0pjqMVBQRAYc0XhGgI3MXMbvQzK8COfCIERlWkcinPM8+mNS8cwM5QkBhF86ZOjiTlDJzqI8pDxzpqPOOPgaeDQ2QevQ7EoIYTvVg94A1JfCcdE1MYcy6sGHqoS6b6kSLlok4XcVpS1PQrFVIdSEZGGOr8Y+/IuC09pjNHepC4A+TUvnyVMpKM+BZfxIj0dg8KGu2RkEaszjovCMdtUnJu3/QmfzxUlprjzBQt4hxT8Fo+63RdF5dJVAanHOJcy9GJbKxPqihY1Fr2fW2pbOXwggND8UkJi6UQagM94OQlTp07Drdr1U53D8b4Gi1hhuzK/COk40CIIRAxwj2ql1tp0jfmGofuilzJYwIEsIRGQTTWxJ+y1OfkfEhhgcyDiJAjZhXXVm9v6BktgZZZcqa5KgY+/SgyxwZ0dgOuy4RN79ZMDYRhPOhxnlA7GkJBCPqSwcVY8mKB6T0eSa3P5r8x7VNT4qvyGqklYpvsoZ10qR073KFidAk8zYNm9jx7Yxrcv8HaVPdKj3pFEJUZlgQoGYKr6QjCdTMqSls8lGHhDxLF71ojlC8auOPNQXW9VPWAOFyDkDeWUe210mO5k9dcIFDkAkFaQy2TTWnJMk8rlL0vOPbmDhazy4Vx2Mb8D+ayEuQQjkxCCJogmPDichKdRWFJBMjm4yn3DcXG6fBwsQh8yLpYdcv2EU1QxO39WMsWFFQ8JOMSOsXrqSNjsVCKu3Fe8odHkqFE55U69MFzwr5CnUBrA/MBDp4TCjxlDd6PgophZEeDLlcFIGgUvquY+3cQ2NSMae3UD+pLnOgfCycz6o5l/HM1CTnutrClXxeWyKi8HHOwPG3V3BMPguLqhGlooqagDrNXCp+eLsuPKGK+LCm0Fadl9V2J5AYoNvhBDggs+8kerEppyV6z2TuOKnpXNBqlnLe1z+dcH5o0hJyT95OLVFpMSGqw6Kr4lqd1WIqRDEQvYQ8mup9bSEiyksOABLZtyvboSyBLWPjzxspu+l6PBd7trCiLAxaxxUF5wT/yJFXVnY7zLqysSCWgqEI90XVOuZ9KHyKd2xXRRk+vJgRh8TqZ/mqJikwiekX0c291wcrbbp7Mk0MC/vu6O5POOGf68QTz27VjiZCfioFY4qCu/ltH37cL3JQy5OqSt0RokD3mBu6Z3oDKLyDhmWnyAvWJzRUhWOa/TouWPZ8p+06MzNcAkRtnqCUHTFzZWPOlWPXTsXXjLIdtNWtmrGWzrIKQuJR/EqFANZYq75a+Xq9OTcAGo6Qx71H5V/ugOBt5230J82k3pPv0IbLnUVcdUGqzInXNX4tPUO6y4uBKA3l9rcPpFsf+l/FWDp0IUHrQBp3oWrVXn17u3u7/d1n/cFhNhg8Pxw8bzU3PuzoN6edxb49Ojz8lczhW3k6tgxD+r0+OKh3c6S1lrbu2ArsSgpy+q1vDOSnSMSGkP0+XJ/K9M57+ryHiBhtTGYbj8F/06ZHPfjqghidMWajU8CGadcgGpn2URrt+R8dBZ+SELUfY58+3Pxh0OVMe7hFY2uHNP1x+Km+hnyKZMtuunQ3hxgFQXLF8CH7A9/WbRj9QvgbzLDHTOs8Kybaw5Bh9vo17OwH613nQ/maaUmHeyEU0VgKKp3fjuEXf2OC/o/KBE0dClPtHlOdAQ98Z8Dv5GtBa8DUOMkmgU/h2r0sUFrDqQHHpUQqNYf32nI9vUwwVKKctsdfAHYUIHmK2vzp/u6Amc0HbQj+SsJrPMmXWA87G+sRxNlI8NIQ8vMZvC4oGuysUu9juAxzuUjb8F1PB0svZ4yiRT/S6YgDwbxaPsJ6oouoheVuXhLLTGDv3XyiV70rf0KjGYMzmANnW8cx4QrpAWX3YFEoRZQxEkVSIh5dQIOWGluQqaXnQJ2T7uH0laOsgX9o0krPoLzG5b0JIlADNkTC8K0IYKrdTzlNlK/yi+Puc1FeYOk0GbNKo1u/6RNVHHnMJ2O/PdnZ9xRS5dYA1BRI0s7ypg8czWyfj0K0rJYBxvC6ojiUUJNl5uJXIo5olO1HI0viWSaps23G63xRjj7fZ29RIKZn8pfjox+kXis43x6SrcLWLjduPUpuBdtZ2U1vaiszlk3SviRQPFdNOotefUskaSKT+J7Y2WQGLOxoAh3jcbvRjJqPTrV3x7haNKk6yo4xGsyNFJI1JVsdoShwbqR8mROqFDSzW+h1Y8e45h3l1Ei+jKnHbzHJg27Y+p/TydMa0QBiyykdIitZuUw5u5Noel4z/IaMfLW47VZ3DBGchmqWEq3oD0S8VMmDHZ9DLUnBEJ7een6O8zowUsxFAHxQGJYxmVCDSCYxS0XuaMhg5n7EjhjiiifQj+lMdj7LP+T38IvlJSZ4x0XV9TCmmaLuJbBFfAGF/Zn23J/r5BkOLnBFlfSTNlWYz5aSEKR1TMjQYLBY00S0KuyFUy9wkvZ/3kAU7lw9jO/3pSKDZYMLDzF21m2QU/N5s/Zdo5GaAg0uF/e8ZO4GGFls3svFrO+deEqyoE3YPb6/EmRSt64NhqupBpPbW5Jhu/Beccd9vCECP8pacAKDSrS9VPhuvVRQieI2I5I4vDCbCVJWjiBe7nIkWwwVVIjiY/mJaF1+gGhod32Stph5ilPtzoSzFkvXC+c6C0msO1CUxI/0HcukaFt9nFLYN49MR+vsJ/AOXWDSsxPqDqzRUOIdcKxa7D24CrUrM3WHl2tgLvM6FNGH2wcPcwZ/PHtFbRRxGLQJJ1xB0TRYJOTdsIR0T3gVZu9uubWltc5YZDquA0/va8hv177JLB3rU57iywTs31qG3UZG58jUO1EmT4AyV1DGmoDk/spz6I5vUpRpNwKlsyYh6XKkTXR+aKnWTl8afOcWXXNRfBHnYZOFtnEHwkRiO1ivpUQ2f1+DNFt/EbsWQILlvA7VTLguxGt0uJ+1N76tfnzWkakRAj325uV6Qwxpaq7rfH5z780wMaFOeXrO0NhwMdmz4Z94RREgMqN30GAQ4r678hxUAWr72YClvsSbNMaImulMk3CbSYJ0Bta1OwEB0Mc51al8X1XXRGeE1KAaJiI2jXFLQF3e6/lJyo6usHcKItT4nEak+xK6kd5NjPuVijEAighZtFHJgj9WvdCZHolOueHFig0NHN2kN3XotfA5cqcc8gJezaXQVa0yUtmhhDzrnw09sLe6gXYI3ofI8pXdLVculYgCJNjZDqd65VNs/JFfam/7aXL03VaqIXAqWvLIfocY65WAZ+7qfdCWDoMrtomjAAQpjcb5Mq1mG5Oy5qCQs+4TjPh2gj6uSQccU2INMzTTTeZO8Noc1fHcGtiOobFuJP6CdbGYX8Nfh08OrFPH9OqSMdxW0tjSdJGiY7lypMDizVt2roI5ytkfOc9INFXKPVqmS4wSpt9lXX0uZmo0osPekmacmXM1EA6wKIg57VNXrSnZie6fobcf1dXcFd37nhe0HiGvt+cBfCi+SpFojJdZzrgDEHUKVZZU0zWEgKkepurZ9v3+sOQJzdrchMjWT5DpDHSCQW7ZtQom18g4s8bQ2qB6VNs5dOB8pf/SZQjkeFneltiYmapWEoJitzv4YAk2GAumej+SBd+tcSeLL3OsWWf3m+pOxS4k4AqFN9DFd1aCVY/do1qvPvCAJSSdAGMquRq1a0npqMi4kRMgPRkpKpZY+EKIj13BJv2I49vOGOKCax7aGwE6RsdUuL7BT0Qq3WbZycnvMs4b83UQlXfJZaYzjPrCmo4k/63YSTrTHBBOajfLNZKIGaBI8YghQ2uk5CayQCO1+1L4oVDVu9ihJgdJ9dVlgxdUHy51WzyjfTix6cVqD60haay1obxEhySgl2K/SFbxyvSAc1ABLtNKWyGtZpomK2VA7QVmNw+Tr7DC9VQ8PejTvy8X2sjgIQZK6H5Ln3Onhqji3EI+OCWcTiiET7BHRpqoJIVMh61DdQ2qFNjwf4mqWgz7RIbV0/GwwI+qhSmII54Np1scA+HsXtVsaoatTIDH3kY8agxaxaxOZCI9V7JEGiDyfoK1tL6auHYp4zvhkQbf+I3QtLY/OYc/O8wdlyqGYOOdMKwcbnVnoMy4ZmH4FizAKvF3ExEIp7fI60yIzlOrAwqdqPrbn53V1TV3ulLHLTV8xS270QQPP1auS/tOIZkPqgmrRkhmSozmjnvCGrKCsQsl02/CE/A9w864tFDOjQmrEbTNJw7b0S73+8AtYkuXSW9A1uC8xCb1LLoh0yQN9ra8LeIcE+kUOH9Yb7mlkId8EgSeN1uxRHzQLN+qdXNCiNd4EoKko0cR0/WcgiovJQRwLlH0porvKXZv1WStz7+2Yh3KEoPygau11JZlOdI1H7YoBODpUhX3Vr7Ka4YJkm6s1mB7/8/Aau093z2MsFrP1mG1Bnt7+8cH+w6vJTP4SrzW7sPwWmk0yN+QWz8Xcuv4+DCN3Pob0iV2Nf6GdPkPjnQhmNse3CnYhnFfPYk+n4s02G3fg90o9Bdh3boHSyLejuDrr/KmdH0+tlh1cK001epojEi6WWvkgpsDpnBtg4PBYI+BbYnq+k2uu8flp2vRXSGdEQCcr+1MiMY1jOdY77XQEDR3UPcJqa6h7G7HHgG3wW3Z+HugRp/+5eBDOFyLhoIjJ+pWUk6TcyKCqvnJRSqcJTZLdiuzcRjbB7eRbjFUv6NjNQ9cwhCN4CCzOCKFSKNzx/DUpmK3f1xR8l9DaxSMJdpmmYyjRmzBXeUQ0GQXLDcRu1vYSkhbHJYxKEI4jPxRfEffeZvm1g6fYIezoep2LHLLP63LG37AFKiS0ZQ9NBK5MVPL+UvMKPPbk98lD1jr0toIOZNSEq+nCZZXWu1SUASPdlBq4WafE8cLR7LmSWOpGpQEpG4WW3BCJKpOcp7YQlajPJp7UPVEqXtVFGNludEZanSKjpacQjbaL6UroTJohGQscaI9iiY46DcW1VCRa/h2VAPuA2dmQ2a0D7wtRGWLUrA/r+6ojioALt4Vl7ZePIaWoaD+YpHYLpVFgpVbwobZSN8Rp5Wvr3yyIg4POrkANx4p2QPnxmW5glPXkg2ysp1nsEXwEezxemtNN5mj05RvWxAhgZwHSspZk65zVVtPCjIklMq/9iVVksFAZX+rZanTapxPOsY3/huTxCUXS1KIyRiwGpahjRIsZqovAvys+DIva7Gdy2nhw6z2E4yhV2PbrIRmZNokECAC07Mgi+sO+INroxxWRDcxOmZ/e8CXvarvDO2pGclDmMqFBicsDZy+ArEUmw/Y7kHLVxLiwgdTUHpH2uidvnyfXMAOJM0q3E6rwyyGUAnX6NhrUbNih8iMaNNTaMgVgB1nymjWtA3Q2e8aK4SB+dz1ax/qoe9RT3KUtmNEOTEi0jF7dEU1cb06i+Y618tT8GLwRg6HHG8Si2G9PrHeoIJ27EF10SfOo3QKPUDf8KExERNaQ1ItbtywwscBrq4scXpnyqz7AVuJ7HFC62/RHJw4xl82WIctMpUztWBvX/Pq0OfJjWgFwE+lOifI85DatGhmqnTFJ38sTvM5HLh8+z//p770ydEYWsQk0ehMVpjPAaiA7AMjabhHAVZmErqbyWVAoJMkRQH1UzXroFkKxhVZE1paJGl4YGwSRtgevUHYP1gqMimQySX9q3YZxuK7Is8S8lhtokSSMFqdh+VeTkEpvyspu8Pdqa3K8RahqJlkPMGeg7RBZ/s6fVdVC7AQ8nl20G1aYds8wm07OlNZhVZGPdJytkHAFR0SlCrRiaE8IhbiSb5Ijf0R0Se6FrWeZWiNIYQzZVsF5KbrDMe5akPhcFUsEiay5FXHlfZysJbKZnv4x3+gOIzYkXxT0QhD3orYknypqGNuv8BdMalJs+vRlI8wVFeNxdflBizYm4XhUJQF1dAAypNEVc1e2xIQ+ovQEsFuj/gnr0wCe0a7vmC5ifWcLKaqLQP4SeI6uO+Zk0G8m3a80AFLTmGTR0Yvx7FzdIqjtPvqd9kidU2FRrOwFy4zTiL3CB7qrUywb+SvaOf1zjoFOZeed8i3IFQlckmQlTtCRFNnc2tvg1mgxOlELoD3h41+3cSWJb8OziBIV8HbK2gCO9XPiJZSOAl8KATj8h7zPG9ZMcEDsU79CvPRCJRzBRGk1Ck1FUKEct9d/kThLO/SblgblJdSMNTZjWp5vXFBIVl5hypQP07rGJbdBWjQ7NPF6dopRFxdnnfLtZTdOO40C+uDI73rR3bWEFPRbjq+Nn+0UTJCb5TYoMfIxqp2F3Z2HbEZaLM/CuTIlRFK6JkiSTooJhJbFxhcSXFRBbqDIx0UK7rCZiu529GVbE3tJ2vojBGnWCjGgecpC2Lm7qIb6xj6Ex7b2kdh1DtqSFtJ2zEXSwgyDgLxpPZT4sv7TjOrHsdcpF4uSNyO3jWXOhff/pP8QMT8T8rRZxdCDEJ9OXGrCxt5My82sLg9CGQOE1/ohVfsjsRAtEmqemOmDfpWRi1QQz5J16t888dLS/bw+fyKzFeq8pj6J3FidqP9JDtyUl1iSSXeBjVCYi6WcNmFyYbJ0J1tn7Y3Ps0Mvw6FA21sSEMD58ViOXd8a7SQFBmpZkn1Qpc1kNjGWNhvRwI3AA25PRNFovaqmEMrjKCWNR81lqg42MANaZRw1wVQhWifa18X3qFJvljLIk56Eq75Oz0gAobBMOjNscwhvzFE2iMRwK8sCUBqHi17bFWxHfeZ2wyQHm6245AyMR5mnm3HlDAG23AUSCU54yki1tEOY7iBs1eO4um46jwDsKfoApwF8Hexk7AyVyho2+zqquVc4Qea6CZ52kGHRyIGDYO0xRTbd3V4FQHCP3WVWlnejQ5AV2Qp2NCum4s5MH/oXiHela+V7XKNHi2KfTKs1JjaaG4dEF1HK0nWKc/G9r9rx4PloanquUTBHBXUJQ1mS+esJSbIUCZ9t2UDJJlUyHJxayKzzd1hCnkO2O0uuS4mlryBwNQJGstNL/Sh7DNiBKDIEfaZoe/mc01dkYvIsIttNWzKRV1oCLnlkzlUFLMVZt6Z6mVHf7GDl9vFGtcEM9oRZwtFBaN/PsIFRLF/qUAyXASXauCo+pYyMIimmwox1sYFcZazS7vBVKqtpRuJM9vNeV8vDEjYuAIFUvujuHStkxt3hYyhSmFeK7lvLN7ukFTAXDYONz9EfKWuaWj0Pid2TU28jd2eaQzTQUU4YBvgjj1sX8IsvkKsw9xpq0YrjlC5sFhPBjTDWK36FhLYlJpKp5nm+T3pvOtqkak633gvzOE1mdc7ZGICezpbzu7gWo+tMtAD+BWnym7/pGr8Wmt+O1jzWkr2HCKwtb0c63PTya/zNODaXF2zB0SWzAHJIKLbomF1uVoz/zWPifcbFLawkuurotj2CYL2u+n2lrW3OmmLWYM97DWjo02a26SUfJDCSH7djM3cQFX2BlWB7e4bEyDaEDPSPjF8CP2N1bCAM+9JuZlEbak0ya3E6ma3NaFZ5Az4NVT0AFmDLL9SRVs/cB4KzpS1N1g3yWV1JVKUvyCa759MMi+wMVZz4XCnPe773Q5UYfxspVn4RkVb2OvMVQmqxBPIOwcoXV2siu+1jwlSnImlbqmsjghWQvuJ/vXRJx9181i4LevIbjjbhLJJ/RL710uyBzVGdCDyMPLDshcPictcIQOeOFFRvrB7LhFO49pjmXz6XiIEzsTfeAns4VsFc0npbLqWlPF4bn/lQ8twgiwTdVftBe9t+ClmnBTUiN0Z70qKk7u0TFCAgbaM+m28St2w+XaqR8DjUoquXaAkPrAGt9UuSue0jfIZCBu2O1OE3gEJAUIYRnC5C6qHLSbtzjJhuil0OP3y4yNBlUpYWq4pJfwldo1HAteS29OFGNLoeNmuAmtCSfZwUuxTWMhlJMvHiOhA6fmSOHl7qQL772oJNLbxJq6RBSmBrr4HPG5HyRNLopjmfsHdJbC5qQt1uSWhMFUHoS9d9VXOCWarSwfYaxgVlqdQa23di/cPW5PeMhtFpfNyBqIHnjFVte12UkwDB1XxKowWNP4ZjWM65AXyqAY/0/BRpzg1A6mX3ot4Jz6SzVTIRfK+hxAcSSTL10JXE3CHQboXzbzA0iJpP6uuM9ffpKnF23gEJV/3Ls5djbXDJG7YMrcRE4Ij4mlbdPRBSgbrONLnmEUZ0xmRvfDSv7EM+NRiDilMNY3FbcbgfQnKjiGMjQ6OrJxgrvlG86uwo3F3U9Fhp0wZSrISZesijdvtiH4oITzLIWm/A09wwioZuQhAPwL0inPnHj1mGjkErUOYvzYH1YTUJylUuj6Pcpr+5vATvY/X+YBEkZ2M2FBtX18E/NyzsYXcZpL4ATncEOsPhc9Ag2JgR0M25kJyNRoSrI3atvlGkL9Lovgtap8pBivB9LvvPjDaGccrgtzrQC9sXCjI/WBWgUk6u8a4KjJ9rLH6A76fdqCFmP3Jbe7SP6F1lFOegq5ZqtmQdOusEDiwyKNGfV1wYE+suLipmiIcMpiSiDDZtJuq+txYEoYw0MZ1tKhhKGtFIC3ygBO3bQUboWbk9PgwDNrjzjXPqhHRtfG9YMSbspXgY7PGc2fkGwzITTrGwdC2b5armVFhj5wiiZl3PcMkPjm3n0LQbMH/OABNgN+ol3GHOp26EHMRVTUFYsncwA7WyD4X2IgegxNlZkTO0+XhA+gTDmtAOso7Kmk36f08izE6yXO7ZWsT6vL6ZhGk4ETsr9eOieRy2tE2FwWDxIG1ZAPeHe/c5k5UTxNuTHFdMaTGZBMk5a6yVRAwnZVNwVuFV7EdQZEwJzn7rHXoC3RGkc8hMJMf+LRofAO6RO1reS0Yjmpyupu82MxnD6QNGiV1WZ/KpQtbbQQk5hs9or/BI5BxWADNLk/gRF4+uUMKIIf10C93SL8VjSZU/NPYpKALg4j4Y36bC7Z5NdIhVCOGd0fsNe+++EOyqBZkdgpbcPlT5+y7WKUCjgpTX+AVrbjyb4XadVTAzdLqaGvuvtQmOentM48x6fbc0zS6NjoqsOyt7RIUEQ1WG9EjbTdMF1NpC+42iKANxBQRLF2yFntv++jPqsXeO4pqsY/X1mI/e7Z35Cqx5fk/TyX2qlrFv9Vj/1z12Ht4Rv43LdDkNhN7nhBgms87mkzs+brbd/SlsMVEa4xkue0+XFGtt9WIBRJAUHM0JIfyDefxlYXOEZ+3DTMel7fsrn3byydUKYT/f1/CTD1KT8hHvRfmt/T2Jz8hOxn9E0TdLa3KN3l2UxdX3/Z2/g5V8LAcf/v06f7BYPBfEGWDrh5nUfqLG7iL1zfwv0Xfvmafw23axfjb3vByks8+93Czvu3NKmwfh/V6Vc11NnXvhQzq8GPmlb/ZyV8I7O4e4XFS7vaocd67kkBRkxfwGP1xwdnA1sISvUjWJO8dHh9rTfJuF+gCBqLQqkOovMvrzzD/s2r+ad64vq0/np2si/XRoK+4XRgGIrSm8GUpTGCv2ep2nFedkBp7ioU8MkGlOuWJlr5l2BZeHY40jRbU9YC/LQEWndA9nOfH5KyzJWaTLHBsp+VPLgHFd/pJYo7ncyySvwJHHPT7Zy4LGC3J6IQD53ZLfBd4AI1eYFf1q+rHcgbSL2FVOIq4ZoHe4wshbGq+2ZE/BKw3b6uRzhR5jSxShPwMem/si+vk0VhQlAFqA4Sey3SZIILE1eE/dDLw4h0z+fQGsUIoQ6eF8rDeFJO5I4xC+/Fq6YOwueNkcB2fZT95zziIjVOuWRCaLHKCFhZFFFwH4SXkczGmRuzZK464V654S/GQjzFQ08jX+SwwElR7DdLD8JKl9j8qyVPDZ6yHnV3MrQwEntDS+IJesv0FAEAvPsPDmv8x/yJ/jd+umoALxC1Zw8gH+mFWNunj+0z13U2VveeJwjY6animJSFRzoQqa8QxApINJigY7GPC/WY+L2YwVa9iUV1fM2TLP+jTV51E73kyahhUx3I6V/AO7cMbDuXn3BEPRQBWGMSqVdoddb4ZRsSQmBf+gAUS46KYs6CmYCtZsjg+5xgSUcuPiD4msvHiJr8tJdOGizPkxRnq4vjIPwZcphT4e3yDnRrLhbNYJuLG4VysHsRgGD2BWmTchUf5HUbmC3SLZgvpOVFjcoNopDDDgM+aIhN7eZX1PsFev7vPVOr0NGbBhRqVLx5Jpxc8sTFdrMH20+dhvbm7RC/jS4RiXcA0txy6klM/xFM/1FM/rPNxCTIYW0m6pdpyRSFCf48PF9/Y/TK5xkZEckz3MPs8jQmk2X688jHe0kMbYIJbXF/PmWTreGUTFd5dNNri4IJKH08KbrKKQnRSqg3wGiQBHV9MABXCA899znA2f2zAmqk+l0XMjEzP+JGl8I9nXME3EXsWu+dQKMBFaW7yGufuOpK4siUXb6UCwi0p3Qp3BqwK3PQhs6mbAh3KszlO3vN/eBuZqjTHc0YXyCDMd4PgGfael4vK4Z4Rc4i5Qs7NTbjmCZ+5nV3cYKdxEvW0WMX4Wi68NBNGzdJIDk4SaxXB8+HmIHTIaaGUEE3Hb+8Y4S+yzjPUs3xL8f23mj17OQj/oMuuisqq6w4zqjUaDeBj/iLIbkoM/v4KdnIuLX7dU4PhPrBVBEP6Wf32PGgfZPgdn3YBzf2Pk26WLd4OhmvhxT+ZUx6T02mRHJ5QrbvjuvrUU9t0cfrQbnO5ZdBasmAus4KVFFJSL998BZOe5xCDFjy9Iy4m+/ZmbM0V+/iqawsNd6trSuGEn6mxq8ladlUg7rt50BT6q4amGjO2+HCq4GeU+cvv0AB6d48CQBlbj7ePusaPyesTzxNqHF9wzsRg8lXs2XyJhgfbXAjLFJxGchs6Anyh7eej1kuHM3EVG1xcrVVOARG7fVKLQN43MA8hYHp24E6DWNqZYBaH5JwMyr9+txzdqEHeGiIyUTnZM+bCeOPORfMIWNVVFFmJETH4Sut0Vx2Oj4jvqnE2Aipu5BmfM89RkgEi9fjUQN0cEhwZiJ0d3ozjboHz8rzzNy154YRUF99KS0izE8OvxlZK6PSGckOu0Yo1ZR2KlSUYgJuhWmymxPYWynGpsittEIxqrJIv2pmyvAPXU5g5Ckr2CBlBo+psVS1NmPbmSxSZgXBw80l17aiG7rgDu+XsmuafC3++pT0fn0RflYCxmSQiLwDnq1E6Z9OtFcQgUamFyM6WQ4pkgunCeOj3En+FiD1CKhc11ahiIvQ3Fxdn52vmIcCZHpeRgaDza0KQw5RB2fNd1xsKTJyfv0UZICAlvoosFeHdiArKJm9676vsDbEqvsZWfb0NjfljvlhL7CSMeU0evfxJJMbZ+++dYz7aBI/MHY24lpxizVzxb9K7rlcwknwzg85NeU1cmHRCqEEDnO8P75kqhsficcPnvyxgl0fcIBh8xv5yRkEJV58ZfvsHUFwVBo+mlA8ly5K4bnwApAnSixQLusojZ/jvZ1Tejm/83N/HqKSKrEVcXDhkePkG8OfJxCVhJPFCVr+kpemKbme/qe7Qv9yCrc0ofCDd4CosL8AmV5yNhWPJa0783vhEbeNM41haoi3+uns+N2GgUC7230zKipb+ZFkxqbjX5UMExEfwR0YMw+h9JAqungbKRS6oRJiVI1iTT0kx3dmoJQW7fJd7IEikDc9/+B75Jow56eMDomLECQd5zLEsA2jD7xIWbAaS5dUXOJE1zHgw6HiYS0RqAgIjXcrhUM6cXtCaAwpI0bHU5heRz2MNAnNq07rYXHUWSGkvVkyyx3z3RNo2LOSeJOWiMwlZ1OI3kzMIxSG+e2TBBDVAHIxgKL8A9CPWiEBYuwhHygDs4gScBXRwyHrvJCmmc9H5E6yDBCRMJEEwALBtkUpJY2xsy/EAK+8YY5USNn69NeMpsQchMkrfqyOfhXYGw7S02U8ZQZd53Bshx6ezRRWXptKI1LIY2xIiaBZ1USza2K+WRcRkTYrd5hOmK22tftJBbl8VhUGtKUJfZa1R5i7dpzcSnfrXf/p/MfA1vXcP+Nd/+l+oz4ktAJMOehXxaKLzFJQdSUQmoe+EV0jt7uCF+hjaw3AE5+MxXqy8g2bfeieEMZYf9eRUJg5yJxtewtbwR5tX306LrjH29fAE+HzHzAwTb5rwOH7l31tofpbSP3mdnxzb90weFBQOdXUuRcSESkLp8xNJEsaaXq9hl2CglASnKFrmup/pnNNeBp9yk69lGwJPAsrrAPVMiH5feeoHTB7oz9gmBs0kWsP4tbjokXyzPoa1clDoHWyiRjc813QY/BncBFyb0/NzJcdJHrOWF+TGNblYwmey6sdoqwvGRGHr51H6wTZ39pbW86DMoxkh9hD2B3OxoHB6W6hu6vvFjZiPPRzpp0pYdthD6GUCOYAvL0ZhgFyTQ/IMEUVkwqLBkfiyRtPQHNGfUYY1VQt0IsclCGaSSRNpOI18mHOlLVH4TFKEOkzFusSXGAYqpziY3XGgXhODU7g7kYFR4jxx6FhpJs4lE7IXsxFbPQon4FwWHMu4x5B2eMQzgmeaNsiFO5Vcg8TN48uCLdk7x5QmpsQe/jyR7tDBw0CgU68csvsm29s+2s4ea8u/I0ro5ChnyulyqqaN6/cuaIN7cJ5SecKor5jTtCr98Bj23pZTIYL+cHUF66Oi3AUSN3TCDo2RpLutzqAlYGIw+k1ua1UivZ+83usaZDpoQoLh1fcI9EEORPeHQQ0b3EbQI5gRt6XL5KdiZwm+xw1mhaWumoS2Gi62drbEmXQNmwvTpqCD4V7mtZ5/8JztoK6m7oGxVsyo1V4gzciSJIeM4vuOkoxZfeBHYhM2q4xCdxK5lcJjimBvCfzkV74AgPpAPrGLlRgsEWIPrzzc9+wxaJf7/qLq4/+GCe0niTGNCDEoWGb7c+F+ssRRGIz47y3aassXD+dpD9bJe2gkalhJkuev9IXoy+Wct8Hab2LwIf3JK4yZFNKp44oK8WCxl52BPjDUMZRntt+WuDrF2M/elaDEmupqYXRmWlfnKe1BDQMb55hcokhkNsNpF0Wdi6+0RTRe26D2PA7KgSMn39QaI2p6m36QxWLYg9KPVRQLq4bQFw5mmjR84ly3xi+0SE6KCMUJ3BLlx1n/ZJv4rqiDMX3UniKOzgV2Itau85PSZdInRZ7uyxfrdjZXw35W58vplI4102mhBJLwHBr/aO8RP9FyFrV2OQ1cJbArw1QFQkzCozkHY5v1kA2lYlCBG7lwPeKW6BVQUuOqaGaPOPwUvRy9jSHhmcNymMs0jprVGPZjnm8E92ivHKo1UFcNtSBF3Y+PAf/MIq/QOXO1eFf6gceDeO8fp8UEA/juwak1YvwgUHo2aES3RurgokOIGddigoGK/HqGxUVuG0zdUCv7EXJU1hXJGHQSPL0FRy8LV5GnmlRj31QKigKGfDPGnyMmkOoBSEihTG9g6tO8K7aVkOHICAhDeb5cdNjqsYsmw+NW9ebtQtVKRY14bpjK4rfCU+/7aZMMiPqsJR7CnF++/SkDFuIQXRDuuAoCL1l840k9TeHEVmOWnqwH0IYM4twgtMh9284Un/SGeLNAFDx3ICUUlPfVUjT1spw4Z0JL1XKaJLnecywmRMiy+HTtRayuGC9K5oGS0IH2ZtABGu9PnCiE71oYbyrIRiATCdVLY2NRLmsaBnvWnrLJvTABhdR3GkkqqUxaJVYiPMpdZQtDHyMLsVZzcoZS/iARoCerJ+YCGpMQJclRLW4SzzA/FRLn+RVMPykDOgLZxqDkXFMsCNCucJiUahY6MFI1wXZlHHLgB/Si0FMvOi107+fJrlo8gIOpqqHEcj6fULUCEhDlAhAhlBEmpiZcIS2iZTs7ZZSXVjBpIJOqta08F8mcjGS63jwSvaIFKnHJF8VkUjIOEl1zIlZBuy4dSVrcg3maEYRaciokLJJaFpMCWkkPO7ylETQ8ShgB6CPoWksTGV3mepvS/nAwITF2vMvGLFB8k5gdDQtivYmP97f3d7e/PEmdsQ4to/H+66JyB1nd876m1BCIR+vaGet+LEleuNx3KCipNnKLtLiE9BwJMF/R8/O30QXz84ktmL40nkSNy3/BGatuurwPf9QVUI6fAjvdlMZjExrqxxhM6VNGb/zE1VBys2tdLGl40UIzdA3+L/9MroPq1Ln2lsCyj7ntoO2ziU1qZKomZSYIfz4toS3eV0VIKOVtWsBKaBAPvFZbhsnE4OCvls0kjsHT56iFY7LD+/Yrxfu/eZlyrjvoYta6eUgFcfwg91DDLfuIqYt/R+X75tK5rn/4gWney4MEV18DZ8l6y46itEMsStv9mhahh/B/s8Hu8/3j54P9sCztcHddWdr+4dHx4b6pS9v9WXuEJsp5/laR9nNVpB3sPgsq0v5WnPNLKs7xNXMo3foc/knXzB34mrkT+Vq7ai4YJd2kEo7DR8RTSahJawFMOT0nejSAxK1cXMxGw3bpbpW7g13XrfLpilaN5KXhal3XNgtFU9rONDzHQHE3gQBrVF5p3TN9hDvNr0AMVTJSewO5Vtnm9EZKSJjmsFnlwl4WizvEfPLTmHjne6aao+a18JIJk9XVUE8E0vO41ft2K9H4dkuLup+0eJpcV1HPzQPLiVaoXzG2jpSKgqMSfgoSyWhVh3cRMISmAPKEc3mH3SHH+XJiF5ZMStwtpq2sKfQt6GYKzjNAuKsJ09ME1EYNm6t8AbefS9cpB1GNyGwdqzjxxwW9NMMQKSTkftERXr+uUP9pAONY4Sue6E1bTsYY4cIVdm0J0qe7moWldZj1z25Abtn9eOAjdY/o7tmHXRFycdbX++Nv/HorMHdFbSi+qaBWivQIxVWMXUVH+yWltoJvLFHd9wOq+694T8ZU+sdp88zobSUo3H3IwtxP1wRmPsBtpKR5WOsNEjZ0zGWjFHohMdUjJ6k7Jtx5KwJMlu9h4faH3kNdEVnPmO0/S0hvu6YWO+JONjNbcpmlOcxy1as6wHu5U3NL9W91wSSl7gmq4mmdYYno5KBYI6iuelh895GD69oAr6LZezHdFT/c5u+QhepGYwGqz7d8M+6NKWNGORJsfypR6qgzZva43C62WwuyJuZEvPWSwgjeNWAFil40cuvMQej4gT0qymvNUYtQNAV9VIPSg9gbjkDodup8Bihg2Tdpga72GU8TgQwleu3onoES6Xj7ILWyrHwiqfDddyz8N5R/zE6TNza8ExD1ErG9OW0iMKngLvgxon4R0dWwKNQiGf/TDZaXJV90f3kXKQojS8+sywzLHo/jO7JBQ5KCb38HVQVL7uT27a8VqLQ8MyIHYpMdTTMMdmeP3pwPzz5+eP3m7avh2cn3rx65wpd5Z5wjVBQ53wYwPoTXu60UOMCgPLnCl2+F0lsMpiY65m38WLaVVz1Ynru55tMqQwSo9ydwDiciTl0VXxMlwdYEtQ2VJp4VzmGpl5irTAkbM7hMEPMFqlw3ZJrx8ZJq08iAbukVYXDPfl8h8y1clWm5nK46ZAme0O+4Vq9NF2ZUNm0zaDCwIpTqCtZMaKpOAkcp/diNznY5u0KLsqDGXUpir9AzSiTOb/s4K6Lu6RO9UojTlfUXWi1HZWas/FE+V6GAi5cHvs4fq8sMIQ+t8OWZcCmgEtfJ9M6LhEvVU1C8oAHF7/cTUIp30ZFCy8lxjvQRHPsCIVlqSnHGDXpWRyKto4NzAnsJ7IPR53tHoswuAIfN0WiYFGyqzrJ9+c4Gj+LXiR/WpYBzTfJRv3Rrl6LFvJzkggudLLFbWICj6BoJsb/eoRGh7sctrVuTPKph6LWV7OSTr5htWUKEYUtZnOxQawW4oMMVVN5iR9NRPpFUh0JATDPAHIs1qFbDsg+tNke8dmsM2Z8c+FJ4dtPxirJIPyMR/r5yJiRO20XVWXMKXWSrvaoSJ/bQeEW5yTlhDFPBO7cQ1Guk+tybn4FHTQKKQk94r5sbASQo3bgwiTftx8Qqh3nVpRRKDhMzFhJTFJrUr9f2lX+6imdvrTPO3ALiAzYdLiKRLcVr0oRfqak5B58vMgkso2M03S5E319ouhJHwPyKag30iAXEFLbmCGf21fNCx5155Fk10BFhQL+UwYAo/Hh+nl0V6Wfrk5UN6678XPbxJO32nBz6ppnnM7E7o9uiXqmrMw3vi9da8GwaZb2fE53VZgl/4Xds2KZmunjUpKErK7U/an88Nr98IsvjChi9U2d/goNu4IkFQQyHuxEHjSRC5N4lhuy0sd8uQeb+eJZdcIX3lTYnaMTwSfgCucvc0oIIpoXkQaM9oplD/At3gWZmYu5mQvskBY6kD7uoHLsXpOWuo4mVcvmv+fYw8Cw8ym2PgrerkNwClWHmgrCLrJpGaNcJtpxylj59rnOwvWDWqX6giR/00LbqMYiag0i91sFsh323s/fV3Zax+ulKbDIWE4KHsovnc4FahX82rO5m9qec4mC4oCtDtsqImN+bm+A3xo7ErV/v4hIWPpZFTFP+RdtAhH5nagcsN7LYjUFrK21/oDey945MTqze6XGO1aopNODC2LbvvZn5rhwbevJR9HtcXC4FqWEXIPat0R9iP4Ea1odPYInJzwkaAYfi0aJ2ZXyCAWuX+lZA1IpQI8WPu6hRWNG3wvKyjFKzLN8S3Pd3XC5M3/PVq2UMZbSQmGq09ACALqJqqkDCvnf06PgCrIj/Bu+5tt4sKY/wCR9fnV/Q6mKJehlBls/SlascWRkvRxJp8Iiccrq+D5/WgAYBYqXo9dFv77MlqzF+m9/m50zZZOyAMH4LUsDavXXR9twy1/KSO4shkRFVhEuPHnK+r220KVj3da01jR5su6Uty9zGVWb9SRRYEeexrK1rtDYLldz0MPcUBPw7B2KjV0TIRJiUTbqtZ8OYPRU6M1/RkIxlpk8J6kGJH4ptZcGgJi1mtUxqf1qGeMeJXvv2HcYNI62c6+8dpFwcFQoPGTW29kHK5yWJaB/pYPasaDM9F7hykUtw/QTZDdUubz8zbvzaivx6uLYTcan1XZsPCm+lRsmZo4OJ0JrQyWNT/f/f3tcut3FkWf7ejuh3wPDHSA4TJEGQFMl2e0KiPloeyWKIktW9GxuMAqpIlgWgMCiAFNzhiH2N/edn8aPsk2zez7yZlQWA7bZmYkITHR6RBDKzsjJv3rz33HOADfYudbgS1n1SzAFK19UioXREzehjzPxBJg6KfdrEDNIqA2eRBo7Qpi5gm9yCWSSXW7zYXmc/VowZnjHOWel/DLgikQ2nUJONM2yZSuFGuKGOIi4tBVC6kYjgLDh0dzrnBIEGjAyf8OTtoiQQMlcQbBOhfMQeSes9Pr49dfNjKdwRRo7OSwqSbkNtA0YKak5tC1TXtxrFXOEeCScxcccBOBnw3fxZvpG34RuieIJHA0QJAAzBuGtqYzsMNQhKf8K1sZhwtKzAmk0QrYcpxBDv+s0eRMJ9jNW1A+eOEpvhIaQZePRCtDD0SncoXwcSHQak12KKo50+K8J58MPhTLHrzPnRw49dAoo0gkdReUCqm7riWxiVGt6xe7n0DCdUeKZhm0TCOOzmdcOJ89G29DTHbzx+rRC8pQdG1y/dLdLUiGVNfyQ+bkX4Zc3r4Y81v27eTcLsHG1WJ3qwdxAmSTXZljB8EmZrOaq3gqyQGe9Wx5TCZEHaiDJ8yBijzs0IsxDph2qmLH67eF2DYUTVO421YpobDtWzIMqL9+LyRxAE0VBLrwQrJEeXGGaNrT6mnjpR8suJGi5z4vphVGzRoEU88kqwehvBtXQKJSmhpQOJzhoSQ5NIsCL5UK1eQjOQB+vReeLX14WGQNDrI5/K7l39uI1aNbErLXDj453j36aCcRypYJysVcHoHT+yKhg8gs+INo5gkF/Qxp8LbXx43E/rX3wBfH4BfN4T8InYaLe776ZdrBVOw6L7Hhb9A30qQEUH308ConvHR1ETEiDy06wS88K+akgnalJmdU/vnusWg9ZLdNiH7uyrix2kly/G3zbe+Te77rdeuUTvzljB59qHL+F4wg9qUJ9cliQMu3/SPyAYdn8Fc84KTZqaUAKekUIZo+1WCsv0CLWoQVP3uFgVHBWSr+19Vly7dWiTKOoeNqaQdl1j54RUhzAupkBMqZrbTLB5VFJZh7lQhgLVrpSwiFYLEeklufXMluwRNaJHQRd6Hlp6DFB4G9Y81AF1Aj/GYMkeC+bVgVcHGcxxmcrYmrp2QS9nKcY3fPBQ39nrontCuI17bMoIKVmuSKciayxhVV6OYfFcjMo8rWuwvjXz+lYwqPR3knrChvmLp1lQQXcoN+cM3mKqco2sQp8EQ/IdHVczRzZe2Zh4OJJ7bU9d6uC+pZprPFaEdMiiknU73fDqIXjPTnEY1YcnhTwTFV9/Qj7/VB3tTSGweQWsiiplVlcT965Z6FZGQCZ3FbE40+16tnvEHgHVp3/LDXR01AZmNgoUnOQkMuR43Lo5u5mhYq5qUQav0CeoWto1z8irhikNQCEmL0AaKqCzb/868Xgz9ZYmmWSW+MKzcpoyCpuBGw3uLukqVDM56+2rbmkiWA3VJGVaPeMTEaYl4br9djJniPLIrveMqNTOtrBw00JRZQhVR06LrkKTwch9i4nPT5BQsbUHT9jV2pfv5G/uG4sBcJnkRdXWmZu/qbOf4H4FYCXhUPXrOrmGgw0MZfQQtmtAoyTnwsxTOI8JGxo0Fp80HHaVJYe+drZwfuUMrf1vawg1iVY304Rokm3QUlsS+/CManLMZ/XaZ41aYh4budwPlriuoV6PJnEwn3TddakCBp4lIWMaOaig/YDFPrFtVJEEXzmpBM2VZK/Nat0gnzcCOcl/5/Jxmdkn662CWBR9GZHFoYnZzJDS4ejMy53yEjVpjiwdRzWS0mmJMXndo4QvZnoJWfXhq9ZjaXtgnwgh5TslV2PqBXnGMmnaVi+nVILXizaDd/ZTMYOQvsgBCR5N3sCaE+ppgfH9oHbdbYXZUuZOFy/BvUADPjDRh77UYTMw/4fzZwDbRvwD+IQjDEa7pX7n2lwh8dWPQKTxu4uFEcqJqrMwVowRFYMlQ+Lr8I20PElYeeXVb71aB1qaQNvSeIbtTkZryz+AXefBkwqS5P/pJEC7v3Id6vJNmjKpS8SMkAi3lTBJADmoo/WDU7V2+JBMwgEa+oJ3wPZFj7HBRNvaJgXy+lOKbfOLmbNm6zdi2wS47QmkOTQPKhPPt0Z6gE12fPP4UUunGuLmHHv5dL0jBswYGPZeejeMXagzGp46GhtOJt1s6Cv+oAjUIKJnkHAOEEW1yXhBy/Y2zvLypZu0IUa0ZPEnrs1oQRgunJI9HAvqHDjo5qClziwZV5WoMBLREXKGxQYKqBxDMbPAdqxL4mDzDIqovex4dPYlzkZ8NnbuVx6MAcUpHRX2SKbHyeRcoKW6fpWb0oeGoBDptWsdDTe9HiNxg+ANvoy7R1M+aDlWqSrR4OOG1XRp/SdMmuJtdYPTIXgeSpMwvw9QvGeQqYg034I32wpjXtGLBQEKV8txsvW9RnIluFQ07JH9DKrHsCGV94SElqMouLJtQiusJQn3N2abkLLFvBD4i/HuWyshaYCGdRUWOBQB1STYgju5wBXi5aW4SkL4jOp5GHSk+AskrJwFxW+3lI7ozuzKuYOPtqzn7g14Xxo3UrQsV7YUeLxZw8+V2Gk5+RFZOrTGG9KK8zKNyEy4fkwax04jXNgb1oxKVbHaWCl+MVNaXGfDpWAIGMNEuIJngfOZxPMY7NNwVk2nEg6li6XaPMJd5/RrkwJdj6sN4c+29ApRrxDVJ9XZHGioZ0WQqISSTo1HyoIOsDssUxC4tVmCgWUDMBNZFrHDVCV4C3fPXOsb5DJrHK0NyqmswlzWeLOEY8Ziag634ZeelrclZGof32Z5ljAUe+3BBxP65/I4OfcjW/BkzXXS0EESQZjl8rrG8AZc8dVLsMdje6uRnlrkuDa80wmqtHiFjTJ1RkXODa8A4dxDOcnmxNOW2fYZHfwSxbfVGSLsCjXtnQBpo2UoJP3gB8JUhZPO48d/7VARIm0HPvJQIRD4uicLd0Q4B4nKZCXzh2uaAsLJ041siZFkDC95inNGfA47n7hGcTfrBA3F70t0Eh31GjuUXCEefbOyRnAVd851QjSimKneT1azaVuoinmJFkuAD+VFMQZLlbIJQOhEoJn2QgwUkR6IhSfDOLRoK6YeYU0dOdng+t9LrWy5j4MSXr8FySzHEJxr1rHgtHY6oRD2YJdMxCAQGJkWXwfxJXIokOP/FI5qduxN3b/QalneK1TLalIKyM2G0i1Av03oSs0Ep0bYRADxvTKPzBwLO6F6dugihdzR0e0nmEt7V+OrXcr5TtxIg0/8hQV/mn+5oOwobX9o4a3KudDvdsOwcviqWwNl5moWhm+dB1glfm8iAuHw5tmsM6PxpGbHHegAy0j87ZzLlvzFLdU8eIUjcsHDPyc4rc2LlpibPUbQZnKMBnLVzhj7Q2WSaygZvxOugBfOHZjBRqMkNKGqfVgNvl37xGEz2qXfD65FCkalFBWMi31SFQOKWJqxsVcIsQ9zTHimuPXntnSxrWUBoyDwvNmMJeKDZvpWzZsaIZrjiF4+6spN07CYYlAlFtysq3ifApJCkrU+/9qIdRA75RjtA4ErxJclO9I2HvIoGHlmiASMfeXHTCRIAkdmXs5HRUwp13Tc2avzKOpkaNgA7E6a5T/N2wmGSGssEmevbBvm8q4A+W0J88PKKZGwJFvSYp7LsNof68X7l/R4N0XMsW1FHthtMKJq6XMnuAbTipeQTCiOwXpNpipFdhnhgKnac0CfhvtVS53Hqh7HRZ6Bupq7HxJC6Jr2Krmass3YccHbFZBYAAKiLYSf1Me14bK+CouExLfBC2+Hr8xvIn4hNClvn78ggASmhxsz0fbt5rbChqiF2nuCP8TScmvTcxDklkt3gX83SmRM9Z9dA1RmjkfAbZl7C0gKFfMYgG5ce9jwEpsA2+TOoevCZ9d5VoIw+F2Zy0e8bsZDdw4lZYaMfxJ7XdCKEeRTbZ0wSJV8rRsBuX8kdVpEbetRUJnoWjQe2tIkTAmoRQ9345q7xtuTSCctWOgMUlYcOMB4C9QhJbMg8BRt7O1Kivfk4kBI8SCEBqqNVSB4ztzxFsDCr2xNuki2J/FM4ToaVJ/81IhV0IBMXi0GSA7tXPZ1wBqjoJFw4duGFBoULIXB6oLZ0pb+mbiKjW61tBkEzmq0wErIIMEkKRjtUDGrgqd5+bivCHEz2ZmbomjrLtwuLQTQ9lKEpxZRn3fBEJTw/omLi2LQqXWTXjVeBONAa5k4aPUKbO0HRKRtHD3AJS1IRlZ8FKuwImPW3n7L4YoQucnIQtIomQ+6RVS2ym8MVzeURQwKpT7ygQN1YVoAqOHVLxG0iBHyQqMWf/B+N9V3cFOlTMRZNgIstchAE2RIjEeAHN24C5vqCHcRUkQRBhGNF1BH1Tcx8fLa4Uc32Jb9ntrksZZOYiEfN92xZ0BUL2UmSDE9zzz4IIqWAtqA/RkMsVcEpWXhODyhRyCUVubIDAdFJuX1jT1zgucK+n7BKqikEtcWYLRCaLx942PVk1qtzdbwwyDwr0GDSdCpayk6AcwIPsz7t6+MklFSnBPqsNgyVbMEtLE1FwwrE/GL/+TdkvAjxdLIA6IoEkhSo6mh+H9qL7VqsLZFfiSult4rwhiHtgZx6J3HcXiplZYwFTosfbBzXDjfUHy5cQH5+LIO5Q7zkEUQUOiBSDM0cQnaLWHkrZVMjq9oIrCApMTWJuOFrZnSwZEmZnZdtFmFPJvYTJkI78CvA1Mn2g/2Gx0AkU2oobY0s2YhOUfpVGNKfc0aM/wzWESuTWphiWpz6uoCKuLQzxYDJAc0MPe5h+1iMi7k7Cvnco2ahRGrOhV+tQnD6MWS3jWYFHy9bhXFknLGl80L9y6H6Gi1eK/H7THMZg6a3wdI1cEO++6i5UavTlE5UwhJK9BrZTfp9invqjGDD+dPso9yRWBxOMmxt6Zc2jpHV5Otlyp6Bc/QMigxeeT2pbI0jWBS6n20XHRpfVWzj1cQaRf2amj77J2Bx9l66fY9uSwwTDmhwxfgRlPQGp/Vevo0kAuJyFtLVrsyxSxvweC9hlgB0lgK/ccGgZAmhiscCU8wHpKSEqSjiz+wTZyS6o1qNhUAeeHdykuAtjltwdDMtcjd1N0tV/O1tCSclVOU05gRTcJq60um8NNJaamouwxj0PAwiDODqeZrUMXqvbwNlFjUsy1mAaaTgO+JRZfgDRIzYjLoKI3jeY+yQTMt9KqMXIXAGRsmsgNSTRKFugZLT6ouYREMa4lQXhUZvZo09QhD8WmeuZ1H2Evjadm8L127PfABdhEV7z3evIoYXTyjqW5WYQByEH+k1Nr29htCPS0nkFwh39i5hQaLHmcOMwSbgOWpQVBrg/sSoQJ1sqM9hZdxQgvRSa0fxFp2LebSKrX/WFTz0Io1y4/7p1v9f0TrSIuP3f+OouLjw7VaR3vHWnrc/5wyR1Gl4Zea489Vc/zo0WG65vhLfWN8/H+pb/xvXt+INdP7VnDNba1RwdIus2QJNXxcZVzx48K/GdVSr2o3WVq9f+g2+2PiPkLyUYxpMuM3/JNaQRXoEu/ahG5rsDdu841p24AVZcjEH42xhXD0KFyM1dnOXhghd0/zCHeJWgMKXvKKSgARIAZiHmLbkJQM9eWhGR67z8GGcwguEQFlUxXa+8dw3JEUYkpHSD1sY3Al8EkdC+FnTSjttOw9ZyUZIA2BgRL4FwqkyoVMh/1wTXmbhpfYWykv0x44xUAImYnoxdyhwnjG1FqyFuxDNC4BKQ3JlCTIxXIyvHFXGNBsklC330CF21k4s6Kbqga0mhhMJ+x/o3GMhj+IIFFfZ/hGORADblLw2Qeeo5yYTasEaLqXikCFRXHMIE+QYchvkEPN9Auc+ChJlcZQIye7ai82tl1VLA5NkshMKyusZUTAjO8z2UVr9MSmEFErqnK3lusF57BA/Z04PQomr+Wem3RSCuakZIwquCrXRWPMCpNuH3diYih3iF25jTwhDHmCK37uLc+rEBtvhhsWU1NmcVx5QXnIGrNFSXw7SlbzjsEW2gBgvU04C4nMQ+sLxIIh8D+BY+fVhugOVUlFl4iWirBfs4VC9yHRDD00FXHHszICQ4UPxs1zW1x1kGhNQzdh9U/ye3Z2NhGgRWU01/br/dehNWq2dxSk9ewAY15CkxoI6ZKD1o7T7+6089wQSeULolnkIkc5I4aQUzCSwbSzhEib4btuJLdJbu9T4Z3BOBx/mRZJkYuikBo7dwKxI+c+yWnvZKPfXYjuky18Gk+dnc0mnGWlGzRfqyNCWH6hCZWM74u7084PmXOsILFhP4c7a0gAK0hRZt2ftjuPu/9zu7PXPdnu3CynbigUL6qnyGbfIu6DXbyqhiAGyFLZZUP3IxyHO2F5KEPhbwfw3i38XfEkEz+xP7njGhpNroam6KEnBwzPVgQBIBOf68JduSfDiJlav8ijU4wEF5wgCmJUuZcj9hT38Ta4UxhsmhMRhDhVxCO4ug9oAZuzrbgGSuIDd1MxcX7O0nh3zUtPcl7Yc+KfrJeiL+TCbD/Yy2LkuJwJKE61afff90KRccqsVHzfRLbQGm1XciQHwUj6wU/7wU9klT+gQLpJoIFJok+s+Ts95XssFs2NgBSOAifv7O2zp+7HPuR9b7wGPH3FRpVocDdekJs+En2iR5/otTbShw/sKVjrgkHEVM9a78hfqYHn5Yx8O5jVnSRpHfrE94sb9U66e/3u/lGnd+iuHaf7e/clrXvUP+kdaORIR/B5Ykerb1ZfQkmfK5TUOziWUNJ/rXsY6US7572rqiEXNaxgRXM7QK/0jfrKWDc63WpaPLqn93lUvIddXnTlq1beyfYKJokLqlJ34eNHh4eiGX3SLFvw9ZsU+RhjBgTUtVGjFyFdnTvTnRxoUZ4A5iY39zOqnU3N0rriL82EGIBzVIM4B7WgFU2kiUnHeMB8LDqPAQfG52CVv85mqdzNSj2j1ms5LoFtQZRDF/cr812pVvR79ppeFwKp5ciheP9h0nWQyI2G5O6rabPysh4itJcfjFxfkKHRQC0mOMn4rKzuDjN8iYCvgeRI0RIvruxWItbNkmlC20WqveEjNjaWxhvpYs6A0oIrdZEs7Lqg+76i3A2sALkP/Dj6zlFoAYK3McZYaIZpWDrj8lcMSri1U8+BPlt4Q7BhTk8my4pT190IbSfvy9drcrxXCpsJfT1KLcdHTRRn6m79H4vMebbwrADz9VAB87jCQUSVkAZLiLAvIkFYi1+4zWYlxtN0zSCBPY/fl0tu0EfwkI1d10rvAQiYLM/h+Bu6N9F1tqwOKqLTu8FghIJ85SCbozJGBUSr/C7owVYAxhqDUh4dobJnNAKLZaH4Ii8CZs8gSA4thAT7WzA5LSGEeIdGMDY6fEjqAMCR2x0SzwD2vk+J2E8Lh7UhzhHWhCOTW2oWomOTcmtIKPYQIXpJmjfGR04+eoNe3LAseLQ2CjzDTirb6cwvIH2PoJPOOb+KC3cWSHFL4s/npuAsz2PS/GbMJX4fF4uBOjU1NREiFyZYINR8kaymSwlww5egawjCmnNjypGrHaEYrCTB1XTrQY44PaR6e/HsTazDAouHQGBu/WCGwz1FQvVxZevQkKAwy5+Kl/N/iWGSmxzs0Ip7MXQehkvlPjBlaOavfKiahzVEg/Na9gzWPFXOZV+NKZPSdTBMzj3s8kvqErwCUxA1RihINQ+CWqCUw8GxjLWlqYvWDjKv2Tuo8qVBQ2kHWjokq0TQQiv8kRYxr9DmJ5am33o4OsI+F3kzR2U+6dnEkG0sOZ6DVGAl3GmaJf7g7xJU7W+jGHZtYSOhY81tIYVeNb9ZjAc1aYSQ0VTYZudh52DncGev83Xnq3Ts5nX2sWjZwFz8QFINjMSEzF4XQoQtoaCn1eTBXBPJKOsAoGOtq/TTqdAsgIlOlnO+DmRzT90iMq3k66+IdK0aw4r+JXybQBREI8GgqPBORyXyK3u3s0qUDumhRG1ziJ8ff1AgD5zb2w/IX4H9klx/PkVmAspvJrUzX27l5tfoopMYHxM4ZT9m7kNdTpMbZy+cfDfaqdnnrYpaul7fTNhK0V1vG8YMvgNY0Tyx2CQFGN4UyjEWRs0j56CxO1xvvPR9iH8buMyqcYa0bCMoLhwNF3TQOP/YjoAXeiRXkYqLyuTclrUYZ37T24DXIylouEPJ+4+cfy15Rjl4nNlyos+e8qPUiFA0wVNVtAzH9qbYNPF9GifWaefD2Q/h82PwHRcZu0DVxPt+2W2RaCPlf1UJ19SZvucvOZD/D4+lSSOdaOlsVKEUJ/it7rWYhqWSVjqAZPEdFERWM580RDixckAn2n9LoOV6PnPTAh4yJHQwtKUUqHSysN/u8Z+BRHQyWcVnkhHJ9UciHhBPigzIsJ5AYKlouacnz8VkTqyxb7xHQJkvn3JGjbAoZ9YqM+WOFq7t9ERQalNMZuvDma5/w5RF2Ffimku1/YTYrhSWvBQalvjAH4LbcnZxkWrkeSwlJ1DYWXZnShcqobnBxvUSnGqR87akPJQCtp0hi00fD+sflRcQlcqh45WNn4+yYXFTwUsXFq3yJzbj1PDXyTE5K9h15yZUjl0zzIpfSeVfSeqbGvM7f9x5g4fXk6VwFg8kCVWC2zLBmnvkaAtuEGA3B7yB/gc33jCqYivJm9Evw6Tkw5nzbYy3aBdRIEUVLP/VBHjfuG1bTa6/vRtiCPfSLfVLOXYwIP/NLn9CysrbMoNJ5nn/BDiUr5s53pVcs/H3haGXJonMgbzGxPtbIcbNNkSXjiHZIbJyyCe3KScmm8JoChEF04LiUfrkeYo6s71dzJNnd7L9oYpoDNKw0pq+udRVxb81XS7/7//835iVsr1z/zAmPCMEmfxgyVYkMOjJelNjHSyW3WrWpVrJxFhXjVN6oKXw4eySr/inpzC/l77MUwGCtY2cOTs7LBSohasqsW2OWiJ1kQP53YU7Byb5COE9iQ0gRzXftKJQq232rDXI+aSq0Bz+a+fxFEMtmDo4p5MqoqxKPkk6HHcqle4Ln71F+KpGH4mROigEqDq4DiIa2PCBgic5NTeVAzfXqfE1YArkN/x5a9rbIkls+UXtfiH2n3fdmRv7dSVWvHMXXDiCCtms+Z1M66paFLItVmC/DSsg4bGVV2vDeEE6bEFNBDv4ULKJeOLEUpL7WRQAkEGcNSBIbjjv6DLc7Ty+K6iMkWcAUcWzZVvsZtVy5IwTWssMxUJhGcjorFhXa/3sUbMCHJ/hHSlbsyRkPAFyAO/v9L5e4WBRWLKaeAS2XCP1baRG1G8PVoQFa9nseiEi3+kokdGqb9JN1Ls8gTe8ravwUoRNJF/I5GpUktgIdRNUtg0qKCWiDBO/FgPaCuNSdtWMnJ2/KgsResHMzWABFs1MXHhvS75Qe+3tdOw1xgR2hJ2GGBsj90sQmVI6Hd68uS6uvOL1prfEsyYFU9C/J3yz5VyuE6aGkx0xVZ+2uY3nUVV70MFLLdGzX7Tu6Uv1MYKMH++nSSVOiOcXoQ4avuGK1VR8KoYL4XVhTU47ngcmWX9J5WCXEMa4hIjQJSCtHuCK/MwjwHdwyRNyWS/G42y2bB2JhddSZKGc3CIR0VKKM+0FPVw6pN2X6xnGvkyYcBNOpOQa7wXxJB+IUDNAl1TmY+Tf8VHUnWeD2tJ9YbqECoUau9IWU9oplPa1ljLZibkf1NKXrUiKR9uUbNjI/FUt3fMlxpk/qg66VSUP99dGF3TpcI24RQBOXYls/rcUgAakhSdGUDJXVHIkdHa95IRzQP+qGwyoaqDZ2gTqWgRPT3b2fpPg6eH9BU+P+oeHRvCUR/CZBE/b8DtfYGOfCza2DxKSXyBLqyBLBGBzk+f84lwRY2vr0QgldKYfD5FrzeaSqLUD94bdK3Gnx5JltkZC7GOmk+rIksVa/QNTrNWIQWoJeiZk9SBhDQcf31qvSTINX6brs+cvT+H3hTQuGxqhWiIEYnydVYWj8SYzoBT68/d2FRmxpCCzQpE2mxTEyNVoVmCR3FBAEog1NtP4vDGsNZiDVVCDRJnGvSJNqEOXmCD6OIUdrEVBmUMKCZ7zYmFcyFkgvhMgv1sqnhL7hXHZyGLaaGeTlK+7fY/LxbjzHspI3hbXpLGL5QjOe0ksBkAkfI9BltNO572z+VdImoHsFaeUubt0Zw+4t4lvbxjNO7Qpe+efkDF3c3vpTlA0CfhLZxLwF40Izn5Cy7LXlgPf8L33IkbylVgafS89nCwF+nJNRWJwzcJDNzraZKAriS1vvXNr5iNoBHZeExhqCwOSPuQZBsVelxPAyyyAHuhTst6hn+rVbnmQx4BnAWp1WiSwHZnHXyJ3binPrgu1hjwLJVqVsUbwfv2FI+IQR42yIWZIjYrCs9ZlfwzSKgTTg4TlzFbCy0t4/GOG5SspU0D77InMjzJTr4q6YsW9LennqqUzXqFUSQzvqQuSzQxaS809xd5xkLrM3G8J31QHgCOe2do8PRcpIyvON1nnxlnRP2/t/tv0z/2jR0cH+1vf8hwdhuUm3+xm7kUgoxfY63JCmSPWY8WXBl4G49l2QGr6HfxSYqJYvqyEehg34jQgDZDog+1kBhQw38A94du//x0O2UvAyHx0k3TJk/Tzz9/s4t/tlQLV6ZoALNgHZn6RqIUzLivss18f8l7oi+50z0CO5V4IIxu9AIOzH79iqFAhI5xYwrZwKAKjROVKBwTj4wbJiF0wSBmnmkVH23ZJX797IOvdfU3rZcJKG0n/oLOB12KoZofMAoZZMR5UYzZZvrdvvhdXlSJaKEIQYDZ5XoUFEMU4K0cr63pay3p69yzrOe7uwf+grMc5YXv3vpodnxyeHAVlPb3PeDVLOahfrmWf7VrWOw6uZf/NHHW8UrlJY+Hi5F2qZwqBXsnngkuUbSB5e3oENT+dfJYRIDefVVPVB6LUgRJ8EKMGS4TPLbSNAC3Jy9Xe8eGxlP8cbYZ8pBstc56z+Cq9wakEoCEAzgWtrDeOaLRQnX2bKWchWg0gl6uRe8hx5i/E3J4UbCdB5/DyK6ijxXeZEM4yIrEANAHUC0BSRHE60SjmiysQtbiCR4tGbniiN8vIbEJR71E5eCoQw7MPFqyBWENZe6wtNKtiLgP/cE2lZwHFGnH4QPuuTek2zPysIbhYoZqNneA8ONeHfjV0RnqEaRSuM2mnbms+E7O2FaMRWoa/FIMZBF8DXFc49MY1MuGGpOXJWrNfqSQuBEKQlT5onYHlTE4Ew16VCHY3C8nsgMw9I8CcLRVNACXrX1XQvW2JcRE7HLNbR91eVCNQWWzVllAmCBExCtdMP2j73K8FALfSzQyvxxFWk1ckvUqwk4Tm5l8nGLeCls9Q31YgI7iYOL9qxG1QuaFmfd2gLef9dZ37d9oBdts27Wn6rh8TSYLigkZKLjA7ikOnLOJtIYcX1Hcx9bOl4W9NCbY9ohsT0x96KndCypY+l1zk20YhUmgeC6QcmJdxAUEjWWOYfomvZrLA92Jxd08u+vEGTZweb969uNgFYQAPtPOnsJdvSPIx7CdiNXQBM3mtwDwmLHZqfVMAgC5SOSBp6ZJGHBbOtagU3fHurgR10mpKMDl8sWIbwHmPKlqiftqOVKDQQhVVwU4yl/aH8w4LhTMj8X0bf7LI8yVtQwVhStqXTlgBxKbY457D+qcrKRSKMjmIbPKoNEYtpE45Zwfd87wrJ8vXZ8+IIsHN8rjEI5u2cMqaHiZkSuRNY76wyI04kmDtBUCB4dEXC/e7QTG7FgphzE7DCAZwwSVmKGomDQsJe302qZErO+xL2oaznWoDoBa20TXOffRNn1jkIjkYnhzGxICmzwlMKu4VhuMk8pS5cwpp/0Xte6/Qttxq4wpf0ctyz1yzRLZYuxbOEvdWkzOcfJmJcuknsxWlaYx3xRgS3yDT7TZaxTu7PJDZCDelezQ4/kBR/Ry1hWSiQK9ANGAXE9AXJW2JOVc06Ek5Z3EDfJuTqnN2U45yaeUMrGGJyIya1Hfx86+anmY4Rog6soAKnhiFO4/BS8IaLLz1dN5BShhe9mNnjGY5nB10JpLnfVfwFmbsbzZZYsxKijyYi21OR8SSPi/IGcxroSLBdDoCDIvXmVpyQKlcNXxCPD/ziGd/ODqrCcwysnKestnx9uGsGi3GzlNAkYkkri/s60OG6ou0ILfuppfFxE2WuyvTrWkL5wh+78Z9WU6ckfJ/gooNPIoZaJIsLW5cgSILe046FXW81RKr2O+NuqKriUozyR5XGZagIF2mCxTnBoXWD7slAg6WKipXzdEfJBQVxHp9D+J8gfh2xYxqaCaGcIyJH2E5qNyM7rr+vrsQ6dNBIcsMPOruHXyxYi1UPWHQ4NDj1QHwrt1bs7CUmftoOaHb5BQ0GrOxKboZFdmku5jiiQj22BDjl8oK6e4PQBvZoP9LmT1jJdA9Y1/ORxJgrnAohLmCcMos9wrK0ZHf3jqas/Ns+JGutuCVZqM7qAYT84MlGtWiljEgllRCys6LIzfPrQxfvlJs2PlWUC65FSLgG8Vq3reT15jxvR8wbaTQ29wFAbt6sDJXnOaaseesh5tpjILCIYPDcL8YMTMhrlherqo6NpRPT7Lb8prjpCXOFRszOCuv5uIiy8dHZaR91SCggsDmHGhhiEJ1lLFRF75SuMAzham6HiBJzbIRhnWV/HFdjvynWsNLmS0LSg/mqihyiI36kPxVVGVCzpzSmtUL9BoQ+dC+8d6KinSoeA6BCmDzJftZs6+xZEZPKgtHRaqZGgRvA+DoBBPrXiuMAGTIHoKv81V6rb5GtljsNkFApw6qhTQBaxmyvGN/ajPxVMRMhSn3wqQF3H9IL5zYLGCIrCwPhdz4eNnEtYlFhTM9ZurCXanid5MauYxB38XOdxdvvqf14NwCQrjnVPpDywIyRHR9msrJbWje8YtcAZh1doSjLqlhsNl4fqxTo6GVuPs7Duo55l4k4GNh0M9xGT1jaVNZzRrHGjl7O0LeUxKDdD4LLbjIx1Jo7mmn2LneEXv0GKASGCzlX5Dbpz+S+1bMhxta0Km77d+B5Xdn0pwtIRBTY5iZCT31ti0IHV+Bpo8F1+3wJHxq8RkeC39TjKaGsjOJ8wis7OpAFiyP3C+MN+wOQPRIHJgRP+pr4bKNrAKfuUzVAfQQRX3jz/0BsMK65cXCW/nKK7cdiyjf4v5tiPWwXJF72//rbjoedXl//m8ptW85iBD2hYKzNg0NZzhAoNX7EncMUJPZulGTKaZye74gKAHM1r85+/TnH569vXj55vstjDqTCB9vJJxZkNmGnWj8CZlUPublJkA1mxQPUD6/9KaX14Pui1xFy5pCqq7pLpyAREtTEq8u5g7c71lzwkTadiH8nWMFCPOrQUgBirrdz11+FRrClcfyB0LiDh1tqTtx47EVCcqhO8RFr6BU+P37V6+YBp+knMbKEq0fFcEljmeoYivCHV6ZhgXBbNXdn2Sg2+n8p9fOCUIvw1mZGsqRnNledUtoRPnVl3lbdPnKneyc49vq5HEirqZLmAJ/NZOMe4/8l2sQPAMkFzWPZcgmpG6gBesjRMakyTBV3kBkyPDV2MS9W03EoZeYkHURdB88kZNVNqQsny2zr9k15WgcnN+f2KHx9PX+QoRGSt8WnQwzoesqZwIig1oF2q7vrMoE7lto1tCdTOhY6sbiVevm0u2bXfRX0Wl/CG7FNoEBGHlQ5l9peBOqQeA2EN/6rDDFqt5lQlW53HnEA7d8Q32KDyQ3/BiEeThiUM0+aufQ2ajO5FZYbdJh4qr7QzmbI8WX4aG5ZxM+WQBebt3Z7bx117q0Lnj7AU0pD4yjxvcXmHOp/gT4Ox7Z9OJrZzvqhitBe3LfSBPQBKZCbZv6D+gs4NalC2BVGc02L0ANbMnCRacXMj7a5KgtZ4lwImYb0PZdZbfgvHPMKm7EMt1RW2ASZ4WXfdjwiVR3AVWq/K6l4l+Zd787o1LCHyiV9Ix8P3xl9kzRbbjBmWIGFX8tGg0fj/QixClVVTKrBXOvl1tOiF6anP5GKyKdNoUQ2y64kirEjCw7sQxz1BGzc4/h0B5WtVvEbg2zJ11ObDw4fvqEtW7gi/1N89kniGmUcySum30scvEMPpZgwIYAbHM2xn3mo5pOEm/FqSWPsuUWq/lgpcGg0w7IBOrF9TUEfIPrMDm92TUU2KERlfKkMr7xkR5BS78CqdPItVZdwsLwN1SlcyMjrhIHoGVCxzASla+8o8O75KCBjRlI8Hnpb2Mwl/c5XdBg8xHmQzfi6aJGApguUyELTwfmno8bnrEVfRZon018FuPOVwgug2op5qPnheHWwRZ5BFu0uClKPbO7n5J6vqzVrXb5QT8wxUL7+2yx+MzELbeYVri/lrS3/NaCNJNEXmGbbWjZMF4jVyR11JDM/9M0m+ShYQOcCCTwu4C52qwHKKPzL5JTZTelW0Z4pUHPdlpNF9N6s/YMEGSL09I+Kc13HAiDT2nJlrxCRApmMelqMluOCQyZsbYnHxmbHsTWsGNmxBvgvhY/ys0rJIJbEUOwgs/wqp0hmg4qoH/CFYgLQZUIRXthPJ0v0/cmNAtgUsvn4BioVv3B3vQT1UdM7T2N3xHwtVENIr+tQXED1tFNGQQnyW4QS16y27cZevK+1NNSBVFEk4jhFFGFFgj3JpouWfc+f6vXOnxjAGfdbBIfXPjsIUKaqekHxNUpp/UMcxGk4lCb282mu1ZUwId1rf6wGiSy4BskJYmworZZZwkleQgCaa5/WtFcKPOJj+NL79Fq0FVoG9BpkBejR6HYpsq6fx2eAsWsq8I0cOAQjjFZn5PMLQuhMstmVoz63/UV8MWlbBahs8IU9/uXyfYg22NPSLg1aXalSwN7a0nSGST/Et1Iocx0U35n0MnNb8EDcUwb5MZhRNIrvQ6UUnb9JwfJybSA7UAvA1O8DKA/GofdsOaGaWTBQ3jpY25otRsKMfvN7EP7cv1YFBgVBmp44u4AdKlqbuNPUCRAhAHuzoeqRLAsKwIecsoBGoMVxTGyDb3IGRYWkZ0EViwGmFDx+5PqE9lSRPb7i+KqtoPlLpdPlhKsC3dzrEYigfiXd87DsPH3zta/Aq/G1oZjl+vMKLpkWArtpzNnola1x+hg9Ha8WA+FE6H0Gu6zbWkSEQUrPmWw3Fm6F02Ze2t/qxbvFoOiibALXVTdJlsXGbg2bPSJ1wgSB7BAsKKi8nHHAJEmVM/0xAY+IJgT+DUmcpD2t04i2WKb8hwcy6X7NFYyhIEJ8lWSDmTw+t8Mi2zy4ZxzUT7bdgNLrKYAQsyFozwALdAODvzc9KASXbZlLZEaLXdxn+rBF2/6PFr1gxms5T7Qhw9wCZSYEdfBtqmLylAcBx9GPikMzIf6MfnLdkC6RXn7iJxYp4EvBZxz53sGJEio0HJDcVCQQgQpJjfNNS4RaE8uLWF3U/d6JbxBH8W4DcKiTciBO0YlRmxNDJ//DLeIFUh/4z6vSsaPJd1YiP5itL6aQ9CdgwQhtmg7ROmO1KwXRC0zD6ZCafdhEM+J4kJuDlwbIPqR/AZIS++qAhyCL4HWWigqsdNyrVShSVDTumtqOLrU1i4eQV3RdoFF4zxcZzbp77tb32LdNNR5BbWAv61T6c7NSje7zXLurcsTiL+nZIe+/rrLZ0XX3QTckGFk8M1/8sgS0yG6A3jY6rS84N/i2v6dpgdCXV1z8Ltun7pf/ZN7k0mH56zLOurwBf3295/mCuzu3VQnmO2w6RgNKhQQFnURJvM/goAabkUwEnQQCl9A5+UVmQ489+CfoG4zyTmu6ezERM+ZTGQzkISYa5HFGFHSoYEBwd82rNw23piwM71X65DxhER4pdT7wHAIWbYD9n+fIV3gpbkf/viHrsBWI8+881MXS6U7swXjtOfZtS89EUg140oFhTmJzBgdW9CLRIEoPgYpZ76dCoVNgyGVa0TahxhdHvCqdgcHjkdxoifAqXVKVqKHXfgwG1wMtojhGbB61OmW7TVkHj//wQsnA1Qz4h1HA+3p4cG7eBKyo0dUWcH53tYrZ//fQaiJcwoB0aPAl2RB0XNj0tL9+tdf/CJirDDjOwXa2dbva6Bq5h6p3hbglxyq54GgP7fxS4IBHOwdGDojYBRvpO3b2yMdKczVTTOohGM0KLGuEXeQcviWmo/xI3ReTNTkr78Ea12w3+iPXyBhiVn2utiawfNgFsLaqFF1fV00iqIYPZ3FEW9oBys+Go8Pl19O3waoYF9DIQGQ8FL+KBibDwAzGwQ/Mb8iyUGWkA4r3cBnCi5SfHBxBWEj2DEYjBaSNvkqberWdXU3hajz9NdfHvqViXFoyBC5hTC9qX/9hV2trzhjI38wkSwS4otfA3TqU/IRwgHQigLCYfQqhlJpU9RzZqjhJX5jgMdy8tDbYVSSdbLhRk2BKoYdgME0ZFlsMhX+RoFjjJRNcA0YzN1v7STx1HLJwWvDXkio0CF+svPCw9LewPuG79kM6sunhAZaDEZlDZgJdaG1tGdGYYt6CHWqrGThDByUrIwBeMfSShAcBSqGu2IQVIbJUKJrqV1bbgCiAIPAmAb+pA7QTASknhU+4o3THefkIgRVYxxyfmWkFlUOO1sA7AMA7vxmy8+UKfJhskMOCvdjzjmPS9zo6d3lHow7mnlakVuMVNnqvHj53II5WJLb3YFdT5TCx7Re4v64twIZiptIg1G+ICyU6lICfDh9Ibud1eZ2wuyAkyYyK13l0eyzWVUxCN9l/CYBmLB5hzbkwA2A64TclHzXkwiwPL05X9Cz2GWcKam6rscsJIga6gaOFRbHMxoZuFEBC+k/o2lgnXtaXpdQPfCUC9XrVD/J6IadNefyluNMLtjhCMR4UOWW+/yyc1F+mhcgPSw/kyYZ/IZvBNsdvH9td+BKsk30WvRHeJ0dPi7cQ6Aj71yylZGbcxU9kzgwo1QrQfAIuMqdBvWciqqRJZ9TD2BzKEsvZuchalehRHS+bY3kVyE2d4O40iuIjrFTRvGyuTsHZ+4Mr7leikpAJgRG5AjC6lAVrwTOUgf+w/tJXu2+LfJKtdfK2GWPFhm1UkuzVwD5SabBG75/W0MSJpSAID22BA2Z2Rp/J+6trxfl0pqwJvv8pWuUYTdSZ1PJBUxqT7HwGwEg6DkvMJ9dzOaZTkB7o1zfS6DdAJVlrgKoeiUz4EPoTHC7CpnbQtUiEluYQLBePghCyQ2HKSPHXhUN81G0sGnh3Aca2EIqJUOZuytTEd0uGOqDniH+2V91yP28+sdgJNb3wKeXeC51OEbQIMY5J5QIrqcVxdWfvnktYAOGffCK+8cnY+iu2+44WyqNj6YZtUo6QO5NIa03xXeBPIh8E0cnGhTkjb+Ol3gv3ykFxAmGhPbhXd/ARVMxmh567q8tGuhGs8LhRc5g2tM8dumaCMA53qMFBih5FoNi9UVuzpCBCFGAqJpXEKat3M2BRk3VPJJHFVstRMrcoXhw5IOpphcg/YB1BrZeXRE4Bn5PLbsuq6GoGQB9PoPGoc55qOKKwbxsuhwYJ+0RVmC7PiE0YzHdhmO0toVR9aScTguCYHhod6ooFZC3AzisBUc4IM2tegERa8pLQ0p4Jb4XYyB4XpAdzUay2JpYbZ9kRt6Lm/IasmDu6rpYndqqsXDSGdZXCZ9Q4awWA6ModVRsJQh3XuYQeUEZGhDuKTIkvCAk3LZixc3igjPBKGcQMGttbW3tuekM0QPeOUmoZJokgwT/2EJrO+2QdEWxx9lUGDs4MuwfqZrDBGnCkZ+MajzAZZ2Y8j8W2cbjqqwj6SuBtFPsgyFUIaleNEw5dmE8d+6RMZ2MRHk4FbTeArZ/s1iQKy6663G2hR8lh7cmSIaC8J14/bBVc3By4SEh36WLbR3Qs4ROdeBPhg/44r07o6Fy32PuophiVjdtRhTkcNdot93ChqP9ZOSEgtzqmKsWtAsO2Poj/EI01hRzPvJMR6GFT/YcRX9oZvDbcIB9kp9GxrGnze8HyP5ONcSDJf/1F5LCmDQgp1SELJsY67HArXA7HN4anS1S6dw2aBIPVSIZOb4NZIQRFaL9y76EfkP/7LWa8MXRp9O9/pDNCM0fuqVXSSbJ/YSSUOxA04aGSCcjtyB96FF5Q6yarklvZ6obNhhcuknCb060rpGUI8vhRwTOExqDjhCK23ssoaU/JO5WC9tJ9ykRixypUintrRhMnFWWeyLwFRIzcLKd+Zro7+F30HgHeM9Om4NvLa3WXCBSEiJfegFBCjHansiNCD6Sx4pWMPM08ayQ7knH3PbwCgWrbF/qchzsrMDvJiLnSPABSvnJ/7xy912wgqBCGSHcLYhdztdAJLWVVN9IrSCEu17/OC3XBhUOYDhIEsHO5curOvFEiRyOYYQcMwVZyh1RqsnoUIGsxy6eLHgKoHb62D35vR4Jj8wBCeIpuQJV77Cyt5tszK/IGYXrDGY56AcvclzAy1ojbsRw1sxgu91njYo5A6tAKT840iHORmHWkrgexm39+7lU5ikdR/yWKtLLmt/MqoXzyACLlLFmAZPWXZb5n18+3Vr9sJ2tPB916d+XfExcDpaX8LzEqohFevTZoBIMnOQJQETHA5GchsArWJ0HKqK2SwitaVbOVh1jhrrJ75W6gFulDx8G9VAPsY40tazpa1+t6g4uDrsIqkFgK5d9GPd0JBUbNcHLUsViGzyPyWTED5eorGi43vHR1FNu0/Y6rlXlSsg/pihFsKdy/aJDPnL8fbW4rflA4Jct0XpmqxhWHZejauB6+i67zTgzodWzq7GadEgx7gzuTBdMb5T8fvQmGlGuVMQYoxKw04BIws56i4rG0c7h/VU0jrq9o87eyWn/0WnvMKRqPdxbR9W6v3d0ctQ3Mho8hM/D1RryYH4haf1cJK39vZOApDWVKf3C95ni+0T+1303pwr3ym8hbZl36VKUFtfoe0LYx/x5KLjCzwfEsCtbTjLF7p+4PYVXWiGQI2ZYgAdRHZIQkTAY5VYuLQo2muQmqEsxgj/+gXg3bqWSB2EXmjH/6+tX3PIOXoOni9m0IvUzfzf0fUqRwR//gAWyZUREhJldhBXxJHUHqOLHuCUIqA8LCALD3fGTu09QCYh8CV7bH/8g0cAbCtbyghVYinMCsbwyyYZ72BOhkb0GkynwgmQjzwS+0ob3gW77njpIvUfdvV53/6Czvwdvfz+y4AfH6yz4waPDfq9n2LY/owzSmuX6xaB/NoMe2vPVyxYsmOtgXGKSWtGoXT2KUyasyWndeV1yuDE2YisbT1qx/kGPrdhizpdksmYIK0R/DAwat6r3BTwIPIaaAmEMWcNTZVsynIAgi1zYHVQJQWd0UqC58Ua0JHwl3ALnyIEzj2gTgNT+E1YYZrNRyYwN8BUUj5QEFXcumRXh+TCxa057A3raSlS4scMt63Cvt/+ot/XtX6gcRx4fXWyBuEJ0fsq6mdETAs40ZfAO9o5bLZ6vTGpEfWuZWr3RJCpLW74vQd6QcpqjLyZEqU2PGoVmLS1TbjS6kIxlaSYbOBshTh+CXZzghAUAASs8njA0LpVAEncN+mQYPcR9GRHj9u8QQdOJ7gCEj1d0yjwQwQbcdpRZ4/c5Vvp8MXAmbv/gvseKW3pH/aP/pGNljQH5cq58vnMlFNn7Yh3+063Dz7TFryB0DgnOLtSZoRZw+uB+ZIT95Dudl/47obpfW8tplQq3xl9B7hYOPlmlXolCVev4ZkWQCWyS45cT9uAR9ZT0zfsnB4d0VoG8WFpU3tNAFbNxRqxpHikHLkE0khDENHWLASot5WFbkOrhd1rV9SLxEoFuCr0Fl2FIsonus2ANgzuTFYAkvkuEJDD1P36pFgo0HDXr0ZFCUBTdg4k7bqaeSLYe+vBPDq+umrA6d5bneNmztH1XBd/blZ0MJy/daUNGMNCFUARF8Ai8U1QkqwVY6Ouxd/r4WUAPJETjYBgN4ngdBvqVkPqpLeUaAUKkYpbBMegBMitaTTxdmfXn5qoXGiyAlwEU1Mw0zx/L8JbIcWemAR6Jmd+g9VAFK1xjTWEQpEudunmTEEznYKf/dSubNszRYTP+KzJXvi7YnWPsYM9FwV544mSAOC3RKKOOmqjfKOGciO4kDBetixlPrFgqqYDCUeUN1bA7RP/Ia5ssxgOIPiAG5KAjgm8Ala47D8udYoc7gXns7RwAzWL0ZYCRKNTEs4J81fLsKVWFIPCGGXJabDcIN5FaT30EXmuY6AuXxUMEgfI87HS+r+622YSQvrqMXM4VRi3OEvoTOFqu76WfGsx3YgKhB6JcZLQIp+Tk1clycLayEBaVkrRlZF8llnPWGRfzmyq3XOg4K2o11li/lraIFNKcE87jgTOLkyj2lCAJDZnORLMTUfPAMWIz87DCmSdgg3aDmW9yeacgGX4nYKsfi2V6xzVRRM1FJ7PbnE+lAzxbTwcYkgGmbVM8F7bDiglgUTqP227ZI+WkvET9YDlWaeHRoQrpWXc0QBVEeb0IPL3W9gRUKHSYzhHN4CgogX+hwSvRj9FZXD3RKDnCBUCbDYxIyo6hj0s67frLuCqD5jHP6hvivPFIbUzrF6MhAGYEIcosBBustcbt33NZvXj/0o9Cjk1F3hPBknoqfoOJf9ficfmmvCMAkDCsrGFlmsh60zOK/UsREajkY1BBXk+zSdgRB8B5sAm/MOQH85AyVXkBjIGe8thBejhNpVodiFJi2cG0PFVTenbN8/gIPfFltE3XfmLtpnJA79++qrV6jAq6yMSdXVzEa5S+by2FKBIzq2IOuK+rMuGl0aBSG6ppQKwoKJIPiifFBBTpYbHbaBmEaKHSggtXgTMnFxevDNcz/nqbDaVA9+n8UAUOEg8SuN9f3r07h17g/zdmyuIhI3vo6z9EvgNbwLFUw2q0zRmaq5ankkuYrNyWbRjOfPsqy9CvQRUEmHufzUE7DZMHWIP2po+TLzVZZzwlCCrATYxFJkRDcdfS/EYrRgQTZNVsMiePZIvoz6kHueBeYs4TWEEhnyEsONqt85lbPIlVlxxFos+XCZvLLQfOR7TmXvrhYR0lovbxe0HPSVwC3rzvj0s46fYOO73e6eH+6WEcflwrIds76h8ca/RRR/CZJGTboyBfQo+fTUj2JIVR+BLqaQv1bP3pZ4YmpN+VmW/Yx1iYihFDCBjCkpFEruvFucIYTewdr5C2vZDPpXEJbttvPSbNHqYzz93e88CKIMlP5OZ0hmm2D1BlNJg8JB2WAAP4Ox+JwPkORaeK+tSDV8IUm7nzPpxgcEuINGzdhPieVPovmAJNGGo4Bw5+rT1DaITh1SAHMZl4O1KowYE5YXpewIF+Mv/uy/nMPx0HPz0KfjoKfjoMfgrb7Ac/7Qc/2ZHYz9Gn3gIxAdSfOz95Xg4h3j+QKix8l6cdm8j8c3+vt3dyvPVttGpOO4+Rq/C5M9XMVeVe/Qv9QPTGz+mNI3EODqa34WB+/eX3HU3ywMS3K/uCyz0u5ejr0dHnTr4rKO1bdnsHR/297mDe3d9xxgIxN+7kwvNQDCeVijjDMJdmjo7SJ+jdtMtJlF2Scal39/d6h7t7B7vR/t6ZTq75UM8Wbp3PpLM3EzcDL6oLVDKq5bc4nZeYmI6eJB7CjbuLxp3ttiYpDwH7snd82j9wFj/0EjZAL56cnPQfmSSlzPvncRNSRjNMp6Q/8U8/K3F8YP17jf66Q9C6SRpz+HS0EUgZp8Wk9/po0kfl9c38roD/drBxtuFWqEaaw7/sdC6E+g21pYR7SuqiSOKHWwG7OyrmD5hGj+DtPxWAIMRc0V2WBnH1IWlsbQMwTj0t5hjE4sIVqlNm+im6hcHh8Q2cvN/KsV3vTG+m3+zi7/74B1ASBG46t4vdT+VV5yGB8S+pePZh58GH86dPL/mEeND5qvOv2Xj6J/xP51+kTf9prru7zPORoOLhS191/g7HOPUiRcPzKuqZ2bseYp+vLllv5sFv79NiQVrwB73f1549agEkbG7PaKET1dp/Bdt21N076PZ6Hefpux1+sH9fAEa/f7B/bHF9vc8IwGi3IyHSa9XnfgfU8h5s8S/betM+4UoAk+nebr0YqC2vL8duAvFIQN9/D97DtJzp/c/9ujw9PDo51uvBPizvZX3JDGDRZZN/e8kBaH8iYe/uu6Nq6C5rP0UXTPgLSgrAfehyOA3/woul7o4z/IvbVn+nRaNQaiyll32uP8CXbcSW16wzObnsoICXO/676eEJijpzDwP9odcEQ/KCNyClfTOM11RQ/RoVXdUYUZl1d2x+a8aGKQF2KOfyb/sBjIvxB27l3/bhITzKDzfmf26ABnHWK4h47J9w/12a4l37A2yqxguAwAHMZhcpL9w9tEuMF7t8M+LvmDnvGxwxTfNu+KPpx78GmGGe8N08F4CYfDR8G7aHcN6jUbW8mAOeBHcgDa/ks/bNgCl0hxW+iF35h/mgvqH+kRmJey279ocuVtfPzBTJmzvoJ2NSu4nfybfDN/kzNneT1TfkLrlDPd8/7B0e9U6ujvOTo5N8vzjJH7n/27/qZ0PnV/MGBscKCOou4cyo4UWWRX1JUVU4W3pHe3v7B48eHe5z4OrS2Z7L3uXeJV7+LyFYNjjt/enn/w8C9J8R', 'no');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(2198, 'wp_installer_clientside_active_tab', 'featured', 'no'),
(2216, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:2:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.5.1.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.5.1.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.5.1-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.5.1-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.5.1\";s:7:\"version\";s:5:\"5.5.1\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.3\";s:15:\"partial_version\";s:0:\"\";}i:1;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.5.1.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.5.1.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.5.1-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.5.1-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.5.1\";s:7:\"version\";s:5:\"5.5.1\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.3\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}}s:12:\"last_checked\";i:1600419747;s:15:\"version_checked\";s:5:\"5.4.2\";s:12:\"translations\";a:0:{}}', 'no'),
(2219, 'weglot_version', '3.1.9', 'yes'),
(2220, 'widget_weglot-translate', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(2222, 'weglot-translate-v3', 'a:7:{s:18:\"has_first_settings\";b:1;s:23:\"show_box_first_settings\";b:0;s:13:\"menu_switcher\";a:0:{}s:11:\"custom_urls\";a:0:{}s:8:\"flag_css\";s:0:\"\";s:16:\"active_wc_reload\";b:1;s:7:\"allowed\";b:0;}', 'yes'),
(2227, 'action_scheduler_migration_status', 'complete', 'yes'),
(2280, 'woocommerce_simple_registration_register_page', '0', 'yes'),
(2281, 'woocommerce_simple_registration_name_fields', 'yes', 'yes'),
(2282, 'woocommerce_simple_registration_name_fields_required', 'yes', 'yes'),
(2295, '_transient_timeout_wc_featured_products', '1602845788', 'no'),
(2296, '_transient_wc_featured_products', 'a:0:{}', 'no'),
(2297, '_transient_timeout_wc_products_onsale', '1602845788', 'no'),
(2298, '_transient_wc_products_onsale', 'a:1:{i:0;i:39;}', 'no'),
(2299, '_transient_timeout_wc_product_loop_2d1b64a884653b0fbfae67c9714e6cd0', '1602845788', 'no'),
(2300, '_transient_wc_product_loop_2d1b64a884653b0fbfae67c9714e6cd0', 'a:2:{s:7:\"version\";s:10:\"1600193977\";s:5:\"value\";O:8:\"stdClass\":5:{s:3:\"ids\";a:1:{i:0;i:39;}s:5:\"total\";i:1;s:11:\"total_pages\";i:1;s:8:\"per_page\";i:2;s:12:\"current_page\";i:1;}}', 'no'),
(2373, '_transient_timeout_orders-all-statuses', '1600866847', 'no'),
(2374, '_transient_orders-all-statuses', 'a:2:{s:7:\"version\";s:10:\"1599546732\";s:5:\"value\";a:0:{}}', 'no'),
(2386, '_transient_is_multi_author', '0', 'yes'),
(2408, '_transient_timeout_wc_report_orders_stats_3f5393362fdfda011e8d634e16eefe7b', '1600868873', 'no'),
(2409, '_transient_timeout_wc_report_orders_stats_5fe4e740eb737f2303623517289a5b2b', '1600868873', 'no'),
(2410, '_transient_timeout_wc_report_orders_stats_2161c916cbb50d53d23e0ae8008b4e6a', '1600868873', 'no'),
(2411, '_transient_timeout_wc_report_orders_stats_982600df1f1c8c851e16fd6f2166f606', '1600868873', 'no'),
(2412, '_transient_timeout_wc_report_orders_stats_b0d071cb20e6f2bab2664b40a9ed92e3', '1600868873', 'no'),
(2413, '_transient_wc_report_orders_stats_3f5393362fdfda011e8d634e16eefe7b', 'a:2:{s:7:\"version\";s:10:\"1599546732\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":16:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:4:{i:0;a:6:{s:8:\"interval\";s:7:\"2019-38\";s:10:\"date_start\";s:19:\"2019-09-16 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-09-16 00:00:00\";s:8:\"date_end\";s:19:\"2019-09-16 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-09-16 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:1;a:6:{s:8:\"interval\";s:7:\"2019-37\";s:10:\"date_start\";s:19:\"2019-09-09 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-09-09 00:00:00\";s:8:\"date_end\";s:19:\"2019-09-15 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-09-15 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:2;a:6:{s:8:\"interval\";s:7:\"2019-36\";s:10:\"date_start\";s:19:\"2019-09-02 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-09-02 00:00:00\";s:8:\"date_end\";s:19:\"2019-09-08 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-09-08 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:3;a:6:{s:8:\"interval\";s:7:\"2019-35\";s:10:\"date_start\";s:19:\"2019-09-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-09-01 00:00:00\";s:8:\"date_end\";s:19:\"2019-09-01 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-09-01 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:4;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(2414, '_transient_wc_report_orders_stats_b0d071cb20e6f2bab2664b40a9ed92e3', 'a:2:{s:7:\"version\";s:10:\"1599546732\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":5:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:8:\"products\";i:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:16:{i:0;a:6:{s:8:\"interval\";s:10:\"2020-09-01\";s:10:\"date_start\";s:19:\"2020-09-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-01 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-01 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-01 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:1;a:6:{s:8:\"interval\";s:10:\"2020-09-02\";s:10:\"date_start\";s:19:\"2020-09-02 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-02 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-02 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-02 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:2;a:6:{s:8:\"interval\";s:10:\"2020-09-03\";s:10:\"date_start\";s:19:\"2020-09-03 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-03 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-03 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-03 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:3;a:6:{s:8:\"interval\";s:10:\"2020-09-04\";s:10:\"date_start\";s:19:\"2020-09-04 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-04 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-04 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-04 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:4;a:6:{s:8:\"interval\";s:10:\"2020-09-05\";s:10:\"date_start\";s:19:\"2020-09-05 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-05 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-05 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-05 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:5;a:6:{s:8:\"interval\";s:10:\"2020-09-06\";s:10:\"date_start\";s:19:\"2020-09-06 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-06 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-06 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-06 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:6;a:6:{s:8:\"interval\";s:10:\"2020-09-07\";s:10:\"date_start\";s:19:\"2020-09-07 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-07 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-07 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-07 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:7;a:6:{s:8:\"interval\";s:10:\"2020-09-08\";s:10:\"date_start\";s:19:\"2020-09-08 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-08 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-08 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-08 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:8;a:6:{s:8:\"interval\";s:10:\"2020-09-09\";s:10:\"date_start\";s:19:\"2020-09-09 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-09 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-09 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-09 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:9;a:6:{s:8:\"interval\";s:10:\"2020-09-10\";s:10:\"date_start\";s:19:\"2020-09-10 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-10 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-10 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-10 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:10;a:6:{s:8:\"interval\";s:10:\"2020-09-11\";s:10:\"date_start\";s:19:\"2020-09-11 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-11 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-11 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-11 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:11;a:6:{s:8:\"interval\";s:10:\"2020-09-12\";s:10:\"date_start\";s:19:\"2020-09-12 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-12 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-12 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-12 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:12;a:6:{s:8:\"interval\";s:10:\"2020-09-13\";s:10:\"date_start\";s:19:\"2020-09-13 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-13 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-13 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-13 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:13;a:6:{s:8:\"interval\";s:10:\"2020-09-14\";s:10:\"date_start\";s:19:\"2020-09-14 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-14 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-14 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-14 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:14;a:6:{s:8:\"interval\";s:10:\"2020-09-15\";s:10:\"date_start\";s:19:\"2020-09-15 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-15 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-15 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-15 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:15;a:6:{s:8:\"interval\";s:10:\"2020-09-16\";s:10:\"date_start\";s:19:\"2020-09-16 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-16 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-16 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-16 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:16;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(2415, '_transient_wc_report_orders_stats_2161c916cbb50d53d23e0ae8008b4e6a', 'a:2:{s:7:\"version\";s:10:\"1599546732\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":7:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:8:\"products\";i:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:16:{i:0;a:6:{s:8:\"interval\";s:10:\"2019-09-01\";s:10:\"date_start\";s:19:\"2019-09-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-09-01 00:00:00\";s:8:\"date_end\";s:19:\"2019-09-01 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-09-01 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:1;a:6:{s:8:\"interval\";s:10:\"2019-09-02\";s:10:\"date_start\";s:19:\"2019-09-02 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-09-02 00:00:00\";s:8:\"date_end\";s:19:\"2019-09-02 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-09-02 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:2;a:6:{s:8:\"interval\";s:10:\"2019-09-03\";s:10:\"date_start\";s:19:\"2019-09-03 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-09-03 00:00:00\";s:8:\"date_end\";s:19:\"2019-09-03 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-09-03 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:3;a:6:{s:8:\"interval\";s:10:\"2019-09-04\";s:10:\"date_start\";s:19:\"2019-09-04 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-09-04 00:00:00\";s:8:\"date_end\";s:19:\"2019-09-04 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-09-04 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:4;a:6:{s:8:\"interval\";s:10:\"2019-09-05\";s:10:\"date_start\";s:19:\"2019-09-05 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-09-05 00:00:00\";s:8:\"date_end\";s:19:\"2019-09-05 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-09-05 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:5;a:6:{s:8:\"interval\";s:10:\"2019-09-06\";s:10:\"date_start\";s:19:\"2019-09-06 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-09-06 00:00:00\";s:8:\"date_end\";s:19:\"2019-09-06 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-09-06 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:6;a:6:{s:8:\"interval\";s:10:\"2019-09-07\";s:10:\"date_start\";s:19:\"2019-09-07 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-09-07 00:00:00\";s:8:\"date_end\";s:19:\"2019-09-07 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-09-07 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:7;a:6:{s:8:\"interval\";s:10:\"2019-09-08\";s:10:\"date_start\";s:19:\"2019-09-08 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-09-08 00:00:00\";s:8:\"date_end\";s:19:\"2019-09-08 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-09-08 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:8;a:6:{s:8:\"interval\";s:10:\"2019-09-09\";s:10:\"date_start\";s:19:\"2019-09-09 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-09-09 00:00:00\";s:8:\"date_end\";s:19:\"2019-09-09 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-09-09 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:9;a:6:{s:8:\"interval\";s:10:\"2019-09-10\";s:10:\"date_start\";s:19:\"2019-09-10 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-09-10 00:00:00\";s:8:\"date_end\";s:19:\"2019-09-10 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-09-10 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:10;a:6:{s:8:\"interval\";s:10:\"2019-09-11\";s:10:\"date_start\";s:19:\"2019-09-11 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-09-11 00:00:00\";s:8:\"date_end\";s:19:\"2019-09-11 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-09-11 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:11;a:6:{s:8:\"interval\";s:10:\"2019-09-12\";s:10:\"date_start\";s:19:\"2019-09-12 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-09-12 00:00:00\";s:8:\"date_end\";s:19:\"2019-09-12 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-09-12 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:12;a:6:{s:8:\"interval\";s:10:\"2019-09-13\";s:10:\"date_start\";s:19:\"2019-09-13 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-09-13 00:00:00\";s:8:\"date_end\";s:19:\"2019-09-13 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-09-13 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:13;a:6:{s:8:\"interval\";s:10:\"2019-09-14\";s:10:\"date_start\";s:19:\"2019-09-14 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-09-14 00:00:00\";s:8:\"date_end\";s:19:\"2019-09-14 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-09-14 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:14;a:6:{s:8:\"interval\";s:10:\"2019-09-15\";s:10:\"date_start\";s:19:\"2019-09-15 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-09-15 00:00:00\";s:8:\"date_end\";s:19:\"2019-09-15 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-09-15 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:15;a:6:{s:8:\"interval\";s:10:\"2019-09-16\";s:10:\"date_start\";s:19:\"2019-09-16 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-09-16 00:00:00\";s:8:\"date_end\";s:19:\"2019-09-16 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-09-16 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:16;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(2416, '_transient_wc_report_orders_stats_5fe4e740eb737f2303623517289a5b2b', 'a:2:{s:7:\"version\";s:10:\"1599546732\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":16:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:3:{i:0;a:6:{s:8:\"interval\";s:7:\"2020-38\";s:10:\"date_start\";s:19:\"2020-09-14 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-14 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-16 19:17:00\";s:12:\"date_end_gmt\";s:19:\"2020-09-16 19:17:00\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:1;a:6:{s:8:\"interval\";s:7:\"2020-37\";s:10:\"date_start\";s:19:\"2020-09-07 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-07 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-13 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-13 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:2;a:6:{s:8:\"interval\";s:7:\"2020-36\";s:10:\"date_start\";s:19:\"2020-09-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-01 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-06 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-06 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:3;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(2417, '_transient_wc_report_orders_stats_982600df1f1c8c851e16fd6f2166f606', 'a:2:{s:7:\"version\";s:10:\"1599546732\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":7:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:8:\"products\";i:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:16:{i:0;a:6:{s:8:\"interval\";s:10:\"2020-09-01\";s:10:\"date_start\";s:19:\"2020-09-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-01 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-01 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-01 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:1;a:6:{s:8:\"interval\";s:10:\"2020-09-02\";s:10:\"date_start\";s:19:\"2020-09-02 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-02 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-02 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-02 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:2;a:6:{s:8:\"interval\";s:10:\"2020-09-03\";s:10:\"date_start\";s:19:\"2020-09-03 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-03 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-03 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-03 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:3;a:6:{s:8:\"interval\";s:10:\"2020-09-04\";s:10:\"date_start\";s:19:\"2020-09-04 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-04 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-04 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-04 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:4;a:6:{s:8:\"interval\";s:10:\"2020-09-05\";s:10:\"date_start\";s:19:\"2020-09-05 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-05 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-05 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-05 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:5;a:6:{s:8:\"interval\";s:10:\"2020-09-06\";s:10:\"date_start\";s:19:\"2020-09-06 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-06 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-06 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-06 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:6;a:6:{s:8:\"interval\";s:10:\"2020-09-07\";s:10:\"date_start\";s:19:\"2020-09-07 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-07 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-07 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-07 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:7;a:6:{s:8:\"interval\";s:10:\"2020-09-08\";s:10:\"date_start\";s:19:\"2020-09-08 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-08 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-08 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-08 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:8;a:6:{s:8:\"interval\";s:10:\"2020-09-09\";s:10:\"date_start\";s:19:\"2020-09-09 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-09 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-09 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-09 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:9;a:6:{s:8:\"interval\";s:10:\"2020-09-10\";s:10:\"date_start\";s:19:\"2020-09-10 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-10 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-10 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-10 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:10;a:6:{s:8:\"interval\";s:10:\"2020-09-11\";s:10:\"date_start\";s:19:\"2020-09-11 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-11 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-11 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-11 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:11;a:6:{s:8:\"interval\";s:10:\"2020-09-12\";s:10:\"date_start\";s:19:\"2020-09-12 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-12 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-12 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-12 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:12;a:6:{s:8:\"interval\";s:10:\"2020-09-13\";s:10:\"date_start\";s:19:\"2020-09-13 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-13 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-13 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-13 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:13;a:6:{s:8:\"interval\";s:10:\"2020-09-14\";s:10:\"date_start\";s:19:\"2020-09-14 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-14 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-14 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-14 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:14;a:6:{s:8:\"interval\";s:10:\"2020-09-15\";s:10:\"date_start\";s:19:\"2020-09-15 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-15 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-15 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-15 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:15;a:6:{s:8:\"interval\";s:10:\"2020-09-16\";s:10:\"date_start\";s:19:\"2020-09-16 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-16 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-16 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-16 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:16;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(2418, '_transient_timeout_wc_report_orders_stats_807affb24ba2df6da448521861c87ca8', '1600868874', 'no'),
(2419, '_transient_timeout_wc_report_orders_stats_8c2dc015dcfe0ff7cfdc6fe508337bd1', '1600868874', 'no'),
(2420, '_transient_timeout_wc_report_orders_stats_751a0f59f51e41c5842a1a63edcde95c', '1600868874', 'no'),
(2421, '_transient_wc_report_orders_stats_807affb24ba2df6da448521861c87ca8', 'a:2:{s:7:\"version\";s:10:\"1599546732\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":16:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:4:{i:0;a:6:{s:8:\"interval\";s:7:\"2019-38\";s:10:\"date_start\";s:19:\"2019-09-16 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-09-16 00:00:00\";s:8:\"date_end\";s:19:\"2019-09-16 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-09-16 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:1;a:6:{s:8:\"interval\";s:7:\"2019-37\";s:10:\"date_start\";s:19:\"2019-09-09 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-09-09 00:00:00\";s:8:\"date_end\";s:19:\"2019-09-15 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-09-15 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:2;a:6:{s:8:\"interval\";s:7:\"2019-36\";s:10:\"date_start\";s:19:\"2019-09-02 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-09-02 00:00:00\";s:8:\"date_end\";s:19:\"2019-09-08 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-09-08 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:3;a:6:{s:8:\"interval\";s:7:\"2019-35\";s:10:\"date_start\";s:19:\"2019-09-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-09-01 00:00:00\";s:8:\"date_end\";s:19:\"2019-09-01 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-09-01 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:4;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(2422, '_transient_wc_report_orders_stats_8c2dc015dcfe0ff7cfdc6fe508337bd1', 'a:2:{s:7:\"version\";s:10:\"1599546732\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":5:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:8:\"products\";i:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:16:{i:0;a:6:{s:8:\"interval\";s:10:\"2019-09-01\";s:10:\"date_start\";s:19:\"2019-09-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-09-01 00:00:00\";s:8:\"date_end\";s:19:\"2019-09-01 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-09-01 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:1;a:6:{s:8:\"interval\";s:10:\"2019-09-02\";s:10:\"date_start\";s:19:\"2019-09-02 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-09-02 00:00:00\";s:8:\"date_end\";s:19:\"2019-09-02 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-09-02 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:2;a:6:{s:8:\"interval\";s:10:\"2019-09-03\";s:10:\"date_start\";s:19:\"2019-09-03 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-09-03 00:00:00\";s:8:\"date_end\";s:19:\"2019-09-03 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-09-03 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:3;a:6:{s:8:\"interval\";s:10:\"2019-09-04\";s:10:\"date_start\";s:19:\"2019-09-04 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-09-04 00:00:00\";s:8:\"date_end\";s:19:\"2019-09-04 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-09-04 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:4;a:6:{s:8:\"interval\";s:10:\"2019-09-05\";s:10:\"date_start\";s:19:\"2019-09-05 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-09-05 00:00:00\";s:8:\"date_end\";s:19:\"2019-09-05 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-09-05 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:5;a:6:{s:8:\"interval\";s:10:\"2019-09-06\";s:10:\"date_start\";s:19:\"2019-09-06 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-09-06 00:00:00\";s:8:\"date_end\";s:19:\"2019-09-06 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-09-06 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:6;a:6:{s:8:\"interval\";s:10:\"2019-09-07\";s:10:\"date_start\";s:19:\"2019-09-07 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-09-07 00:00:00\";s:8:\"date_end\";s:19:\"2019-09-07 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-09-07 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:7;a:6:{s:8:\"interval\";s:10:\"2019-09-08\";s:10:\"date_start\";s:19:\"2019-09-08 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-09-08 00:00:00\";s:8:\"date_end\";s:19:\"2019-09-08 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-09-08 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:8;a:6:{s:8:\"interval\";s:10:\"2019-09-09\";s:10:\"date_start\";s:19:\"2019-09-09 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-09-09 00:00:00\";s:8:\"date_end\";s:19:\"2019-09-09 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-09-09 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:9;a:6:{s:8:\"interval\";s:10:\"2019-09-10\";s:10:\"date_start\";s:19:\"2019-09-10 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-09-10 00:00:00\";s:8:\"date_end\";s:19:\"2019-09-10 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-09-10 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:10;a:6:{s:8:\"interval\";s:10:\"2019-09-11\";s:10:\"date_start\";s:19:\"2019-09-11 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-09-11 00:00:00\";s:8:\"date_end\";s:19:\"2019-09-11 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-09-11 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:11;a:6:{s:8:\"interval\";s:10:\"2019-09-12\";s:10:\"date_start\";s:19:\"2019-09-12 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-09-12 00:00:00\";s:8:\"date_end\";s:19:\"2019-09-12 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-09-12 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:12;a:6:{s:8:\"interval\";s:10:\"2019-09-13\";s:10:\"date_start\";s:19:\"2019-09-13 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-09-13 00:00:00\";s:8:\"date_end\";s:19:\"2019-09-13 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-09-13 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:13;a:6:{s:8:\"interval\";s:10:\"2019-09-14\";s:10:\"date_start\";s:19:\"2019-09-14 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-09-14 00:00:00\";s:8:\"date_end\";s:19:\"2019-09-14 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-09-14 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:14;a:6:{s:8:\"interval\";s:10:\"2019-09-15\";s:10:\"date_start\";s:19:\"2019-09-15 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-09-15 00:00:00\";s:8:\"date_end\";s:19:\"2019-09-15 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-09-15 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:15;a:6:{s:8:\"interval\";s:10:\"2019-09-16\";s:10:\"date_start\";s:19:\"2019-09-16 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2019-09-16 00:00:00\";s:8:\"date_end\";s:19:\"2019-09-16 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2019-09-16 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:16;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(2423, '_transient_wc_report_orders_stats_751a0f59f51e41c5842a1a63edcde95c', 'a:2:{s:7:\"version\";s:10:\"1599546732\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":16:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:3:{i:0;a:6:{s:8:\"interval\";s:7:\"2020-38\";s:10:\"date_start\";s:19:\"2020-09-14 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-14 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-16 19:17:00\";s:12:\"date_end_gmt\";s:19:\"2020-09-16 19:17:00\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:1;a:6:{s:8:\"interval\";s:7:\"2020-37\";s:10:\"date_start\";s:19:\"2020-09-07 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-07 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-13 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-13 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:2;a:6:{s:8:\"interval\";s:7:\"2020-36\";s:10:\"date_start\";s:19:\"2020-09-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-01 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-06 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-06 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:3;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(2442, '_transient_timeout_wc_report_orders_stats_42056dd0c881f7642972ff56b0f7aa7e', '1600870675', 'no'),
(2444, '_transient_wc_report_orders_stats_42056dd0c881f7642972ff56b0f7aa7e', 'a:2:{s:7:\"version\";s:10:\"1599546732\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":16:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:3:{i:0;a:6:{s:8:\"interval\";s:7:\"2020-38\";s:10:\"date_start\";s:19:\"2020-09-14 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-14 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-16 19:47:00\";s:12:\"date_end_gmt\";s:19:\"2020-09-16 19:47:00\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:1;a:6:{s:8:\"interval\";s:7:\"2020-37\";s:10:\"date_start\";s:19:\"2020-09-07 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-07 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-13 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-13 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:2;a:6:{s:8:\"interval\";s:7:\"2020-36\";s:10:\"date_start\";s:19:\"2020-09-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-01 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-06 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-06 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:3;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(2445, '_transient_timeout_wc_report_orders_stats_bb50d769f9a199511ce270144f90b205', '1600870676', 'no'),
(2446, '_transient_wc_report_orders_stats_bb50d769f9a199511ce270144f90b205', 'a:2:{s:7:\"version\";s:10:\"1599546732\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":16:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:3:{i:0;a:6:{s:8:\"interval\";s:7:\"2020-38\";s:10:\"date_start\";s:19:\"2020-09-14 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-14 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-16 19:47:00\";s:12:\"date_end_gmt\";s:19:\"2020-09-16 19:47:00\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:1;a:6:{s:8:\"interval\";s:7:\"2020-37\";s:10:\"date_start\";s:19:\"2020-09-07 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-07 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-13 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-13 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:2;a:6:{s:8:\"interval\";s:7:\"2020-36\";s:10:\"date_start\";s:19:\"2020-09-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-01 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-06 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-06 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:3;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(2448, '_transient_timeout_wc_report_orders_stats_2964709f24dbed17a34508b1403488a3', '1600872477', 'no');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(2449, '_transient_wc_report_orders_stats_2964709f24dbed17a34508b1403488a3', 'a:2:{s:7:\"version\";s:10:\"1599546732\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":16:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:3:{i:0;a:6:{s:8:\"interval\";s:7:\"2020-38\";s:10:\"date_start\";s:19:\"2020-09-14 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-14 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-16 20:17:00\";s:12:\"date_end_gmt\";s:19:\"2020-09-16 20:17:00\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:1;a:6:{s:8:\"interval\";s:7:\"2020-37\";s:10:\"date_start\";s:19:\"2020-09-07 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-07 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-13 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-13 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:2;a:6:{s:8:\"interval\";s:7:\"2020-36\";s:10:\"date_start\";s:19:\"2020-09-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-01 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-06 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-06 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:3;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(2450, '_transient_timeout_wc_report_orders_stats_b80f060e62a3d9d4a657b0f229c82a9a', '1600872478', 'no'),
(2451, '_transient_wc_report_orders_stats_b80f060e62a3d9d4a657b0f229c82a9a', 'a:2:{s:7:\"version\";s:10:\"1599546732\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":16:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:3:{i:0;a:6:{s:8:\"interval\";s:7:\"2020-38\";s:10:\"date_start\";s:19:\"2020-09-14 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-14 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-16 20:17:00\";s:12:\"date_end_gmt\";s:19:\"2020-09-16 20:17:00\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:1;a:6:{s:8:\"interval\";s:7:\"2020-37\";s:10:\"date_start\";s:19:\"2020-09-07 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-07 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-13 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-13 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:2;a:6:{s:8:\"interval\";s:7:\"2020-36\";s:10:\"date_start\";s:19:\"2020-09-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-01 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-06 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-06 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:3;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(2453, '_transient_timeout_wc_report_orders_stats_b19e998b2b4a763ce82587be227606db', '1600874277', 'no'),
(2454, '_transient_wc_report_orders_stats_b19e998b2b4a763ce82587be227606db', 'a:2:{s:7:\"version\";s:10:\"1599546732\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":16:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:3:{i:0;a:6:{s:8:\"interval\";s:7:\"2020-38\";s:10:\"date_start\";s:19:\"2020-09-14 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-14 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-16 20:47:00\";s:12:\"date_end_gmt\";s:19:\"2020-09-16 20:47:00\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:1;a:6:{s:8:\"interval\";s:7:\"2020-37\";s:10:\"date_start\";s:19:\"2020-09-07 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-07 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-13 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-13 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:2;a:6:{s:8:\"interval\";s:7:\"2020-36\";s:10:\"date_start\";s:19:\"2020-09-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-01 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-06 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-06 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:3;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(2455, '_transient_timeout_wc_report_orders_stats_44c12a1a372c3d104c1d5c3129a7ed44', '1600874278', 'no'),
(2456, '_transient_wc_report_orders_stats_44c12a1a372c3d104c1d5c3129a7ed44', 'a:2:{s:7:\"version\";s:10:\"1599546732\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":16:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:3:{i:0;a:6:{s:8:\"interval\";s:7:\"2020-38\";s:10:\"date_start\";s:19:\"2020-09-14 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-14 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-16 20:47:00\";s:12:\"date_end_gmt\";s:19:\"2020-09-16 20:47:00\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:1;a:6:{s:8:\"interval\";s:7:\"2020-37\";s:10:\"date_start\";s:19:\"2020-09-07 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-07 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-13 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-13 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:2;a:6:{s:8:\"interval\";s:7:\"2020-36\";s:10:\"date_start\";s:19:\"2020-09-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-01 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-06 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-06 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:3;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(2457, '_transient_timeout_wc_report_orders_stats_138745618014fff46b4abfde3887e200', '1600874280', 'no'),
(2458, '_transient_wc_report_orders_stats_138745618014fff46b4abfde3887e200', 'a:2:{s:7:\"version\";s:10:\"1599546732\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":16:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:3:{i:0;a:6:{s:8:\"interval\";s:7:\"2020-38\";s:10:\"date_start\";s:19:\"2020-09-14 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-14 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-16 20:48:00\";s:12:\"date_end_gmt\";s:19:\"2020-09-16 20:48:00\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:1;a:6:{s:8:\"interval\";s:7:\"2020-37\";s:10:\"date_start\";s:19:\"2020-09-07 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-07 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-13 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-13 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:2;a:6:{s:8:\"interval\";s:7:\"2020-36\";s:10:\"date_start\";s:19:\"2020-09-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-01 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-06 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-06 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:3;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(2459, '_transient_timeout_wc_report_orders_stats_7461b1a15937cc739155605806bcffeb', '1600874280', 'no'),
(2460, '_transient_wc_report_orders_stats_7461b1a15937cc739155605806bcffeb', 'a:2:{s:7:\"version\";s:10:\"1599546732\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":16:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:3:{i:0;a:6:{s:8:\"interval\";s:7:\"2020-38\";s:10:\"date_start\";s:19:\"2020-09-14 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-14 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-16 20:48:00\";s:12:\"date_end_gmt\";s:19:\"2020-09-16 20:48:00\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:1;a:6:{s:8:\"interval\";s:7:\"2020-37\";s:10:\"date_start\";s:19:\"2020-09-07 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-07 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-13 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-13 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:2;a:6:{s:8:\"interval\";s:7:\"2020-36\";s:10:\"date_start\";s:19:\"2020-09-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-01 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-06 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-06 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:3;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(2472, '_transient_timeout_wc_report_orders_stats_a638db082f0e3663ae77b3ecb09ddb98', '1600876078', 'no'),
(2473, '_transient_wc_report_orders_stats_a638db082f0e3663ae77b3ecb09ddb98', 'a:2:{s:7:\"version\";s:10:\"1599546732\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":16:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:3:{i:0;a:6:{s:8:\"interval\";s:7:\"2020-38\";s:10:\"date_start\";s:19:\"2020-09-14 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-14 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-16 21:17:00\";s:12:\"date_end_gmt\";s:19:\"2020-09-16 21:17:00\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:1;a:6:{s:8:\"interval\";s:7:\"2020-37\";s:10:\"date_start\";s:19:\"2020-09-07 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-07 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-13 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-13 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:2;a:6:{s:8:\"interval\";s:7:\"2020-36\";s:10:\"date_start\";s:19:\"2020-09-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-01 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-06 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-06 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:3;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(2474, '_transient_timeout_wc_report_orders_stats_d244a51edf0be1a5b555b398255866f3', '1600876078', 'no'),
(2475, '_transient_wc_report_orders_stats_d244a51edf0be1a5b555b398255866f3', 'a:2:{s:7:\"version\";s:10:\"1599546732\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":16:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:3:{i:0;a:6:{s:8:\"interval\";s:7:\"2020-38\";s:10:\"date_start\";s:19:\"2020-09-14 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-14 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-16 21:17:00\";s:12:\"date_end_gmt\";s:19:\"2020-09-16 21:17:00\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:1;a:6:{s:8:\"interval\";s:7:\"2020-37\";s:10:\"date_start\";s:19:\"2020-09-07 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-07 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-13 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-13 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:2;a:6:{s:8:\"interval\";s:7:\"2020-36\";s:10:\"date_start\";s:19:\"2020-09-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-01 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-06 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-06 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:3;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(2476, '_transient_timeout_wc_report_orders_stats_39d8b49a333585301f734349f2893793', '1600876081', 'no'),
(2477, '_transient_wc_report_orders_stats_39d8b49a333585301f734349f2893793', 'a:2:{s:7:\"version\";s:10:\"1599546732\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":16:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:3:{i:0;a:6:{s:8:\"interval\";s:7:\"2020-38\";s:10:\"date_start\";s:19:\"2020-09-14 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-14 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-16 21:18:00\";s:12:\"date_end_gmt\";s:19:\"2020-09-16 21:18:00\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:1;a:6:{s:8:\"interval\";s:7:\"2020-37\";s:10:\"date_start\";s:19:\"2020-09-07 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-07 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-13 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-13 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:2;a:6:{s:8:\"interval\";s:7:\"2020-36\";s:10:\"date_start\";s:19:\"2020-09-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-01 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-06 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-06 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:3;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(2478, '_transient_timeout_wc_report_orders_stats_9bb39fa0577e666a1e39f534bc4d848c', '1600876081', 'no'),
(2479, '_transient_wc_report_orders_stats_9bb39fa0577e666a1e39f534bc4d848c', 'a:2:{s:7:\"version\";s:10:\"1599546732\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":16:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:3:{i:0;a:6:{s:8:\"interval\";s:7:\"2020-38\";s:10:\"date_start\";s:19:\"2020-09-14 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-14 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-16 21:18:00\";s:12:\"date_end_gmt\";s:19:\"2020-09-16 21:18:00\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:1;a:6:{s:8:\"interval\";s:7:\"2020-37\";s:10:\"date_start\";s:19:\"2020-09-07 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-07 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-13 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-13 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:2;a:6:{s:8:\"interval\";s:7:\"2020-36\";s:10:\"date_start\";s:19:\"2020-09-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-01 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-06 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-06 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:3;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(2483, '_transient_timeout_wc_report_orders_stats_902b37e6cad774b73e0e4bb5f6538122', '1600877879', 'no'),
(2484, '_transient_wc_report_orders_stats_902b37e6cad774b73e0e4bb5f6538122', 'a:2:{s:7:\"version\";s:10:\"1599546732\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":16:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:3:{i:0;a:6:{s:8:\"interval\";s:7:\"2020-38\";s:10:\"date_start\";s:19:\"2020-09-14 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-14 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-16 21:47:00\";s:12:\"date_end_gmt\";s:19:\"2020-09-16 21:47:00\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:1;a:6:{s:8:\"interval\";s:7:\"2020-37\";s:10:\"date_start\";s:19:\"2020-09-07 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-07 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-13 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-13 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:2;a:6:{s:8:\"interval\";s:7:\"2020-36\";s:10:\"date_start\";s:19:\"2020-09-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-01 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-06 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-06 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:3;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(2485, '_transient_timeout_wc_report_orders_stats_e0929ac33cdbf691bcd2d9e73baf77ef', '1600877880', 'no'),
(2486, '_transient_wc_report_orders_stats_e0929ac33cdbf691bcd2d9e73baf77ef', 'a:2:{s:7:\"version\";s:10:\"1599546732\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":16:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:3:{i:0;a:6:{s:8:\"interval\";s:7:\"2020-38\";s:10:\"date_start\";s:19:\"2020-09-14 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-14 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-16 21:47:00\";s:12:\"date_end_gmt\";s:19:\"2020-09-16 21:47:00\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:1;a:6:{s:8:\"interval\";s:7:\"2020-37\";s:10:\"date_start\";s:19:\"2020-09-07 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-07 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-13 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-13 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:2;a:6:{s:8:\"interval\";s:7:\"2020-36\";s:10:\"date_start\";s:19:\"2020-09-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-01 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-06 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-06 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:3;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(2487, '_transient_timeout_wc_report_orders_stats_995c280858b7f1a65514a8c4a6eae96e', '1600877881', 'no'),
(2488, '_transient_wc_report_orders_stats_995c280858b7f1a65514a8c4a6eae96e', 'a:2:{s:7:\"version\";s:10:\"1599546732\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":16:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:3:{i:0;a:6:{s:8:\"interval\";s:7:\"2020-38\";s:10:\"date_start\";s:19:\"2020-09-14 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-14 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-16 21:48:00\";s:12:\"date_end_gmt\";s:19:\"2020-09-16 21:48:00\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:1;a:6:{s:8:\"interval\";s:7:\"2020-37\";s:10:\"date_start\";s:19:\"2020-09-07 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-07 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-13 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-13 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:2;a:6:{s:8:\"interval\";s:7:\"2020-36\";s:10:\"date_start\";s:19:\"2020-09-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-01 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-06 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-06 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:3;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(2489, '_transient_timeout_wc_report_orders_stats_dc1efc5df0c7fe093fbfdbcaffac70a0', '1600877881', 'no'),
(2490, '_transient_wc_report_orders_stats_dc1efc5df0c7fe093fbfdbcaffac70a0', 'a:2:{s:7:\"version\";s:10:\"1599546732\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":16:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:3:{i:0;a:6:{s:8:\"interval\";s:7:\"2020-38\";s:10:\"date_start\";s:19:\"2020-09-14 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-14 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-16 21:48:00\";s:12:\"date_end_gmt\";s:19:\"2020-09-16 21:48:00\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:1;a:6:{s:8:\"interval\";s:7:\"2020-37\";s:10:\"date_start\";s:19:\"2020-09-07 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-07 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-13 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-13 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:2;a:6:{s:8:\"interval\";s:7:\"2020-36\";s:10:\"date_start\";s:19:\"2020-09-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-01 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-06 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-06 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:3;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(2511, '_transient_timeout_wc_report_orders_stats_b878d2d6c8b3e79b369b6a3cdb5dbfb2', '1600883021', 'no'),
(2512, '_transient_wc_report_orders_stats_b878d2d6c8b3e79b369b6a3cdb5dbfb2', 'a:2:{s:7:\"version\";s:10:\"1599546732\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":16:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:3:{i:0;a:6:{s:8:\"interval\";s:7:\"2020-38\";s:10:\"date_start\";s:19:\"2020-09-14 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-14 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-16 23:13:00\";s:12:\"date_end_gmt\";s:19:\"2020-09-16 23:13:00\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:1;a:6:{s:8:\"interval\";s:7:\"2020-37\";s:10:\"date_start\";s:19:\"2020-09-07 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-07 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-13 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-13 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:2;a:6:{s:8:\"interval\";s:7:\"2020-36\";s:10:\"date_start\";s:19:\"2020-09-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-01 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-06 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-06 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:3;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(2513, '_transient_timeout_wc_report_orders_stats_d1e847c768657dc3ba20bc6c7523696b', '1600883021', 'no'),
(2514, '_transient_wc_report_orders_stats_d1e847c768657dc3ba20bc6c7523696b', 'a:2:{s:7:\"version\";s:10:\"1599546732\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":16:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:3:{i:0;a:6:{s:8:\"interval\";s:7:\"2020-38\";s:10:\"date_start\";s:19:\"2020-09-14 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-14 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-16 23:13:00\";s:12:\"date_end_gmt\";s:19:\"2020-09-16 23:13:00\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:1;a:6:{s:8:\"interval\";s:7:\"2020-37\";s:10:\"date_start\";s:19:\"2020-09-07 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-07 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-13 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-13 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}i:2;a:6:{s:8:\"interval\";s:7:\"2020-36\";s:10:\"date_start\";s:19:\"2020-09-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-09-01 00:00:00\";s:8:\"date_end\";s:19:\"2020-09-06 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-09-06 23:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:23:\"num_returning_customers\";i:0;s:17:\"num_new_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:3;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(2550, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1600419747;s:7:\"checked\";a:5:{s:10:\"storefront\";s:5:\"2.7.0\";s:14:\"twentynineteen\";s:3:\"1.5\";s:15:\"twentyseventeen\";s:3:\"2.3\";s:12:\"twentytwenty\";s:3:\"1.2\";s:8:\"wp-store\";s:5:\"1.1.5\";}s:8:\"response\";a:3:{s:14:\"twentynineteen\";a:6:{s:5:\"theme\";s:14:\"twentynineteen\";s:11:\"new_version\";s:3:\"1.7\";s:3:\"url\";s:44:\"https://wordpress.org/themes/twentynineteen/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/theme/twentynineteen.1.7.zip\";s:8:\"requires\";s:5:\"4.9.6\";s:12:\"requires_php\";s:5:\"5.2.4\";}s:15:\"twentyseventeen\";a:6:{s:5:\"theme\";s:15:\"twentyseventeen\";s:11:\"new_version\";s:3:\"2.4\";s:3:\"url\";s:45:\"https://wordpress.org/themes/twentyseventeen/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/theme/twentyseventeen.2.4.zip\";s:8:\"requires\";s:3:\"4.7\";s:12:\"requires_php\";s:5:\"5.2.4\";}s:12:\"twentytwenty\";a:6:{s:5:\"theme\";s:12:\"twentytwenty\";s:11:\"new_version\";s:3:\"1.5\";s:3:\"url\";s:42:\"https://wordpress.org/themes/twentytwenty/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/theme/twentytwenty.1.5.zip\";s:8:\"requires\";s:3:\"4.7\";s:12:\"requires_php\";s:5:\"5.2.4\";}}s:12:\"translations\";a:0:{}}', 'no');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(2551, '_site_transient_update_plugins', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1600419746;s:8:\"response\";a:3:{s:19:\"akismet/akismet.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:5:\"4.1.6\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/akismet.4.1.6.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.5.1\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:27:\"woocommerce/woocommerce.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:25:\"w.org/plugins/woocommerce\";s:4:\"slug\";s:11:\"woocommerce\";s:6:\"plugin\";s:27:\"woocommerce/woocommerce.php\";s:11:\"new_version\";s:5:\"4.5.2\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/woocommerce/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/woocommerce.4.5.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/woocommerce/assets/icon-256x256.png?rev=2366418\";s:2:\"1x\";s:64:\"https://ps.w.org/woocommerce/assets/icon-128x128.png?rev=2366418\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/woocommerce/assets/banner-1544x500.png?rev=2366418\";s:2:\"1x\";s:66:\"https://ps.w.org/woocommerce/assets/banner-772x250.png?rev=2366418\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.5.1\";s:12:\"requires_php\";s:3:\"7.0\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:27:\"woo-product-slider/main.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:32:\"w.org/plugins/woo-product-slider\";s:4:\"slug\";s:18:\"woo-product-slider\";s:6:\"plugin\";s:27:\"woo-product-slider/main.php\";s:11:\"new_version\";s:5:\"2.2.3\";s:3:\"url\";s:49:\"https://wordpress.org/plugins/woo-product-slider/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/woo-product-slider.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:71:\"https://ps.w.org/woo-product-slider/assets/icon-256x256.png?rev=2081375\";s:2:\"1x\";s:71:\"https://ps.w.org/woo-product-slider/assets/icon-128x128.png?rev=2081375\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:73:\"https://ps.w.org/woo-product-slider/assets/banner-772x250.png?rev=1763046\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.5.1\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:9:{s:9:\"hello.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:9:\"hello.php\";s:11:\"new_version\";s:5:\"1.7.2\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/hello-dolly.1.7.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=2052855\";s:2:\"1x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=2052855\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:66:\"https://ps.w.org/hello-dolly/assets/banner-772x250.jpg?rev=2052855\";}s:11:\"banners_rtl\";a:0:{}}s:31:\"multilanguage/multilanguage.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:27:\"w.org/plugins/multilanguage\";s:4:\"slug\";s:13:\"multilanguage\";s:6:\"plugin\";s:31:\"multilanguage/multilanguage.php\";s:11:\"new_version\";s:5:\"1.3.6\";s:3:\"url\";s:44:\"https://wordpress.org/plugins/multilanguage/\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/plugin/multilanguage.1.3.6.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:66:\"https://ps.w.org/multilanguage/assets/icon-256x256.png?rev=2293478\";s:2:\"1x\";s:66:\"https://ps.w.org/multilanguage/assets/icon-128x128.png?rev=2293478\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/multilanguage/assets/banner-1544x500.jpg?rev=2293478\";s:2:\"1x\";s:68:\"https://ps.w.org/multilanguage/assets/banner-772x250.jpg?rev=2293478\";}s:11:\"banners_rtl\";a:0:{}}s:67:\"woocommerce-simple-registration/woocommerce-simple-registration.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:45:\"w.org/plugins/woocommerce-simple-registration\";s:4:\"slug\";s:31:\"woocommerce-simple-registration\";s:6:\"plugin\";s:67:\"woocommerce-simple-registration/woocommerce-simple-registration.php\";s:11:\"new_version\";s:5:\"1.5.2\";s:3:\"url\";s:62:\"https://wordpress.org/plugins/woocommerce-simple-registration/\";s:7:\"package\";s:80:\"https://downloads.wordpress.org/plugin/woocommerce-simple-registration.1.5.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:84:\"https://ps.w.org/woocommerce-simple-registration/assets/icon-256x256.png?rev=1488276\";s:2:\"1x\";s:84:\"https://ps.w.org/woocommerce-simple-registration/assets/icon-256x256.png?rev=1488276\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:87:\"https://ps.w.org/woocommerce-simple-registration/assets/banner-1544x500.png?rev=1488276\";s:2:\"1x\";s:86:\"https://ps.w.org/woocommerce-simple-registration/assets/banner-772x250.png?rev=1488276\";}s:11:\"banners_rtl\";a:0:{}}s:37:\"translatepress-multilingual/index.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:41:\"w.org/plugins/translatepress-multilingual\";s:4:\"slug\";s:27:\"translatepress-multilingual\";s:6:\"plugin\";s:37:\"translatepress-multilingual/index.php\";s:11:\"new_version\";s:5:\"1.8.2\";s:3:\"url\";s:58:\"https://wordpress.org/plugins/translatepress-multilingual/\";s:7:\"package\";s:76:\"https://downloads.wordpress.org/plugin/translatepress-multilingual.1.8.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:80:\"https://ps.w.org/translatepress-multilingual/assets/icon-256x256.png?rev=1722670\";s:2:\"1x\";s:80:\"https://ps.w.org/translatepress-multilingual/assets/icon-128x128.png?rev=1722670\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:83:\"https://ps.w.org/translatepress-multilingual/assets/banner-1544x500.png?rev=2312348\";s:2:\"1x\";s:82:\"https://ps.w.org/translatepress-multilingual/assets/banner-772x250.png?rev=2312348\";}s:11:\"banners_rtl\";a:0:{}}s:17:\"weglot/weglot.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:20:\"w.org/plugins/weglot\";s:4:\"slug\";s:6:\"weglot\";s:6:\"plugin\";s:17:\"weglot/weglot.php\";s:11:\"new_version\";s:5:\"3.1.9\";s:3:\"url\";s:37:\"https://wordpress.org/plugins/weglot/\";s:7:\"package\";s:55:\"https://downloads.wordpress.org/plugin/weglot.3.1.9.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/weglot/assets/icon-256x256.png?rev=2186774\";s:2:\"1x\";s:59:\"https://ps.w.org/weglot/assets/icon-128x128.png?rev=2186774\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:62:\"https://ps.w.org/weglot/assets/banner-1544x500.png?rev=2186774\";s:2:\"1x\";s:61:\"https://ps.w.org/weglot/assets/banner-772x250.png?rev=2186774\";}s:11:\"banners_rtl\";a:0:{}}s:36:\"woo-best-selling-products/woobsp.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:39:\"w.org/plugins/woo-best-selling-products\";s:4:\"slug\";s:25:\"woo-best-selling-products\";s:6:\"plugin\";s:36:\"woo-best-selling-products/woobsp.php\";s:11:\"new_version\";s:5:\"1.2.0\";s:3:\"url\";s:56:\"https://wordpress.org/plugins/woo-best-selling-products/\";s:7:\"package\";s:68:\"https://downloads.wordpress.org/plugin/woo-best-selling-products.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:78:\"https://ps.w.org/woo-best-selling-products/assets/icon-256x256.jpg?rev=2068307\";s:2:\"1x\";s:78:\"https://ps.w.org/woo-best-selling-products/assets/icon-128x128.jpg?rev=2068307\";}s:7:\"banners\";a:0:{}s:11:\"banners_rtl\";a:0:{}}s:57:\"woo-category-slider-by-pluginever/woo-category-slider.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:47:\"w.org/plugins/woo-category-slider-by-pluginever\";s:4:\"slug\";s:33:\"woo-category-slider-by-pluginever\";s:6:\"plugin\";s:57:\"woo-category-slider-by-pluginever/woo-category-slider.php\";s:11:\"new_version\";s:5:\"4.0.9\";s:3:\"url\";s:64:\"https://wordpress.org/plugins/woo-category-slider-by-pluginever/\";s:7:\"package\";s:76:\"https://downloads.wordpress.org/plugin/woo-category-slider-by-pluginever.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:86:\"https://ps.w.org/woo-category-slider-by-pluginever/assets/icon-256x256.png?rev=2149178\";s:2:\"1x\";s:86:\"https://ps.w.org/woo-category-slider-by-pluginever/assets/icon-128x128.png?rev=2149178\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:89:\"https://ps.w.org/woo-category-slider-by-pluginever/assets/banner-1544x500.png?rev=2149181\";s:2:\"1x\";s:88:\"https://ps.w.org/woo-category-slider-by-pluginever/assets/banner-772x250.png?rev=2149181\";}s:11:\"banners_rtl\";a:0:{}}s:45:\"woocommerce-multilingual/wpml-woocommerce.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:38:\"w.org/plugins/woocommerce-multilingual\";s:4:\"slug\";s:24:\"woocommerce-multilingual\";s:6:\"plugin\";s:45:\"woocommerce-multilingual/wpml-woocommerce.php\";s:11:\"new_version\";s:6:\"4.10.2\";s:3:\"url\";s:55:\"https://wordpress.org/plugins/woocommerce-multilingual/\";s:7:\"package\";s:74:\"https://downloads.wordpress.org/plugin/woocommerce-multilingual.4.10.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:77:\"https://ps.w.org/woocommerce-multilingual/assets/icon-256x256.png?rev=2026021\";s:2:\"1x\";s:77:\"https://ps.w.org/woocommerce-multilingual/assets/icon-128x128.png?rev=2026021\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:79:\"https://ps.w.org/woocommerce-multilingual/assets/banner-772x250.png?rev=2026021\";}s:11:\"banners_rtl\";a:0:{}}s:39:\"wpb-woocommerce-product-slider/main.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:44:\"w.org/plugins/wpb-woocommerce-product-slider\";s:4:\"slug\";s:30:\"wpb-woocommerce-product-slider\";s:6:\"plugin\";s:39:\"wpb-woocommerce-product-slider/main.php\";s:11:\"new_version\";s:7:\"2.0.8.7\";s:3:\"url\";s:61:\"https://wordpress.org/plugins/wpb-woocommerce-product-slider/\";s:7:\"package\";s:73:\"https://downloads.wordpress.org/plugin/wpb-woocommerce-product-slider.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:83:\"https://ps.w.org/wpb-woocommerce-product-slider/assets/icon-256x256.png?rev=2225581\";s:2:\"1x\";s:83:\"https://ps.w.org/wpb-woocommerce-product-slider/assets/icon-128x128.png?rev=2225581\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:86:\"https://ps.w.org/wpb-woocommerce-product-slider/assets/banner-1544x500.png?rev=2225581\";s:2:\"1x\";s:85:\"https://ps.w.org/wpb-woocommerce-product-slider/assets/banner-772x250.png?rev=2225581\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no'),
(2600, '_transient_timeout__woocommerce_helper_updates', '1600449795', 'no'),
(2601, '_transient__woocommerce_helper_updates', 'a:4:{s:4:\"hash\";s:32:\"d751713988987e9331980363e24189ce\";s:7:\"updated\";i:1600406595;s:8:\"products\";a:0:{}s:6:\"errors\";a:1:{i:0;s:10:\"http-error\";}}', 'no'),
(2603, '_transient_timeout_tp_product-category_en_US', '1600462941', 'no'),
(2604, '_transient_tp_product-category_en_US', 'product-category', 'no'),
(2605, '_transient_timeout_tp_product-category_ar', '1600462942', 'no'),
(2606, '_transient_tp_product-category_ar', 'product-category', 'no'),
(2607, '_transient_timeout_tp_product-tag_en_US', '1600462942', 'no'),
(2608, '_transient_tp_product-tag_en_US', 'product-tag', 'no'),
(2610, '_transient_timeout_tp_product-tag_ar', '1600462943', 'no'),
(2611, '_transient_tp_product-tag_ar', 'product-tag', 'no'),
(2612, '_transient_timeout_tp_product_en_US', '1600462943', 'no'),
(2613, '_transient_tp_product_en_US', 'product', 'no'),
(2614, '_transient_timeout_tp_product_ar', '1600462943', 'no'),
(2615, '_transient_tp_product_ar', 'product', 'no'),
(2618, '_transient_timeout__woocommerce_helper_subscriptions', '1600420646', 'no'),
(2619, '_transient__woocommerce_helper_subscriptions', 'a:0:{}', 'no'),
(2620, '_site_transient_timeout_theme_roots', '1600421546', 'no'),
(2621, '_site_transient_theme_roots', 'a:5:{s:10:\"storefront\";s:7:\"/themes\";s:14:\"twentynineteen\";s:7:\"/themes\";s:15:\"twentyseventeen\";s:7:\"/themes\";s:12:\"twentytwenty\";s:7:\"/themes\";s:8:\"wp-store\";s:7:\"/themes\";}', 'no'),
(2622, '_site_transient_timeout_available_translations', '1600434285', 'no');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(2623, '_site_transient_available_translations', 'a:122:{s:2:\"af\";a:8:{s:8:\"language\";s:2:\"af\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-07-14 06:20:07\";s:12:\"english_name\";s:9:\"Afrikaans\";s:11:\"native_name\";s:9:\"Afrikaans\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.4.2/af.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"af\";i:2;s:3:\"afr\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Gaan voort\";}}s:2:\"ar\";a:8:{s:8:\"language\";s:2:\"ar\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-09-16 00:18:25\";s:12:\"english_name\";s:6:\"Arabic\";s:11:\"native_name\";s:14:\"العربية\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.4.2/ar.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ar\";i:2;s:3:\"ara\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"المتابعة\";}}s:3:\"ary\";a:8:{s:8:\"language\";s:3:\"ary\";s:7:\"version\";s:5:\"4.7.7\";s:7:\"updated\";s:19:\"2017-01-26 15:42:35\";s:12:\"english_name\";s:15:\"Moroccan Arabic\";s:11:\"native_name\";s:31:\"العربية المغربية\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.7/ary.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ar\";i:3;s:3:\"ary\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"المتابعة\";}}s:2:\"as\";a:8:{s:8:\"language\";s:2:\"as\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-22 18:59:07\";s:12:\"english_name\";s:8:\"Assamese\";s:11:\"native_name\";s:21:\"অসমীয়া\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/as.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"as\";i:2;s:3:\"asm\";i:3;s:3:\"asm\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:2:\"az\";a:8:{s:8:\"language\";s:2:\"az\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-06 00:09:27\";s:12:\"english_name\";s:11:\"Azerbaijani\";s:11:\"native_name\";s:16:\"Azərbaycan dili\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/az.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"az\";i:2;s:3:\"aze\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:5:\"Davam\";}}s:3:\"azb\";a:8:{s:8:\"language\";s:3:\"azb\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-09-12 20:34:31\";s:12:\"english_name\";s:17:\"South Azerbaijani\";s:11:\"native_name\";s:29:\"گؤنئی آذربایجان\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/azb.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"az\";i:3;s:3:\"azb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:3:\"bel\";a:8:{s:8:\"language\";s:3:\"bel\";s:7:\"version\";s:6:\"4.9.15\";s:7:\"updated\";s:19:\"2019-10-29 07:54:22\";s:12:\"english_name\";s:10:\"Belarusian\";s:11:\"native_name\";s:29:\"Беларуская мова\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/translation/core/4.9.15/bel.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"be\";i:2;s:3:\"bel\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Працягнуць\";}}s:5:\"bg_BG\";a:8:{s:8:\"language\";s:5:\"bg_BG\";s:7:\"version\";s:5:\"5.3.3\";s:7:\"updated\";s:19:\"2020-01-22 10:57:09\";s:12:\"english_name\";s:9:\"Bulgarian\";s:11:\"native_name\";s:18:\"Български\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.3/bg_BG.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"bg\";i:2;s:3:\"bul\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Напред\";}}s:5:\"bn_BD\";a:8:{s:8:\"language\";s:5:\"bn_BD\";s:7:\"version\";s:6:\"4.8.14\";s:7:\"updated\";s:19:\"2017-10-01 12:57:10\";s:12:\"english_name\";s:20:\"Bengali (Bangladesh)\";s:11:\"native_name\";s:15:\"বাংলা\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.8.14/bn_BD.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"bn\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:23:\"এগিয়ে চল.\";}}s:2:\"bo\";a:8:{s:8:\"language\";s:2:\"bo\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-07-08 06:57:24\";s:12:\"english_name\";s:7:\"Tibetan\";s:11:\"native_name\";s:21:\"བོད་ཡིག\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.4.2/bo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"bo\";i:2;s:3:\"tib\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:33:\"མུ་མཐུད་དུ།\";}}s:5:\"bs_BA\";a:8:{s:8:\"language\";s:5:\"bs_BA\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-06-17 11:59:40\";s:12:\"english_name\";s:7:\"Bosnian\";s:11:\"native_name\";s:8:\"Bosanski\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.2/bs_BA.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"bs\";i:2;s:3:\"bos\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Nastavi\";}}s:2:\"ca\";a:8:{s:8:\"language\";s:2:\"ca\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-07-22 04:50:37\";s:12:\"english_name\";s:7:\"Catalan\";s:11:\"native_name\";s:7:\"Català\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.4.2/ca.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ca\";i:2;s:3:\"cat\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continua\";}}s:3:\"ceb\";a:8:{s:8:\"language\";s:3:\"ceb\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-03-02 17:25:51\";s:12:\"english_name\";s:7:\"Cebuano\";s:11:\"native_name\";s:7:\"Cebuano\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/ceb.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"ceb\";i:3;s:3:\"ceb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Padayun\";}}s:5:\"cs_CZ\";a:8:{s:8:\"language\";s:5:\"cs_CZ\";s:7:\"version\";s:5:\"5.3.4\";s:7:\"updated\";s:19:\"2020-06-11 08:59:48\";s:12:\"english_name\";s:5:\"Czech\";s:11:\"native_name\";s:9:\"Čeština\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.4/cs_CZ.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"cs\";i:2;s:3:\"ces\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:11:\"Pokračovat\";}}s:2:\"cy\";a:8:{s:8:\"language\";s:2:\"cy\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-07-10 08:55:00\";s:12:\"english_name\";s:5:\"Welsh\";s:11:\"native_name\";s:7:\"Cymraeg\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.4.2/cy.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"cy\";i:2;s:3:\"cym\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Parhau\";}}s:5:\"da_DK\";a:8:{s:8:\"language\";s:5:\"da_DK\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-08-04 08:37:35\";s:12:\"english_name\";s:6:\"Danish\";s:11:\"native_name\";s:5:\"Dansk\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.2/da_DK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"da\";i:2;s:3:\"dan\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Fortsæt\";}}s:12:\"de_DE_formal\";a:8:{s:8:\"language\";s:12:\"de_DE_formal\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-08-02 17:09:01\";s:12:\"english_name\";s:15:\"German (Formal)\";s:11:\"native_name\";s:13:\"Deutsch (Sie)\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/translation/core/5.4.2/de_DE_formal.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Weiter\";}}s:5:\"de_AT\";a:8:{s:8:\"language\";s:5:\"de_AT\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-06-17 18:22:34\";s:12:\"english_name\";s:16:\"German (Austria)\";s:11:\"native_name\";s:21:\"Deutsch (Österreich)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.2/de_AT.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Weiter\";}}s:14:\"de_CH_informal\";a:8:{s:8:\"language\";s:14:\"de_CH_informal\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-06-19 09:59:34\";s:12:\"english_name\";s:30:\"German (Switzerland, Informal)\";s:11:\"native_name\";s:21:\"Deutsch (Schweiz, Du)\";s:7:\"package\";s:73:\"https://downloads.wordpress.org/translation/core/5.4.2/de_CH_informal.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Weiter\";}}s:5:\"de_CH\";a:8:{s:8:\"language\";s:5:\"de_CH\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-06-19 09:59:11\";s:12:\"english_name\";s:20:\"German (Switzerland)\";s:11:\"native_name\";s:17:\"Deutsch (Schweiz)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.2/de_CH.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Weiter\";}}s:5:\"de_DE\";a:8:{s:8:\"language\";s:5:\"de_DE\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-08-02 17:09:40\";s:12:\"english_name\";s:6:\"German\";s:11:\"native_name\";s:7:\"Deutsch\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.2/de_DE.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Weiter\";}}s:3:\"dzo\";a:8:{s:8:\"language\";s:3:\"dzo\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-06-29 08:59:03\";s:12:\"english_name\";s:8:\"Dzongkha\";s:11:\"native_name\";s:18:\"རྫོང་ཁ\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/dzo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"dz\";i:2;s:3:\"dzo\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:2:\"el\";a:8:{s:8:\"language\";s:2:\"el\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-07-08 15:27:00\";s:12:\"english_name\";s:5:\"Greek\";s:11:\"native_name\";s:16:\"Ελληνικά\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.4.2/el.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"el\";i:2;s:3:\"ell\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"Συνέχεια\";}}s:5:\"en_CA\";a:8:{s:8:\"language\";s:5:\"en_CA\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-08-05 17:32:39\";s:12:\"english_name\";s:16:\"English (Canada)\";s:11:\"native_name\";s:16:\"English (Canada)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.2/en_CA.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_AU\";a:8:{s:8:\"language\";s:5:\"en_AU\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-08-03 08:37:17\";s:12:\"english_name\";s:19:\"English (Australia)\";s:11:\"native_name\";s:19:\"English (Australia)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.2/en_AU.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_NZ\";a:8:{s:8:\"language\";s:5:\"en_NZ\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-03-31 22:29:33\";s:12:\"english_name\";s:21:\"English (New Zealand)\";s:11:\"native_name\";s:21:\"English (New Zealand)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.2/en_NZ.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_ZA\";a:8:{s:8:\"language\";s:5:\"en_ZA\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-07-08 05:40:20\";s:12:\"english_name\";s:22:\"English (South Africa)\";s:11:\"native_name\";s:22:\"English (South Africa)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.2/en_ZA.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_GB\";a:8:{s:8:\"language\";s:5:\"en_GB\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-07-14 17:55:29\";s:12:\"english_name\";s:12:\"English (UK)\";s:11:\"native_name\";s:12:\"English (UK)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.2/en_GB.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:2:\"eo\";a:8:{s:8:\"language\";s:2:\"eo\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-07-07 12:15:57\";s:12:\"english_name\";s:9:\"Esperanto\";s:11:\"native_name\";s:9:\"Esperanto\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.4.2/eo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"eo\";i:2;s:3:\"epo\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Daŭrigi\";}}s:5:\"es_CL\";a:8:{s:8:\"language\";s:5:\"es_CL\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-07-09 08:55:58\";s:12:\"english_name\";s:15:\"Spanish (Chile)\";s:11:\"native_name\";s:17:\"Español de Chile\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.2/es_CL.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_MX\";a:8:{s:8:\"language\";s:5:\"es_MX\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-09-16 17:47:15\";s:12:\"english_name\";s:16:\"Spanish (Mexico)\";s:11:\"native_name\";s:19:\"Español de México\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.2/es_MX.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_ES\";a:8:{s:8:\"language\";s:5:\"es_ES\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-09-03 14:16:12\";s:12:\"english_name\";s:15:\"Spanish (Spain)\";s:11:\"native_name\";s:8:\"Español\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.2/es_ES.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_AR\";a:8:{s:8:\"language\";s:5:\"es_AR\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-07-07 22:49:02\";s:12:\"english_name\";s:19:\"Spanish (Argentina)\";s:11:\"native_name\";s:21:\"Español de Argentina\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.2/es_AR.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_CO\";a:8:{s:8:\"language\";s:5:\"es_CO\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-07-08 20:42:40\";s:12:\"english_name\";s:18:\"Spanish (Colombia)\";s:11:\"native_name\";s:20:\"Español de Colombia\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.2/es_CO.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_CR\";a:8:{s:8:\"language\";s:5:\"es_CR\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-06-23 16:46:04\";s:12:\"english_name\";s:20:\"Spanish (Costa Rica)\";s:11:\"native_name\";s:22:\"Español de Costa Rica\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.2/es_CR.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_VE\";a:8:{s:8:\"language\";s:5:\"es_VE\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-07-07 21:12:53\";s:12:\"english_name\";s:19:\"Spanish (Venezuela)\";s:11:\"native_name\";s:21:\"Español de Venezuela\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.2/es_VE.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_PR\";a:8:{s:8:\"language\";s:5:\"es_PR\";s:7:\"version\";s:5:\"5.4.1\";s:7:\"updated\";s:19:\"2020-04-29 15:36:59\";s:12:\"english_name\";s:21:\"Spanish (Puerto Rico)\";s:11:\"native_name\";s:23:\"Español de Puerto Rico\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.1/es_PR.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_UY\";a:8:{s:8:\"language\";s:5:\"es_UY\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2019-11-12 04:43:11\";s:12:\"english_name\";s:17:\"Spanish (Uruguay)\";s:11:\"native_name\";s:19:\"Español de Uruguay\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.2/es_UY.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_GT\";a:8:{s:8:\"language\";s:5:\"es_GT\";s:7:\"version\";s:3:\"5.1\";s:7:\"updated\";s:19:\"2019-03-02 06:35:01\";s:12:\"english_name\";s:19:\"Spanish (Guatemala)\";s:11:\"native_name\";s:21:\"Español de Guatemala\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.1/es_GT.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_PE\";a:8:{s:8:\"language\";s:5:\"es_PE\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-07-23 23:51:44\";s:12:\"english_name\";s:14:\"Spanish (Peru)\";s:11:\"native_name\";s:17:\"Español de Perú\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.2/es_PE.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:2:\"et\";a:8:{s:8:\"language\";s:2:\"et\";s:7:\"version\";s:9:\"5.0-beta3\";s:7:\"updated\";s:19:\"2018-11-28 16:04:33\";s:12:\"english_name\";s:8:\"Estonian\";s:11:\"native_name\";s:5:\"Eesti\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/5.0-beta3/et.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"et\";i:2;s:3:\"est\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Jätka\";}}s:2:\"eu\";a:8:{s:8:\"language\";s:2:\"eu\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-07-08 09:16:23\";s:12:\"english_name\";s:6:\"Basque\";s:11:\"native_name\";s:7:\"Euskara\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.4.2/eu.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"eu\";i:2;s:3:\"eus\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Jarraitu\";}}s:5:\"fa_IR\";a:8:{s:8:\"language\";s:5:\"fa_IR\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-07-24 18:19:16\";s:12:\"english_name\";s:7:\"Persian\";s:11:\"native_name\";s:10:\"فارسی\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.2/fa_IR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fa\";i:2;s:3:\"fas\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"ادامه\";}}s:2:\"fi\";a:8:{s:8:\"language\";s:2:\"fi\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-07-10 15:26:09\";s:12:\"english_name\";s:7:\"Finnish\";s:11:\"native_name\";s:5:\"Suomi\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.4.2/fi.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fi\";i:2;s:3:\"fin\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:5:\"Jatka\";}}s:5:\"fr_CA\";a:8:{s:8:\"language\";s:5:\"fr_CA\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-08-04 12:36:19\";s:12:\"english_name\";s:15:\"French (Canada)\";s:11:\"native_name\";s:19:\"Français du Canada\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.2/fr_CA.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fr\";i:2;s:3:\"fra\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuer\";}}s:5:\"fr_BE\";a:8:{s:8:\"language\";s:5:\"fr_BE\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-07-25 08:39:33\";s:12:\"english_name\";s:16:\"French (Belgium)\";s:11:\"native_name\";s:21:\"Français de Belgique\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.2/fr_BE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fr\";i:2;s:3:\"fra\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuer\";}}s:5:\"fr_FR\";a:8:{s:8:\"language\";s:5:\"fr_FR\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-08-04 11:58:44\";s:12:\"english_name\";s:15:\"French (France)\";s:11:\"native_name\";s:9:\"Français\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.2/fr_FR.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"fr\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuer\";}}s:3:\"fur\";a:8:{s:8:\"language\";s:3:\"fur\";s:7:\"version\";s:5:\"4.8.6\";s:7:\"updated\";s:19:\"2018-01-29 17:32:35\";s:12:\"english_name\";s:8:\"Friulian\";s:11:\"native_name\";s:8:\"Friulian\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8.6/fur.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"fur\";i:3;s:3:\"fur\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:2:\"gd\";a:8:{s:8:\"language\";s:2:\"gd\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-08-23 17:41:37\";s:12:\"english_name\";s:15:\"Scottish Gaelic\";s:11:\"native_name\";s:9:\"Gàidhlig\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/gd.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"gd\";i:2;s:3:\"gla\";i:3;s:3:\"gla\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:15:\"Lean air adhart\";}}s:5:\"gl_ES\";a:8:{s:8:\"language\";s:5:\"gl_ES\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-07-07 22:38:20\";s:12:\"english_name\";s:8:\"Galician\";s:11:\"native_name\";s:6:\"Galego\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.2/gl_ES.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"gl\";i:2;s:3:\"glg\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:2:\"gu\";a:8:{s:8:\"language\";s:2:\"gu\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-14 12:33:48\";s:12:\"english_name\";s:8:\"Gujarati\";s:11:\"native_name\";s:21:\"ગુજરાતી\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.8/gu.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"gu\";i:2;s:3:\"guj\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:31:\"ચાલુ રાખવું\";}}s:3:\"haz\";a:8:{s:8:\"language\";s:3:\"haz\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2015-12-05 00:59:09\";s:12:\"english_name\";s:8:\"Hazaragi\";s:11:\"native_name\";s:15:\"هزاره گی\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.4.2/haz.zip\";s:3:\"iso\";a:1:{i:3;s:3:\"haz\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"ادامه\";}}s:5:\"he_IL\";a:8:{s:8:\"language\";s:5:\"he_IL\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-07-16 16:33:09\";s:12:\"english_name\";s:6:\"Hebrew\";s:11:\"native_name\";s:16:\"עִבְרִית\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.2/he_IL.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"he\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"המשך\";}}s:5:\"hi_IN\";a:8:{s:8:\"language\";s:5:\"hi_IN\";s:7:\"version\";s:5:\"4.9.7\";s:7:\"updated\";s:19:\"2018-06-17 09:33:44\";s:12:\"english_name\";s:5:\"Hindi\";s:11:\"native_name\";s:18:\"हिन्दी\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.7/hi_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hi\";i:2;s:3:\"hin\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"जारी\";}}s:2:\"hr\";a:8:{s:8:\"language\";s:2:\"hr\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-08-15 14:16:07\";s:12:\"english_name\";s:8:\"Croatian\";s:11:\"native_name\";s:8:\"Hrvatski\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.4.2/hr.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hr\";i:2;s:3:\"hrv\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Nastavi\";}}s:3:\"hsb\";a:8:{s:8:\"language\";s:3:\"hsb\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-07-18 16:53:49\";s:12:\"english_name\";s:13:\"Upper Sorbian\";s:11:\"native_name\";s:17:\"Hornjoserbšćina\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.4.2/hsb.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"hsb\";i:3;s:3:\"hsb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:4:\"Dale\";}}s:5:\"hu_HU\";a:8:{s:8:\"language\";s:5:\"hu_HU\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-06-03 10:58:08\";s:12:\"english_name\";s:9:\"Hungarian\";s:11:\"native_name\";s:6:\"Magyar\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.2/hu_HU.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hu\";i:2;s:3:\"hun\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Folytatás\";}}s:2:\"hy\";a:8:{s:8:\"language\";s:2:\"hy\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-12-03 16:21:10\";s:12:\"english_name\";s:8:\"Armenian\";s:11:\"native_name\";s:14:\"Հայերեն\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/hy.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hy\";i:2;s:3:\"hye\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Շարունակել\";}}s:5:\"id_ID\";a:8:{s:8:\"language\";s:5:\"id_ID\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-09-09 07:34:20\";s:12:\"english_name\";s:10:\"Indonesian\";s:11:\"native_name\";s:16:\"Bahasa Indonesia\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.2/id_ID.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"id\";i:2;s:3:\"ind\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Lanjutkan\";}}s:5:\"is_IS\";a:8:{s:8:\"language\";s:5:\"is_IS\";s:7:\"version\";s:6:\"4.7.11\";s:7:\"updated\";s:19:\"2018-09-20 11:13:37\";s:12:\"english_name\";s:9:\"Icelandic\";s:11:\"native_name\";s:9:\"Íslenska\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.7.11/is_IS.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"is\";i:2;s:3:\"isl\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Áfram\";}}s:5:\"it_IT\";a:8:{s:8:\"language\";s:5:\"it_IT\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-07-16 09:32:36\";s:12:\"english_name\";s:7:\"Italian\";s:11:\"native_name\";s:8:\"Italiano\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.2/it_IT.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"it\";i:2;s:3:\"ita\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continua\";}}s:2:\"ja\";a:8:{s:8:\"language\";s:2:\"ja\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-08-15 01:03:55\";s:12:\"english_name\";s:8:\"Japanese\";s:11:\"native_name\";s:9:\"日本語\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.4.2/ja.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"ja\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"次へ\";}}s:5:\"jv_ID\";a:8:{s:8:\"language\";s:5:\"jv_ID\";s:7:\"version\";s:5:\"4.9.5\";s:7:\"updated\";s:19:\"2018-03-24 13:53:29\";s:12:\"english_name\";s:8:\"Javanese\";s:11:\"native_name\";s:9:\"Basa Jawa\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.5/jv_ID.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"jv\";i:2;s:3:\"jav\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Nerusaké\";}}s:5:\"ka_GE\";a:8:{s:8:\"language\";s:5:\"ka_GE\";s:7:\"version\";s:5:\"5.4.1\";s:7:\"updated\";s:19:\"2020-04-30 07:54:16\";s:12:\"english_name\";s:8:\"Georgian\";s:11:\"native_name\";s:21:\"ქართული\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.1/ka_GE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ka\";i:2;s:3:\"kat\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:30:\"გაგრძელება\";}}s:3:\"kab\";a:8:{s:8:\"language\";s:3:\"kab\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-07-08 06:32:24\";s:12:\"english_name\";s:6:\"Kabyle\";s:11:\"native_name\";s:9:\"Taqbaylit\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.4.2/kab.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"kab\";i:3;s:3:\"kab\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Kemmel\";}}s:2:\"kk\";a:8:{s:8:\"language\";s:2:\"kk\";s:7:\"version\";s:5:\"4.9.5\";s:7:\"updated\";s:19:\"2018-03-12 08:08:32\";s:12:\"english_name\";s:6:\"Kazakh\";s:11:\"native_name\";s:19:\"Қазақ тілі\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.5/kk.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"kk\";i:2;s:3:\"kaz\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Жалғастыру\";}}s:2:\"km\";a:8:{s:8:\"language\";s:2:\"km\";s:7:\"version\";s:5:\"5.0.3\";s:7:\"updated\";s:19:\"2019-01-09 07:34:10\";s:12:\"english_name\";s:5:\"Khmer\";s:11:\"native_name\";s:27:\"ភាសាខ្មែរ\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.0.3/km.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"km\";i:2;s:3:\"khm\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"បន្ត\";}}s:2:\"kn\";a:8:{s:8:\"language\";s:2:\"kn\";s:7:\"version\";s:6:\"4.9.15\";s:7:\"updated\";s:19:\"2019-12-04 12:22:34\";s:12:\"english_name\";s:7:\"Kannada\";s:11:\"native_name\";s:15:\"ಕನ್ನಡ\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.9.15/kn.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"kn\";i:2;s:3:\"kan\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:30:\"ಮುಂದುವರೆಸಿ\";}}s:5:\"ko_KR\";a:8:{s:8:\"language\";s:5:\"ko_KR\";s:7:\"version\";s:5:\"5.3.4\";s:7:\"updated\";s:19:\"2020-08-22 04:31:42\";s:12:\"english_name\";s:6:\"Korean\";s:11:\"native_name\";s:9:\"한국어\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.4/ko_KR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ko\";i:2;s:3:\"kor\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"계속\";}}s:3:\"ckb\";a:8:{s:8:\"language\";s:3:\"ckb\";s:7:\"version\";s:6:\"4.9.15\";s:7:\"updated\";s:19:\"2018-12-18 14:32:44\";s:12:\"english_name\";s:16:\"Kurdish (Sorani)\";s:11:\"native_name\";s:13:\"كوردی‎\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/translation/core/4.9.15/ckb.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ku\";i:3;s:3:\"ckb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:30:\"به‌رده‌وام به‌\";}}s:2:\"lo\";a:8:{s:8:\"language\";s:2:\"lo\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-12 09:59:23\";s:12:\"english_name\";s:3:\"Lao\";s:11:\"native_name\";s:21:\"ພາສາລາວ\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/lo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"lo\";i:2;s:3:\"lao\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:18:\"ຕໍ່​ໄປ\";}}s:5:\"lt_LT\";a:8:{s:8:\"language\";s:5:\"lt_LT\";s:7:\"version\";s:5:\"5.2.7\";s:7:\"updated\";s:19:\"2020-07-14 08:45:32\";s:12:\"english_name\";s:10:\"Lithuanian\";s:11:\"native_name\";s:15:\"Lietuvių kalba\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.2.7/lt_LT.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"lt\";i:2;s:3:\"lit\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Tęsti\";}}s:2:\"lv\";a:8:{s:8:\"language\";s:2:\"lv\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-07-14 08:34:14\";s:12:\"english_name\";s:7:\"Latvian\";s:11:\"native_name\";s:16:\"Latviešu valoda\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.4.2/lv.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"lv\";i:2;s:3:\"lav\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Turpināt\";}}s:5:\"mk_MK\";a:8:{s:8:\"language\";s:5:\"mk_MK\";s:7:\"version\";s:5:\"5.2.3\";s:7:\"updated\";s:19:\"2019-09-08 12:57:25\";s:12:\"english_name\";s:10:\"Macedonian\";s:11:\"native_name\";s:31:\"Македонски јазик\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.2.3/mk_MK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"mk\";i:2;s:3:\"mkd\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"Продолжи\";}}s:5:\"ml_IN\";a:8:{s:8:\"language\";s:5:\"ml_IN\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-27 03:43:32\";s:12:\"english_name\";s:9:\"Malayalam\";s:11:\"native_name\";s:18:\"മലയാളം\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/ml_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ml\";i:2;s:3:\"mal\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:18:\"തുടരുക\";}}s:2:\"mn\";a:8:{s:8:\"language\";s:2:\"mn\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-12 07:29:35\";s:12:\"english_name\";s:9:\"Mongolian\";s:11:\"native_name\";s:12:\"Монгол\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/mn.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"mn\";i:2;s:3:\"mon\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:24:\"Үргэлжлүүлэх\";}}s:2:\"mr\";a:8:{s:8:\"language\";s:2:\"mr\";s:7:\"version\";s:6:\"4.8.14\";s:7:\"updated\";s:19:\"2018-02-13 07:38:55\";s:12:\"english_name\";s:7:\"Marathi\";s:11:\"native_name\";s:15:\"मराठी\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8.14/mr.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"mr\";i:2;s:3:\"mar\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:25:\"सुरु ठेवा\";}}s:5:\"ms_MY\";a:8:{s:8:\"language\";s:5:\"ms_MY\";s:7:\"version\";s:6:\"4.9.15\";s:7:\"updated\";s:19:\"2018-08-31 11:57:07\";s:12:\"english_name\";s:5:\"Malay\";s:11:\"native_name\";s:13:\"Bahasa Melayu\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.15/ms_MY.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ms\";i:2;s:3:\"msa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Teruskan\";}}s:5:\"my_MM\";a:8:{s:8:\"language\";s:5:\"my_MM\";s:7:\"version\";s:6:\"4.1.20\";s:7:\"updated\";s:19:\"2015-03-26 15:57:42\";s:12:\"english_name\";s:17:\"Myanmar (Burmese)\";s:11:\"native_name\";s:15:\"ဗမာစာ\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.1.20/my_MM.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"my\";i:2;s:3:\"mya\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:54:\"ဆက်လက်လုပ်ဆောင်ပါ။\";}}s:5:\"nb_NO\";a:8:{s:8:\"language\";s:5:\"nb_NO\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-07-07 22:48:25\";s:12:\"english_name\";s:19:\"Norwegian (Bokmål)\";s:11:\"native_name\";s:13:\"Norsk bokmål\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.2/nb_NO.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nb\";i:2;s:3:\"nob\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Fortsett\";}}s:5:\"ne_NP\";a:8:{s:8:\"language\";s:5:\"ne_NP\";s:7:\"version\";s:5:\"4.9.5\";s:7:\"updated\";s:19:\"2018-03-27 10:30:26\";s:12:\"english_name\";s:6:\"Nepali\";s:11:\"native_name\";s:18:\"नेपाली\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.5/ne_NP.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ne\";i:2;s:3:\"nep\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:43:\"जारी राख्नुहोस्\";}}s:5:\"nl_BE\";a:8:{s:8:\"language\";s:5:\"nl_BE\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-07-11 14:56:43\";s:12:\"english_name\";s:15:\"Dutch (Belgium)\";s:11:\"native_name\";s:20:\"Nederlands (België)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.2/nl_BE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nl\";i:2;s:3:\"nld\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Doorgaan\";}}s:12:\"nl_NL_formal\";a:8:{s:8:\"language\";s:12:\"nl_NL_formal\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-07-08 07:06:32\";s:12:\"english_name\";s:14:\"Dutch (Formal)\";s:11:\"native_name\";s:20:\"Nederlands (Formeel)\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/translation/core/5.4.2/nl_NL_formal.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nl\";i:2;s:3:\"nld\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Doorgaan\";}}s:5:\"nl_NL\";a:8:{s:8:\"language\";s:5:\"nl_NL\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-08-20 10:06:24\";s:12:\"english_name\";s:5:\"Dutch\";s:11:\"native_name\";s:10:\"Nederlands\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.2/nl_NL.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nl\";i:2;s:3:\"nld\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Doorgaan\";}}s:5:\"nn_NO\";a:8:{s:8:\"language\";s:5:\"nn_NO\";s:7:\"version\";s:5:\"5.3.3\";s:7:\"updated\";s:19:\"2020-01-01 08:53:00\";s:12:\"english_name\";s:19:\"Norwegian (Nynorsk)\";s:11:\"native_name\";s:13:\"Norsk nynorsk\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.3/nn_NO.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nn\";i:2;s:3:\"nno\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Hald fram\";}}s:3:\"oci\";a:8:{s:8:\"language\";s:3:\"oci\";s:7:\"version\";s:5:\"4.8.3\";s:7:\"updated\";s:19:\"2017-08-25 10:03:08\";s:12:\"english_name\";s:7:\"Occitan\";s:11:\"native_name\";s:7:\"Occitan\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8.3/oci.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"oc\";i:2;s:3:\"oci\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Contunhar\";}}s:5:\"pa_IN\";a:8:{s:8:\"language\";s:5:\"pa_IN\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-16 05:19:43\";s:12:\"english_name\";s:7:\"Punjabi\";s:11:\"native_name\";s:18:\"ਪੰਜਾਬੀ\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/pa_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"pa\";i:2;s:3:\"pan\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:25:\"ਜਾਰੀ ਰੱਖੋ\";}}s:5:\"pl_PL\";a:8:{s:8:\"language\";s:5:\"pl_PL\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-08-05 08:53:46\";s:12:\"english_name\";s:6:\"Polish\";s:11:\"native_name\";s:6:\"Polski\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.2/pl_PL.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"pl\";i:2;s:3:\"pol\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Kontynuuj\";}}s:2:\"ps\";a:8:{s:8:\"language\";s:2:\"ps\";s:7:\"version\";s:6:\"4.1.20\";s:7:\"updated\";s:19:\"2015-03-29 22:19:48\";s:12:\"english_name\";s:6:\"Pashto\";s:11:\"native_name\";s:8:\"پښتو\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.1.20/ps.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ps\";i:2;s:3:\"pus\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:19:\"دوام ورکړه\";}}s:10:\"pt_PT_ao90\";a:8:{s:8:\"language\";s:10:\"pt_PT_ao90\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-07-08 10:35:41\";s:12:\"english_name\";s:27:\"Portuguese (Portugal, AO90)\";s:11:\"native_name\";s:17:\"Português (AO90)\";s:7:\"package\";s:69:\"https://downloads.wordpress.org/translation/core/5.4.2/pt_PT_ao90.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"pt\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"pt_AO\";a:8:{s:8:\"language\";s:5:\"pt_AO\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-07-01 11:09:51\";s:12:\"english_name\";s:19:\"Portuguese (Angola)\";s:11:\"native_name\";s:20:\"Português de Angola\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.2/pt_AO.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"pt\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"pt_BR\";a:8:{s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-09-10 14:59:24\";s:12:\"english_name\";s:19:\"Portuguese (Brazil)\";s:11:\"native_name\";s:20:\"Português do Brasil\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.2/pt_BR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"pt\";i:2;s:3:\"por\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"pt_PT\";a:8:{s:8:\"language\";s:5:\"pt_PT\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-07-30 20:34:06\";s:12:\"english_name\";s:21:\"Portuguese (Portugal)\";s:11:\"native_name\";s:10:\"Português\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.2/pt_PT.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"pt\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:3:\"rhg\";a:8:{s:8:\"language\";s:3:\"rhg\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-03-16 13:03:18\";s:12:\"english_name\";s:8:\"Rohingya\";s:11:\"native_name\";s:8:\"Ruáinga\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/rhg.zip\";s:3:\"iso\";a:1:{i:3;s:3:\"rhg\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"ro_RO\";a:8:{s:8:\"language\";s:5:\"ro_RO\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-09-16 07:29:24\";s:12:\"english_name\";s:8:\"Romanian\";s:11:\"native_name\";s:8:\"Română\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.2/ro_RO.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ro\";i:2;s:3:\"ron\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuă\";}}s:5:\"ru_RU\";a:8:{s:8:\"language\";s:5:\"ru_RU\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-08-19 16:39:45\";s:12:\"english_name\";s:7:\"Russian\";s:11:\"native_name\";s:14:\"Русский\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.2/ru_RU.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ru\";i:2;s:3:\"rus\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Продолжить\";}}s:3:\"sah\";a:8:{s:8:\"language\";s:3:\"sah\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-21 02:06:41\";s:12:\"english_name\";s:5:\"Sakha\";s:11:\"native_name\";s:14:\"Сахалыы\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/sah.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"sah\";i:3;s:3:\"sah\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Салҕаа\";}}s:3:\"snd\";a:8:{s:8:\"language\";s:3:\"snd\";s:7:\"version\";s:3:\"5.3\";s:7:\"updated\";s:19:\"2019-11-12 04:37:38\";s:12:\"english_name\";s:6:\"Sindhi\";s:11:\"native_name\";s:8:\"سنڌي\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/translation/core/5.3/snd.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"sd\";i:2;s:3:\"snd\";i:3;s:3:\"snd\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:15:\"اڳتي هلو\";}}s:5:\"si_LK\";a:8:{s:8:\"language\";s:5:\"si_LK\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-12 06:00:52\";s:12:\"english_name\";s:7:\"Sinhala\";s:11:\"native_name\";s:15:\"සිංහල\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/si_LK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"si\";i:2;s:3:\"sin\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:44:\"දිගටම කරගෙන යන්න\";}}s:5:\"sk_SK\";a:8:{s:8:\"language\";s:5:\"sk_SK\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-07-14 13:57:15\";s:12:\"english_name\";s:6:\"Slovak\";s:11:\"native_name\";s:11:\"Slovenčina\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.2/sk_SK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sk\";i:2;s:3:\"slk\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Pokračovať\";}}s:3:\"skr\";a:8:{s:8:\"language\";s:3:\"skr\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-08-11 03:43:49\";s:12:\"english_name\";s:7:\"Saraiki\";s:11:\"native_name\";s:14:\"سرائیکی\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.4.2/skr.zip\";s:3:\"iso\";a:1:{i:3;s:3:\"skr\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:17:\"جاری رکھو\";}}s:5:\"sl_SI\";a:8:{s:8:\"language\";s:5:\"sl_SI\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2018-01-04 13:33:13\";s:12:\"english_name\";s:9:\"Slovenian\";s:11:\"native_name\";s:13:\"Slovenščina\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/sl_SI.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sl\";i:2;s:3:\"slv\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Nadaljuj\";}}s:2:\"sq\";a:8:{s:8:\"language\";s:2:\"sq\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-07-20 13:58:58\";s:12:\"english_name\";s:8:\"Albanian\";s:11:\"native_name\";s:5:\"Shqip\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.4.2/sq.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sq\";i:2;s:3:\"sqi\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Vazhdo\";}}s:5:\"sr_RS\";a:8:{s:8:\"language\";s:5:\"sr_RS\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-04-01 01:42:23\";s:12:\"english_name\";s:7:\"Serbian\";s:11:\"native_name\";s:23:\"Српски језик\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.2/sr_RS.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sr\";i:2;s:3:\"srp\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:14:\"Настави\";}}s:5:\"sv_SE\";a:8:{s:8:\"language\";s:5:\"sv_SE\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-07-26 23:48:05\";s:12:\"english_name\";s:7:\"Swedish\";s:11:\"native_name\";s:7:\"Svenska\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.2/sv_SE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sv\";i:2;s:3:\"swe\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Fortsätt\";}}s:2:\"sw\";a:8:{s:8:\"language\";s:2:\"sw\";s:7:\"version\";s:5:\"5.2.6\";s:7:\"updated\";s:19:\"2019-10-22 00:19:41\";s:12:\"english_name\";s:7:\"Swahili\";s:11:\"native_name\";s:9:\"Kiswahili\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.2.6/sw.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sw\";i:2;s:3:\"swa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Endelea\";}}s:3:\"szl\";a:8:{s:8:\"language\";s:3:\"szl\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-09-24 19:58:14\";s:12:\"english_name\";s:8:\"Silesian\";s:11:\"native_name\";s:17:\"Ślōnskŏ gŏdka\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/szl.zip\";s:3:\"iso\";a:1:{i:3;s:3:\"szl\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:13:\"Kōntynuować\";}}s:5:\"ta_IN\";a:8:{s:8:\"language\";s:5:\"ta_IN\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-27 03:22:47\";s:12:\"english_name\";s:5:\"Tamil\";s:11:\"native_name\";s:15:\"தமிழ்\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/ta_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ta\";i:2;s:3:\"tam\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:24:\"தொடரவும்\";}}s:2:\"te\";a:8:{s:8:\"language\";s:2:\"te\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-26 15:47:39\";s:12:\"english_name\";s:6:\"Telugu\";s:11:\"native_name\";s:18:\"తెలుగు\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/te.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"te\";i:2;s:3:\"tel\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:30:\"కొనసాగించు\";}}s:2:\"th\";a:8:{s:8:\"language\";s:2:\"th\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-08-22 08:23:03\";s:12:\"english_name\";s:4:\"Thai\";s:11:\"native_name\";s:9:\"ไทย\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.4.2/th.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"th\";i:2;s:3:\"tha\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:15:\"ต่อไป\";}}s:2:\"tl\";a:8:{s:8:\"language\";s:2:\"tl\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-12-30 02:38:08\";s:12:\"english_name\";s:7:\"Tagalog\";s:11:\"native_name\";s:7:\"Tagalog\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/tl.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"tl\";i:2;s:3:\"tgl\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Magpatuloy\";}}s:5:\"tr_TR\";a:8:{s:8:\"language\";s:5:\"tr_TR\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-07-09 14:00:02\";s:12:\"english_name\";s:7:\"Turkish\";s:11:\"native_name\";s:8:\"Türkçe\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.2/tr_TR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"tr\";i:2;s:3:\"tur\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:5:\"Devam\";}}s:5:\"tt_RU\";a:8:{s:8:\"language\";s:5:\"tt_RU\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-20 20:20:50\";s:12:\"english_name\";s:5:\"Tatar\";s:11:\"native_name\";s:19:\"Татар теле\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/tt_RU.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"tt\";i:2;s:3:\"tat\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:17:\"дәвам итү\";}}s:3:\"tah\";a:8:{s:8:\"language\";s:3:\"tah\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-03-06 18:39:39\";s:12:\"english_name\";s:8:\"Tahitian\";s:11:\"native_name\";s:10:\"Reo Tahiti\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/tah.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"ty\";i:2;s:3:\"tah\";i:3;s:3:\"tah\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"ug_CN\";a:8:{s:8:\"language\";s:5:\"ug_CN\";s:7:\"version\";s:5:\"4.9.5\";s:7:\"updated\";s:19:\"2018-04-12 12:31:53\";s:12:\"english_name\";s:6:\"Uighur\";s:11:\"native_name\";s:16:\"ئۇيغۇرچە\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.5/ug_CN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ug\";i:2;s:3:\"uig\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:26:\"داۋاملاشتۇرۇش\";}}s:2:\"uk\";a:8:{s:8:\"language\";s:2:\"uk\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-08-14 11:06:00\";s:12:\"english_name\";s:9:\"Ukrainian\";s:11:\"native_name\";s:20:\"Українська\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.4.2/uk.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"uk\";i:2;s:3:\"ukr\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Продовжити\";}}s:2:\"ur\";a:8:{s:8:\"language\";s:2:\"ur\";s:7:\"version\";s:5:\"5.1.6\";s:7:\"updated\";s:19:\"2020-04-09 10:48:08\";s:12:\"english_name\";s:4:\"Urdu\";s:11:\"native_name\";s:8:\"اردو\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.1.6/ur.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ur\";i:2;s:3:\"urd\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:19:\"جاری رکھیں\";}}s:5:\"uz_UZ\";a:8:{s:8:\"language\";s:5:\"uz_UZ\";s:7:\"version\";s:6:\"5.0.10\";s:7:\"updated\";s:19:\"2019-01-23 12:32:40\";s:12:\"english_name\";s:5:\"Uzbek\";s:11:\"native_name\";s:11:\"O‘zbekcha\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/5.0.10/uz_UZ.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"uz\";i:2;s:3:\"uzb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:11:\"Davom etish\";}}s:2:\"vi\";a:8:{s:8:\"language\";s:2:\"vi\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-07-13 09:29:34\";s:12:\"english_name\";s:10:\"Vietnamese\";s:11:\"native_name\";s:14:\"Tiếng Việt\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.4.2/vi.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"vi\";i:2;s:3:\"vie\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Tiếp tục\";}}s:5:\"zh_CN\";a:8:{s:8:\"language\";s:5:\"zh_CN\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-08-03 03:32:27\";s:12:\"english_name\";s:15:\"Chinese (China)\";s:11:\"native_name\";s:12:\"简体中文\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.2/zh_CN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"zh\";i:2;s:3:\"zho\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"继续\";}}s:5:\"zh_TW\";a:8:{s:8:\"language\";s:5:\"zh_TW\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-09-05 21:43:18\";s:12:\"english_name\";s:16:\"Chinese (Taiwan)\";s:11:\"native_name\";s:12:\"繁體中文\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.2/zh_TW.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"zh\";i:2;s:3:\"zho\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"繼續\";}}s:5:\"zh_HK\";a:8:{s:8:\"language\";s:5:\"zh_HK\";s:7:\"version\";s:5:\"5.3.3\";s:7:\"updated\";s:19:\"2020-03-08 12:12:22\";s:12:\"english_name\";s:19:\"Chinese (Hong Kong)\";s:11:\"native_name\";s:16:\"香港中文版	\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.3/zh_HK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"zh\";i:2;s:3:\"zho\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"繼續\";}}}', 'no'),
(2624, '_transient_doing_cron', '1600423485.9404489994049072265625', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `wp_postmeta`
--

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'template-fullwidth.php'),
(3, 5, '_wp_attached_file', 'woocommerce-placeholder.png'),
(4, 5, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1200;s:6:\"height\";i:1200;s:4:\"file\";s:27:\"woocommerce-placeholder.png\";s:5:\"sizes\";a:7:{s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:35:\"woocommerce-placeholder-324x324.png\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-416x416.png\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:37:\"woocommerce-placeholder-1024x1024.png\";s:5:\"width\";i:1024;s:6:\"height\";i:1024;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-768x768.png\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(5, 7, '_edit_lock', '1599461843:1'),
(6, 10, '_wp_attached_file', '2020/09/beanie.jpg'),
(7, 10, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:801;s:6:\"height\";i:801;s:4:\"file\";s:18:\"2020/09/beanie.jpg\";s:5:\"sizes\";a:6:{s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:18:\"beanie-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:18:\"beanie-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:18:\"beanie-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"beanie-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"beanie-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:18:\"beanie-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(8, 10, '_starter_content_theme', 'storefront'),
(10, 11, '_wp_attached_file', '2020/09/belt.jpg'),
(11, 11, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:801;s:6:\"height\";i:801;s:4:\"file\";s:16:\"2020/09/belt.jpg\";s:5:\"sizes\";a:6:{s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:16:\"belt-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:16:\"belt-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:16:\"belt-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"belt-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"belt-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:16:\"belt-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(12, 11, '_starter_content_theme', 'storefront'),
(14, 12, '_wp_attached_file', '2020/09/cap.jpg'),
(15, 12, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:801;s:6:\"height\";i:801;s:4:\"file\";s:15:\"2020/09/cap.jpg\";s:5:\"sizes\";a:6:{s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:15:\"cap-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:15:\"cap-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:15:\"cap-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"cap-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"cap-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:15:\"cap-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(16, 12, '_starter_content_theme', 'storefront'),
(18, 13, '_wp_attached_file', '2020/09/hoodie-with-logo.jpg'),
(19, 13, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:801;s:6:\"height\";i:801;s:4:\"file\";s:28:\"2020/09/hoodie-with-logo.jpg\";s:5:\"sizes\";a:6:{s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:28:\"hoodie-with-logo-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:28:\"hoodie-with-logo-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:28:\"hoodie-with-logo-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:28:\"hoodie-with-logo-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:28:\"hoodie-with-logo-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:28:\"hoodie-with-logo-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(20, 13, '_starter_content_theme', 'storefront'),
(22, 14, '_wp_attached_file', '2020/09/hoodie-with-pocket.jpg'),
(23, 14, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:801;s:6:\"height\";i:801;s:4:\"file\";s:30:\"2020/09/hoodie-with-pocket.jpg\";s:5:\"sizes\";a:6:{s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:30:\"hoodie-with-pocket-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:30:\"hoodie-with-pocket-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:30:\"hoodie-with-pocket-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"hoodie-with-pocket-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:30:\"hoodie-with-pocket-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:30:\"hoodie-with-pocket-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(24, 14, '_starter_content_theme', 'storefront'),
(26, 15, '_wp_attached_file', '2020/09/hoodie-with-zipper.jpg'),
(27, 15, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:800;s:4:\"file\";s:30:\"2020/09/hoodie-with-zipper.jpg\";s:5:\"sizes\";a:6:{s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:30:\"hoodie-with-zipper-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:30:\"hoodie-with-zipper-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:30:\"hoodie-with-zipper-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"hoodie-with-zipper-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:30:\"hoodie-with-zipper-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:30:\"hoodie-with-zipper-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(28, 15, '_starter_content_theme', 'storefront'),
(30, 16, '_wp_attached_file', '2020/09/hoodie.jpg'),
(31, 16, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:801;s:6:\"height\";i:801;s:4:\"file\";s:18:\"2020/09/hoodie.jpg\";s:5:\"sizes\";a:6:{s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:18:\"hoodie-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:18:\"hoodie-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:18:\"hoodie-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"hoodie-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"hoodie-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:18:\"hoodie-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(32, 16, '_starter_content_theme', 'storefront'),
(34, 17, '_wp_attached_file', '2020/09/long-sleeve-tee.jpg'),
(35, 17, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:801;s:6:\"height\";i:801;s:4:\"file\";s:27:\"2020/09/long-sleeve-tee.jpg\";s:5:\"sizes\";a:6:{s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:27:\"long-sleeve-tee-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:27:\"long-sleeve-tee-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:27:\"long-sleeve-tee-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:27:\"long-sleeve-tee-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"long-sleeve-tee-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:27:\"long-sleeve-tee-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(36, 17, '_starter_content_theme', 'storefront'),
(38, 18, '_wp_attached_file', '2020/09/polo.jpg'),
(39, 18, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:801;s:6:\"height\";i:800;s:4:\"file\";s:16:\"2020/09/polo.jpg\";s:5:\"sizes\";a:6:{s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:16:\"polo-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:16:\"polo-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:16:\"polo-416x415.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:415;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"polo-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"polo-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:16:\"polo-768x767.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:767;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(40, 18, '_starter_content_theme', 'storefront'),
(42, 19, '_wp_attached_file', '2020/09/sunglasses.jpg'),
(43, 19, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:801;s:6:\"height\";i:801;s:4:\"file\";s:22:\"2020/09/sunglasses.jpg\";s:5:\"sizes\";a:6:{s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:22:\"sunglasses-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:22:\"sunglasses-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:22:\"sunglasses-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"sunglasses-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"sunglasses-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:22:\"sunglasses-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(44, 19, '_starter_content_theme', 'storefront'),
(46, 20, '_wp_attached_file', '2020/09/tshirt.jpg'),
(47, 20, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:801;s:6:\"height\";i:801;s:4:\"file\";s:18:\"2020/09/tshirt.jpg\";s:5:\"sizes\";a:6:{s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:18:\"tshirt-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:18:\"tshirt-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:18:\"tshirt-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"tshirt-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"tshirt-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:18:\"tshirt-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(48, 20, '_starter_content_theme', 'storefront'),
(50, 21, '_wp_attached_file', '2020/09/vneck-tee.jpg'),
(51, 21, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:801;s:6:\"height\";i:800;s:4:\"file\";s:21:\"2020/09/vneck-tee.jpg\";s:5:\"sizes\";a:6:{s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:21:\"vneck-tee-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"vneck-tee-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:21:\"vneck-tee-416x415.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:415;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"vneck-tee-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"vneck-tee-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:21:\"vneck-tee-768x767.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:767;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(52, 21, '_starter_content_theme', 'storefront'),
(54, 22, '_wp_attached_file', '2020/09/hero-scaled.jpg'),
(55, 22, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2560;s:6:\"height\";i:1589;s:4:\"file\";s:23:\"2020/09/hero-scaled.jpg\";s:5:\"sizes\";a:9:{s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:23:\"hero-scaled-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:23:\"hero-scaled-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:23:\"hero-scaled-416x258.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:258;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"hero-300x186.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:186;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:17:\"hero-1024x635.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:635;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"hero-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:16:\"hero-768x477.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:477;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:17:\"hero-1536x953.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:953;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"2048x2048\";a:4:{s:4:\"file\";s:18:\"hero-2048x1271.jpg\";s:5:\"width\";i:2048;s:6:\"height\";i:1271;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(56, 22, '_starter_content_theme', 'storefront'),
(58, 23, '_wp_attached_file', '2020/09/accessories.jpg'),
(59, 23, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:801;s:6:\"height\";i:801;s:4:\"file\";s:23:\"2020/09/accessories.jpg\";s:5:\"sizes\";a:6:{s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:23:\"accessories-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:23:\"accessories-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:23:\"accessories-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"accessories-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"accessories-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:23:\"accessories-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(60, 23, '_starter_content_theme', 'storefront'),
(62, 24, '_wp_attached_file', '2020/09/tshirts.jpg'),
(63, 24, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:801;s:6:\"height\";i:801;s:4:\"file\";s:19:\"2020/09/tshirts.jpg\";s:5:\"sizes\";a:6:{s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:19:\"tshirts-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:19:\"tshirts-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:19:\"tshirts-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"tshirts-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"tshirts-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:19:\"tshirts-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(64, 24, '_starter_content_theme', 'storefront'),
(66, 25, '_wp_attached_file', '2020/09/hoodies.jpg'),
(67, 25, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:800;s:4:\"file\";s:19:\"2020/09/hoodies.jpg\";s:5:\"sizes\";a:6:{s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:19:\"hoodies-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:19:\"hoodies-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:19:\"hoodies-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"hoodies-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"hoodies-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:19:\"hoodies-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(68, 25, '_starter_content_theme', 'storefront'),
(70, 26, '_thumbnail_id', '22'),
(71, 26, '_wp_page_template', 'template-fullwidth.php'),
(73, 26, '_customize_changeset_uuid', 'c0155922-cd97-4248-9936-d521b14d5bf6'),
(75, 27, '_customize_changeset_uuid', 'c0155922-cd97-4248-9936-d521b14d5bf6'),
(76, 28, '_thumbnail_id', '10'),
(78, 28, '_customize_changeset_uuid', 'c0155922-cd97-4248-9936-d521b14d5bf6'),
(79, 29, '_thumbnail_id', '11'),
(81, 29, '_customize_changeset_uuid', 'c0155922-cd97-4248-9936-d521b14d5bf6'),
(82, 30, '_thumbnail_id', '12'),
(84, 30, '_customize_changeset_uuid', 'c0155922-cd97-4248-9936-d521b14d5bf6'),
(85, 31, '_thumbnail_id', '19'),
(87, 31, '_customize_changeset_uuid', 'c0155922-cd97-4248-9936-d521b14d5bf6'),
(88, 32, '_thumbnail_id', '13'),
(90, 32, '_customize_changeset_uuid', 'c0155922-cd97-4248-9936-d521b14d5bf6'),
(91, 33, '_thumbnail_id', '14'),
(93, 33, '_customize_changeset_uuid', 'c0155922-cd97-4248-9936-d521b14d5bf6'),
(94, 34, '_thumbnail_id', '15'),
(96, 34, '_customize_changeset_uuid', 'c0155922-cd97-4248-9936-d521b14d5bf6'),
(97, 35, '_thumbnail_id', '16'),
(99, 35, '_customize_changeset_uuid', 'c0155922-cd97-4248-9936-d521b14d5bf6'),
(100, 36, '_thumbnail_id', '81'),
(102, 36, '_customize_changeset_uuid', 'c0155922-cd97-4248-9936-d521b14d5bf6'),
(103, 37, '_thumbnail_id', '80'),
(105, 37, '_customize_changeset_uuid', 'c0155922-cd97-4248-9936-d521b14d5bf6'),
(106, 38, '_thumbnail_id', '79'),
(108, 38, '_customize_changeset_uuid', 'c0155922-cd97-4248-9936-d521b14d5bf6'),
(109, 39, '_thumbnail_id', '78'),
(111, 39, '_customize_changeset_uuid', 'c0155922-cd97-4248-9936-d521b14d5bf6'),
(112, 28, '_regular_price', '20'),
(113, 28, '_sale_price', '18'),
(114, 28, 'total_sales', '0'),
(115, 28, '_tax_status', 'taxable'),
(116, 28, '_tax_class', ''),
(117, 28, '_manage_stock', 'no'),
(118, 28, '_backorders', 'no'),
(119, 28, '_sold_individually', 'no'),
(120, 28, '_virtual', 'no'),
(121, 28, '_downloadable', 'no'),
(122, 28, '_download_limit', '-1'),
(123, 28, '_download_expiry', '-1'),
(124, 28, '_stock', NULL),
(125, 28, '_stock_status', 'instock'),
(126, 28, '_wc_average_rating', '0'),
(127, 28, '_wc_review_count', '0'),
(128, 28, '_product_version', '4.3.0'),
(129, 28, '_price', '18'),
(130, 29, '_regular_price', '65'),
(131, 29, '_sale_price', '55'),
(132, 29, 'total_sales', '0'),
(133, 29, '_tax_status', 'taxable'),
(134, 29, '_tax_class', ''),
(135, 29, '_manage_stock', 'no'),
(136, 29, '_backorders', 'no'),
(137, 29, '_sold_individually', 'no'),
(138, 29, '_virtual', 'no'),
(139, 29, '_downloadable', 'no'),
(140, 29, '_download_limit', '-1'),
(141, 29, '_download_expiry', '-1'),
(142, 29, '_stock', NULL),
(143, 29, '_stock_status', 'instock'),
(144, 29, '_wc_average_rating', '0'),
(145, 29, '_wc_review_count', '0'),
(146, 29, '_product_version', '4.3.0'),
(147, 29, '_price', '55'),
(148, 30, '_regular_price', '18'),
(149, 30, '_sale_price', '16'),
(150, 30, 'total_sales', '0'),
(151, 30, '_tax_status', 'taxable'),
(152, 30, '_tax_class', ''),
(153, 30, '_manage_stock', 'no'),
(154, 30, '_backorders', 'no'),
(155, 30, '_sold_individually', 'no'),
(156, 30, '_virtual', 'no'),
(157, 30, '_downloadable', 'no'),
(158, 30, '_download_limit', '-1'),
(159, 30, '_download_expiry', '-1'),
(160, 30, '_stock', NULL),
(161, 30, '_stock_status', 'instock'),
(162, 30, '_wc_average_rating', '0'),
(163, 30, '_wc_review_count', '0'),
(164, 30, '_product_version', '4.3.0'),
(165, 30, '_price', '16'),
(166, 31, '_regular_price', '90'),
(167, 31, 'total_sales', '0'),
(168, 31, '_tax_status', 'taxable'),
(169, 31, '_tax_class', ''),
(170, 31, '_manage_stock', 'no'),
(171, 31, '_backorders', 'no'),
(172, 31, '_sold_individually', 'no'),
(173, 31, '_virtual', 'no'),
(174, 31, '_downloadable', 'no'),
(175, 31, '_download_limit', '-1'),
(176, 31, '_download_expiry', '-1'),
(177, 31, '_stock', NULL),
(178, 31, '_stock_status', 'instock'),
(179, 31, '_wc_average_rating', '0'),
(180, 31, '_wc_review_count', '0'),
(181, 31, '_product_version', '4.3.0'),
(182, 31, '_price', '90'),
(183, 32, '_regular_price', '45'),
(184, 32, 'total_sales', '0'),
(185, 32, '_tax_status', 'taxable'),
(186, 32, '_tax_class', ''),
(187, 32, '_manage_stock', 'no'),
(188, 32, '_backorders', 'no'),
(189, 32, '_sold_individually', 'no'),
(190, 32, '_virtual', 'no'),
(191, 32, '_downloadable', 'no'),
(192, 32, '_download_limit', '-1'),
(193, 32, '_download_expiry', '-1'),
(194, 32, '_stock', NULL),
(195, 32, '_stock_status', 'instock'),
(196, 32, '_wc_average_rating', '0'),
(197, 32, '_wc_review_count', '0'),
(198, 32, '_product_version', '4.3.0'),
(199, 32, '_price', '45'),
(200, 33, '_regular_price', '45'),
(201, 33, '_sale_price', '35'),
(202, 33, 'total_sales', '0'),
(203, 33, '_tax_status', 'taxable'),
(204, 33, '_tax_class', ''),
(205, 33, '_manage_stock', 'no'),
(206, 33, '_backorders', 'no'),
(207, 33, '_sold_individually', 'no'),
(208, 33, '_virtual', 'no'),
(209, 33, '_downloadable', 'no'),
(210, 33, '_download_limit', '-1'),
(211, 33, '_download_expiry', '-1'),
(212, 33, '_stock', NULL),
(213, 33, '_stock_status', 'instock'),
(214, 33, '_wc_average_rating', '0'),
(215, 33, '_wc_review_count', '0'),
(216, 33, '_product_version', '4.3.0'),
(217, 33, '_price', '35'),
(218, 34, '_regular_price', '45'),
(219, 34, 'total_sales', '0'),
(220, 34, '_tax_status', 'taxable'),
(221, 34, '_tax_class', ''),
(222, 34, '_manage_stock', 'no'),
(223, 34, '_backorders', 'no'),
(224, 34, '_sold_individually', 'no'),
(225, 34, '_virtual', 'no'),
(226, 34, '_downloadable', 'no'),
(227, 34, '_download_limit', '-1'),
(228, 34, '_download_expiry', '-1'),
(229, 34, '_stock', NULL),
(230, 34, '_stock_status', 'instock'),
(231, 34, '_wc_average_rating', '0'),
(232, 34, '_wc_review_count', '0'),
(233, 34, '_product_version', '4.3.0'),
(234, 34, '_price', '45'),
(235, 35, '_regular_price', '45'),
(236, 35, '_sale_price', '42'),
(237, 35, 'total_sales', '0'),
(238, 35, '_tax_status', 'taxable'),
(239, 35, '_tax_class', ''),
(240, 35, '_manage_stock', 'no'),
(241, 35, '_backorders', 'no'),
(242, 35, '_sold_individually', 'no'),
(243, 35, '_virtual', 'no'),
(244, 35, '_downloadable', 'no'),
(245, 35, '_download_limit', '-1'),
(246, 35, '_download_expiry', '-1'),
(247, 35, '_stock', NULL),
(248, 35, '_stock_status', 'instock'),
(249, 35, '_wc_average_rating', '0'),
(250, 35, '_wc_review_count', '0'),
(251, 35, '_product_version', '4.3.0'),
(252, 35, '_price', '42'),
(253, 36, '_regular_price', '321'),
(254, 36, 'total_sales', '0'),
(255, 36, '_tax_status', 'taxable'),
(256, 36, '_tax_class', ''),
(257, 36, '_manage_stock', 'no'),
(258, 36, '_backorders', 'no'),
(259, 36, '_sold_individually', 'no'),
(260, 36, '_virtual', 'no'),
(261, 36, '_downloadable', 'no'),
(262, 36, '_download_limit', '-1'),
(263, 36, '_download_expiry', '-1'),
(264, 36, '_stock', NULL),
(265, 36, '_stock_status', 'instock'),
(266, 36, '_wc_average_rating', '0'),
(267, 36, '_wc_review_count', '0'),
(268, 36, '_product_version', '4.3.0'),
(269, 36, '_price', '321'),
(270, 37, '_regular_price', '122'),
(271, 37, 'total_sales', '0'),
(272, 37, '_tax_status', 'taxable'),
(273, 37, '_tax_class', ''),
(274, 37, '_manage_stock', 'no'),
(275, 37, '_backorders', 'no'),
(276, 37, '_sold_individually', 'no'),
(277, 37, '_virtual', 'no'),
(278, 37, '_downloadable', 'no'),
(279, 37, '_download_limit', '-1'),
(280, 37, '_download_expiry', '-1'),
(281, 37, '_stock', NULL),
(282, 37, '_stock_status', 'instock'),
(283, 37, '_wc_average_rating', '0'),
(284, 37, '_wc_review_count', '0'),
(285, 37, '_product_version', '4.3.0'),
(286, 37, '_price', '122'),
(287, 38, '_regular_price', '230'),
(288, 38, 'total_sales', '0'),
(289, 38, '_tax_status', 'taxable'),
(290, 38, '_tax_class', ''),
(291, 38, '_manage_stock', 'no'),
(292, 38, '_backorders', 'no'),
(293, 38, '_sold_individually', 'no'),
(294, 38, '_virtual', 'no'),
(295, 38, '_downloadable', 'no'),
(296, 38, '_download_limit', '-1'),
(297, 38, '_download_expiry', '-1'),
(298, 38, '_stock', NULL),
(299, 38, '_stock_status', 'instock'),
(300, 38, '_wc_average_rating', '0'),
(301, 38, '_wc_review_count', '0'),
(302, 38, '_product_version', '4.3.0'),
(303, 38, '_price', '230'),
(304, 39, '_regular_price', '200'),
(305, 39, 'total_sales', '0'),
(306, 39, '_tax_status', 'taxable'),
(307, 39, '_tax_class', ''),
(308, 39, '_manage_stock', 'no'),
(309, 39, '_backorders', 'no'),
(310, 39, '_sold_individually', 'no'),
(311, 39, '_virtual', 'no'),
(312, 39, '_downloadable', 'no'),
(313, 39, '_download_limit', '-1'),
(314, 39, '_download_expiry', '-1'),
(315, 39, '_stock', NULL),
(316, 39, '_stock_status', 'instock'),
(317, 39, '_wc_average_rating', '0'),
(318, 39, '_wc_review_count', '0'),
(319, 39, '_product_version', '4.3.0'),
(320, 39, '_price', '120'),
(321, 41, '_wp_attached_file', '2020/09/logo.jpg'),
(322, 41, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:258;s:6:\"height\";i:111;s:4:\"file\";s:16:\"2020/09/logo.jpg\";s:5:\"sizes\";a:3:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:16:\"logo-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"logo-150x111.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:111;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:16:\"logo-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(323, 40, '_edit_lock', '1599654398:1'),
(324, 40, '_wp_trash_meta_status', 'publish'),
(325, 40, '_wp_trash_meta_time', '1599654419'),
(326, 26, '_edit_lock', '1600262654:1'),
(329, 45, '_wp_attached_file', '2020/09/banner1.jpg'),
(330, 45, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:793;s:4:\"file\";s:19:\"2020/09/banner1.jpg\";s:5:\"sizes\";a:11:{s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"banner1-300x124.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:124;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:20:\"banner1-1024x423.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:423;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"banner1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:19:\"banner1-768x317.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:317;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:20:\"banner1-1536x634.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:634;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:19:\"banner1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:19:\"banner1-416x172.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:172;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:19:\"banner1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:19:\"banner1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:19:\"banner1-416x172.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:172;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:19:\"banner1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(331, 47, '_wp_attached_file', '2020/09/cropped-logo.jpg'),
(332, 47, '_wp_attachment_context', 'custom-logo'),
(333, 47, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:258;s:6:\"height\";i:73;s:4:\"file\";s:24:\"2020/09/cropped-logo.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"cropped-logo-150x73.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:73;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:23:\"cropped-logo-100x73.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:73;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:23:\"cropped-logo-100x73.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:73;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(335, 48, '_wp_trash_meta_status', 'publish'),
(336, 48, '_wp_trash_meta_time', '1599795064'),
(337, 49, '_menu_item_type', 'post_type'),
(338, 49, '_menu_item_menu_item_parent', '0'),
(339, 49, '_menu_item_object_id', '26'),
(340, 49, '_menu_item_object', 'page'),
(341, 49, '_menu_item_target', ''),
(342, 49, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(343, 49, '_menu_item_xfn', ''),
(344, 49, '_menu_item_url', ''),
(346, 50, '_menu_item_type', 'post_type'),
(347, 50, '_menu_item_menu_item_parent', '0'),
(348, 50, '_menu_item_object_id', '6'),
(349, 50, '_menu_item_object', 'page'),
(350, 50, '_menu_item_target', ''),
(351, 50, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(352, 50, '_menu_item_xfn', ''),
(353, 50, '_menu_item_url', ''),
(355, 51, '_edit_lock', '1599804581:1'),
(356, 51, '_wp_page_template', 'template-fullwidth.php'),
(357, 53, '_menu_item_type', 'post_type'),
(358, 53, '_menu_item_menu_item_parent', '0'),
(359, 53, '_menu_item_object_id', '51'),
(360, 53, '_menu_item_object', 'page'),
(361, 53, '_menu_item_target', ''),
(362, 53, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(363, 53, '_menu_item_xfn', ''),
(364, 53, '_menu_item_url', ''),
(365, 57, '_wp_attached_file', 'hugeit-slider/slide1.jpg'),
(366, 58, '_wp_attached_file', 'hugeit-slider/slide2.jpg'),
(367, 59, '_wp_attached_file', 'hugeit-slider/slide3.jpg'),
(368, 60, '_wp_attached_file', '2020/09/banner1-1.jpg'),
(369, 60, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:793;s:4:\"file\";s:21:\"2020/09/banner1-1.jpg\";s:5:\"sizes\";a:11:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"banner1-1-300x124.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:124;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:22:\"banner1-1-1024x423.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:423;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"banner1-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:21:\"banner1-1-768x317.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:317;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:22:\"banner1-1-1536x634.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:634;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:21:\"banner1-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:21:\"banner1-1-416x172.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:172;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"banner1-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:21:\"banner1-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:21:\"banner1-1-416x172.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:172;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"banner1-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(370, 63, 'categories', 'a:3:{i:18;a:5:{s:4:\"name\";s:0:\"\";s:3:\"url\";s:0:\"\";s:11:\"description\";s:0:\"\";s:8:\"image_id\";s:0:\"\";s:4:\"icon\";s:0:\"\";}i:17;a:5:{s:4:\"name\";s:0:\"\";s:3:\"url\";s:0:\"\";s:11:\"description\";s:0:\"\";s:8:\"image_id\";s:0:\"\";s:4:\"icon\";s:0:\"\";}i:16;a:5:{s:4:\"name\";s:0:\"\";s:3:\"url\";s:0:\"\";s:11:\"description\";s:0:\"\";s:8:\"image_id\";s:0:\"\";s:4:\"icon\";s:0:\"\";}}'),
(371, 63, 'selection_type', 'all'),
(372, 63, 'selected_categories', ''),
(373, 63, 'limit_number', '10'),
(374, 63, 'include_child', 'off'),
(375, 63, 'hide_empty', 'off'),
(376, 63, 'hide_image', 'off'),
(377, 63, 'hide_content', 'off'),
(378, 63, 'hide_button', 'on'),
(379, 63, 'hide_icon', 'off'),
(380, 63, 'hide_name', 'off'),
(381, 63, 'hide_count', 'on'),
(382, 63, 'hide_nav', 'off'),
(383, 63, 'hide_paginate', 'off'),
(384, 63, 'hide_border', 'off'),
(385, 63, 'hover_style', 'hover-zoom-in'),
(386, 63, 'theme', 'default'),
(387, 63, 'autoplay', 'off'),
(388, 63, 'rtl', 'off'),
(389, 63, '_edit_last', '1'),
(390, 63, '_edit_lock', '1600242971:1'),
(391, 64, '_wp_attached_file', '2020/09/category_1.jpg'),
(392, 64, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:401;s:6:\"height\";i:442;s:4:\"file\";s:22:\"2020/09/category_1.jpg\";s:5:\"sizes\";a:6:{s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"category_1-272x300.jpg\";s:5:\"width\";i:272;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"category_1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:22:\"category_1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:22:\"category_1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:22:\"category_1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:22:\"category_1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(393, 65, '_wp_attached_file', '2020/09/category_2.jpg'),
(394, 65, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:401;s:6:\"height\";i:442;s:4:\"file\";s:22:\"2020/09/category_2.jpg\";s:5:\"sizes\";a:6:{s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"category_2-272x300.jpg\";s:5:\"width\";i:272;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"category_2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:22:\"category_2-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:22:\"category_2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:22:\"category_2-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:22:\"category_2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(395, 66, '_wp_attached_file', '2020/09/category_3.jpg'),
(396, 66, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:401;s:6:\"height\";i:442;s:4:\"file\";s:22:\"2020/09/category_3.jpg\";s:5:\"sizes\";a:6:{s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"category_3-272x300.jpg\";s:5:\"width\";i:272;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"category_3-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:22:\"category_3-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:22:\"category_3-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:22:\"category_3-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:22:\"category_3-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(397, 39, '_edit_last', '1'),
(398, 39, '_edit_lock', '1600183373:1'),
(399, 38, '_edit_last', '1'),
(400, 38, '_edit_lock', '1600183396:1'),
(401, 37, '_edit_last', '1'),
(402, 37, '_edit_lock', '1600183408:1'),
(403, 36, '_edit_last', '1'),
(404, 36, '_edit_lock', '1600183422:1'),
(405, 35, '_edit_last', '1'),
(406, 35, '_edit_lock', '1600175068:1'),
(407, 34, '_edit_last', '1'),
(408, 34, '_edit_lock', '1600175083:1'),
(409, 28, '_edit_last', '1'),
(410, 28, '_edit_lock', '1600175116:1'),
(411, 29, '_edit_last', '1'),
(412, 29, '_edit_lock', '1600175126:1'),
(413, 30, '_edit_last', '1'),
(414, 30, '_edit_lock', '1600175138:1'),
(415, 75, '_edit_last', '1'),
(416, 75, '_edit_lock', '1600243022:1'),
(417, 75, 'wps_slider_type', 'product_slider'),
(418, 75, 'wps_themes', 'theme_two');
INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(419, 75, 'wps_products_from', 'latest'),
(420, 75, 'wps_number_of_column', '4'),
(421, 75, 'wps_number_of_column_desktop', '4'),
(422, 75, 'wps_number_of_column_tablet', '3'),
(423, 75, 'wps_number_of_column_mobile', '1'),
(424, 75, 'wps_number_of_total_products', '10'),
(425, 75, 'wps_order_by', 'date'),
(426, 75, 'wps_order', 'DESC'),
(427, 75, 'wps_auto_play', 'on'),
(428, 75, 'wps_auto_play_speed', '3000'),
(429, 75, 'wps_scroll_speed', '600'),
(430, 75, 'wps_pause_on_hover', 'on'),
(431, 75, 'wps_show_navigation', 'on'),
(432, 75, 'wps_nav_arrow_color', '#ffffff'),
(433, 75, 'wps_nav_arrow_bg', '#444444'),
(434, 75, 'wps_show_pagination', 'off'),
(435, 75, 'wps_pagination_color', '#cccccc'),
(436, 75, 'wps_pagination_active_color', '#333333'),
(437, 75, 'wps_touch_swipe', 'on'),
(438, 75, 'wps_mouse_draggable', 'on'),
(439, 75, 'wps_rtl', 'off'),
(440, 75, 'wps_slider_title', 'on'),
(441, 75, 'wps_slider_title_font_size', '30'),
(442, 75, 'wps_slider_title_color', '#009cdd'),
(443, 75, 'wps_product_name', 'on'),
(444, 75, 'wps_product_name_font_size', '15'),
(445, 75, 'wps_product_name_color', '#0c0c0c'),
(446, 75, 'wps_product_name_hover_color', '#444444'),
(447, 75, 'wps_product_price', 'on'),
(448, 75, 'wps_price_color', '#c5c5c5'),
(449, 75, 'wps_discount_price_color', '#58acca'),
(450, 75, 'wps_product_rating', 'on'),
(451, 75, 'wps_add_to_cart', 'on'),
(452, 75, 'wps_add_to_cart_color', '#444444'),
(453, 75, 'wps_add_to_cart_bg', '#494949'),
(454, 75, 'wps_add_to_cart_border_color', '#222222'),
(455, 75, 'wps_add_to_cart_hover_color', '#ffffff'),
(456, 75, 'wps_add_to_cart_hover_bg', '#222222'),
(457, 75, 'wps_add_to_cart_border_hover_color', '#222222'),
(458, 78, '_wp_attached_file', '2020/09/img_1.jpg'),
(459, 78, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:438;s:6:\"height\";i:542;s:4:\"file\";s:17:\"2020/09/img_1.jpg\";s:5:\"sizes\";a:8:{s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"img_1-242x300.jpg\";s:5:\"width\";i:242;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"img_1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:17:\"img_1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:17:\"img_1-416x515.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:515;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:17:\"img_1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:17:\"img_1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:17:\"img_1-416x515.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:515;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:17:\"img_1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(460, 39, '_sale_price', '120'),
(461, 79, '_wp_attached_file', '2020/09/img_2.jpg'),
(462, 79, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:438;s:6:\"height\";i:542;s:4:\"file\";s:17:\"2020/09/img_2.jpg\";s:5:\"sizes\";a:8:{s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"img_2-242x300.jpg\";s:5:\"width\";i:242;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"img_2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:17:\"img_2-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:17:\"img_2-416x515.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:515;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:17:\"img_2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:17:\"img_2-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:17:\"img_2-416x515.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:515;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:17:\"img_2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(463, 80, '_wp_attached_file', '2020/09/img_3.jpg'),
(464, 80, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:438;s:6:\"height\";i:542;s:4:\"file\";s:17:\"2020/09/img_3.jpg\";s:5:\"sizes\";a:8:{s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"img_3-242x300.jpg\";s:5:\"width\";i:242;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"img_3-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:17:\"img_3-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:17:\"img_3-416x515.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:515;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:17:\"img_3-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:17:\"img_3-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:17:\"img_3-416x515.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:515;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:17:\"img_3-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(465, 81, '_wp_attached_file', '2020/09/img_4.jpg'),
(466, 81, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:438;s:6:\"height\";i:542;s:4:\"file\";s:17:\"2020/09/img_4.jpg\";s:5:\"sizes\";a:8:{s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"img_4-242x300.jpg\";s:5:\"width\";i:242;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"img_4-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:17:\"img_4-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:17:\"img_4-416x515.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:515;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:17:\"img_4-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:17:\"img_4-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:17:\"img_4-416x515.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:515;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:17:\"img_4-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(467, 35, '_wp_trash_meta_status', 'publish'),
(468, 35, '_wp_trash_meta_time', '1600183327'),
(469, 35, '_wp_desired_post_slug', 'hoodie'),
(470, 34, '_wp_trash_meta_status', 'publish'),
(471, 34, '_wp_trash_meta_time', '1600183327'),
(472, 34, '_wp_desired_post_slug', 'hoodie-with-zipper'),
(473, 33, '_wp_trash_meta_status', 'publish'),
(474, 33, '_wp_trash_meta_time', '1600183328'),
(475, 33, '_wp_desired_post_slug', 'hoodie-with-pocket'),
(476, 32, '_wp_trash_meta_status', 'publish'),
(477, 32, '_wp_trash_meta_time', '1600183329'),
(478, 32, '_wp_desired_post_slug', 'hoodie-with-logo'),
(479, 31, '_wp_trash_meta_status', 'publish'),
(480, 31, '_wp_trash_meta_time', '1600183329'),
(481, 31, '_wp_desired_post_slug', 'sunglasses'),
(482, 30, '_wp_trash_meta_status', 'publish'),
(483, 30, '_wp_trash_meta_time', '1600183330'),
(484, 30, '_wp_desired_post_slug', 'cap'),
(485, 29, '_wp_trash_meta_status', 'publish'),
(486, 29, '_wp_trash_meta_time', '1600183331'),
(487, 29, '_wp_desired_post_slug', 'belt'),
(488, 28, '_wp_trash_meta_status', 'publish'),
(489, 28, '_wp_trash_meta_time', '1600183331'),
(490, 28, '_wp_desired_post_slug', 'beanie'),
(491, 39, '_wp_old_slug', 'vneck-tee'),
(492, 38, '_wp_old_slug', 'tshirt'),
(493, 37, '_wp_old_slug', 'polo'),
(494, 36, '_wp_old_slug', 'long-sleeve-tee'),
(495, 82, '_regular_price', '213'),
(497, 82, 'total_sales', '0'),
(498, 82, '_tax_status', 'taxable'),
(499, 82, '_tax_class', ''),
(500, 82, '_manage_stock', 'no'),
(501, 82, '_backorders', 'no'),
(502, 82, '_sold_individually', 'no'),
(503, 82, '_virtual', 'no'),
(504, 82, '_downloadable', 'no'),
(505, 82, '_download_limit', '-1'),
(506, 82, '_download_expiry', '-1'),
(507, 82, '_thumbnail_id', '83'),
(508, 82, '_stock', NULL),
(509, 82, '_stock_status', 'instock'),
(510, 82, '_wc_average_rating', '0'),
(511, 82, '_wc_review_count', '0'),
(512, 82, '_product_version', '4.3.0'),
(513, 82, '_price', '213'),
(514, 82, '_customize_changeset_uuid', 'c0155922-cd97-4248-9936-d521b14d5bf6'),
(515, 82, '_edit_lock', '1600183675:1'),
(516, 82, '_edit_last', '1'),
(517, 83, '_wp_attached_file', '2020/09/img_8.jpg'),
(518, 83, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:438;s:6:\"height\";i:541;s:4:\"file\";s:17:\"2020/09/img_8.jpg\";s:5:\"sizes\";a:8:{s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"img_8-243x300.jpg\";s:5:\"width\";i:243;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"img_8-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:17:\"img_8-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:17:\"img_8-416x514.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:514;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:17:\"img_8-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:17:\"img_8-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:17:\"img_8-416x514.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:514;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:17:\"img_8-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(519, 84, '_regular_price', '213'),
(520, 84, 'total_sales', '0'),
(521, 84, '_tax_status', 'taxable'),
(522, 84, '_tax_class', ''),
(523, 84, '_manage_stock', 'no'),
(524, 84, '_backorders', 'no'),
(525, 84, '_sold_individually', 'no'),
(526, 84, '_virtual', 'no'),
(527, 84, '_downloadable', 'no'),
(528, 84, '_download_limit', '-1'),
(529, 84, '_download_expiry', '-1'),
(530, 84, '_thumbnail_id', '85'),
(531, 84, '_stock', NULL),
(532, 84, '_stock_status', 'instock'),
(533, 84, '_wc_average_rating', '0'),
(534, 84, '_wc_review_count', '1'),
(535, 84, '_product_version', '4.3.0'),
(536, 84, '_price', '213'),
(537, 84, '_customize_changeset_uuid', 'c0155922-cd97-4248-9936-d521b14d5bf6'),
(538, 84, '_edit_lock', '1600264070:1'),
(539, 85, '_wp_attached_file', '2020/09/img_7.jpg'),
(540, 85, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:438;s:6:\"height\";i:541;s:4:\"file\";s:17:\"2020/09/img_7.jpg\";s:5:\"sizes\";a:8:{s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"img_7-243x300.jpg\";s:5:\"width\";i:243;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"img_7-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:17:\"img_7-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:17:\"img_7-416x514.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:514;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:17:\"img_7-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:17:\"img_7-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:17:\"img_7-416x514.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:514;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:17:\"img_7-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(541, 84, '_edit_last', '1'),
(542, 102, '_wp_attached_file', '2020/09/quad-pay-banner.jpg'),
(543, 102, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:885;s:4:\"file\";s:27:\"2020/09/quad-pay-banner.jpg\";s:5:\"sizes\";a:9:{s:6:\"medium\";a:4:{s:4:\"file\";s:27:\"quad-pay-banner-300x277.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:277;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"quad-pay-banner-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:27:\"quad-pay-banner-768x708.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:708;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:27:\"quad-pay-banner-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:27:\"quad-pay-banner-416x384.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:384;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:27:\"quad-pay-banner-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:27:\"quad-pay-banner-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:27:\"quad-pay-banner-416x384.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:384;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:27:\"quad-pay-banner-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(544, 108, '_edit_lock', '1600194004:1'),
(545, 109, '_menu_item_type', 'post_type'),
(546, 109, '_menu_item_menu_item_parent', '0'),
(547, 109, '_menu_item_object_id', '27'),
(548, 109, '_menu_item_object', 'page'),
(549, 109, '_menu_item_target', ''),
(550, 109, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(551, 109, '_menu_item_xfn', ''),
(552, 109, '_menu_item_url', ''),
(563, 111, '_menu_item_type', 'post_type'),
(564, 111, '_menu_item_menu_item_parent', '0'),
(565, 111, '_menu_item_object_id', '51'),
(566, 111, '_menu_item_object', 'page'),
(567, 111, '_menu_item_target', ''),
(568, 111, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(569, 111, '_menu_item_xfn', ''),
(570, 111, '_menu_item_url', ''),
(572, 112, '_menu_item_type', 'post_type'),
(573, 112, '_menu_item_menu_item_parent', '0'),
(574, 112, '_menu_item_object_id', '6'),
(575, 112, '_menu_item_object', 'page'),
(576, 112, '_menu_item_target', ''),
(577, 112, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(578, 112, '_menu_item_xfn', ''),
(579, 112, '_menu_item_url', ''),
(581, 108, '_customize_restore_dismissed', '1'),
(582, 113, '_edit_lock', '1600194459:1'),
(583, 113, '_wp_trash_meta_status', 'publish'),
(584, 113, '_wp_trash_meta_time', '1600194506'),
(585, 114, '_edit_lock', '1600194607:1'),
(586, 114, '_wp_trash_meta_status', 'publish'),
(587, 114, '_wp_trash_meta_time', '1600194621'),
(588, 115, '_edit_lock', '1600229469:1'),
(589, 115, '_wp_trash_meta_status', 'publish'),
(590, 115, '_wp_trash_meta_time', '1600229471'),
(591, 116, '_edit_lock', '1600261955:1'),
(592, 2, '_edit_lock', '1600246997:1'),
(593, 116, '_customize_restore_dismissed', '1'),
(594, 9, '_edit_lock', '1600262417:1'),
(595, 9, '_wp_page_template', 'template-fullwidth.php'),
(596, 3, '_edit_last', '1'),
(597, 3, '_edit_lock', '1600255042:1'),
(598, 122, '_edit_lock', '1600254928:1'),
(599, 122, '_wp_page_template', 'template-fullwidth.php'),
(600, 124, '_edit_lock', '1600254955:1'),
(601, 124, '_wp_page_template', 'template-fullwidth.php'),
(602, 126, '_menu_item_type', 'post_type'),
(603, 126, '_menu_item_menu_item_parent', '0'),
(604, 126, '_menu_item_object_id', '3'),
(605, 126, '_menu_item_object', 'page'),
(606, 126, '_menu_item_target', ''),
(607, 126, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(608, 126, '_menu_item_xfn', ''),
(609, 126, '_menu_item_url', ''),
(611, 127, '_menu_item_type', 'post_type'),
(612, 127, '_menu_item_menu_item_parent', '0'),
(613, 127, '_menu_item_object_id', '124'),
(614, 127, '_menu_item_object', 'page'),
(615, 127, '_menu_item_target', ''),
(616, 127, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(617, 127, '_menu_item_xfn', ''),
(618, 127, '_menu_item_url', ''),
(620, 128, '_menu_item_type', 'post_type'),
(621, 128, '_menu_item_menu_item_parent', '0'),
(622, 128, '_menu_item_object_id', '122'),
(623, 128, '_menu_item_object', 'page'),
(624, 128, '_menu_item_target', ''),
(625, 128, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(626, 128, '_menu_item_xfn', ''),
(627, 128, '_menu_item_url', ''),
(629, 129, '_wp_trash_meta_status', 'publish'),
(630, 129, '_wp_trash_meta_time', '1600255915'),
(631, 130, '_wp_trash_meta_status', 'publish'),
(632, 130, '_wp_trash_meta_time', '1600257653'),
(633, 131, '_wp_trash_meta_status', 'publish'),
(634, 131, '_wp_trash_meta_time', '1600259535'),
(635, 26, '_edit_last', '1');

-- --------------------------------------------------------

--
-- Table structure for table `wp_posts`
--

CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2020-09-06 07:02:24', '2020-09-06 07:02:24', '<!-- wp:paragraph -->\n<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\n<!-- /wp:paragraph -->', 'Hello world!', '', 'publish', 'open', 'open', '', 'hello-world', '', '', '2020-09-06 07:02:24', '2020-09-06 07:02:24', '', 0, 'http://localhost/wallart/?p=1', 0, 'post', '', 1),
(2, 1, '2020-09-06 07:02:24', '2020-09-06 07:02:24', '<!-- wp:paragraph -->\n<p>This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...or something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>As a new WordPress user, you should go to <a href=\"http://localhost/wallart/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!</p>\n<!-- /wp:paragraph -->', 'Sample Page', '', 'publish', 'closed', 'open', '', 'sample-page', '', '', '2020-09-06 07:02:24', '2020-09-06 07:02:24', '', 0, 'http://localhost/wallart/?page_id=2', 0, 'page', '', 0),
(3, 1, '2020-09-06 07:02:24', '2020-09-06 07:02:24', '<!-- wp:heading --><h2>Who we are</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Our website address is: http://localhost/wallart.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What personal data we collect and why we collect it</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Comments</h3><!-- /wp:heading --><!-- wp:paragraph --><p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Media</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Contact forms</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Cookies</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you visit our login page, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Embedded content from other websites</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Analytics</h3><!-- /wp:heading --><!-- wp:heading --><h2>Who we share your data with</h2><!-- /wp:heading --><!-- wp:heading --><h2>How long we retain your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What rights you have over your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Where we send your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Visitor comments may be checked through an automated spam detection service.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Your contact information</h2><!-- /wp:heading --><!-- wp:heading --><h2>Additional information</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>How we protect your data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What data breach procedures we have in place</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What third parties we receive data from</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What automated decision making and/or profiling we do with user data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Industry regulatory disclosure requirements</h3><!-- /wp:heading -->', 'Privacy Policy', '', 'publish', 'closed', 'closed', '', 'privacy-policy', '', '', '2020-09-16 11:17:21', '2020-09-16 11:17:21', '', 0, 'http://localhost/wallart/?page_id=3', 0, 'page', '', 0),
(5, 1, '2020-09-07 06:09:43', '2020-09-07 06:09:43', '', 'woocommerce-placeholder', '', 'inherit', 'open', 'closed', '', 'woocommerce-placeholder', '', '', '2020-09-07 06:09:43', '2020-09-07 06:09:43', '', 0, 'http://localhost/wallart/wp-content/uploads/2020/09/woocommerce-placeholder.png', 0, 'attachment', 'image/png', 0),
(6, 1, '2020-09-07 06:13:46', '2020-09-07 06:13:46', '', 'Shop', '', 'publish', 'closed', 'closed', '', 'shop', '', '', '2020-09-07 06:13:46', '2020-09-07 06:13:46', '', 0, 'http://localhost/wallart/index.php/shop/', 0, 'page', '', 0),
(7, 1, '2020-09-07 06:13:47', '2020-09-07 06:13:47', '<!-- wp:shortcode -->[woocommerce_cart]<!-- /wp:shortcode -->', 'Cart', '', 'publish', 'closed', 'closed', '', 'cart', '', '', '2020-09-07 06:13:47', '2020-09-07 06:13:47', '', 0, 'http://localhost/wallart/index.php/cart/', 0, 'page', '', 0),
(8, 1, '2020-09-07 06:13:47', '2020-09-07 06:13:47', '<!-- wp:shortcode -->[woocommerce_checkout]<!-- /wp:shortcode -->', 'Checkout', '', 'publish', 'closed', 'closed', '', 'checkout', '', '', '2020-09-07 06:13:47', '2020-09-07 06:13:47', '', 0, 'http://localhost/wallart/index.php/checkout/', 0, 'page', '', 0),
(9, 1, '2020-09-07 06:13:47', '2020-09-07 06:13:47', '<!-- wp:shortcode -->\n[woocommerce_my_account]\n<!-- /wp:shortcode -->', 'My account', '', 'publish', 'closed', 'closed', '', 'my-account', '', '', '2020-09-16 10:13:02', '2020-09-16 10:13:02', '', 0, 'http://localhost/wallart/index.php/my-account/', 0, 'page', '', 0),
(10, 1, '2020-09-09 12:26:34', '2020-09-09 12:26:34', '', 'Beanie', '', 'inherit', 'open', 'closed', '', 'beanie-image', '', '', '2020-09-09 12:26:34', '2020-09-09 12:26:34', '', 0, 'http://localhost/wallart/wp-content/uploads/2020/09/beanie.jpg', 0, 'attachment', 'image/jpeg', 0),
(11, 1, '2020-09-09 12:26:35', '2020-09-09 12:26:35', '', 'Belt', '', 'inherit', 'open', 'closed', '', 'belt-image', '', '', '2020-09-09 12:26:35', '2020-09-09 12:26:35', '', 0, 'http://localhost/wallart/wp-content/uploads/2020/09/belt.jpg', 0, 'attachment', 'image/jpeg', 0),
(12, 1, '2020-09-09 12:26:35', '2020-09-09 12:26:35', '', 'Cap', '', 'inherit', 'open', 'closed', '', 'cap-image', '', '', '2020-09-09 12:26:35', '2020-09-09 12:26:35', '', 0, 'http://localhost/wallart/wp-content/uploads/2020/09/cap.jpg', 0, 'attachment', 'image/jpeg', 0),
(13, 1, '2020-09-09 12:26:35', '2020-09-09 12:26:35', '', 'Hoodie with Logo', '', 'inherit', 'open', 'closed', '', 'hoodie-with-logo-image', '', '', '2020-09-09 12:26:35', '2020-09-09 12:26:35', '', 0, 'http://localhost/wallart/wp-content/uploads/2020/09/hoodie-with-logo.jpg', 0, 'attachment', 'image/jpeg', 0),
(14, 1, '2020-09-09 12:26:35', '2020-09-09 12:26:35', '', 'Hoodie with Pocket', '', 'inherit', 'open', 'closed', '', 'hoodie-with-pocket-image', '', '', '2020-09-09 12:26:35', '2020-09-09 12:26:35', '', 0, 'http://localhost/wallart/wp-content/uploads/2020/09/hoodie-with-pocket.jpg', 0, 'attachment', 'image/jpeg', 0),
(15, 1, '2020-09-09 12:26:35', '2020-09-09 12:26:35', '', 'Hoodie with Zipper', '', 'inherit', 'open', 'closed', '', 'hoodie-with-zipper-image', '', '', '2020-09-09 12:26:35', '2020-09-09 12:26:35', '', 0, 'http://localhost/wallart/wp-content/uploads/2020/09/hoodie-with-zipper.jpg', 0, 'attachment', 'image/jpeg', 0),
(16, 1, '2020-09-09 12:26:35', '2020-09-09 12:26:35', '', 'Hoodie', '', 'inherit', 'open', 'closed', '', 'hoodie-image', '', '', '2020-09-09 12:26:35', '2020-09-09 12:26:35', '', 0, 'http://localhost/wallart/wp-content/uploads/2020/09/hoodie.jpg', 0, 'attachment', 'image/jpeg', 0),
(17, 1, '2020-09-09 12:26:36', '2020-09-09 12:26:36', '', 'Long Sleeve Tee', '', 'inherit', 'open', 'closed', '', 'long-sleeve-tee-image', '', '', '2020-09-09 12:26:36', '2020-09-09 12:26:36', '', 0, 'http://localhost/wallart/wp-content/uploads/2020/09/long-sleeve-tee.jpg', 0, 'attachment', 'image/jpeg', 0),
(18, 1, '2020-09-09 12:26:36', '2020-09-09 12:26:36', '', 'Polo', '', 'inherit', 'open', 'closed', '', 'polo-image', '', '', '2020-09-09 12:26:36', '2020-09-09 12:26:36', '', 0, 'http://localhost/wallart/wp-content/uploads/2020/09/polo.jpg', 0, 'attachment', 'image/jpeg', 0),
(19, 1, '2020-09-09 12:26:36', '2020-09-09 12:26:36', '', 'Sunglasses', '', 'inherit', 'open', 'closed', '', 'sunglasses-image', '', '', '2020-09-09 12:26:36', '2020-09-09 12:26:36', '', 0, 'http://localhost/wallart/wp-content/uploads/2020/09/sunglasses.jpg', 0, 'attachment', 'image/jpeg', 0),
(20, 1, '2020-09-09 12:26:36', '2020-09-09 12:26:36', '', 'Tshirt', '', 'inherit', 'open', 'closed', '', 'tshirt-image', '', '', '2020-09-09 12:26:36', '2020-09-09 12:26:36', '', 0, 'http://localhost/wallart/wp-content/uploads/2020/09/tshirt.jpg', 0, 'attachment', 'image/jpeg', 0),
(21, 1, '2020-09-09 12:26:36', '2020-09-09 12:26:36', '', 'Vneck Tshirt', '', 'inherit', 'open', 'closed', '', 'vneck-tee-image', '', '', '2020-09-09 12:26:36', '2020-09-09 12:26:36', '', 0, 'http://localhost/wallart/wp-content/uploads/2020/09/vneck-tee.jpg', 0, 'attachment', 'image/jpeg', 0),
(22, 1, '2020-09-09 12:26:36', '2020-09-09 12:26:36', '', 'Hero', '', 'inherit', 'open', 'closed', '', 'hero-image', '', '', '2020-09-09 12:26:36', '2020-09-09 12:26:36', '', 0, 'http://localhost/wallart/wp-content/uploads/2020/09/hero.jpg', 0, 'attachment', 'image/jpeg', 0),
(23, 1, '2020-09-09 12:26:36', '2020-09-09 12:26:36', '', 'Accessories', '', 'inherit', 'open', 'closed', '', 'accessories-image', '', '', '2020-09-09 12:26:36', '2020-09-09 12:26:36', '', 0, 'http://localhost/wallart/wp-content/uploads/2020/09/accessories.jpg', 0, 'attachment', 'image/jpeg', 0),
(24, 1, '2020-09-09 12:26:37', '2020-09-09 12:26:37', '', 'T-shirts', '', 'inherit', 'open', 'closed', '', 'tshirts-image', '', '', '2020-09-09 12:26:37', '2020-09-09 12:26:37', '', 0, 'http://localhost/wallart/wp-content/uploads/2020/09/tshirts.jpg', 0, 'attachment', 'image/jpeg', 0),
(25, 1, '2020-09-09 12:26:37', '2020-09-09 12:26:37', '', 'Hoodies', '', 'inherit', 'open', 'closed', '', 'hoodies-image', '', '', '2020-09-09 12:26:37', '2020-09-09 12:26:37', '', 0, 'http://localhost/wallart/wp-content/uploads/2020/09/hoodies.jpg', 0, 'attachment', 'image/jpeg', 0),
(26, 1, '2020-09-09 12:26:37', '2020-09-09 12:26:37', '<!-- wp:shortcode -->\n[huge_it_slider id=\"1\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:heading {\"customTextColor\":\"#009cdd\",\"className\":\"has-vivid-cyan-blue-color has-text-align-center margin-null\"} -->\n<h2 class=\"has-text-color has-vivid-cyan-blue-color has-text-align-center margin-null\" style=\"color:#009cdd\"><strong>SHOP BY CATEGORY</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"align\":\"center\",\"className\":\"margin-null\"} -->\n<p class=\"has-text-align-center margin-null\"><strong>test purpose only test purpose only test purpose only</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:shortcode -->\n[woo_category_slider id=\'63\']\n<!-- /wp:shortcode -->\n\n<!-- wp:spacer {\"height\":20} -->\n<div style=\"height:20px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:shortcode -->\n[woo_product_slider id=\"75\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:spacer {\"height\":20} -->\n<div style=\"height:20px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:heading {\"customTextColor\":\"#009cdd\",\"className\":\"has-vivid-cyan-blue-color has-text-align-center margin-null\"} -->\n<h2 class=\"has-text-color has-vivid-cyan-blue-color has-text-align-center margin-null\" style=\"color:#009cdd\"><strong>BEST SALES</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:spacer {\"height\":47} -->\n<div style=\"height:47px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:woocommerce/product-best-sellers {\"columns\":4,\"rows\":1,\"alignButtons\":true} /-->\n\n<!-- wp:columns {\"customBackgroundColor\":\"#f3f3f3\",\"className\":\"quadpay-section-home\"} -->\n<div class=\"wp-block-columns has-background quadpay-section-home\" style=\"background-color:#f3f3f3\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:image {\"id\":102,\"sizeSlug\":\"large\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"http://localhost/wallart/wp-content/uploads/2020/09/quad-pay-banner.jpg\" alt=\"\" class=\"wp-image-102\"/></figure>\n<!-- /wp:image --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:spacer {\"height\":52} -->\n<div style=\"height:52px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:heading {\"align\":\"center\",\"level\":1,\"customTextColor\":\"#009cdd\"} -->\n<h1 class=\"has-text-color has-text-align-center\" style=\"color:#009cdd\"><strong>BUY NOW. PAY LATER WITH</strong></h1>\n<!-- /wp:heading -->\n\n<!-- wp:heading {\"align\":\"center\",\"level\":1} -->\n<h1 class=\"has-text-align-center\"><strong>QUAD PAY</strong></h1>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:columns -->\n<div class=\"wp-block-columns\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:heading {\"align\":\"center\"} -->\n<h2 class=\"has-text-align-center\"><strong>499</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"align\":\"center\",\"textColor\":\"vivid-cyan-blue\"} -->\n<p class=\"has-text-color has-text-align-center has-vivid-cyan-blue-color\"><strong>Already Sold</strong></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:heading {\"align\":\"center\"} -->\n<h2 class=\"has-text-align-center\"><strong>125</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"align\":\"center\",\"textColor\":\"vivid-cyan-blue\"} -->\n<p class=\"has-text-color has-text-align-center has-vivid-cyan-blue-color\"><strong>Pre Orders</strong></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:heading {\"align\":\"center\"} -->\n<h2 class=\"has-text-align-center\"><strong>100</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"align\":\"center\",\"textColor\":\"vivid-cyan-blue\"} -->\n<p class=\"has-text-color has-text-align-center has-vivid-cyan-blue-color\"><strong>Only for Order Over</strong></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->', 'Home', '', 'publish', 'closed', 'closed', '', 'home', '', '', '2020-09-16 13:24:14', '2020-09-16 13:24:14', '', 0, 'http://localhost/wallart/?page_id=26', 0, 'page', '', 0),
(27, 1, '2020-09-09 12:26:37', '2020-09-09 12:26:37', '', 'Blog', '', 'publish', 'closed', 'closed', '', 'blog', '', '', '2020-09-09 12:26:37', '2020-09-09 12:26:37', '', 0, 'http://localhost/wallart/?page_id=27', 0, 'page', '', 0),
(28, 1, '2020-09-09 12:26:37', '2020-09-09 12:26:37', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Beanie', '', 'trash', 'open', 'closed', '', 'beanie__trashed', '', '', '2020-09-15 15:22:11', '2020-09-15 15:22:11', '', 0, 'http://localhost/wallart/?p=28', 0, 'product', '', 0),
(29, 1, '2020-09-09 12:26:39', '2020-09-09 12:26:39', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Belt', '', 'trash', 'open', 'closed', '', 'belt__trashed', '', '', '2020-09-15 15:22:11', '2020-09-15 15:22:11', '', 0, 'http://localhost/wallart/?p=29', 0, 'product', '', 0),
(30, 1, '2020-09-09 12:26:41', '2020-09-09 12:26:41', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Cap', '', 'trash', 'open', 'closed', '', 'cap__trashed', '', '', '2020-09-15 15:22:10', '2020-09-15 15:22:10', '', 0, 'http://localhost/wallart/?p=30', 0, 'product', '', 0),
(31, 1, '2020-09-09 12:26:43', '2020-09-09 12:26:43', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Sunglasses', '', 'trash', 'open', 'closed', '', 'sunglasses__trashed', '', '', '2020-09-15 15:22:09', '2020-09-15 15:22:09', '', 0, 'http://localhost/wallart/?p=31', 0, 'product', '', 0),
(32, 1, '2020-09-09 12:26:44', '2020-09-09 12:26:44', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Hoodie with Logo', '', 'trash', 'open', 'closed', '', 'hoodie-with-logo__trashed', '', '', '2020-09-15 15:22:09', '2020-09-15 15:22:09', '', 0, 'http://localhost/wallart/?p=32', 0, 'product', '', 0),
(33, 1, '2020-09-09 12:26:46', '2020-09-09 12:26:46', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Hoodie with Pocket', '', 'trash', 'open', 'closed', '', 'hoodie-with-pocket__trashed', '', '', '2020-09-15 15:22:08', '2020-09-15 15:22:08', '', 0, 'http://localhost/wallart/?p=33', 0, 'product', '', 0),
(34, 1, '2020-09-09 12:26:48', '2020-09-09 12:26:48', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Hoodie with Zipper', '', 'trash', 'open', 'closed', '', 'hoodie-with-zipper__trashed', '', '', '2020-09-15 15:22:07', '2020-09-15 15:22:07', '', 0, 'http://localhost/wallart/?p=34', 0, 'product', '', 0),
(35, 1, '2020-09-09 12:26:50', '2020-09-09 12:26:50', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Hoodie', '', 'trash', 'open', 'closed', '', 'hoodie__trashed', '', '', '2020-09-15 15:22:07', '2020-09-15 15:22:07', '', 0, 'http://localhost/wallart/?p=35', 0, 'product', '', 0),
(36, 1, '2020-09-09 12:26:52', '2020-09-09 12:26:52', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Separate Yourself to elevate yourself', '', 'publish', 'open', 'closed', '', 'separate-yourself', '', '', '2020-09-15 15:23:41', '2020-09-15 15:23:41', '', 0, 'http://localhost/wallart/?p=36', 0, 'product', '', 0),
(37, 1, '2020-09-09 12:26:53', '2020-09-09 12:26:53', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Erotica - You\'re Mine', '', 'publish', 'open', 'closed', '', 'erotica', '', '', '2020-09-15 15:23:27', '2020-09-15 15:23:27', '', 0, 'http://localhost/wallart/?p=37', 0, 'product', '', 0),
(38, 1, '2020-09-09 12:26:55', '2020-09-09 12:26:55', '<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. <strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry.', 'Shine like a greek god', '', 'publish', 'open', 'closed', '', 'shine-greek-god', '', '', '2020-09-15 15:23:15', '2020-09-15 15:23:15', '', 0, 'http://localhost/wallart/?p=38', 0, 'product', '', 0),
(39, 1, '2020-09-09 12:26:57', '2020-09-09 12:26:57', '<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. <strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry.', 'Nipsey hussle', '', 'publish', 'open', 'closed', '', 'nipsey-hussle', '', '', '2020-09-15 15:22:52', '2020-09-15 15:22:52', '', 0, 'http://localhost/wallart/?p=39', 0, 'product', '', 0),
(40, 1, '2020-09-09 12:26:34', '2020-09-09 12:26:34', '{\n    \"nav_menus_created_posts\": {\n        \"value\": [\n            10,\n            11,\n            12,\n            13,\n            14,\n            15,\n            16,\n            17,\n            18,\n            19,\n            20,\n            21,\n            22,\n            23,\n            24,\n            25,\n            26,\n            27,\n            28,\n            29,\n            30,\n            31,\n            32,\n            33,\n            34,\n            35,\n            36,\n            37,\n            38,\n            39\n        ],\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-09-09 12:26:34\"\n    },\n    \"show_on_front\": {\n        \"starter_content\": true,\n        \"value\": \"page\",\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-09-09 12:18:30\"\n    },\n    \"page_on_front\": {\n        \"starter_content\": true,\n        \"value\": 26,\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-09-09 12:18:30\"\n    },\n    \"page_for_posts\": {\n        \"starter_content\": true,\n        \"value\": 27,\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-09-09 12:18:30\"\n    },\n    \"storefront::custom_logo\": {\n        \"value\": 41,\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-09-09 12:24:34\"\n    },\n    \"storefront::storefront_heading_color\": {\n        \"value\": \"#000000\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-09-09 12:24:34\"\n    },\n    \"storefront::storefront_text_color\": {\n        \"value\": \"#161616\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-09-09 12:25:34\"\n    },\n    \"storefront::storefront_button_background_color\": {\n        \"value\": \"#333333\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-09-09 12:26:31\"\n    },\n    \"storefront::storefront_button_text_color\": {\n        \"value\": \"#ffffff\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-09-09 12:26:31\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'c0155922-cd97-4248-9936-d521b14d5bf6', '', '', '2020-09-09 12:26:34', '2020-09-09 12:26:34', '', 0, 'http://localhost/wallart/?p=40', 0, 'customize_changeset', '', 0),
(41, 1, '2020-09-09 12:23:11', '2020-09-09 12:23:11', '', 'logo', '', 'inherit', 'open', 'closed', '', 'logo', '', '', '2020-09-09 12:23:11', '2020-09-09 12:23:11', '', 0, 'http://localhost/wallart/wp-content/uploads/2020/09/logo.jpg', 0, 'attachment', 'image/jpeg', 0),
(42, 1, '2020-09-09 12:26:37', '2020-09-09 12:26:37', 'This is your homepage which is what most visitors will see when they first visit your shop.\n\nYou can change this text by editing the &quot;Welcome&quot; page via the &quot;Pages&quot; menu in your dashboard.', 'Welcome', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2020-09-09 12:26:37', '2020-09-09 12:26:37', '', 26, 'http://localhost/wallart/index.php/2020/09/09/26-revision-v1/', 0, 'revision', '', 0),
(43, 1, '2020-09-09 12:26:37', '2020-09-09 12:26:37', '', 'Blog', '', 'inherit', 'closed', 'closed', '', '27-revision-v1', '', '', '2020-09-09 12:26:37', '2020-09-09 12:26:37', '', 27, 'http://localhost/wallart/index.php/2020/09/09/27-revision-v1/', 0, 'revision', '', 0),
(45, 1, '2020-09-10 06:45:15', '2020-09-10 06:45:15', '', 'banner1', '', 'inherit', 'open', 'closed', '', 'banner1', '', '', '2020-09-10 06:45:15', '2020-09-10 06:45:15', '', 0, 'http://localhost/wallart/wp-content/uploads/2020/09/banner1.jpg', 0, 'attachment', 'image/jpeg', 0),
(47, 1, '2020-09-11 03:30:31', '2020-09-11 03:30:31', 'http://localhost/wallart/wp-content/uploads/2020/09/cropped-logo.jpg', 'cropped-logo.jpg', '', 'inherit', 'open', 'closed', '', 'cropped-logo-jpg', '', '', '2020-09-11 03:30:31', '2020-09-11 03:30:31', '', 0, 'http://localhost/wallart/wp-content/uploads/2020/09/cropped-logo.jpg', 0, 'attachment', 'image/jpeg', 0),
(48, 1, '2020-09-11 03:31:03', '2020-09-11 03:31:03', '{\n    \"storefront::custom_logo\": {\n        \"value\": 47,\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-09-11 03:31:03\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '73669216-b3fc-402b-9cfd-d1a6d2fcd542', '', '', '2020-09-11 03:31:03', '2020-09-11 03:31:03', '', 0, 'http://localhost/wallart/73669216-b3fc-402b-9cfd-d1a6d2fcd542/', 0, 'customize_changeset', '', 0),
(49, 1, '2020-09-11 06:10:30', '2020-09-11 06:10:30', ' ', '', '', 'publish', 'closed', 'closed', '', '49', '', '', '2020-09-11 06:40:13', '2020-09-11 06:40:13', '', 0, 'http://localhost/wallart/?p=49', 1, 'nav_menu_item', '', 0),
(50, 1, '2020-09-11 06:10:30', '2020-09-11 06:10:30', ' ', '', '', 'publish', 'closed', 'closed', '', '50', '', '', '2020-09-11 06:40:13', '2020-09-11 06:40:13', '', 0, 'http://localhost/wallart/?p=50', 2, 'nav_menu_item', '', 0),
(51, 1, '2020-09-11 06:12:00', '2020-09-11 06:12:00', '', 'Refer & Earn', '', 'publish', 'closed', 'closed', '', 'refer-earn', '', '', '2020-09-11 06:12:00', '2020-09-11 06:12:00', '', 0, 'http://localhost/wallart/?page_id=51', 0, 'page', '', 0),
(52, 1, '2020-09-11 06:12:00', '2020-09-11 06:12:00', '', 'Refer & Earn', '', 'inherit', 'closed', 'closed', '', '51-revision-v1', '', '', '2020-09-11 06:12:00', '2020-09-11 06:12:00', '', 51, 'http://localhost/wallart/51-revision-v1/', 0, 'revision', '', 0),
(53, 1, '2020-09-11 06:40:13', '2020-09-11 06:40:13', ' ', '', '', 'publish', 'closed', 'closed', '', '53', '', '', '2020-09-11 06:40:13', '2020-09-11 06:40:13', '', 0, 'http://localhost/wallart/?p=53', 3, 'nav_menu_item', '', 0),
(54, 1, '2020-09-14 12:17:58', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2020-09-14 12:17:58', '0000-00-00 00:00:00', '', 0, 'http://localhost/wallart/?p=54', 0, 'post', '', 0),
(55, 1, '2020-09-15 05:54:26', '2020-09-15 05:54:26', 'This is your homepage which is what most visitors will see when they first visit your shop.\n\nYou can change this text by editing the \"Welcome\" page via the \"Pages\" menu in your dashboard.', 'Welcome', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2020-09-15 05:54:26', '2020-09-15 05:54:26', '', 26, 'http://localhost/wallart/26-revision-v1/', 0, 'revision', '', 0),
(57, 1, '2020-09-15 10:16:09', '2020-09-15 10:16:09', '', 'Huge-IT First Slide.', '', 'inherit', 'open', 'closed', '', 'huge-it-first-slide', '', '', '2020-09-15 10:16:09', '2020-09-15 10:16:09', '', 0, 'http://localhost/wallart/huge-it-first-slide/', 0, 'attachment', 'jpg', 0),
(58, 1, '2020-09-15 10:16:09', '2020-09-15 10:16:09', '', 'Huge-IT Second Slide.', '', 'inherit', 'open', 'closed', '', 'huge-it-second-slide', '', '', '2020-09-15 10:16:09', '2020-09-15 10:16:09', '', 0, 'http://localhost/wallart/huge-it-second-slide/', 0, 'attachment', 'jpg', 0),
(59, 1, '2020-09-15 10:16:09', '2020-09-15 10:16:09', '', 'Huge-IT Third Slide.', '', 'inherit', 'open', 'closed', '', 'huge-it-third-slide', '', '', '2020-09-15 10:16:09', '2020-09-15 10:16:09', '', 0, 'http://localhost/wallart/huge-it-third-slide/', 0, 'attachment', 'jpg', 0),
(60, 1, '2020-09-15 10:31:21', '2020-09-15 10:31:21', '', 'banner1', '', 'inherit', 'open', 'closed', '', 'banner1-2', '', '', '2020-09-15 10:31:21', '2020-09-15 10:31:21', '', 0, 'http://localhost/wallart/wp-content/uploads/2020/09/banner1-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(61, 1, '2020-09-15 10:46:25', '2020-09-15 10:46:25', '<!-- wp:shortcode -->\n[huge_it_slider id=\"1\"]\n<!-- /wp:shortcode -->', 'Welcome', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2020-09-15 10:46:25', '2020-09-15 10:46:25', '', 26, 'http://localhost/wallart/26-revision-v1/', 0, 'revision', '', 0),
(63, 1, '2020-09-15 12:51:33', '2020-09-15 12:51:33', '', 'Category Slider', '', 'publish', 'closed', 'closed', '', 'category-slider', '', '', '2020-09-15 13:02:47', '2020-09-15 13:02:47', '', 0, 'http://localhost/wallart/?post_type=wc_category_slider&#038;p=63', 0, 'wc_category_slider', '', 0),
(64, 1, '2020-09-15 12:53:02', '2020-09-15 12:53:02', '', 'category_1', '', 'inherit', 'open', 'closed', '', 'category_1', '', '', '2020-09-15 12:53:02', '2020-09-15 12:53:02', '', 0, 'http://localhost/wallart/wp-content/uploads/2020/09/category_1.jpg', 0, 'attachment', 'image/jpeg', 0),
(65, 1, '2020-09-15 12:53:53', '2020-09-15 12:53:53', '', 'category_2', '', 'inherit', 'open', 'closed', '', 'category_2', '', '', '2020-09-15 12:53:53', '2020-09-15 12:53:53', '', 0, 'http://localhost/wallart/wp-content/uploads/2020/09/category_2.jpg', 0, 'attachment', 'image/jpeg', 0),
(66, 1, '2020-09-15 12:54:39', '2020-09-15 12:54:39', '', 'category_3', '', 'inherit', 'open', 'closed', '', 'category_3', '', '', '2020-09-15 12:54:39', '2020-09-15 12:54:39', '', 0, 'http://localhost/wallart/wp-content/uploads/2020/09/category_3.jpg', 0, 'attachment', 'image/jpeg', 0),
(67, 1, '2020-09-15 12:58:01', '2020-09-15 12:58:01', '<!-- wp:shortcode -->\n[huge_it_slider id=\"1\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:shortcode -->\n[woo_category_slider id=\'63\']\n<!-- /wp:shortcode -->', 'Welcome', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2020-09-15 12:58:01', '2020-09-15 12:58:01', '', 26, 'http://localhost/wallart/26-revision-v1/', 0, 'revision', '', 0),
(68, 1, '2020-09-15 13:57:34', '2020-09-15 13:57:34', '<!-- wp:shortcode -->\n[huge_it_slider id=\"1\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:heading {\"align\":\"center\",\"level\":3,\"textColor\":\"vivid-cyan-blue\"} -->\n<h3 class=\"has-vivid-cyan-blue-color has-text-color has-text-align-center\"><strong>SHOP BY CATEGORY</strong></h3>\n<!-- /wp:heading -->\n\n<!-- wp:shortcode -->\n[woo_category_slider id=\'63\']\n<!-- /wp:shortcode -->', 'Welcome', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2020-09-15 13:57:34', '2020-09-15 13:57:34', '', 26, 'http://localhost/wallart/26-revision-v1/', 0, 'revision', '', 0),
(69, 1, '2020-09-15 13:58:11', '2020-09-15 13:58:11', '<!-- wp:shortcode -->\n[huge_it_slider id=\"1\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:heading {\"align\":\"center\",\"textColor\":\"vivid-cyan-blue\"} -->\n<h2 class=\"has-vivid-cyan-blue-color has-text-color has-text-align-center\"><strong>SHOP BY CATEGORY</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:shortcode -->\n[woo_category_slider id=\'63\']\n<!-- /wp:shortcode -->', 'Welcome', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2020-09-15 13:58:11', '2020-09-15 13:58:11', '', 26, 'http://localhost/wallart/26-revision-v1/', 0, 'revision', '', 0),
(70, 1, '2020-09-15 13:59:45', '2020-09-15 13:59:45', '<!-- wp:shortcode -->\n[huge_it_slider id=\"1\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:heading {\"align\":\"center\",\"textColor\":\"vivid-cyan-blue\"} -->\n<h2 class=\"has-vivid-cyan-blue-color has-text-color has-text-align-center\"><strong>SHOP BY CATEGORY</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"align\":\"center\"} -->\n<p class=\"has-text-align-center\"><strong>test purpose only test purpose only test purpose only</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:shortcode -->\n[woo_category_slider id=\'63\']\n<!-- /wp:shortcode -->', 'Welcome', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2020-09-15 13:59:45', '2020-09-15 13:59:45', '', 26, 'http://localhost/wallart/26-revision-v1/', 0, 'revision', '', 0),
(71, 1, '2020-09-15 14:02:22', '2020-09-15 14:02:22', '<!-- wp:shortcode -->\n[huge_it_slider id=\"1\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:heading {\"className\":\"has-vivid-cyan-blue-color has-text-color has-text-align-center\"} -->\n<h2 class=\"has-vivid-cyan-blue-color has-text-color has-text-align-center\"><strong>SHOP BY CATEGORY</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"align\":\"center\"} -->\n<p class=\"has-text-align-center\"><strong>test purpose only test purpose only test purpose only</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:shortcode -->\n[woo_category_slider id=\'63\']\n<!-- /wp:shortcode -->', 'Welcome', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2020-09-15 14:02:22', '2020-09-15 14:02:22', '', 26, 'http://localhost/wallart/26-revision-v1/', 0, 'revision', '', 0),
(72, 1, '2020-09-15 14:04:51', '2020-09-15 14:04:51', '<!-- wp:shortcode -->\n[huge_it_slider id=\"1\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:heading {\"className\":\"has-vivid-cyan-blue-color has-text-color has-text-align-center margin-null\"} -->\n<h2 class=\"has-vivid-cyan-blue-color has-text-color has-text-align-center margin-null\"><strong>SHOP BY CATEGORY</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"align\":\"center\"} -->\n<p class=\"has-text-align-center\"><strong>test purpose only test purpose only test purpose only</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:shortcode -->\n[woo_category_slider id=\'63\']\n<!-- /wp:shortcode -->', 'Welcome', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2020-09-15 14:04:51', '2020-09-15 14:04:51', '', 26, 'http://localhost/wallart/26-revision-v1/', 0, 'revision', '', 0),
(73, 1, '2020-09-15 14:15:35', '2020-09-15 14:15:35', '<!-- wp:shortcode -->\n[huge_it_slider id=\"1\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:heading {\"className\":\"has-vivid-cyan-blue-color has-text-color has-text-align-center margin-null\"} -->\n<h2 class=\"has-vivid-cyan-blue-color has-text-color has-text-align-center margin-null\"><strong>SHOP BY CATEGORY</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"align\":\"center\",\"className\":\"margin-null\"} -->\n<p class=\"has-text-align-center margin-null\"><strong>test purpose only test purpose only test purpose only</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:shortcode -->\n[woo_category_slider id=\'63\']\n<!-- /wp:shortcode -->', 'Welcome', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2020-09-15 14:15:35', '2020-09-15 14:15:35', '', 26, 'http://localhost/wallart/26-revision-v1/', 0, 'revision', '', 0),
(74, 1, '2020-09-15 14:19:28', '2020-09-15 14:19:28', '<!-- wp:shortcode -->\n[huge_it_slider id=\"1\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:heading {\"customTextColor\":\"#009cdd\",\"className\":\"has-vivid-cyan-blue-color has-text-color has-text-align-center margin-null\"} -->\n<h2 class=\"has-text-color has-vivid-cyan-blue-color has-text-color has-text-align-center margin-null\" style=\"color:#009cdd\"><strong>SHOP BY CATEGORY</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"align\":\"center\",\"className\":\"margin-null\"} -->\n<p class=\"has-text-align-center margin-null\"><strong>test purpose only test purpose only test purpose only</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:shortcode -->\n[woo_category_slider id=\'63\']\n<!-- /wp:shortcode -->', 'Welcome', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2020-09-15 14:19:28', '2020-09-15 14:19:28', '', 26, 'http://localhost/wallart/26-revision-v1/', 0, 'revision', '', 0),
(75, 1, '2020-09-15 14:33:07', '2020-09-15 14:33:07', '', 'New Arrivals', '', 'publish', 'closed', 'closed', '', 'new-arrivals', '', '', '2020-09-15 15:59:21', '2020-09-15 15:59:21', '', 0, 'http://localhost/wallart/?post_type=sp_wps_shortcodes&#038;p=75', 0, 'sp_wps_shortcodes', '', 0),
(76, 1, '2020-09-15 14:34:09', '2020-09-15 14:34:09', '<!-- wp:shortcode -->\n[huge_it_slider id=\"1\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:heading {\"customTextColor\":\"#009cdd\",\"className\":\"has-vivid-cyan-blue-color has-text-align-center margin-null\"} -->\n<h2 class=\"has-text-color has-vivid-cyan-blue-color has-text-align-center margin-null\" style=\"color:#009cdd\"><strong>SHOP BY CATEGORY</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"align\":\"center\",\"className\":\"margin-null\"} -->\n<p class=\"has-text-align-center margin-null\"><strong>test purpose only test purpose only test purpose only</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:shortcode -->\n[woo_category_slider id=\'63\']\n<!-- /wp:shortcode -->\n\n<!-- wp:shortcode -->\n[woo_product_slider id=\"75\"]\n<!-- /wp:shortcode -->', 'Welcome', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2020-09-15 14:34:09', '2020-09-15 14:34:09', '', 26, 'http://localhost/wallart/26-revision-v1/', 0, 'revision', '', 0),
(77, 1, '2020-09-15 15:10:28', '2020-09-15 15:10:28', '<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. <strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry.', 'Nipsey hussle', '', 'inherit', 'closed', 'closed', '', '39-autosave-v1', '', '', '2020-09-15 15:10:28', '2020-09-15 15:10:28', '', 39, 'http://localhost/wallart/39-autosave-v1/', 0, 'revision', '', 0),
(78, 1, '2020-09-15 15:17:36', '2020-09-15 15:17:36', '', 'img_1', '', 'inherit', 'open', 'closed', '', 'img_1', '', '', '2020-09-15 15:17:36', '2020-09-15 15:17:36', '', 39, 'http://localhost/wallart/wp-content/uploads/2020/09/img_1.jpg', 0, 'attachment', 'image/jpeg', 0),
(79, 1, '2020-09-15 15:18:51', '2020-09-15 15:18:51', '', 'img_2', '', 'inherit', 'open', 'closed', '', 'img_2', '', '', '2020-09-15 15:18:51', '2020-09-15 15:18:51', '', 38, 'http://localhost/wallart/wp-content/uploads/2020/09/img_2.jpg', 0, 'attachment', 'image/jpeg', 0),
(80, 1, '2020-09-15 15:20:31', '2020-09-15 15:20:31', '', 'img_3', '', 'inherit', 'open', 'closed', '', 'img_3', '', '', '2020-09-15 15:20:31', '2020-09-15 15:20:31', '', 37, 'http://localhost/wallart/wp-content/uploads/2020/09/img_3.jpg', 0, 'attachment', 'image/jpeg', 0),
(81, 1, '2020-09-15 15:21:41', '2020-09-15 15:21:41', '', 'img_4', '', 'inherit', 'open', 'closed', '', 'img_4', '', '', '2020-09-15 15:21:41', '2020-09-15 15:21:41', '', 36, 'http://localhost/wallart/wp-content/uploads/2020/09/img_4.jpg', 0, 'attachment', 'image/jpeg', 0),
(82, 1, '2020-09-15 15:28:42', '2020-09-15 15:28:42', '<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. <strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry.', 'Mind your business', '', 'publish', 'open', 'closed', '', 'mind-your-business', '', '', '2020-09-15 15:29:58', '2020-09-15 15:29:58', '', 0, 'http://localhost/wallart/?post_type=product&#038;p=82', 0, 'product', '', 0),
(83, 1, '2020-09-15 15:29:48', '2020-09-15 15:29:48', '', 'img_8', '', 'inherit', 'open', 'closed', '', 'img_8', '', '', '2020-09-15 15:29:48', '2020-09-15 15:29:48', '', 82, 'http://localhost/wallart/wp-content/uploads/2020/09/img_8.jpg', 0, 'attachment', 'image/jpeg', 0),
(84, 1, '2020-09-15 15:30:25', '2020-09-15 15:30:25', '<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. <strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry.', 'Elevate Yourself', '', 'publish', 'open', 'closed', '', 'elevate-yourself', '', '', '2020-09-15 16:16:11', '2020-09-15 16:16:11', '', 0, 'http://localhost/wallart/?post_type=product&#038;p=84', 0, 'product', '', 1),
(85, 1, '2020-09-15 15:31:07', '2020-09-15 15:31:07', '', 'img_7', '', 'inherit', 'open', 'closed', '', 'img_7', '', '', '2020-09-15 15:31:07', '2020-09-15 15:31:07', '', 84, 'http://localhost/wallart/wp-content/uploads/2020/09/img_7.jpg', 0, 'attachment', 'image/jpeg', 0),
(86, 1, '2020-09-15 15:34:56', '2020-09-15 15:34:56', '<!-- wp:shortcode -->\n[huge_it_slider id=\"1\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:heading {\"customTextColor\":\"#009cdd\",\"className\":\"has-vivid-cyan-blue-color has-text-align-center margin-null\"} -->\n<h2 class=\"has-text-color has-vivid-cyan-blue-color has-text-align-center margin-null\" style=\"color:#009cdd\"><strong>SHOP BY CATEGORY</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"align\":\"center\",\"className\":\"margin-null\"} -->\n<p class=\"has-text-align-center margin-null\"><strong>test purpose only test purpose only test purpose only</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:shortcode -->\n[woo_category_slider id=\'63\']\n<!-- /wp:shortcode -->\n\n<!-- wp:spacer {\"height\":20} -->\n<div style=\"height:20px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:shortcode -->\n[woo_product_slider id=\"75\"]\n<!-- /wp:shortcode -->', 'Welcome', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2020-09-15 15:34:56', '2020-09-15 15:34:56', '', 26, 'http://localhost/wallart/26-revision-v1/', 0, 'revision', '', 0),
(88, 1, '2020-09-15 16:16:23', '2020-09-15 16:16:23', '<!-- wp:shortcode -->\n[huge_it_slider id=\"1\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:heading {\"customTextColor\":\"#009cdd\",\"className\":\"has-vivid-cyan-blue-color has-text-align-center margin-null\"} -->\n<h2 class=\"has-text-color has-vivid-cyan-blue-color has-text-align-center margin-null\" style=\"color:#009cdd\"><strong>SHOP BY CATEGORY</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"align\":\"center\",\"className\":\"margin-null\"} -->\n<p class=\"has-text-align-center margin-null\"><strong>test purpose only test purpose only test purpose only</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:shortcode -->\n[woo_category_slider id=\'63\']\n<!-- /wp:shortcode -->\n\n<!-- wp:spacer {\"height\":20} -->\n<div style=\"height:20px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:shortcode -->\n[woo_product_slider id=\"75\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:woocommerce/all-reviews -->\n<div class=\"wp-block-woocommerce-all-reviews wc-block-all-reviews has-image has-name has-date has-rating has-content has-product-name\" data-image-type=\"reviewer\" data-orderby=\"most-recent\" data-reviews-on-page-load=\"10\" data-reviews-on-load-more=\"10\" data-show-load-more=\"true\" data-show-orderby=\"true\"></div>\n<!-- /wp:woocommerce/all-reviews -->', 'Welcome', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2020-09-15 16:16:23', '2020-09-15 16:16:23', '', 26, 'http://localhost/wallart/26-revision-v1/', 0, 'revision', '', 0),
(90, 1, '2020-09-15 16:19:42', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2020-09-15 16:19:42', '0000-00-00 00:00:00', '', 0, 'http://localhost/wallart/?post_type=sp_wps_shortcodes&p=90', 0, 'sp_wps_shortcodes', '', 0),
(91, 1, '2020-09-15 16:42:31', '2020-09-15 16:42:31', '<!-- wp:shortcode -->\n[huge_it_slider id=\"1\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:heading {\"customTextColor\":\"#009cdd\",\"className\":\"has-vivid-cyan-blue-color has-text-align-center margin-null\"} -->\n<h2 class=\"has-text-color has-vivid-cyan-blue-color has-text-align-center margin-null\" style=\"color:#009cdd\"><strong>SHOP BY CATEGORY</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"align\":\"center\",\"className\":\"margin-null\"} -->\n<p class=\"has-text-align-center margin-null\"><strong>test purpose only test purpose only test purpose only</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:shortcode -->\n[woo_category_slider id=\'63\']\n<!-- /wp:shortcode -->\n\n<!-- wp:spacer {\"height\":20} -->\n<div style=\"height:20px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:shortcode -->\n[woo_product_slider id=\"75\"]\n<!-- /wp:shortcode -->', 'Welcome', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2020-09-15 16:42:31', '2020-09-15 16:42:31', '', 26, 'http://localhost/wallart/26-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(92, 1, '2020-09-15 16:56:09', '2020-09-15 16:56:09', '<!-- wp:shortcode -->\n[huge_it_slider id=\"1\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:heading {\"customTextColor\":\"#009cdd\",\"className\":\"has-vivid-cyan-blue-color has-text-align-center margin-null\"} -->\n<h2 class=\"has-text-color has-vivid-cyan-blue-color has-text-align-center margin-null\" style=\"color:#009cdd\"><strong>SHOP BY CATEGORY</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"align\":\"center\",\"className\":\"margin-null\"} -->\n<p class=\"has-text-align-center margin-null\"><strong>test purpose only test purpose only test purpose only</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:shortcode -->\n[woo_category_slider id=\'63\']\n<!-- /wp:shortcode -->\n\n<!-- wp:spacer {\"height\":20} -->\n<div style=\"height:20px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:shortcode -->\n[woo_product_slider id=\"75\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:shortcode -->\n[products limit=\"3\" columns=\"3\" best_selling=\"true\" ]\n<!-- /wp:shortcode -->', 'Welcome', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2020-09-15 16:56:09', '2020-09-15 16:56:09', '', 26, 'http://localhost/wallart/26-revision-v1/', 0, 'revision', '', 0),
(93, 1, '2020-09-15 16:57:03', '2020-09-15 16:57:03', '<!-- wp:shortcode -->\n[huge_it_slider id=\"1\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:heading {\"customTextColor\":\"#009cdd\",\"className\":\"has-vivid-cyan-blue-color has-text-align-center margin-null\"} -->\n<h2 class=\"has-text-color has-vivid-cyan-blue-color has-text-align-center margin-null\" style=\"color:#009cdd\"><strong>SHOP BY CATEGORY</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"align\":\"center\",\"className\":\"margin-null\"} -->\n<p class=\"has-text-align-center margin-null\"><strong>test purpose only test purpose only test purpose only</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:shortcode -->\n[woo_category_slider id=\'63\']\n<!-- /wp:shortcode -->\n\n<!-- wp:spacer {\"height\":20} -->\n<div style=\"height:20px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:shortcode -->\n[woo_product_slider id=\"75\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:shortcode -->\n[products limit=\"4\" columns=\"2\" best_selling=\"true\" ]\n<!-- /wp:shortcode -->', 'Welcome', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2020-09-15 16:57:03', '2020-09-15 16:57:03', '', 26, 'http://localhost/wallart/26-revision-v1/', 0, 'revision', '', 0),
(94, 1, '2020-09-15 16:57:26', '2020-09-15 16:57:26', '<!-- wp:shortcode -->\n[huge_it_slider id=\"1\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:heading {\"customTextColor\":\"#009cdd\",\"className\":\"has-vivid-cyan-blue-color has-text-align-center margin-null\"} -->\n<h2 class=\"has-text-color has-vivid-cyan-blue-color has-text-align-center margin-null\" style=\"color:#009cdd\"><strong>SHOP BY CATEGORY</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"align\":\"center\",\"className\":\"margin-null\"} -->\n<p class=\"has-text-align-center margin-null\"><strong>test purpose only test purpose only test purpose only</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:shortcode -->\n[woo_category_slider id=\'63\']\n<!-- /wp:shortcode -->\n\n<!-- wp:spacer {\"height\":20} -->\n<div style=\"height:20px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:shortcode -->\n[woo_product_slider id=\"75\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:shortcode -->\n[products limit=\"4\" columns=\"1\" best_selling=\"true\" ]\n<!-- /wp:shortcode -->', 'Welcome', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2020-09-15 16:57:26', '2020-09-15 16:57:26', '', 26, 'http://localhost/wallart/26-revision-v1/', 0, 'revision', '', 0),
(95, 1, '2020-09-15 16:57:42', '2020-09-15 16:57:42', '<!-- wp:shortcode -->\n[huge_it_slider id=\"1\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:heading {\"customTextColor\":\"#009cdd\",\"className\":\"has-vivid-cyan-blue-color has-text-align-center margin-null\"} -->\n<h2 class=\"has-text-color has-vivid-cyan-blue-color has-text-align-center margin-null\" style=\"color:#009cdd\"><strong>SHOP BY CATEGORY</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"align\":\"center\",\"className\":\"margin-null\"} -->\n<p class=\"has-text-align-center margin-null\"><strong>test purpose only test purpose only test purpose only</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:shortcode -->\n[woo_category_slider id=\'63\']\n<!-- /wp:shortcode -->\n\n<!-- wp:spacer {\"height\":20} -->\n<div style=\"height:20px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:shortcode -->\n[woo_product_slider id=\"75\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:shortcode -->\n[products limit=\"4\" columns=\"4\" best_selling=\"true\" ]\n<!-- /wp:shortcode -->', 'Welcome', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2020-09-15 16:57:42', '2020-09-15 16:57:42', '', 26, 'http://localhost/wallart/26-revision-v1/', 0, 'revision', '', 0),
(96, 1, '2020-09-15 17:00:19', '2020-09-15 17:00:19', '<!-- wp:shortcode -->\n[huge_it_slider id=\"1\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:heading {\"customTextColor\":\"#009cdd\",\"className\":\"has-vivid-cyan-blue-color has-text-align-center margin-null\"} -->\n<h2 class=\"has-text-color has-vivid-cyan-blue-color has-text-align-center margin-null\" style=\"color:#009cdd\"><strong>SHOP BY CATEGORY</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"align\":\"center\",\"className\":\"margin-null\"} -->\n<p class=\"has-text-align-center margin-null\"><strong>test purpose only test purpose only test purpose only</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:shortcode -->\n[woo_category_slider id=\'63\']\n<!-- /wp:shortcode -->\n\n<!-- wp:spacer {\"height\":20} -->\n<div style=\"height:20px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:shortcode -->\n[woo_product_slider id=\"75\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:woocommerce/product-best-sellers {\"rows\":1,\"alignButtons\":true} /-->', 'Welcome', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2020-09-15 17:00:19', '2020-09-15 17:00:19', '', 26, 'http://localhost/wallart/26-revision-v1/', 0, 'revision', '', 0),
(97, 1, '2020-09-15 17:00:53', '2020-09-15 17:00:53', '<!-- wp:shortcode -->\n[huge_it_slider id=\"1\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:heading {\"customTextColor\":\"#009cdd\",\"className\":\"has-vivid-cyan-blue-color has-text-align-center margin-null\"} -->\n<h2 class=\"has-text-color has-vivid-cyan-blue-color has-text-align-center margin-null\" style=\"color:#009cdd\"><strong>SHOP BY CATEGORY</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"align\":\"center\",\"className\":\"margin-null\"} -->\n<p class=\"has-text-align-center margin-null\"><strong>test purpose only test purpose only test purpose only</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:shortcode -->\n[woo_category_slider id=\'63\']\n<!-- /wp:shortcode -->\n\n<!-- wp:spacer {\"height\":20} -->\n<div style=\"height:20px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:shortcode -->\n[woo_product_slider id=\"75\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:woocommerce/product-best-sellers {\"columns\":4,\"rows\":1,\"alignButtons\":true} /-->', 'Welcome', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2020-09-15 17:00:53', '2020-09-15 17:00:53', '', 26, 'http://localhost/wallart/26-revision-v1/', 0, 'revision', '', 0),
(98, 1, '2020-09-15 17:02:43', '2020-09-15 17:02:43', '<!-- wp:shortcode -->\n[huge_it_slider id=\"1\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:heading {\"customTextColor\":\"#009cdd\",\"className\":\"has-vivid-cyan-blue-color has-text-align-center margin-null\"} -->\n<h2 class=\"has-text-color has-vivid-cyan-blue-color has-text-align-center margin-null\" style=\"color:#009cdd\"><strong>SHOP BY CATEGORY</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"align\":\"center\",\"className\":\"margin-null\"} -->\n<p class=\"has-text-align-center margin-null\"><strong>test purpose only test purpose only test purpose only</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:shortcode -->\n[woo_category_slider id=\'63\']\n<!-- /wp:shortcode -->\n\n<!-- wp:spacer {\"height\":20} -->\n<div style=\"height:20px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:shortcode -->\n[woo_product_slider id=\"75\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:heading {\"customTextColor\":\"#009cdd\",\"className\":\"has-vivid-cyan-blue-color has-text-align-center margin-null\"} -->\n<h2 class=\"has-text-color has-vivid-cyan-blue-color has-text-align-center margin-null\" style=\"color:#009cdd\"><strong>BEST SALES</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:woocommerce/product-best-sellers {\"columns\":4,\"rows\":1,\"alignButtons\":true} /-->', 'Welcome', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2020-09-15 17:02:43', '2020-09-15 17:02:43', '', 26, 'http://localhost/wallart/26-revision-v1/', 0, 'revision', '', 0),
(100, 1, '2020-09-15 17:05:14', '2020-09-15 17:05:14', '<!-- wp:shortcode -->\n[huge_it_slider id=\"1\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:heading {\"customTextColor\":\"#009cdd\",\"className\":\"has-vivid-cyan-blue-color has-text-align-center margin-null\"} -->\n<h2 class=\"has-text-color has-vivid-cyan-blue-color has-text-align-center margin-null\" style=\"color:#009cdd\"><strong>SHOP BY CATEGORY</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"align\":\"center\",\"className\":\"margin-null\"} -->\n<p class=\"has-text-align-center margin-null\"><strong>test purpose only test purpose only test purpose only</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:shortcode -->\n[woo_category_slider id=\'63\']\n<!-- /wp:shortcode -->\n\n<!-- wp:spacer {\"height\":20} -->\n<div style=\"height:20px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:shortcode -->\n[woo_product_slider id=\"75\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:spacer {\"height\":20} -->\n<div style=\"height:20px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:heading {\"customTextColor\":\"#009cdd\",\"className\":\"has-vivid-cyan-blue-color has-text-align-center margin-null\"} -->\n<h2 class=\"has-text-color has-vivid-cyan-blue-color has-text-align-center margin-null\" style=\"color:#009cdd\"><strong>BEST SALES</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:spacer {\"height\":20} -->\n<div style=\"height:20px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:woocommerce/product-best-sellers {\"columns\":4,\"rows\":1,\"alignButtons\":true} /-->', 'Welcome', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2020-09-15 17:05:14', '2020-09-15 17:05:14', '', 26, 'http://localhost/wallart/26-revision-v1/', 0, 'revision', '', 0),
(101, 1, '2020-09-15 17:05:59', '2020-09-15 17:05:59', '<!-- wp:shortcode -->\n[huge_it_slider id=\"1\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:heading {\"customTextColor\":\"#009cdd\",\"className\":\"has-vivid-cyan-blue-color has-text-align-center margin-null\"} -->\n<h2 class=\"has-text-color has-vivid-cyan-blue-color has-text-align-center margin-null\" style=\"color:#009cdd\"><strong>SHOP BY CATEGORY</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"align\":\"center\",\"className\":\"margin-null\"} -->\n<p class=\"has-text-align-center margin-null\"><strong>test purpose only test purpose only test purpose only</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:shortcode -->\n[woo_category_slider id=\'63\']\n<!-- /wp:shortcode -->\n\n<!-- wp:spacer {\"height\":20} -->\n<div style=\"height:20px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:shortcode -->\n[woo_product_slider id=\"75\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:spacer {\"height\":20} -->\n<div style=\"height:20px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:heading {\"customTextColor\":\"#009cdd\",\"className\":\"has-vivid-cyan-blue-color has-text-align-center margin-null\"} -->\n<h2 class=\"has-text-color has-vivid-cyan-blue-color has-text-align-center margin-null\" style=\"color:#009cdd\"><strong>BEST SALES</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:spacer {\"height\":47} -->\n<div style=\"height:47px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:woocommerce/product-best-sellers {\"columns\":4,\"rows\":1,\"alignButtons\":true} /-->', 'Welcome', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2020-09-15 17:05:59', '2020-09-15 17:05:59', '', 26, 'http://localhost/wallart/26-revision-v1/', 0, 'revision', '', 0),
(102, 1, '2020-09-15 17:08:02', '2020-09-15 17:08:02', '', 'quad-pay-banner', '', 'inherit', 'open', 'closed', '', 'quad-pay-banner', '', '', '2020-09-15 17:08:02', '2020-09-15 17:08:02', '', 26, 'http://localhost/wallart/wp-content/uploads/2020/09/quad-pay-banner.jpg', 0, 'attachment', 'image/jpeg', 0),
(103, 1, '2020-09-15 17:15:15', '2020-09-15 17:15:15', '<!-- wp:shortcode -->\n[huge_it_slider id=\"1\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:heading {\"customTextColor\":\"#009cdd\",\"className\":\"has-vivid-cyan-blue-color has-text-align-center margin-null\"} -->\n<h2 class=\"has-text-color has-vivid-cyan-blue-color has-text-align-center margin-null\" style=\"color:#009cdd\"><strong>SHOP BY CATEGORY</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"align\":\"center\",\"className\":\"margin-null\"} -->\n<p class=\"has-text-align-center margin-null\"><strong>test purpose only test purpose only test purpose only</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:shortcode -->\n[woo_category_slider id=\'63\']\n<!-- /wp:shortcode -->\n\n<!-- wp:spacer {\"height\":20} -->\n<div style=\"height:20px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:shortcode -->\n[woo_product_slider id=\"75\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:spacer {\"height\":20} -->\n<div style=\"height:20px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:heading {\"customTextColor\":\"#009cdd\",\"className\":\"has-vivid-cyan-blue-color has-text-align-center margin-null\"} -->\n<h2 class=\"has-text-color has-vivid-cyan-blue-color has-text-align-center margin-null\" style=\"color:#009cdd\"><strong>BEST SALES</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:spacer {\"height\":47} -->\n<div style=\"height:47px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:woocommerce/product-best-sellers {\"columns\":4,\"rows\":1,\"alignButtons\":true} /-->\n\n<!-- wp:columns -->\n<div class=\"wp-block-columns\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:image {\"id\":102,\"sizeSlug\":\"large\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"http://localhost/wallart/wp-content/uploads/2020/09/quad-pay-banner.jpg\" alt=\"\" class=\"wp-image-102\"/></figure>\n<!-- /wp:image --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:heading {\"align\":\"center\",\"customTextColor\":\"#009cdd\"} -->\n<h2 class=\"has-text-color has-text-align-center\" style=\"color:#009cdd\"><strong>BUY NOW. PAY LATER WITH</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:heading {\"align\":\"center\"} -->\n<h2 class=\"has-text-align-center\"><strong>QUAD PAY</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:columns -->\n<div class=\"wp-block-columns\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:heading {\"align\":\"center\"} -->\n<h2 class=\"has-text-align-center\"><strong>499</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"align\":\"center\",\"textColor\":\"vivid-cyan-blue\"} -->\n<p class=\"has-text-color has-text-align-center has-vivid-cyan-blue-color\"><strong>Already Sold</strong></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:heading {\"align\":\"center\"} -->\n<h2 class=\"has-text-align-center\"><strong>125</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"align\":\"center\",\"textColor\":\"vivid-cyan-blue\"} -->\n<p class=\"has-text-color has-text-align-center has-vivid-cyan-blue-color\"><strong>Pre Orders</strong></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:heading {\"align\":\"center\"} -->\n<h2 class=\"has-text-align-center\"><strong>100</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"align\":\"center\",\"textColor\":\"vivid-cyan-blue\"} -->\n<p class=\"has-text-color has-text-align-center has-vivid-cyan-blue-color\"><strong>Only for Order Over</strong></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->', 'Welcome', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2020-09-15 17:15:15', '2020-09-15 17:15:15', '', 26, 'http://localhost/wallart/26-revision-v1/', 0, 'revision', '', 0),
(104, 1, '2020-09-15 17:34:44', '2020-09-15 17:34:44', '<!-- wp:shortcode -->\n[huge_it_slider id=\"1\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:heading {\"customTextColor\":\"#009cdd\",\"className\":\"has-vivid-cyan-blue-color has-text-align-center margin-null\"} -->\n<h2 class=\"has-text-color has-vivid-cyan-blue-color has-text-align-center margin-null\" style=\"color:#009cdd\"><strong>SHOP BY CATEGORY</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"align\":\"center\",\"className\":\"margin-null\"} -->\n<p class=\"has-text-align-center margin-null\"><strong>test purpose only test purpose only test purpose only</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:shortcode -->\n[woo_category_slider id=\'63\']\n<!-- /wp:shortcode -->\n\n<!-- wp:spacer {\"height\":20} -->\n<div style=\"height:20px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:shortcode -->\n[woo_product_slider id=\"75\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:spacer {\"height\":20} -->\n<div style=\"height:20px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:heading {\"customTextColor\":\"#009cdd\",\"className\":\"has-vivid-cyan-blue-color has-text-align-center margin-null\"} -->\n<h2 class=\"has-text-color has-vivid-cyan-blue-color has-text-align-center margin-null\" style=\"color:#009cdd\"><strong>BEST SALES</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:spacer {\"height\":47} -->\n<div style=\"height:47px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:woocommerce/product-best-sellers {\"columns\":4,\"rows\":1,\"alignButtons\":true} /-->\n\n<!-- wp:columns -->\n<div class=\"wp-block-columns\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:image {\"id\":102,\"sizeSlug\":\"large\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"http://localhost/wallart/wp-content/uploads/2020/09/quad-pay-banner.jpg\" alt=\"\" class=\"wp-image-102\"/></figure>\n<!-- /wp:image --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:spacer {\"height\":52} -->\n<div style=\"height:52px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:heading {\"align\":\"center\",\"customTextColor\":\"#009cdd\"} -->\n<h2 class=\"has-text-color has-text-align-center\" style=\"color:#009cdd\"><strong>BUY NOW. PAY LATER WITH</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:heading {\"align\":\"center\"} -->\n<h2 class=\"has-text-align-center\"><strong>QUAD PAY</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:columns -->\n<div class=\"wp-block-columns\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:heading {\"align\":\"center\"} -->\n<h2 class=\"has-text-align-center\"><strong>499</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"align\":\"center\",\"textColor\":\"vivid-cyan-blue\"} -->\n<p class=\"has-text-color has-text-align-center has-vivid-cyan-blue-color\"><strong>Already Sold</strong></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:heading {\"align\":\"center\"} -->\n<h2 class=\"has-text-align-center\"><strong>125</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"align\":\"center\",\"textColor\":\"vivid-cyan-blue\"} -->\n<p class=\"has-text-color has-text-align-center has-vivid-cyan-blue-color\"><strong>Pre Orders</strong></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:heading {\"align\":\"center\"} -->\n<h2 class=\"has-text-align-center\"><strong>100</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"align\":\"center\",\"textColor\":\"vivid-cyan-blue\"} -->\n<p class=\"has-text-color has-text-align-center has-vivid-cyan-blue-color\"><strong>Only for Order Over</strong></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->', 'Welcome', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2020-09-15 17:34:44', '2020-09-15 17:34:44', '', 26, 'http://localhost/wallart/26-revision-v1/', 0, 'revision', '', 0),
(105, 1, '2020-09-15 17:39:29', '2020-09-15 17:39:29', '<!-- wp:shortcode -->\n[huge_it_slider id=\"1\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:heading {\"customTextColor\":\"#009cdd\",\"className\":\"has-vivid-cyan-blue-color has-text-align-center margin-null\"} -->\n<h2 class=\"has-text-color has-vivid-cyan-blue-color has-text-align-center margin-null\" style=\"color:#009cdd\"><strong>SHOP BY CATEGORY</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"align\":\"center\",\"className\":\"margin-null\"} -->\n<p class=\"has-text-align-center margin-null\"><strong>test purpose only test purpose only test purpose only</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:shortcode -->\n[woo_category_slider id=\'63\']\n<!-- /wp:shortcode -->\n\n<!-- wp:spacer {\"height\":20} -->\n<div style=\"height:20px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:shortcode -->\n[woo_product_slider id=\"75\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:spacer {\"height\":20} -->\n<div style=\"height:20px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:heading {\"customTextColor\":\"#009cdd\",\"className\":\"has-vivid-cyan-blue-color has-text-align-center margin-null\"} -->\n<h2 class=\"has-text-color has-vivid-cyan-blue-color has-text-align-center margin-null\" style=\"color:#009cdd\"><strong>BEST SALES</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:spacer {\"height\":47} -->\n<div style=\"height:47px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:woocommerce/product-best-sellers {\"columns\":4,\"rows\":1,\"alignButtons\":true} /-->\n\n<!-- wp:columns -->\n<div class=\"wp-block-columns\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:image {\"id\":102,\"sizeSlug\":\"large\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"http://localhost/wallart/wp-content/uploads/2020/09/quad-pay-banner.jpg\" alt=\"\" class=\"wp-image-102\"/></figure>\n<!-- /wp:image --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:spacer {\"height\":52} -->\n<div style=\"height:52px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:heading {\"align\":\"center\",\"level\":1,\"customTextColor\":\"#009cdd\"} -->\n<h1 class=\"has-text-color has-text-align-center\" style=\"color:#009cdd\"><strong>BUY NOW. PAY LATER WITH</strong></h1>\n<!-- /wp:heading -->\n\n<!-- wp:heading {\"align\":\"center\",\"level\":1} -->\n<h1 class=\"has-text-align-center\"><strong>QUAD PAY</strong></h1>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:columns -->\n<div class=\"wp-block-columns\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:heading {\"align\":\"center\"} -->\n<h2 class=\"has-text-align-center\"><strong>499</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"align\":\"center\",\"textColor\":\"vivid-cyan-blue\"} -->\n<p class=\"has-text-color has-text-align-center has-vivid-cyan-blue-color\"><strong>Already Sold</strong></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:heading {\"align\":\"center\"} -->\n<h2 class=\"has-text-align-center\"><strong>125</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"align\":\"center\",\"textColor\":\"vivid-cyan-blue\"} -->\n<p class=\"has-text-color has-text-align-center has-vivid-cyan-blue-color\"><strong>Pre Orders</strong></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:heading {\"align\":\"center\"} -->\n<h2 class=\"has-text-align-center\"><strong>100</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"align\":\"center\",\"textColor\":\"vivid-cyan-blue\"} -->\n<p class=\"has-text-color has-text-align-center has-vivid-cyan-blue-color\"><strong>Only for Order Over</strong></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->', 'Welcome', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2020-09-15 17:39:29', '2020-09-15 17:39:29', '', 26, 'http://localhost/wallart/26-revision-v1/', 0, 'revision', '', 0),
(106, 1, '2020-09-15 17:46:59', '2020-09-15 17:46:59', '<!-- wp:shortcode -->\n[huge_it_slider id=\"1\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:heading {\"customTextColor\":\"#009cdd\",\"className\":\"has-vivid-cyan-blue-color has-text-align-center margin-null\"} -->\n<h2 class=\"has-text-color has-vivid-cyan-blue-color has-text-align-center margin-null\" style=\"color:#009cdd\"><strong>SHOP BY CATEGORY</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"align\":\"center\",\"className\":\"margin-null\"} -->\n<p class=\"has-text-align-center margin-null\"><strong>test purpose only test purpose only test purpose only</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:shortcode -->\n[woo_category_slider id=\'63\']\n<!-- /wp:shortcode -->\n\n<!-- wp:spacer {\"height\":20} -->\n<div style=\"height:20px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:shortcode -->\n[woo_product_slider id=\"75\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:spacer {\"height\":20} -->\n<div style=\"height:20px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:heading {\"customTextColor\":\"#009cdd\",\"className\":\"has-vivid-cyan-blue-color has-text-align-center margin-null\"} -->\n<h2 class=\"has-text-color has-vivid-cyan-blue-color has-text-align-center margin-null\" style=\"color:#009cdd\"><strong>BEST SALES</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:spacer {\"height\":47} -->\n<div style=\"height:47px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:woocommerce/product-best-sellers {\"columns\":4,\"rows\":1,\"alignButtons\":true} /-->\n\n<!-- wp:columns {\"customBackgroundColor\":\"#f3f3f3\"} -->\n<div class=\"wp-block-columns has-background\" style=\"background-color:#f3f3f3\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:image {\"id\":102,\"sizeSlug\":\"large\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"http://localhost/wallart/wp-content/uploads/2020/09/quad-pay-banner.jpg\" alt=\"\" class=\"wp-image-102\"/></figure>\n<!-- /wp:image --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:spacer {\"height\":52} -->\n<div style=\"height:52px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:heading {\"align\":\"center\",\"level\":1,\"customTextColor\":\"#009cdd\"} -->\n<h1 class=\"has-text-color has-text-align-center\" style=\"color:#009cdd\"><strong>BUY NOW. PAY LATER WITH</strong></h1>\n<!-- /wp:heading -->\n\n<!-- wp:heading {\"align\":\"center\",\"level\":1} -->\n<h1 class=\"has-text-align-center\"><strong>QUAD PAY</strong></h1>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:columns -->\n<div class=\"wp-block-columns\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:heading {\"align\":\"center\"} -->\n<h2 class=\"has-text-align-center\"><strong>499</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"align\":\"center\",\"textColor\":\"vivid-cyan-blue\"} -->\n<p class=\"has-text-color has-text-align-center has-vivid-cyan-blue-color\"><strong>Already Sold</strong></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:heading {\"align\":\"center\"} -->\n<h2 class=\"has-text-align-center\"><strong>125</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"align\":\"center\",\"textColor\":\"vivid-cyan-blue\"} -->\n<p class=\"has-text-color has-text-align-center has-vivid-cyan-blue-color\"><strong>Pre Orders</strong></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:heading {\"align\":\"center\"} -->\n<h2 class=\"has-text-align-center\"><strong>100</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"align\":\"center\",\"textColor\":\"vivid-cyan-blue\"} -->\n<p class=\"has-text-color has-text-align-center has-vivid-cyan-blue-color\"><strong>Only for Order Over</strong></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->', 'Welcome', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2020-09-15 17:46:59', '2020-09-15 17:46:59', '', 26, 'http://localhost/wallart/26-revision-v1/', 0, 'revision', '', 0),
(107, 1, '2020-09-15 17:49:37', '2020-09-15 17:49:37', '<!-- wp:shortcode -->\n[huge_it_slider id=\"1\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:heading {\"customTextColor\":\"#009cdd\",\"className\":\"has-vivid-cyan-blue-color has-text-align-center margin-null\"} -->\n<h2 class=\"has-text-color has-vivid-cyan-blue-color has-text-align-center margin-null\" style=\"color:#009cdd\"><strong>SHOP BY CATEGORY</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"align\":\"center\",\"className\":\"margin-null\"} -->\n<p class=\"has-text-align-center margin-null\"><strong>test purpose only test purpose only test purpose only</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:shortcode -->\n[woo_category_slider id=\'63\']\n<!-- /wp:shortcode -->\n\n<!-- wp:spacer {\"height\":20} -->\n<div style=\"height:20px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:shortcode -->\n[woo_product_slider id=\"75\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:spacer {\"height\":20} -->\n<div style=\"height:20px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:heading {\"customTextColor\":\"#009cdd\",\"className\":\"has-vivid-cyan-blue-color has-text-align-center margin-null\"} -->\n<h2 class=\"has-text-color has-vivid-cyan-blue-color has-text-align-center margin-null\" style=\"color:#009cdd\"><strong>BEST SALES</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:spacer {\"height\":47} -->\n<div style=\"height:47px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:woocommerce/product-best-sellers {\"columns\":4,\"rows\":1,\"alignButtons\":true} /-->\n\n<!-- wp:columns {\"customBackgroundColor\":\"#f3f3f3\",\"className\":\"quadpay-section-home\"} -->\n<div class=\"wp-block-columns has-background quadpay-section-home\" style=\"background-color:#f3f3f3\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:image {\"id\":102,\"sizeSlug\":\"large\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"http://localhost/wallart/wp-content/uploads/2020/09/quad-pay-banner.jpg\" alt=\"\" class=\"wp-image-102\"/></figure>\n<!-- /wp:image --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:spacer {\"height\":52} -->\n<div style=\"height:52px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:heading {\"align\":\"center\",\"level\":1,\"customTextColor\":\"#009cdd\"} -->\n<h1 class=\"has-text-color has-text-align-center\" style=\"color:#009cdd\"><strong>BUY NOW. PAY LATER WITH</strong></h1>\n<!-- /wp:heading -->\n\n<!-- wp:heading {\"align\":\"center\",\"level\":1} -->\n<h1 class=\"has-text-align-center\"><strong>QUAD PAY</strong></h1>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:columns -->\n<div class=\"wp-block-columns\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:heading {\"align\":\"center\"} -->\n<h2 class=\"has-text-align-center\"><strong>499</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"align\":\"center\",\"textColor\":\"vivid-cyan-blue\"} -->\n<p class=\"has-text-color has-text-align-center has-vivid-cyan-blue-color\"><strong>Already Sold</strong></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:heading {\"align\":\"center\"} -->\n<h2 class=\"has-text-align-center\"><strong>125</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"align\":\"center\",\"textColor\":\"vivid-cyan-blue\"} -->\n<p class=\"has-text-color has-text-align-center has-vivid-cyan-blue-color\"><strong>Pre Orders</strong></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:heading {\"align\":\"center\"} -->\n<h2 class=\"has-text-align-center\"><strong>100</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"align\":\"center\",\"textColor\":\"vivid-cyan-blue\"} -->\n<p class=\"has-text-color has-text-align-center has-vivid-cyan-blue-color\"><strong>Only for Order Over</strong></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->', 'Welcome', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2020-09-15 17:49:37', '2020-09-15 17:49:37', '', 26, 'http://localhost/wallart/26-revision-v1/', 0, 'revision', '', 0),
(108, 1, '2020-09-15 18:06:04', '0000-00-00 00:00:00', '{\n    \"sidebars_widgets[footer-1]\": {\n        \"value\": [\n            \"nav_menu-3\"\n        ],\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-09-15 18:06:04\"\n    },\n    \"widget_nav_menu[3]\": {\n        \"value\": {\n            \"encoded_serialized_instance\": \"YToxOntzOjg6Im5hdl9tZW51IjtpOjE5O30=\",\n            \"title\": \"\",\n            \"is_widget_customizer_js_value\": true,\n            \"instance_hash_key\": \"a3e9105a326c144089cf7103901431a1\"\n        },\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-09-15 18:06:04\"\n    }\n}', '', '', 'auto-draft', 'closed', 'closed', '', 'd0da0cdd-5fae-4373-bbac-832e61bb1f26', '', '', '2020-09-15 18:06:04', '0000-00-00 00:00:00', '', 0, 'http://localhost/wallart/?p=108', 0, 'customize_changeset', '', 0),
(109, 1, '2020-09-15 18:19:36', '2020-09-15 18:19:36', ' ', '', '', 'publish', 'closed', 'closed', '', '109', '', '', '2020-09-16 11:21:48', '2020-09-16 11:21:48', '', 0, 'http://localhost/wallart/?p=109', 1, 'nav_menu_item', '', 0),
(111, 1, '2020-09-15 18:19:36', '2020-09-15 18:19:36', ' ', '', '', 'publish', 'closed', 'closed', '', '111', '', '', '2020-09-16 11:21:49', '2020-09-16 11:21:49', '', 0, 'http://localhost/wallart/?p=111', 4, 'nav_menu_item', '', 0),
(112, 1, '2020-09-15 18:19:37', '2020-09-15 18:19:37', ' ', '', '', 'publish', 'closed', 'closed', '', '112', '', '', '2020-09-16 11:21:48', '2020-09-16 11:21:48', '', 0, 'http://localhost/wallart/?p=112', 3, 'nav_menu_item', '', 0),
(113, 1, '2020-09-15 18:28:26', '2020-09-15 18:28:26', '{\n    \"sidebars_widgets[footer-1]\": {\n        \"value\": [\n            \"nav_menu-3\"\n        ],\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-09-15 18:27:06\"\n    },\n    \"widget_nav_menu[3]\": {\n        \"value\": {\n            \"encoded_serialized_instance\": \"YToxOntzOjg6Im5hdl9tZW51IjtpOjI0O30=\",\n            \"title\": \"\",\n            \"is_widget_customizer_js_value\": true,\n            \"instance_hash_key\": \"2ee771ee3c10843a3e200e7b6a625bc5\"\n        },\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-09-15 18:27:06\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '5d33a889-cecb-4d53-a16f-4f0eb61eeaae', '', '', '2020-09-15 18:28:26', '2020-09-15 18:28:26', '', 0, 'http://localhost/wallart/?p=113', 0, 'customize_changeset', '', 0),
(114, 1, '2020-09-15 18:30:20', '2020-09-15 18:30:20', '{\n    \"sidebars_widgets[footer-2]\": {\n        \"value\": [\n            \"text-3\"\n        ],\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-09-15 18:30:06\"\n    },\n    \"widget_text[3]\": {\n        \"value\": {\n            \"encoded_serialized_instance\": \"YTo0OntzOjU6InRpdGxlIjtzOjEwOiJDb250YWN0IFVzIjtzOjQ6InRleHQiO3M6MDoiIjtzOjY6ImZpbHRlciI7YjoxO3M6NjoidmlzdWFsIjtiOjE7fQ==\",\n            \"title\": \"Contact Us\",\n            \"is_widget_customizer_js_value\": true,\n            \"instance_hash_key\": \"86febf04e37dc7c82c48d3cf327d1039\"\n        },\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-09-15 18:30:06\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'b45e0d9f-0436-4ab5-8676-069674075af2', '', '', '2020-09-15 18:30:20', '2020-09-15 18:30:20', '', 0, 'http://localhost/wallart/?p=114', 0, 'customize_changeset', '', 0),
(115, 1, '2020-09-16 04:11:10', '2020-09-16 04:11:10', '{\n    \"widget_text[3]\": {\n        \"value\": {\n            \"encoded_serialized_instance\": \"YTo0OntzOjU6InRpdGxlIjtzOjEwOiJDb250YWN0IFVzIjtzOjQ6InRleHQiO3M6Nzk6IkVtYWlsOiBzdXBwb3J0QGZyYW1laG9vZC5jb20NCg0KQWRkcmVzczogMyBMeWxpYSBEci4NCg0KV2VzdCBOZXcgWW9yaywgTkogMDcwOTMiO3M6NjoiZmlsdGVyIjtiOjE7czo2OiJ2aXN1YWwiO2I6MTt9\",\n            \"title\": \"Contact Us\",\n            \"is_widget_customizer_js_value\": true,\n            \"instance_hash_key\": \"63072e9903cbc10e27d0ec4925c7bc5e\"\n        },\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-09-16 03:44:57\"\n    },\n    \"widget_nav_menu[3]\": {\n        \"value\": {\n            \"encoded_serialized_instance\": \"YToyOntzOjU6InRpdGxlIjtzOjU6IkxpbmtzIjtzOjg6Im5hdl9tZW51IjtpOjI0O30=\",\n            \"title\": \"Links\",\n            \"is_widget_customizer_js_value\": true,\n            \"instance_hash_key\": \"47bc8093d10162147caead6a418f0c51\"\n        },\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-09-16 03:44:57\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'b5ac98ca-48c9-4ec5-84ac-b4276ef736f8', '', '', '2020-09-16 04:11:10', '2020-09-16 04:11:10', '', 0, 'http://localhost/wallart/?p=115', 0, 'customize_changeset', '', 0),
(116, 1, '2020-09-16 05:49:32', '0000-00-00 00:00:00', '{\n    \"sidebars_widgets[footer-3]\": {\n        \"value\": [],\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-09-16 05:49:32\"\n    },\n    \"widget_rss[3]\": {\n        \"value\": [],\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-09-16 05:49:32\"\n    }\n}', '', '', 'auto-draft', 'closed', 'closed', '', '9647e63d-cc90-4c8c-b593-6c799b634a68', '', '', '2020-09-16 05:49:32', '0000-00-00 00:00:00', '', 0, 'http://localhost/wallart/?p=116', 0, 'customize_changeset', '', 0),
(117, 1, '2020-09-16 08:07:11', '2020-09-16 08:07:11', 'en_US', 'English', '', 'publish', 'closed', 'closed', '', 'english', '', '', '2020-09-16 08:07:11', '2020-09-16 08:07:11', '', 0, 'http://localhost/wallart/language_switcher/english/', 0, 'language_switcher', '', 0),
(118, 1, '2020-09-16 08:07:11', '2020-09-16 08:07:11', 'ar', 'Arabic', '', 'publish', 'closed', 'closed', '', 'arabic', '', '', '2020-09-16 08:07:11', '2020-09-16 08:07:11', '', 0, 'http://localhost/wallart/language_switcher/arabic/', 0, 'language_switcher', '', 0),
(119, 1, '2020-09-16 08:07:11', '2020-09-16 08:07:11', 'current_language', 'Current Language', '', 'publish', 'closed', 'closed', '', 'current-language', '', '', '2020-09-16 08:07:11', '2020-09-16 08:07:11', '', 0, 'http://localhost/wallart/language_switcher/current-language/', 0, 'language_switcher', '', 0),
(120, 1, '2020-09-16 10:13:02', '2020-09-16 10:13:02', '<!-- wp:shortcode -->\n[woocommerce_my_account]\n<!-- /wp:shortcode -->', 'My account', '', 'inherit', 'closed', 'closed', '', '9-revision-v1', '', '', '2020-09-16 10:13:02', '2020-09-16 10:13:02', '', 9, 'http://localhost/wallart/9-revision-v1/', 0, 'revision', '', 0),
(121, 1, '2020-09-16 11:17:21', '2020-09-16 11:17:21', '<!-- wp:heading --><h2>Who we are</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Our website address is: http://localhost/wallart.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What personal data we collect and why we collect it</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Comments</h3><!-- /wp:heading --><!-- wp:paragraph --><p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Media</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Contact forms</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Cookies</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you visit our login page, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Embedded content from other websites</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Analytics</h3><!-- /wp:heading --><!-- wp:heading --><h2>Who we share your data with</h2><!-- /wp:heading --><!-- wp:heading --><h2>How long we retain your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What rights you have over your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Where we send your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Visitor comments may be checked through an automated spam detection service.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Your contact information</h2><!-- /wp:heading --><!-- wp:heading --><h2>Additional information</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>How we protect your data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What data breach procedures we have in place</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What third parties we receive data from</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What automated decision making and/or profiling we do with user data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Industry regulatory disclosure requirements</h3><!-- /wp:heading -->', 'Privacy Policy', '', 'inherit', 'closed', 'closed', '', '3-revision-v1', '', '', '2020-09-16 11:17:21', '2020-09-16 11:17:21', '', 3, 'http://localhost/wallart/3-revision-v1/', 0, 'revision', '', 0),
(122, 1, '2020-09-16 11:17:48', '2020-09-16 11:17:48', '', 'Shipping Policy', '', 'publish', 'closed', 'closed', '', 'shipping-policy', '', '', '2020-09-16 11:17:48', '2020-09-16 11:17:48', '', 0, 'http://localhost/wallart/?page_id=122', 0, 'page', '', 0),
(123, 1, '2020-09-16 11:17:48', '2020-09-16 11:17:48', '', 'Shipping Policy', '', 'inherit', 'closed', 'closed', '', '122-revision-v1', '', '', '2020-09-16 11:17:48', '2020-09-16 11:17:48', '', 122, 'http://localhost/wallart/122-revision-v1/', 0, 'revision', '', 0),
(124, 1, '2020-09-16 11:18:10', '2020-09-16 11:18:10', '', 'FAQ', '', 'publish', 'closed', 'closed', '', 'faq', '', '', '2020-09-16 11:18:10', '2020-09-16 11:18:10', '', 0, 'http://localhost/wallart/?page_id=124', 0, 'page', '', 0),
(125, 1, '2020-09-16 11:18:10', '2020-09-16 11:18:10', '', 'FAQ', '', 'inherit', 'closed', 'closed', '', '124-revision-v1', '', '', '2020-09-16 11:18:10', '2020-09-16 11:18:10', '', 124, 'http://localhost/wallart/124-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(126, 1, '2020-09-16 11:21:49', '2020-09-16 11:21:49', ' ', '', '', 'publish', 'closed', 'closed', '', '126', '', '', '2020-09-16 11:21:49', '2020-09-16 11:21:49', '', 0, 'http://localhost/wallart/?p=126', 5, 'nav_menu_item', '', 0),
(127, 1, '2020-09-16 11:21:48', '2020-09-16 11:21:48', ' ', '', '', 'publish', 'closed', 'closed', '', '127', '', '', '2020-09-16 11:21:48', '2020-09-16 11:21:48', '', 0, 'http://localhost/wallart/?p=127', 2, 'nav_menu_item', '', 0),
(128, 1, '2020-09-16 11:21:49', '2020-09-16 11:21:49', ' ', '', '', 'publish', 'closed', 'closed', '', '128', '', '', '2020-09-16 11:21:49', '2020-09-16 11:21:49', '', 0, 'http://localhost/wallart/?p=128', 6, 'nav_menu_item', '', 0),
(129, 1, '2020-09-16 11:31:54', '2020-09-16 11:31:54', '{\n    \"storefront::nav_menu_locations[primary]\": {\n        \"value\": 19,\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-09-16 11:31:54\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '35fb6808-eaf2-499c-982f-3b0f2bea079c', '', '', '2020-09-16 11:31:54', '2020-09-16 11:31:54', '', 0, 'http://localhost/wallart/35fb6808-eaf2-499c-982f-3b0f2bea079c/', 0, 'customize_changeset', '', 0),
(130, 1, '2020-09-16 12:00:53', '2020-09-16 12:00:53', '{\n    \"widget_text[3]\": {\n        \"value\": {\n            \"encoded_serialized_instance\": \"YTo0OntzOjU6InRpdGxlIjtzOjEwOiJDb250YWN0IFVzIjtzOjQ6InRleHQiO3M6Nzk6IkVtYWlsOiBzdXBwb3J0QGZyYW1laG9vZC5jb20NCkFkZHJlc3M6IA0KMyBMeWxpYSBEci4NCldlc3QgTmV3IFlvcmssIA0KTkogMDcwOTMiO3M6NjoiZmlsdGVyIjtiOjE7czo2OiJ2aXN1YWwiO2I6MTt9\",\n            \"title\": \"Contact Us\",\n            \"is_widget_customizer_js_value\": true,\n            \"instance_hash_key\": \"8cc2b34026d542ea63fd1ae24fb28202\"\n        },\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-09-16 12:00:53\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '92d1186a-c608-40b6-90cb-6759be19fa3a', '', '', '2020-09-16 12:00:53', '2020-09-16 12:00:53', '', 0, 'http://localhost/wallart/92d1186a-c608-40b6-90cb-6759be19fa3a/', 0, 'customize_changeset', '', 0),
(131, 1, '2020-09-16 12:32:15', '2020-09-16 12:32:15', '{\n    \"storefront::nav_menu_locations[primary]\": {\n        \"value\": 19,\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-09-16 12:32:15\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '3cf1d77a-92d1-40ce-93f2-20cc7005095d', '', '', '2020-09-16 12:32:15', '2020-09-16 12:32:15', '', 0, 'http://localhost/wallart/3cf1d77a-92d1-40ce-93f2-20cc7005095d/', 0, 'customize_changeset', '', 0),
(132, 1, '2020-09-16 13:24:14', '2020-09-16 13:24:14', '<!-- wp:shortcode -->\n[huge_it_slider id=\"1\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:heading {\"customTextColor\":\"#009cdd\",\"className\":\"has-vivid-cyan-blue-color has-text-align-center margin-null\"} -->\n<h2 class=\"has-text-color has-vivid-cyan-blue-color has-text-align-center margin-null\" style=\"color:#009cdd\"><strong>SHOP BY CATEGORY</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"align\":\"center\",\"className\":\"margin-null\"} -->\n<p class=\"has-text-align-center margin-null\"><strong>test purpose only test purpose only test purpose only</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:shortcode -->\n[woo_category_slider id=\'63\']\n<!-- /wp:shortcode -->\n\n<!-- wp:spacer {\"height\":20} -->\n<div style=\"height:20px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:shortcode -->\n[woo_product_slider id=\"75\"]\n<!-- /wp:shortcode -->\n\n<!-- wp:spacer {\"height\":20} -->\n<div style=\"height:20px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:heading {\"customTextColor\":\"#009cdd\",\"className\":\"has-vivid-cyan-blue-color has-text-align-center margin-null\"} -->\n<h2 class=\"has-text-color has-vivid-cyan-blue-color has-text-align-center margin-null\" style=\"color:#009cdd\"><strong>BEST SALES</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:spacer {\"height\":47} -->\n<div style=\"height:47px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:woocommerce/product-best-sellers {\"columns\":4,\"rows\":1,\"alignButtons\":true} /-->\n\n<!-- wp:columns {\"customBackgroundColor\":\"#f3f3f3\",\"className\":\"quadpay-section-home\"} -->\n<div class=\"wp-block-columns has-background quadpay-section-home\" style=\"background-color:#f3f3f3\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:image {\"id\":102,\"sizeSlug\":\"large\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"http://localhost/wallart/wp-content/uploads/2020/09/quad-pay-banner.jpg\" alt=\"\" class=\"wp-image-102\"/></figure>\n<!-- /wp:image --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:spacer {\"height\":52} -->\n<div style=\"height:52px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:heading {\"align\":\"center\",\"level\":1,\"customTextColor\":\"#009cdd\"} -->\n<h1 class=\"has-text-color has-text-align-center\" style=\"color:#009cdd\"><strong>BUY NOW. PAY LATER WITH</strong></h1>\n<!-- /wp:heading -->\n\n<!-- wp:heading {\"align\":\"center\",\"level\":1} -->\n<h1 class=\"has-text-align-center\"><strong>QUAD PAY</strong></h1>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:columns -->\n<div class=\"wp-block-columns\"><!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:heading {\"align\":\"center\"} -->\n<h2 class=\"has-text-align-center\"><strong>499</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"align\":\"center\",\"textColor\":\"vivid-cyan-blue\"} -->\n<p class=\"has-text-color has-text-align-center has-vivid-cyan-blue-color\"><strong>Already Sold</strong></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:heading {\"align\":\"center\"} -->\n<h2 class=\"has-text-align-center\"><strong>125</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"align\":\"center\",\"textColor\":\"vivid-cyan-blue\"} -->\n<p class=\"has-text-color has-text-align-center has-vivid-cyan-blue-color\"><strong>Pre Orders</strong></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"><!-- wp:heading {\"align\":\"center\"} -->\n<h2 class=\"has-text-align-center\"><strong>100</strong></h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"align\":\"center\",\"textColor\":\"vivid-cyan-blue\"} -->\n<p class=\"has-text-color has-text-align-center has-vivid-cyan-blue-color\"><strong>Only for Order Over</strong></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->', 'Home', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2020-09-16 13:24:14', '2020-09-16 13:24:14', '', 26, 'http://localhost/wallart/26-revision-v1/', 0, 'revision', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_termmeta`
--

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_termmeta`
--

INSERT INTO `wp_termmeta` (`meta_id`, `term_id`, `meta_key`, `meta_value`) VALUES
(1, 16, 'thumbnail_id', '64'),
(2, 16, 'product_count_product_cat', '2'),
(3, 17, 'thumbnail_id', '65'),
(4, 17, 'product_count_product_cat', '1'),
(5, 18, 'thumbnail_id', '66'),
(6, 18, 'product_count_product_cat', '2'),
(7, 16, 'display_type', ''),
(8, 17, 'display_type', ''),
(9, 18, 'display_type', ''),
(10, 16, 'order', '1'),
(11, 18, 'order', '3'),
(12, 17, 'order', '2'),
(13, 15, 'order', '4'),
(20, 22, 'order', '0'),
(21, 22, 'display_type', ''),
(22, 22, 'thumbnail_id', '64'),
(26, 22, 'product_count_product_cat', '1'),
(30, 15, 'product_count_product_cat', '0');

-- --------------------------------------------------------

--
-- Table structure for table `wp_terms`
--

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'simple', 'simple', 0),
(3, 'grouped', 'grouped', 0),
(4, 'variable', 'variable', 0),
(5, 'external', 'external', 0),
(6, 'exclude-from-search', 'exclude-from-search', 0),
(7, 'exclude-from-catalog', 'exclude-from-catalog', 0),
(8, 'featured', 'featured', 0),
(9, 'outofstock', 'outofstock', 0),
(10, 'rated-1', 'rated-1', 0),
(11, 'rated-2', 'rated-2', 0),
(12, 'rated-3', 'rated-3', 0),
(13, 'rated-4', 'rated-4', 0),
(14, 'rated-5', 'rated-5', 0),
(15, 'Uncategorized', 'uncategorized', 0),
(16, 'Shop Motivational Art', 'motivational-art', 0),
(17, 'Shop Cultural Art', 'cultural-art', 0),
(18, 'Shop Astrology Art', 'astrology-art', 0),
(19, 'Header Menu', 'header-menu', 0),
(22, 'Comics', 'comics', 0),
(24, 'Footer Menu', 'footer-menu', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_relationships`
--

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(28, 2, 0),
(28, 18, 0),
(29, 2, 0),
(29, 18, 0),
(30, 2, 0),
(30, 15, 0),
(31, 2, 0),
(31, 8, 0),
(31, 16, 0),
(32, 2, 0),
(32, 17, 0),
(33, 2, 0),
(33, 8, 0),
(33, 17, 0),
(34, 2, 0),
(34, 8, 0),
(34, 15, 0),
(35, 2, 0),
(35, 8, 0),
(35, 22, 0),
(36, 2, 0),
(36, 18, 0),
(37, 2, 0),
(37, 18, 0),
(38, 2, 0),
(38, 17, 0),
(39, 2, 0),
(39, 16, 0),
(49, 19, 0),
(50, 19, 0),
(53, 19, 0),
(82, 2, 0),
(82, 22, 0),
(84, 2, 0),
(84, 16, 0),
(109, 24, 0),
(111, 24, 0),
(112, 24, 0),
(126, 24, 0),
(127, 24, 0),
(128, 24, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_taxonomy`
--

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'product_type', '', 0, 6),
(3, 3, 'product_type', '', 0, 0),
(4, 4, 'product_type', '', 0, 0),
(5, 5, 'product_type', '', 0, 0),
(6, 6, 'product_visibility', '', 0, 0),
(7, 7, 'product_visibility', '', 0, 0),
(8, 8, 'product_visibility', '', 0, 0),
(9, 9, 'product_visibility', '', 0, 0),
(10, 10, 'product_visibility', '', 0, 0),
(11, 11, 'product_visibility', '', 0, 0),
(12, 12, 'product_visibility', '', 0, 0),
(13, 13, 'product_visibility', '', 0, 0),
(14, 14, 'product_visibility', '', 0, 0),
(15, 15, 'product_cat', '', 0, 0),
(16, 16, 'product_cat', 'A short category description', 0, 2),
(17, 17, 'product_cat', 'A short category description', 0, 1),
(18, 18, 'product_cat', 'A short category description', 0, 2),
(19, 19, 'nav_menu', '', 0, 3),
(22, 22, 'product_cat', '', 0, 1),
(24, 24, 'nav_menu', '', 0, 6);

-- --------------------------------------------------------

--
-- Table structure for table `wp_trp_dictionary_en_us_ar`
--

CREATE TABLE `wp_trp_dictionary_en_us_ar` (
  `id` bigint(20) NOT NULL,
  `original` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `translated` longtext COLLATE utf8mb4_unicode_520_ci,
  `status` int(20) DEFAULT '0',
  `block_type` int(20) DEFAULT '0',
  `original_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_trp_dictionary_en_us_ar`
--

INSERT INTO `wp_trp_dictionary_en_us_ar` (`id`, `original`, `translated`, `status`, `block_type`, `original_id`) VALUES
(1, 'My Account', 'حسابي', 2, 0, 1),
(2, 'Cart', 'عربة التسوق', 2, 0, 2),
(3, 'Blog', 'مدونة', 2, 0, 3),
(4, 'Checkout', 'الدفع', 2, 0, 4),
(5, 'My account', 'حسابي', 2, 0, 5),
(6, 'Refer &#038; Earn', 'الرجوع وكسب', 2, 0, 6),
(7, 'Sample Page', 'نموذج الصفحة', 2, 0, 7),
(8, 'Shop', 'متجر', 2, 0, 8),
(9, 'SPEND MORE...', 'انفق أكثر', 2, 0, 9),
(10, 'SAVE MORE...', 'احفظ أكثر...', 2, 0, 10),
(11, '$100 OFF ON EVERY $950 SPENT', 'خصم 100 دولار على كل 950 دولارًا أمريكيًا', 2, 0, 11),
(12, 'SHOP BY CATEGORY', 'تسوق حسب الاقسام', 2, 0, 12),
(13, 'test purpose only test purpose only test purpose only', 'الغرض من الاختبار فقط لغرض الاختبار فقط لغرض الاختبار فقط', 2, 0, 13),
(14, 'Comics', '', 0, 0, 14),
(15, 'Shop Astrology Art', '', 0, 0, 15),
(16, 'Shop Cultural Art', '', 0, 0, 16),
(17, 'Shop Motivational Art', '', 0, 0, 17),
(18, 'New Arrivals', 'الوافدون الجدد', 2, 0, 18),
(19, 'Elevate Yourself', '', 0, 0, 19),
(20, 'Mind your business', '', 0, 0, 20),
(21, 'Nipsey hussle', '', 0, 0, 21),
(22, 'Shine like a greek god', '', 0, 0, 22),
(23, 'Erotica &#8211; You&#8217;re Mine', '', 0, 0, 23),
(24, 'Separate Yourself to elevate yourself', '', 0, 0, 24),
(25, 'BEST SALES', 'أفضل المبيعات', 2, 0, 25),
(26, 'BUY NOW. PAY LATER WITH', 'اشتري الآن. ادفع لاحقًا باستخدام', 2, 0, 26),
(27, 'QUAD PAY', '', 0, 0, 27),
(28, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', '', 0, 0, 28),
(29, 'Already Sold', 'تم بيعه', 2, 0, 29),
(30, 'Pre Orders', 'الطلبات المسبقة', 2, 0, 30),
(31, 'Only for Order Over', 'فقط للطلب أكثر', 2, 0, 31),
(32, 'Links', 'الروابط', 2, 0, 32),
(33, 'Contact Us', 'اتصل بنا', 2, 0, 33),
(34, 'Email: support@framehood.com', '', 0, 0, 34),
(35, 'Address: 3 Lylia Dr.', '', 0, 0, 35),
(36, 'West New York, NJ 07093', '', 0, 0, 36),
(37, '&copy; wallart 2020', '', 0, 0, 37),
(38, 'http://localhost/wallart/wp-content/uploads/2020/09/cropped-logo.jpg', '', 0, 0, 38),
(39, 'http://localhost/wallart/wp-content/uploads/2020/09/banner1-1.jpg', '', 0, 0, 39),
(40, 'http://localhost/wallart/wp-content/uploads/2020/09/category_1.jpg', '', 0, 0, 40),
(41, 'http://localhost/wallart/wp-content/uploads/2020/09/category_3.jpg', '', 0, 0, 41),
(42, 'http://localhost/wallart/wp-content/uploads/2020/09/category_2.jpg', '', 0, 0, 42),
(43, 'http://localhost/wallart/wp-content/uploads/2020/09/img_7.jpg', '', 0, 0, 43),
(44, 'http://localhost/wallart/wp-content/uploads/2020/09/img_8.jpg', '', 0, 0, 44),
(45, 'http://localhost/wallart/wp-content/uploads/2020/09/img_1.jpg', '', 0, 0, 45),
(46, 'http://localhost/wallart/wp-content/uploads/2020/09/img_2.jpg', '', 0, 0, 46),
(47, 'http://localhost/wallart/wp-content/uploads/2020/09/img_3.jpg', '', 0, 0, 47),
(48, 'http://localhost/wallart/wp-content/uploads/2020/09/img_4.jpg', '', 0, 0, 48),
(49, 'http://localhost/wallart/wp-content/uploads/2020/09/img_7-324x324.jpg', '', 0, 0, 49),
(50, 'http://localhost/wallart/wp-content/uploads/2020/09/img_8-324x324.jpg', '', 0, 0, 50),
(51, 'http://localhost/wallart/wp-content/uploads/2020/09/img_1-324x324.jpg', '', 0, 0, 51),
(52, 'http://localhost/wallart/wp-content/uploads/2020/09/img_2-324x324.jpg', '', 0, 0, 52),
(53, 'http://localhost/wallart/wp-content/uploads/2020/09/quad-pay-banner.jpg', '', 0, 0, 53),
(54, 'Share on Facebook', '', 0, 0, 54),
(55, 'Share on Twitter', '', 0, 0, 55),
(56, 'Share on Google+', '', 0, 0, 56),
(57, 'https://www.facebook.com/sharer/sharer.php?u=http://localhost/wallart/?trp-edit-translation=preview', '', 0, 0, 57),
(58, 'https://twitter.com/share?url=http://localhost/wallart/?trp-edit-translation=preview&text=Share Buttons Demo&via=sunnyismoi', '', 0, 0, 58),
(59, 'https://plus.google.com/share?url=http://localhost/wallart/?trp-edit-translation=preview', '', 0, 0, 59),
(60, '₹', '', 0, 0, 60),
(61, '₹', '', 0, 0, 60),
(62, 'https://www.facebook.com/sharer/sharer.php?u=http://localhost/wallart/ar/?trp-edit-translation=preview', '', 0, 0, 62),
(63, 'https://twitter.com/share?url=http://localhost/wallart/ar/?trp-edit-translation=preview&text=Share Buttons Demo&via=sunnyismoi', '', 0, 0, 63),
(64, 'https://plus.google.com/share?url=http://localhost/wallart/ar/?trp-edit-translation=preview', '', 0, 0, 64),
(65, 'https://www.facebook.com/sharer/sharer.php?u=http://localhost/wallart/ar/', '', 0, 0, 65),
(66, 'https://twitter.com/share?url=http://localhost/wallart/ar/&text=Share Buttons Demo&via=sunnyismoi', '', 0, 0, 66),
(67, 'https://plus.google.com/share?url=http://localhost/wallart/ar/', '', 0, 0, 67),
(68, 'This is an example page. It&#8217;s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:', '', 0, 0, 68),
(69, 'Hi there! I&#8217;m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin&#8217; caught in the rain.)', '', 0, 0, 69),
(70, '&#8230;or something like this:', '', 0, 0, 70),
(71, 'The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.', '', 0, 0, 71),
(72, 'As a new WordPress user, you should go to', '', 0, 0, 72),
(73, 'your dashboard', '', 0, 0, 73),
(74, 'to delete this page and create new pages for your content. Have fun!', '', 0, 0, 74),
(75, 'Hello world!', '', 0, 0, 75),
(76, 'Uncategorized', '', 0, 0, 76),
(77, 'Home', '', 0, 0, 77),
(78, 'FAQ', '', 0, 0, 78),
(79, 'Privacy Policy', '', 0, 0, 79),
(80, 'Shipping Policy', '', 0, 0, 80),
(81, 'Address:', '', 0, 0, 81),
(82, '3 Lylia Dr.', '', 0, 0, 82),
(83, 'West New York,', '', 0, 0, 83),
(84, 'NJ 07093', '', 0, 0, 84),
(85, '© 2020 - Wallart - All Rights Reserved', '', 0, 0, 85);

-- --------------------------------------------------------

--
-- Table structure for table `wp_trp_gettext_ar`
--

CREATE TABLE `wp_trp_gettext_ar` (
  `id` bigint(20) NOT NULL,
  `original` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `translated` longtext COLLATE utf8mb4_unicode_520_ci,
  `domain` longtext COLLATE utf8mb4_unicode_520_ci,
  `status` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_trp_gettext_ar`
--

INSERT INTO `wp_trp_gettext_ar` (`id`, `original`, `translated`, `domain`, `status`) VALUES
(1, 'Skip to navigation', '', 'storefront', 0),
(2, 'Skip to content', '', 'storefront', 0),
(3, 'Search for:', 'بحث عن:', 'woocommerce', 2),
(4, 'Search products&hellip;', 'البحث عن المنتجات…', 'woocommerce', 2),
(5, 'Search', 'بحث', 'woocommerce', 2),
(6, 'Menu', '', 'storefront', 0),
(7, 'Home', 'الرئيسية', 'default', 2),
(8, 'View your shopping cart', '', 'storefront', 0),
(9, '%d items', '', 'storefront', 0),
(10, 'Add to cart', 'إضافة إلى السلة', 'woocommerce', 2),
(11, 'Sale', 'سعر العرض', 'woocommerce', 2),
(12, 'Product on sale', 'منتج مخفض', 'woocommerce', 2),
(13, 'Edit <span class=\"screen-reader-text\">%s</span>', '', 'storefront', 0),
(14, 'WooCommerce - The Best eCommerce Platform for WordPress', '', 'storefront', 0),
(15, 'Built with Storefront &amp; WooCommerce', '', 'storefront', 0),
(16, 'My Account', '', 'storefront', 0),
(17, 'Search', '', 'storefront', 0),
(18, 'No products in the cart.', 'لا توجد منتجات في سلة المشتريات.', 'woocommerce', 2),
(19, 'View cart', 'عرض السلة', 'woocommerce', 2),
(20, 'Primary Navigation', '', 'storefront', 0),
(21, 'Pages', 'صفحات', 'default', 2),
(22, 'Home', 'الرئيسية', 'woocommerce', 2),
(23, 'Continue reading %s', 'تابع قراءة %s', 'default', 2),
(24, '(more&hellip;)', '(المزيد&hellip;)', 'default', 2),
(25, 'Shop and search results', 'المتجر ونتائج البحث', 'woocommerce', 2),
(26, 'Shop only', 'المتجر فقط', 'woocommerce', 2),
(27, 'Search results only', 'نتائج البحث فقط', 'woocommerce', 2),
(28, 'Hidden', 'مخفي', 'woocommerce', 2),
(29, 'In stock', 'متوفر في المخزون', 'woocommerce', 2),
(30, 'Out of stock', 'غير متوفر في المخزون', 'woocommerce', 2),
(31, 'On backorder', 'متاح للطلب المسبق', 'woocommerce', 2),
(32, 'Add &ldquo;%s&rdquo; to your cart', 'إضافة \"%s\" إلى سلة مشترياتك', 'woocommerce', 2),
(33, 'Shop Now', '', 'woo-category-slider-by-pluginever', 0),
(34, 'Products', '', 'woo-category-slider-by-pluginever', 0),
(35, 'Pages:', '', 'storefront', 0),
(36, 'Pages:', 'الصفحات:', 'default', 2),
(37, 'Next page', 'الصفحة التالية', 'default', 2),
(38, 'Previous page', 'الصفحة السابقة', 'default', 2),
(39, 'words', '', 'default', 0),
(40, 'Edit My Profile', 'تحرير ملفي الشخصي', 'default', 2),
(41, 'Log Out', 'تسجيل الخروج', 'default', 2),
(42, 'Search', 'البحث', 'default', 2),
(43, 'Howdy, %s', 'مرحبًا، %s', 'default', 2),
(44, 'About WordPress', 'نبذة عن ووردبريس', 'default', 2),
(45, 'WordPress.org', '', 'default', 0),
(46, 'https://wordpress.org/', 'https://ar.wordpress.org/', 'default', 2),
(47, 'Documentation', 'وثائق المساعدة', 'default', 2),
(48, 'https://codex.wordpress.org/', '', 'default', 0),
(49, 'Support', 'الدعم', 'default', 2),
(50, 'https://wordpress.org/support/', 'https://ar.wordpress.org/support/', 'default', 2),
(51, 'Feedback', 'طلبات واقتراحات', 'default', 2),
(52, 'https://wordpress.org/support/forum/requests-and-feedback', '', 'default', 0),
(53, 'Dashboard', 'الرئيسية', 'default', 2),
(54, 'Themes', 'قوالب', 'default', 2),
(55, 'Widgets', 'ودجات', 'default', 2),
(56, 'Menus', 'قوائم', 'default', 2),
(57, 'Background', 'خلفية', 'default', 2),
(58, 'Header', 'ترويسة', 'default', 2),
(59, 'Customize', 'تخصيص', 'default', 2),
(60, '%d Plugin Updates', '%d إضافات تحتاج إلى تحديث', 'default', 2),
(61, '%d Theme Updates', '%d قوالب تحتاج إلى تحديث', 'default', 2),
(62, '%s Comments in moderation', 'لا تعليق بانتظار الموافقة', 'default', 2),
(63, 'User', 'عضو', 'default', 2),
(64, 'New', 'جديد', 'default', 2),
(65, 'Skip to toolbar', 'التخطي إلى شريط الأدوات', 'default', 2),
(66, 'Toolbar', 'شريط الأدوات', 'default', 2),
(67, 'Search for:', '', 'default', 0),
(68, 'Search &hellip;', '', 'default', 0),
(69, 'Recent Posts', 'أحدث المقالات', 'default', 2),
(70, 'Recent Comments', 'أحدث التعليقات', 'default', 2),
(71, '%1$s on %2$s', '%1$s على %2$s', 'default', 2),
(72, 'Archives', 'الأرشيف', 'default', 2),
(73, '%1$s %2$d', '', 'default', 0),
(74, 'Categories', 'تصنيفات', 'default', 2),
(75, 'Meta', 'منوعات', 'default', 2),
(76, 'Site Admin', 'إدارة الموقع', 'default', 2),
(77, 'Log out', 'تسجيل الخروج', 'default', 2),
(78, 'Entries feed', 'إدخالات الخلاصات Feed', 'default', 2),
(79, 'Comments feed', 'خلاصة التعليقات', 'default', 2),
(80, 'WordPress &rsaquo; Error', 'ووردبريس &rsaquo; خطأ', 'default', 2);

-- --------------------------------------------------------

--
-- Table structure for table `wp_trp_gettext_en_us`
--

CREATE TABLE `wp_trp_gettext_en_us` (
  `id` bigint(20) NOT NULL,
  `original` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `translated` longtext COLLATE utf8mb4_unicode_520_ci,
  `domain` longtext COLLATE utf8mb4_unicode_520_ci,
  `status` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_trp_gettext_en_us`
--

INSERT INTO `wp_trp_gettext_en_us` (`id`, `original`, `translated`, `domain`, `status`) VALUES
(1, 'View cart', '', 'woocommerce', 0),
(2, 'Skip to navigation', '', 'storefront', 0),
(3, 'Skip to content', '', 'storefront', 0),
(4, 'Search for:', '', 'woocommerce', 0),
(5, 'Search products&hellip;', 'Search products…', 'woocommerce', 2),
(6, 'Search', '', 'woocommerce', 0),
(7, 'Primary Navigation', '', 'storefront', 0),
(8, 'Menu', '', 'storefront', 0),
(9, 'Home', '', 'default', 0),
(10, 'Pages', '', 'default', 0),
(11, 'View your shopping cart', '', 'storefront', 0),
(12, '%d items', '', 'storefront', 0),
(13, 'Home', '', 'woocommerce', 0),
(14, 'Continue reading %s', '', 'default', 0),
(15, '(more&hellip;)', '', 'default', 0),
(16, 'Shop and search results', '', 'woocommerce', 0),
(17, 'Shop only', '', 'woocommerce', 0),
(18, 'Search results only', '', 'woocommerce', 0),
(19, 'Hidden', '', 'woocommerce', 0),
(20, 'In stock', '', 'woocommerce', 0),
(21, 'Out of stock', '', 'woocommerce', 0),
(22, 'On backorder', '', 'woocommerce', 0),
(23, 'Add &ldquo;%s&rdquo; to your cart', '', 'woocommerce', 0),
(24, 'Add to cart', '', 'woocommerce', 0),
(25, 'Sale', '$', 'woocommerce', 2),
(26, 'Product on sale', '', 'woocommerce', 0),
(27, 'Shop Now', '', 'woo-category-slider-by-pluginever', 0),
(28, 'Products', '', 'woo-category-slider-by-pluginever', 0),
(29, 'Pages:', '', 'storefront', 0),
(30, 'Pages:', '', 'default', 0),
(31, 'Next page', '', 'default', 0),
(32, 'Previous page', '', 'default', 0),
(33, 'Edit <span class=\"screen-reader-text\">%s</span>', '', 'storefront', 0),
(34, 'words', '', 'default', 0),
(35, 'WooCommerce - The Best eCommerce Platform for WordPress', '', 'storefront', 0),
(36, 'Built with Storefront &amp; WooCommerce', '', 'storefront', 0),
(37, 'My Account', '', 'storefront', 0),
(38, 'Search', '', 'storefront', 0),
(39, 'No products in the cart.', '', 'woocommerce', 0),
(40, 'WordPress &rsaquo; Error', '', 'default', 0),
(41, 'Edit My Profile', '', 'default', 0),
(42, 'Log Out', '', 'default', 0),
(43, 'Search', '', 'default', 0),
(44, 'Howdy, %s', '', 'default', 0),
(45, 'About WordPress', '', 'default', 0),
(46, 'WordPress.org', '', 'default', 0),
(47, 'https://wordpress.org/', '', 'default', 0),
(48, 'Documentation', '', 'default', 0),
(49, 'https://codex.wordpress.org/', '', 'default', 0),
(50, 'Support', '', 'default', 0),
(51, 'https://wordpress.org/support/', '', 'default', 0),
(52, 'Feedback', '', 'default', 0),
(53, 'https://wordpress.org/support/forum/requests-and-feedback', '', 'default', 0),
(54, 'Dashboard', '', 'default', 0),
(55, 'Themes', '', 'default', 0),
(56, 'Widgets', '', 'default', 0),
(57, 'Menus', '', 'default', 0),
(58, 'Background', '', 'default', 0),
(59, 'Header', '', 'default', 0),
(60, 'Customize', '', 'default', 0),
(61, '%d Plugin Updates', '', 'default', 0),
(62, '%d Theme Updates', '', 'default', 0),
(63, '%s Comments in moderation', '', 'default', 0),
(64, 'User', '', 'default', 0),
(65, 'New', '', 'default', 0),
(66, 'Skip to toolbar', '', 'default', 0),
(67, 'Toolbar', '', 'default', 0),
(68, 'Search for:', '', 'default', 0),
(69, 'Search &hellip;', '', 'default', 0),
(70, 'Recent Posts', '', 'default', 0),
(71, 'Recent Comments', '', 'default', 0),
(72, '%1$s on %2$s', '', 'default', 0),
(73, 'Archives', '', 'default', 0),
(74, '%1$s %2$d', '', 'default', 0),
(75, 'Categories', '', 'default', 0),
(76, 'No categories', '', 'default', 0),
(77, 'Meta', '', 'default', 0),
(78, 'Site Admin', '', 'default', 0),
(79, 'Log out', '', 'default', 0),
(80, 'Entries feed', '', 'default', 0),
(81, 'Comments feed', '', 'default', 0),
(82, 'Log in', '', 'default', 0),
(83, '&#8220;%s&#8221;', '', 'default', 0),
(84, 'Shift-click to edit this element.', '', 'default', 0),
(85, 'This link is not live-previewable.', '', 'default', 0),
(86, 'This form is not live-previewable.', '', 'default', 0),
(87, 'Shift-click to edit this widget.', '', 'default', 0),
(88, 'Click to edit this menu.', '', 'default', 0),
(89, 'Click to edit this widget.', '', 'default', 0),
(90, 'Click to edit the site title.', '', 'default', 0),
(91, 'Click to edit this element.', '', 'default', 0),
(92, '%s is forbidden', '', 'default', 0),
(93, 'Login', '', 'woocommerce', 0),
(94, 'Username or email address', '', 'woocommerce', 0),
(95, 'Password', '', 'woocommerce', 0),
(96, 'Remember me', '', 'woocommerce', 0),
(97, 'Log in', '', 'woocommerce', 0),
(98, 'Lost your password?', '', 'woocommerce', 0),
(99, 'Register', '', 'woocommerce', 0),
(100, 'First Name', '', 'woocommerce-simple-registration', 0),
(101, 'Last Name', '', 'woocommerce-simple-registration', 0),
(102, 'Email address', '', 'woocommerce', 0),
(103, 'A password will be sent to your email address.', '', 'woocommerce', 0),
(104, 'Your personal data will be used to support your experience throughout this website, to manage access to your account, and for other purposes described in our %s.', '', 'woocommerce', 0),
(105, 'privacy policy', '', 'woocommerce', 0),
(106, 'terms and conditions', '', 'woocommerce', 0),
(107, 'Error 404', '', 'woocommerce', 0),
(108, 'Oops! That page can&rsquo;t be found.', '', 'storefront', 0),
(109, 'Nothing was found at this location. Try searching, or check out the links below.', '', 'storefront', 0),
(110, 'Promoted Products', '', 'storefront', 0),
(111, 'On Sale Now', '', 'storefront', 0),
(112, 'Sale!', '', 'woocommerce', 0),
(113, 'Product Categories', '', 'storefront', 0),
(114, 'No product categories exist.', '', 'woocommerce', 0),
(115, 'Popular Products', '', 'storefront', 0),
(116, 'Widgets are independent sections of content that can be placed into widgetized areas provided by your theme (commonly called sidebars).', '', 'default', 0),
(117, 'New order', '', 'woocommerce', 0),
(118, 'New order emails are sent to chosen recipient(s) when a new order is received.', '', 'woocommerce', 0),
(119, 'Available placeholders: %s', '', 'woocommerce', 0),
(120, 'Enable/Disable', '', 'woocommerce', 0),
(121, 'Enable this email notification', '', 'woocommerce', 0),
(122, 'Recipient(s)', '', 'woocommerce', 0),
(123, 'Enter recipients (comma separated) for this email. Defaults to %s.', '', 'woocommerce', 0),
(124, 'Subject', '', 'woocommerce', 0),
(125, '[{site_title}]: New order #{order_number}', '', 'woocommerce', 0),
(126, 'Email heading', '', 'woocommerce', 0),
(127, 'New Order: #{order_number}', '', 'woocommerce', 0),
(128, 'Additional content', '', 'woocommerce', 0),
(129, 'Text to appear below the main email content.', '', 'woocommerce', 0),
(130, 'N/A', '', 'woocommerce', 0),
(131, 'Congratulations on the sale.', '', 'woocommerce', 0),
(132, 'Email type', '', 'woocommerce', 0),
(133, 'Choose which format of email to send.', '', 'woocommerce', 0),
(134, 'Plain text', '', 'woocommerce', 0),
(135, 'HTML', '', 'woocommerce', 0),
(136, 'Multipart', '', 'woocommerce', 0),
(137, 'Cancelled order', '', 'woocommerce', 0),
(138, 'Cancelled order emails are sent to chosen recipient(s) when orders have been marked cancelled (if they were previously processing or on-hold).', '', 'woocommerce', 0),
(139, '[{site_title}]: Order #{order_number} has been cancelled', '', 'woocommerce', 0),
(140, 'Order Cancelled: #{order_number}', '', 'woocommerce', 0),
(141, 'Thanks for reading.', '', 'woocommerce', 0),
(142, 'Failed order', '', 'woocommerce', 0),
(143, 'Failed order emails are sent to chosen recipient(s) when orders have been marked failed (if they were previously pending or on-hold).', '', 'woocommerce', 0),
(144, '[{site_title}]: Order #{order_number} has failed', '', 'woocommerce', 0),
(145, 'Order Failed: #{order_number}', '', 'woocommerce', 0),
(146, 'Hopefully they’ll be back. Read more about <a href=\"https://docs.woocommerce.com/document/managing-orders/\">troubleshooting failed payments</a>.', '', 'woocommerce', 0),
(147, 'Order on-hold', '', 'woocommerce', 0),
(148, 'This is an order notification sent to customers containing order details after an order is placed on-hold.', '', 'woocommerce', 0),
(149, 'Your {site_title} order has been received!', '', 'woocommerce', 0),
(150, 'Thank you for your order', '', 'woocommerce', 0),
(151, 'We look forward to fulfilling your order soon.', '', 'woocommerce', 0),
(152, 'Processing order', '', 'woocommerce', 0),
(153, 'This is an order notification sent to customers containing order details after payment.', '', 'woocommerce', 0),
(154, 'Thanks for using {site_url}!', '', 'woocommerce', 0),
(155, 'Completed order', '', 'woocommerce', 0),
(156, 'Order complete emails are sent to customers when their orders are marked completed and usually indicate that their orders have been shipped.', '', 'woocommerce', 0),
(157, 'Your {site_title} order is now complete', '', 'woocommerce', 0),
(158, 'Thanks for shopping with us', '', 'woocommerce', 0),
(159, 'Thanks for shopping with us.', '', 'woocommerce', 0),
(160, 'Refunded order', '', 'woocommerce', 0),
(161, 'Order refunded emails are sent to customers when their orders are refunded.', '', 'woocommerce', 0),
(162, 'Full refund subject', '', 'woocommerce', 0),
(163, 'Your {site_title} order #{order_number} has been refunded', '', 'woocommerce', 0),
(164, 'Partial refund subject', '', 'woocommerce', 0),
(165, 'Your {site_title} order #{order_number} has been partially refunded', '', 'woocommerce', 0),
(166, 'Full refund email heading', '', 'woocommerce', 0),
(167, 'Order Refunded: {order_number}', '', 'woocommerce', 0),
(168, 'Partial refund email heading', '', 'woocommerce', 0),
(169, 'Partial Refund: Order {order_number}', '', 'woocommerce', 0),
(170, 'We hope to see you again soon.', '', 'woocommerce', 0),
(171, 'Customer invoice / Order details', '', 'woocommerce', 0),
(172, 'Customer invoice emails can be sent to customers containing their order information and payment links.', '', 'woocommerce', 0),
(173, 'Your latest {site_title} invoice', '', 'woocommerce', 0),
(174, 'Your invoice for order #{order_number}', '', 'woocommerce', 0),
(175, 'Subject (paid)', '', 'woocommerce', 0),
(176, 'Invoice for order #{order_number} on {site_title}', '', 'woocommerce', 0),
(177, 'Email heading (paid)', '', 'woocommerce', 0),
(178, 'Invoice for order #{order_number}', '', 'woocommerce', 0),
(179, 'Customer note', '', 'woocommerce', 0),
(180, 'Customer note emails are sent when you add a note to an order.', '', 'woocommerce', 0),
(181, 'Note added to your {site_title} order from {order_date}', '', 'woocommerce', 0),
(182, 'A note has been added to your order', '', 'woocommerce', 0),
(183, 'Reset password', '', 'woocommerce', 0),
(184, 'Customer \"reset password\" emails are sent when customers reset their passwords.', '', 'woocommerce', 0),
(185, 'Password Reset Request for {site_title}', '', 'woocommerce', 0),
(186, 'Password Reset Request', '', 'woocommerce', 0),
(187, 'New account', '', 'woocommerce', 0),
(188, 'Customer \"new account\" emails are sent to the customer when a customer signs up via checkout or account pages.', '', 'woocommerce', 0),
(189, 'Your {site_title} account has been created!', '', 'woocommerce', 0),
(190, 'Welcome to {site_title}', '', 'woocommerce', 0),
(191, 'We look forward to seeing you soon.', '', 'woocommerce', 0),
(192, 'Hi %s,', '', 'woocommerce', 0),
(193, 'Thanks for creating an account on %1$s. Your username is %2$s. You can access your account area to view orders, change your password, and more at: %3$s', '', 'woocommerce', 0),
(194, 'Your password has been automatically generated: %s', '', 'woocommerce', 0),
(195, 'Your account was created successfully and a password has been sent to your email address.', '', 'woocommerce', 0),
(196, 'Dashboard', '', 'woocommerce', 0),
(197, 'Orders', '', 'woocommerce', 0),
(198, 'Downloads', '', 'woocommerce', 0),
(199, 'Addresses', '', 'woocommerce', 0),
(200, 'Payment methods', '', 'woocommerce', 0),
(201, 'Account details', '', 'woocommerce', 0),
(202, 'Logout', '', 'woocommerce', 0),
(203, 'Direct bank transfer', '', 'woocommerce', 0),
(204, 'Take payments in person via BACS. More commonly known as direct bank/wire transfer', '', 'woocommerce', 0),
(205, 'Enable bank transfer', '', 'woocommerce', 0),
(206, 'Title', '', 'woocommerce', 0),
(207, 'This controls the title which the user sees during checkout.', '', 'woocommerce', 0),
(208, 'Description', '', 'woocommerce', 0),
(209, 'Payment method description that the customer will see on your checkout.', '', 'woocommerce', 0),
(210, 'Make your payment directly into our bank account. Please use your Order ID as the payment reference. Your order will not be shipped until the funds have cleared in our account.', '', 'woocommerce', 0),
(211, 'Instructions', '', 'woocommerce', 0),
(212, 'Instructions that will be added to the thank you page and emails.', '', 'woocommerce', 0),
(213, 'Check payments', '', 'woocommerce', 0),
(214, 'Take payments in person via checks. This offline gateway can also be useful to test purchases.', '', 'woocommerce', 0),
(215, 'Enable check payments', '', 'woocommerce', 0),
(216, 'Please send a check to Store Name, Store Street, Store Town, Store State / County, Store Postcode.', '', 'woocommerce', 0),
(217, 'Cash on delivery', '', 'woocommerce', 0),
(218, 'Have your customers pay with cash (or by other means) upon delivery.', '', 'woocommerce', 0),
(219, 'Enable cash on delivery', '', 'woocommerce', 0),
(220, 'Payment method description that the customer will see on your website.', '', 'woocommerce', 0),
(221, 'Pay with cash upon delivery.', '', 'woocommerce', 0),
(222, 'Instructions that will be added to the thank you page.', '', 'woocommerce', 0),
(223, 'Enable for shipping methods', '', 'woocommerce', 0),
(224, 'If COD is only available for certain methods, set it up here. Leave blank to enable for all methods.', '', 'woocommerce', 0),
(225, 'Select shipping methods', '', 'woocommerce', 0),
(226, 'Accept for virtual orders', '', 'woocommerce', 0),
(227, 'Accept COD if the order is virtual', '', 'woocommerce', 0),
(228, 'Proceed to PayPal', '', 'woocommerce', 0),
(229, 'PayPal', '', 'woocommerce', 0),
(230, 'PayPal Standard redirects customers to PayPal to enter their payment information.', '', 'woocommerce', 0),
(231, 'Enable PayPal Standard', '', 'woocommerce', 0),
(232, 'This controls the description which the user sees during checkout.', '', 'woocommerce', 0),
(233, 'Pay via PayPal; you can pay with your credit card if you don\'t have a PayPal account.', '', 'woocommerce', 0),
(234, 'PayPal email', '', 'woocommerce', 0),
(235, 'Please enter your PayPal email address; this is needed in order to take payment.', '', 'woocommerce', 0),
(236, 'Advanced options', '', 'woocommerce', 0),
(237, 'PayPal sandbox', '', 'woocommerce', 0),
(238, 'Enable PayPal sandbox', '', 'woocommerce', 0),
(239, 'PayPal sandbox can be used to test payments. Sign up for a <a href=\"%s\">developer account</a>.', '', 'woocommerce', 0),
(240, 'Debug log', '', 'woocommerce', 0),
(241, 'Enable logging', '', 'woocommerce', 0),
(242, 'Log PayPal events, such as IPN requests, inside %s Note: this may log personal information. We recommend using this for debugging purposes only and deleting the logs when finished.', '', 'woocommerce', 0),
(243, 'IPN Email Notifications', '', 'woocommerce', 0),
(244, 'Enable IPN email notifications', '', 'woocommerce', 0),
(245, 'Send notifications when an IPN is received from PayPal indicating refunds, chargebacks and cancellations.', '', 'woocommerce', 0),
(246, 'Receiver email', '', 'woocommerce', 0),
(247, 'If your main PayPal email differs from the PayPal email entered above, input your main receiver email for your PayPal account here. This is used to validate IPN requests.', '', 'woocommerce', 0),
(248, 'PayPal identity token', '', 'woocommerce', 0),
(249, 'Optionally enable \"Payment Data Transfer\" (Profile > Profile and Settings > My Selling Tools > Website Preferences) and then copy your identity token here. This will allow payments to be verified without the need for PayPal IPN.', '', 'woocommerce', 0),
(250, 'Invoice prefix', '', 'woocommerce', 0),
(251, 'Please enter a prefix for your invoice numbers. If you use your PayPal account for multiple stores ensure this prefix is unique as PayPal will not allow orders with the same invoice number.', '', 'woocommerce', 0),
(252, 'Shipping details', '', 'woocommerce', 0),
(253, 'Send shipping details to PayPal instead of billing.', '', 'woocommerce', 0),
(254, 'PayPal allows us to send one address. If you are using PayPal for shipping labels you may prefer to send the shipping address rather than billing. Turning this option off may prevent PayPal Seller protection from applying.', '', 'woocommerce', 0),
(255, 'Address override', '', 'woocommerce', 0),
(256, 'Enable \"address_override\" to prevent address information from being changed.', '', 'woocommerce', 0),
(257, 'PayPal verifies addresses therefore this setting can cause errors (we recommend keeping it disabled).', '', 'woocommerce', 0),
(258, 'Payment action', '', 'woocommerce', 0),
(259, 'Choose whether you wish to capture funds immediately or authorize payment only.', '', 'woocommerce', 0),
(260, 'Capture', '', 'woocommerce', 0),
(261, 'Authorize', '', 'woocommerce', 0),
(262, 'Page style', '', 'woocommerce', 0),
(263, 'Optionally enter the name of the page style you wish to use. These are defined within your PayPal account. This affects classic PayPal checkout screens.', '', 'woocommerce', 0),
(264, 'Optional', '', 'woocommerce', 0),
(265, 'Image url', '', 'woocommerce', 0),
(266, 'Optionally enter the URL to a 150x50px image displayed as your logo in the upper left corner of the PayPal checkout pages.', '', 'woocommerce', 0),
(267, 'API credentials', '', 'woocommerce', 0),
(268, 'Enter your PayPal API credentials to process refunds via PayPal. Learn how to access your <a href=\"%s\">PayPal API Credentials</a>.', '', 'woocommerce', 0),
(269, 'Live API username', '', 'woocommerce', 0),
(270, 'Get your API credentials from PayPal.', '', 'woocommerce', 0),
(271, 'Live API password', '', 'woocommerce', 0),
(272, 'Live API signature', '', 'woocommerce', 0),
(273, 'Sandbox API username', '', 'woocommerce', 0),
(274, 'Sandbox API password', '', 'woocommerce', 0),
(275, 'Sandbox API signature', '', 'woocommerce', 0),
(276, 'billing', '', 'woocommerce', 0),
(277, 'shipping', '', 'woocommerce', 0),
(278, 'Hello %1$s (not %1$s? <a href=\"%2$s\">Log out</a>)', '', 'woocommerce', 0),
(279, 'From your account dashboard you can view your <a href=\"%1$s\">recent orders</a>, manage your <a href=\"%2$s\">shipping and billing addresses</a>, and <a href=\"%3$s\">edit your password and account details</a>.', '', 'woocommerce', 0),
(280, 'Pending payment', '', 'woocommerce', 0),
(281, 'Processing', '', 'woocommerce', 0),
(282, 'On hold', '', 'woocommerce', 0),
(283, 'Completed', '', 'woocommerce', 0),
(284, 'Cancelled', '', 'woocommerce', 0),
(285, 'Refunded', '', 'woocommerce', 0),
(286, 'Failed', '', 'woocommerce', 0),
(287, 'Draft', '', 'woocommerce', 0),
(288, 'Browse products', '', 'woocommerce', 0),
(289, 'No order has been made yet.', '', 'woocommerce', 0),
(290, 'No downloads available yet.', '', 'woocommerce', 0),
(291, 'Apartment, suite, unit, etc. (optional)', '', 'woocommerce', 0),
(292, 'First name', '', 'woocommerce', 0),
(293, 'Last name', '', 'woocommerce', 0),
(294, 'Company name', '', 'woocommerce', 0),
(295, 'Country / Region', '', 'woocommerce', 0),
(296, 'Street address', '', 'woocommerce', 0),
(297, 'House number and street name', '', 'woocommerce', 0),
(298, 'Town / City', '', 'woocommerce', 0),
(299, 'State / County', '', 'woocommerce', 0),
(300, 'Postcode / ZIP', '', 'woocommerce', 0),
(301, 'Province', '', 'woocommerce', 0),
(302, 'Suburb', '', 'woocommerce', 0),
(303, 'Postcode', '', 'woocommerce', 0),
(304, 'State', '', 'woocommerce', 0),
(305, 'District', '', 'woocommerce', 0),
(306, 'Postal code', '', 'woocommerce', 0),
(307, 'Canton', '', 'woocommerce', 0),
(308, 'Region', '', 'woocommerce', 0),
(309, 'Town / District', '', 'woocommerce', 0),
(310, 'County', '', 'woocommerce', 0),
(311, 'Eircode', '', 'woocommerce', 0),
(312, 'Prefecture', '', 'woocommerce', 0),
(313, 'Municipality', '', 'woocommerce', 0),
(314, 'State / Zone', '', 'woocommerce', 0),
(315, 'Municipality / District', '', 'woocommerce', 0),
(316, 'Town / Village', '', 'woocommerce', 0),
(317, 'ZIP', '', 'woocommerce', 0),
(318, 'Shipping address', '', 'woocommerce', 0),
(319, 'Billing address', '', 'woocommerce', 0),
(320, 'The following addresses will be used on the checkout page by default.', '', 'woocommerce', 0),
(321, 'Add', '', 'woocommerce', 0),
(322, 'You have not set up this type of address yet.', '', 'woocommerce', 0),
(323, 'required', '', 'woocommerce', 0),
(324, 'optional', '', 'woocommerce', 0),
(325, 'Bengo', '', 'woocommerce', 0),
(326, 'Benguela', '', 'woocommerce', 0),
(327, 'Bié', '', 'woocommerce', 0),
(328, 'Cabinda', '', 'woocommerce', 0),
(329, 'Cunene', '', 'woocommerce', 0),
(330, 'Huambo', '', 'woocommerce', 0),
(331, 'Huíla', '', 'woocommerce', 0),
(332, 'Kuando Kubango', '', 'woocommerce', 0),
(333, 'Kwanza-Norte', '', 'woocommerce', 0),
(334, 'Kwanza-Sul', '', 'woocommerce', 0),
(335, 'Luanda', '', 'woocommerce', 0),
(336, 'Lunda-Norte', '', 'woocommerce', 0),
(337, 'Lunda-Sul', '', 'woocommerce', 0),
(338, 'Malanje', '', 'woocommerce', 0),
(339, 'Moxico', '', 'woocommerce', 0),
(340, 'Namibe', '', 'woocommerce', 0),
(341, 'Uíge', '', 'woocommerce', 0),
(342, 'Zaire', '', 'woocommerce', 0),
(343, 'Ciudad Aut&oacute;noma de Buenos Aires', '', 'woocommerce', 0),
(344, 'Buenos Aires', '', 'woocommerce', 0),
(345, 'Catamarca', '', 'woocommerce', 0),
(346, 'Chaco', '', 'woocommerce', 0),
(347, 'Chubut', '', 'woocommerce', 0),
(348, 'C&oacute;rdoba', '', 'woocommerce', 0),
(349, 'Corrientes', '', 'woocommerce', 0),
(350, 'Entre R&iacute;os', '', 'woocommerce', 0),
(351, 'Formosa', '', 'woocommerce', 0),
(352, 'Jujuy', '', 'woocommerce', 0),
(353, 'La Pampa', '', 'woocommerce', 0),
(354, 'La Rioja', '', 'woocommerce', 0),
(355, 'Mendoza', '', 'woocommerce', 0),
(356, 'Misiones', '', 'woocommerce', 0),
(357, 'Neuqu&eacute;n', '', 'woocommerce', 0),
(358, 'R&iacute;o Negro', '', 'woocommerce', 0),
(359, 'Salta', '', 'woocommerce', 0),
(360, 'San Juan', '', 'woocommerce', 0),
(361, 'San Luis', '', 'woocommerce', 0),
(362, 'Santa Cruz', '', 'woocommerce', 0),
(363, 'Santa Fe', '', 'woocommerce', 0),
(364, 'Santiago del Estero', '', 'woocommerce', 0),
(365, 'Tierra del Fuego', '', 'woocommerce', 0),
(366, 'Tucum&aacute;n', '', 'woocommerce', 0),
(367, 'Australian Capital Territory', '', 'woocommerce', 0),
(368, 'New South Wales', '', 'woocommerce', 0),
(369, 'Northern Territory', '', 'woocommerce', 0),
(370, 'Queensland', '', 'woocommerce', 0),
(371, 'South Australia', '', 'woocommerce', 0),
(372, 'Tasmania', '', 'woocommerce', 0),
(373, 'Victoria', '', 'woocommerce', 0),
(374, 'Western Australia', '', 'woocommerce', 0),
(375, 'Bagerhat', '', 'woocommerce', 0),
(376, 'Bandarban', '', 'woocommerce', 0),
(377, 'Barguna', '', 'woocommerce', 0),
(378, 'Barishal', '', 'woocommerce', 0),
(379, 'Bhola', '', 'woocommerce', 0),
(380, 'Bogura', '', 'woocommerce', 0),
(381, 'Brahmanbaria', '', 'woocommerce', 0),
(382, 'Chandpur', '', 'woocommerce', 0),
(383, 'Chattogram', '', 'woocommerce', 0),
(384, 'Chuadanga', '', 'woocommerce', 0),
(385, 'Cox\'s Bazar', '', 'woocommerce', 0),
(386, 'Cumilla', '', 'woocommerce', 0),
(387, 'Dhaka', '', 'woocommerce', 0),
(388, 'Dinajpur', '', 'woocommerce', 0),
(389, 'Faridpur ', '', 'woocommerce', 0),
(390, 'Feni', '', 'woocommerce', 0),
(391, 'Gaibandha', '', 'woocommerce', 0),
(392, 'Gazipur', '', 'woocommerce', 0),
(393, 'Gopalganj', '', 'woocommerce', 0),
(394, 'Habiganj', '', 'woocommerce', 0),
(395, 'Jamalpur', '', 'woocommerce', 0),
(396, 'Jashore', '', 'woocommerce', 0),
(397, 'Jhalokati', '', 'woocommerce', 0),
(398, 'Jhenaidah', '', 'woocommerce', 0),
(399, 'Joypurhat', '', 'woocommerce', 0),
(400, 'Khagrachhari', '', 'woocommerce', 0),
(401, 'Khulna', '', 'woocommerce', 0),
(402, 'Kishoreganj', '', 'woocommerce', 0),
(403, 'Kurigram', '', 'woocommerce', 0),
(404, 'Kushtia', '', 'woocommerce', 0),
(405, 'Lakshmipur', '', 'woocommerce', 0),
(406, 'Lalmonirhat', '', 'woocommerce', 0),
(407, 'Madaripur', '', 'woocommerce', 0),
(408, 'Magura', '', 'woocommerce', 0),
(409, 'Manikganj ', '', 'woocommerce', 0),
(410, 'Meherpur', '', 'woocommerce', 0),
(411, 'Moulvibazar', '', 'woocommerce', 0),
(412, 'Munshiganj', '', 'woocommerce', 0),
(413, 'Mymensingh', '', 'woocommerce', 0),
(414, 'Naogaon', '', 'woocommerce', 0),
(415, 'Narail', '', 'woocommerce', 0),
(416, 'Narayanganj', '', 'woocommerce', 0),
(417, 'Narsingdi', '', 'woocommerce', 0),
(418, 'Natore', '', 'woocommerce', 0),
(419, 'Nawabganj', '', 'woocommerce', 0),
(420, 'Netrakona', '', 'woocommerce', 0),
(421, 'Nilphamari', '', 'woocommerce', 0),
(422, 'Noakhali', '', 'woocommerce', 0),
(423, 'Pabna', '', 'woocommerce', 0),
(424, 'Panchagarh', '', 'woocommerce', 0),
(425, 'Patuakhali', '', 'woocommerce', 0),
(426, 'Pirojpur', '', 'woocommerce', 0),
(427, 'Rajbari', '', 'woocommerce', 0),
(428, 'Rajshahi', '', 'woocommerce', 0),
(429, 'Rangamati', '', 'woocommerce', 0),
(430, 'Rangpur', '', 'woocommerce', 0),
(431, 'Satkhira', '', 'woocommerce', 0),
(432, 'Shariatpur', '', 'woocommerce', 0),
(433, 'Sherpur', '', 'woocommerce', 0),
(434, 'Sirajganj', '', 'woocommerce', 0),
(435, 'Sunamganj', '', 'woocommerce', 0),
(436, 'Sylhet', '', 'woocommerce', 0),
(437, 'Tangail', '', 'woocommerce', 0),
(438, 'Thakurgaon', '', 'woocommerce', 0),
(439, 'Blagoevgrad', '', 'woocommerce', 0),
(440, 'Burgas', '', 'woocommerce', 0),
(441, 'Dobrich', '', 'woocommerce', 0),
(442, 'Gabrovo', '', 'woocommerce', 0),
(443, 'Haskovo', '', 'woocommerce', 0),
(444, 'Kardzhali', '', 'woocommerce', 0),
(445, 'Kyustendil', '', 'woocommerce', 0),
(446, 'Lovech', '', 'woocommerce', 0),
(447, 'Montana', '', 'woocommerce', 0),
(448, 'Pazardzhik', '', 'woocommerce', 0),
(449, 'Pernik', '', 'woocommerce', 0),
(450, 'Pleven', '', 'woocommerce', 0),
(451, 'Plovdiv', '', 'woocommerce', 0),
(452, 'Razgrad', '', 'woocommerce', 0),
(453, 'Ruse', '', 'woocommerce', 0),
(454, 'Shumen', '', 'woocommerce', 0),
(455, 'Silistra', '', 'woocommerce', 0),
(456, 'Sliven', '', 'woocommerce', 0),
(457, 'Smolyan', '', 'woocommerce', 0),
(458, 'Sofia', '', 'woocommerce', 0),
(459, 'Sofia-Grad', '', 'woocommerce', 0),
(460, 'Stara Zagora', '', 'woocommerce', 0),
(461, 'Targovishte', '', 'woocommerce', 0),
(462, 'Varna', '', 'woocommerce', 0),
(463, 'Veliko Tarnovo', '', 'woocommerce', 0),
(464, 'Vidin', '', 'woocommerce', 0),
(465, 'Vratsa', '', 'woocommerce', 0),
(466, 'Yambol', '', 'woocommerce', 0),
(467, 'Chuquisaca', '', 'woocommerce', 0),
(468, 'Beni', '', 'woocommerce', 0),
(469, 'Cochabamba', '', 'woocommerce', 0),
(470, 'La Paz', '', 'woocommerce', 0),
(471, 'Oruro', '', 'woocommerce', 0),
(472, 'Pando', '', 'woocommerce', 0),
(473, 'Potosí', '', 'woocommerce', 0),
(474, 'Tarija', '', 'woocommerce', 0),
(475, 'Acre', '', 'woocommerce', 0),
(476, 'Alagoas', '', 'woocommerce', 0),
(477, 'Amap&aacute;', '', 'woocommerce', 0),
(478, 'Amazonas', '', 'woocommerce', 0),
(479, 'Bahia', '', 'woocommerce', 0),
(480, 'Cear&aacute;', '', 'woocommerce', 0),
(481, 'Distrito Federal', '', 'woocommerce', 0),
(482, 'Esp&iacute;rito Santo', '', 'woocommerce', 0),
(483, 'Goi&aacute;s', '', 'woocommerce', 0),
(484, 'Maranh&atilde;o', '', 'woocommerce', 0),
(485, 'Mato Grosso', '', 'woocommerce', 0),
(486, 'Mato Grosso do Sul', '', 'woocommerce', 0),
(487, 'Minas Gerais', '', 'woocommerce', 0),
(488, 'Par&aacute;', '', 'woocommerce', 0),
(489, 'Para&iacute;ba', '', 'woocommerce', 0),
(490, 'Paran&aacute;', '', 'woocommerce', 0),
(491, 'Pernambuco', '', 'woocommerce', 0),
(492, 'Piau&iacute;', '', 'woocommerce', 0),
(493, 'Rio de Janeiro', '', 'woocommerce', 0),
(494, 'Rio Grande do Norte', '', 'woocommerce', 0),
(495, 'Rio Grande do Sul', '', 'woocommerce', 0),
(496, 'Rond&ocirc;nia', '', 'woocommerce', 0),
(497, 'Roraima', '', 'woocommerce', 0),
(498, 'Santa Catarina', '', 'woocommerce', 0),
(499, 'S&atilde;o Paulo', '', 'woocommerce', 0),
(500, 'Sergipe', '', 'woocommerce', 0),
(501, 'Tocantins', '', 'woocommerce', 0),
(502, 'Alberta', '', 'woocommerce', 0),
(503, 'British Columbia', '', 'woocommerce', 0),
(504, 'Manitoba', '', 'woocommerce', 0),
(505, 'New Brunswick', '', 'woocommerce', 0),
(506, 'Newfoundland and Labrador', '', 'woocommerce', 0),
(507, 'Northwest Territories', '', 'woocommerce', 0),
(508, 'Nova Scotia', '', 'woocommerce', 0),
(509, 'Nunavut', '', 'woocommerce', 0),
(510, 'Ontario', '', 'woocommerce', 0),
(511, 'Prince Edward Island', '', 'woocommerce', 0),
(512, 'Quebec', '', 'woocommerce', 0),
(513, 'Saskatchewan', '', 'woocommerce', 0),
(514, 'Yukon Territory', '', 'woocommerce', 0),
(515, 'Aargau', '', 'woocommerce', 0),
(516, 'Appenzell Ausserrhoden', '', 'woocommerce', 0),
(517, 'Appenzell Innerrhoden', '', 'woocommerce', 0),
(518, 'Basel-Landschaft', '', 'woocommerce', 0),
(519, 'Basel-Stadt', '', 'woocommerce', 0),
(520, 'Bern', '', 'woocommerce', 0),
(521, 'Fribourg', '', 'woocommerce', 0),
(522, 'Geneva', '', 'woocommerce', 0),
(523, 'Glarus', '', 'woocommerce', 0),
(524, 'Graub&uuml;nden', '', 'woocommerce', 0),
(525, 'Jura', '', 'woocommerce', 0),
(526, 'Luzern', '', 'woocommerce', 0),
(527, 'Neuch&acirc;tel', '', 'woocommerce', 0),
(528, 'Nidwalden', '', 'woocommerce', 0),
(529, 'Obwalden', '', 'woocommerce', 0),
(530, 'Schaffhausen', '', 'woocommerce', 0),
(531, 'Schwyz', '', 'woocommerce', 0),
(532, 'Solothurn', '', 'woocommerce', 0),
(533, 'St. Gallen', '', 'woocommerce', 0),
(534, 'Thurgau', '', 'woocommerce', 0),
(535, 'Ticino', '', 'woocommerce', 0),
(536, 'Uri', '', 'woocommerce', 0),
(537, 'Valais', '', 'woocommerce', 0),
(538, 'Vaud', '', 'woocommerce', 0),
(539, 'Zug', '', 'woocommerce', 0),
(540, 'Z&uuml;rich', '', 'woocommerce', 0),
(541, 'Yunnan / &#20113;&#21335;', '', 'woocommerce', 0),
(542, 'Beijing / &#21271;&#20140;', '', 'woocommerce', 0),
(543, 'Tianjin / &#22825;&#27941;', '', 'woocommerce', 0),
(544, 'Hebei / &#27827;&#21271;', '', 'woocommerce', 0),
(545, 'Shanxi / &#23665;&#35199;', '', 'woocommerce', 0),
(546, 'Inner Mongolia / &#20839;&#33945;&#21476;', '', 'woocommerce', 0),
(547, 'Liaoning / &#36797;&#23425;', '', 'woocommerce', 0),
(548, 'Jilin / &#21513;&#26519;', '', 'woocommerce', 0),
(549, 'Heilongjiang / &#40657;&#40857;&#27743;', '', 'woocommerce', 0),
(550, 'Shanghai / &#19978;&#28023;', '', 'woocommerce', 0),
(551, 'Jiangsu / &#27743;&#33487;', '', 'woocommerce', 0),
(552, 'Zhejiang / &#27993;&#27743;', '', 'woocommerce', 0),
(553, 'Anhui / &#23433;&#24509;', '', 'woocommerce', 0),
(554, 'Fujian / &#31119;&#24314;', '', 'woocommerce', 0),
(555, 'Jiangxi / &#27743;&#35199;', '', 'woocommerce', 0),
(556, 'Shandong / &#23665;&#19996;', '', 'woocommerce', 0),
(557, 'Henan / &#27827;&#21335;', '', 'woocommerce', 0),
(558, 'Hubei / &#28246;&#21271;', '', 'woocommerce', 0),
(559, 'Hunan / &#28246;&#21335;', '', 'woocommerce', 0),
(560, 'Guangdong / &#24191;&#19996;', '', 'woocommerce', 0),
(561, 'Guangxi Zhuang / &#24191;&#35199;&#22766;&#26063;', '', 'woocommerce', 0),
(562, 'Hainan / &#28023;&#21335;', '', 'woocommerce', 0),
(563, 'Chongqing / &#37325;&#24198;', '', 'woocommerce', 0),
(564, 'Sichuan / &#22235;&#24029;', '', 'woocommerce', 0),
(565, 'Guizhou / &#36149;&#24030;', '', 'woocommerce', 0),
(566, 'Shaanxi / &#38485;&#35199;', '', 'woocommerce', 0),
(567, 'Gansu / &#29976;&#32899;', '', 'woocommerce', 0),
(568, 'Qinghai / &#38738;&#28023;', '', 'woocommerce', 0),
(569, 'Ningxia Hui / &#23425;&#22799;', '', 'woocommerce', 0),
(570, 'Macao / &#28595;&#38376;', '', 'woocommerce', 0),
(571, 'Tibet / &#35199;&#34255;', '', 'woocommerce', 0),
(572, 'Xinjiang / &#26032;&#30086;', '', 'woocommerce', 0),
(573, 'A Coru&ntilde;a', '', 'woocommerce', 0),
(574, 'Araba/&Aacute;lava', '', 'woocommerce', 0),
(575, 'Albacete', '', 'woocommerce', 0),
(576, 'Alicante', '', 'woocommerce', 0),
(577, 'Almer&iacute;a', '', 'woocommerce', 0),
(578, 'Asturias', '', 'woocommerce', 0),
(579, '&Aacute;vila', '', 'woocommerce', 0),
(580, 'Badajoz', '', 'woocommerce', 0),
(581, 'Baleares', '', 'woocommerce', 0),
(582, 'Barcelona', '', 'woocommerce', 0),
(583, 'Burgos', '', 'woocommerce', 0),
(584, 'C&aacute;ceres', '', 'woocommerce', 0),
(585, 'C&aacute;diz', '', 'woocommerce', 0),
(586, 'Cantabria', '', 'woocommerce', 0),
(587, 'Castell&oacute;n', '', 'woocommerce', 0),
(588, 'Ceuta', '', 'woocommerce', 0),
(589, 'Ciudad Real', '', 'woocommerce', 0),
(590, 'Cuenca', '', 'woocommerce', 0),
(591, 'Girona', '', 'woocommerce', 0),
(592, 'Granada', '', 'woocommerce', 0),
(593, 'Guadalajara', '', 'woocommerce', 0),
(594, 'Gipuzkoa', '', 'woocommerce', 0),
(595, 'Huelva', '', 'woocommerce', 0),
(596, 'Huesca', '', 'woocommerce', 0),
(597, 'Ja&eacute;n', '', 'woocommerce', 0),
(598, 'Las Palmas', '', 'woocommerce', 0),
(599, 'Le&oacute;n', '', 'woocommerce', 0),
(600, 'Lleida', '', 'woocommerce', 0),
(601, 'Lugo', '', 'woocommerce', 0),
(602, 'Madrid', '', 'woocommerce', 0),
(603, 'M&aacute;laga', '', 'woocommerce', 0),
(604, 'Melilla', '', 'woocommerce', 0),
(605, 'Murcia', '', 'woocommerce', 0),
(606, 'Navarra', '', 'woocommerce', 0),
(607, 'Ourense', '', 'woocommerce', 0),
(608, 'Palencia', '', 'woocommerce', 0),
(609, 'Pontevedra', '', 'woocommerce', 0),
(610, 'Salamanca', '', 'woocommerce', 0),
(611, 'Santa Cruz de Tenerife', '', 'woocommerce', 0),
(612, 'Segovia', '', 'woocommerce', 0),
(613, 'Sevilla', '', 'woocommerce', 0),
(614, 'Soria', '', 'woocommerce', 0),
(615, 'Tarragona', '', 'woocommerce', 0),
(616, 'Teruel', '', 'woocommerce', 0),
(617, 'Toledo', '', 'woocommerce', 0),
(618, 'Valencia', '', 'woocommerce', 0),
(619, 'Valladolid', '', 'woocommerce', 0),
(620, 'Bizkaia', '', 'woocommerce', 0),
(621, 'Zamora', '', 'woocommerce', 0),
(622, 'Zaragoza', '', 'woocommerce', 0),
(623, 'Ahafo', '', 'woocommerce', 0),
(624, 'Ashanti', '', 'woocommerce', 0),
(625, 'Brong-Ahafo', '', 'woocommerce', 0),
(626, 'Bono', '', 'woocommerce', 0),
(627, 'Bono East', '', 'woocommerce', 0),
(628, 'Central', '', 'woocommerce', 0),
(629, 'Eastern', '', 'woocommerce', 0),
(630, 'Greater Accra', '', 'woocommerce', 0),
(631, 'North East', '', 'woocommerce', 0),
(632, 'Northern', '', 'woocommerce', 0),
(633, 'Oti', '', 'woocommerce', 0),
(634, 'Savannah', '', 'woocommerce', 0),
(635, 'Upper East', '', 'woocommerce', 0),
(636, 'Upper West', '', 'woocommerce', 0),
(637, 'Volta', '', 'woocommerce', 0),
(638, 'Western', '', 'woocommerce', 0),
(639, 'Western North', '', 'woocommerce', 0),
(640, 'Αττική', '', 'woocommerce', 0),
(641, 'Ανατολική Μακεδονία και Θράκη', '', 'woocommerce', 0),
(642, 'Κεντρική Μακεδονία', '', 'woocommerce', 0),
(643, 'Δυτική Μακεδονία', '', 'woocommerce', 0),
(644, 'Ήπειρος', '', 'woocommerce', 0),
(645, 'Θεσσαλία', '', 'woocommerce', 0),
(646, 'Ιόνιοι Νήσοι', '', 'woocommerce', 0),
(647, 'Δυτική Ελλάδα', '', 'woocommerce', 0),
(648, 'Στερεά Ελλάδα', '', 'woocommerce', 0),
(649, 'Πελοπόννησος', '', 'woocommerce', 0),
(650, 'Βόρειο Αιγαίο', '', 'woocommerce', 0),
(651, 'Νότιο Αιγαίο', '', 'woocommerce', 0),
(652, 'Κρήτη', '', 'woocommerce', 0),
(653, 'Hong Kong Island', '', 'woocommerce', 0),
(654, 'Kowloon', '', 'woocommerce', 0),
(655, 'New Territories', '', 'woocommerce', 0),
(656, 'Bács-Kiskun', '', 'woocommerce', 0),
(657, 'Békés', '', 'woocommerce', 0),
(658, 'Baranya', '', 'woocommerce', 0),
(659, 'Borsod-Abaúj-Zemplén', '', 'woocommerce', 0),
(660, 'Budapest', '', 'woocommerce', 0),
(661, 'Csongrád', '', 'woocommerce', 0),
(662, 'Fejér', '', 'woocommerce', 0),
(663, 'Győr-Moson-Sopron', '', 'woocommerce', 0),
(664, 'Hajdú-Bihar', '', 'woocommerce', 0),
(665, 'Heves', '', 'woocommerce', 0),
(666, 'Jász-Nagykun-Szolnok', '', 'woocommerce', 0),
(667, 'Komárom-Esztergom', '', 'woocommerce', 0),
(668, 'Nógrád', '', 'woocommerce', 0),
(669, 'Pest', '', 'woocommerce', 0),
(670, 'Somogy', '', 'woocommerce', 0),
(671, 'Szabolcs-Szatmár-Bereg', '', 'woocommerce', 0),
(672, 'Tolna', '', 'woocommerce', 0),
(673, 'Vas', '', 'woocommerce', 0),
(674, 'Veszprém', '', 'woocommerce', 0),
(675, 'Zala', '', 'woocommerce', 0),
(676, 'Daerah Istimewa Aceh', '', 'woocommerce', 0),
(677, 'Sumatera Utara', '', 'woocommerce', 0),
(678, 'Sumatera Barat', '', 'woocommerce', 0),
(679, 'Riau', '', 'woocommerce', 0),
(680, 'Kepulauan Riau', '', 'woocommerce', 0),
(681, 'Jambi', '', 'woocommerce', 0),
(682, 'Sumatera Selatan', '', 'woocommerce', 0),
(683, 'Bangka Belitung', '', 'woocommerce', 0),
(684, 'Bengkulu', '', 'woocommerce', 0),
(685, 'Lampung', '', 'woocommerce', 0),
(686, 'DKI Jakarta', '', 'woocommerce', 0),
(687, 'Jawa Barat', '', 'woocommerce', 0),
(688, 'Banten', '', 'woocommerce', 0),
(689, 'Jawa Tengah', '', 'woocommerce', 0),
(690, 'Jawa Timur', '', 'woocommerce', 0),
(691, 'Daerah Istimewa Yogyakarta', '', 'woocommerce', 0),
(692, 'Bali', '', 'woocommerce', 0),
(693, 'Nusa Tenggara Barat', '', 'woocommerce', 0),
(694, 'Nusa Tenggara Timur', '', 'woocommerce', 0),
(695, 'Kalimantan Barat', '', 'woocommerce', 0),
(696, 'Kalimantan Tengah', '', 'woocommerce', 0),
(697, 'Kalimantan Timur', '', 'woocommerce', 0),
(698, 'Kalimantan Selatan', '', 'woocommerce', 0),
(699, 'Kalimantan Utara', '', 'woocommerce', 0),
(700, 'Sulawesi Utara', '', 'woocommerce', 0),
(701, 'Sulawesi Tengah', '', 'woocommerce', 0),
(702, 'Sulawesi Tenggara', '', 'woocommerce', 0),
(703, 'Sulawesi Barat', '', 'woocommerce', 0),
(704, 'Sulawesi Selatan', '', 'woocommerce', 0),
(705, 'Gorontalo', '', 'woocommerce', 0),
(706, 'Maluku', '', 'woocommerce', 0),
(707, 'Maluku Utara', '', 'woocommerce', 0),
(708, 'Papua', '', 'woocommerce', 0),
(709, 'Papua Barat', '', 'woocommerce', 0),
(710, 'Carlow', '', 'woocommerce', 0),
(711, 'Cavan', '', 'woocommerce', 0),
(712, 'Clare', '', 'woocommerce', 0),
(713, 'Cork', '', 'woocommerce', 0),
(714, 'Donegal', '', 'woocommerce', 0),
(715, 'Dublin', '', 'woocommerce', 0),
(716, 'Galway', '', 'woocommerce', 0),
(717, 'Kerry', '', 'woocommerce', 0),
(718, 'Kildare', '', 'woocommerce', 0),
(719, 'Kilkenny', '', 'woocommerce', 0),
(720, 'Laois', '', 'woocommerce', 0),
(721, 'Leitrim', '', 'woocommerce', 0),
(722, 'Limerick', '', 'woocommerce', 0),
(723, 'Longford', '', 'woocommerce', 0),
(724, 'Louth', '', 'woocommerce', 0),
(725, 'Mayo', '', 'woocommerce', 0),
(726, 'Meath', '', 'woocommerce', 0),
(727, 'Monaghan', '', 'woocommerce', 0),
(728, 'Offaly', '', 'woocommerce', 0),
(729, 'Roscommon', '', 'woocommerce', 0),
(730, 'Sligo', '', 'woocommerce', 0),
(731, 'Tipperary', '', 'woocommerce', 0),
(732, 'Waterford', '', 'woocommerce', 0),
(733, 'Westmeath', '', 'woocommerce', 0),
(734, 'Wexford', '', 'woocommerce', 0),
(735, 'Wicklow', '', 'woocommerce', 0),
(736, 'Andhra Pradesh', '', 'woocommerce', 0),
(737, 'Arunachal Pradesh', '', 'woocommerce', 0),
(738, 'Assam', '', 'woocommerce', 0),
(739, 'Bihar', '', 'woocommerce', 0),
(740, 'Chhattisgarh', '', 'woocommerce', 0),
(741, 'Goa', '', 'woocommerce', 0),
(742, 'Gujarat', '', 'woocommerce', 0),
(743, 'Haryana', '', 'woocommerce', 0),
(744, 'Himachal Pradesh', '', 'woocommerce', 0),
(745, 'Jammu and Kashmir', '', 'woocommerce', 0),
(746, 'Jharkhand', '', 'woocommerce', 0),
(747, 'Karnataka', '', 'woocommerce', 0),
(748, 'Kerala', '', 'woocommerce', 0),
(749, 'Madhya Pradesh', '', 'woocommerce', 0),
(750, 'Maharashtra', '', 'woocommerce', 0),
(751, 'Manipur', '', 'woocommerce', 0),
(752, 'Meghalaya', '', 'woocommerce', 0),
(753, 'Mizoram', '', 'woocommerce', 0),
(754, 'Nagaland', '', 'woocommerce', 0),
(755, 'Orissa', '', 'woocommerce', 0),
(756, 'Punjab', '', 'woocommerce', 0),
(757, 'Rajasthan', '', 'woocommerce', 0),
(758, 'Sikkim', '', 'woocommerce', 0),
(759, 'Tamil Nadu', '', 'woocommerce', 0),
(760, 'Telangana', '', 'woocommerce', 0),
(761, 'Tripura', '', 'woocommerce', 0),
(762, 'Uttarakhand', '', 'woocommerce', 0),
(763, 'Uttar Pradesh', '', 'woocommerce', 0),
(764, 'West Bengal', '', 'woocommerce', 0),
(765, 'Andaman and Nicobar Islands', '', 'woocommerce', 0),
(766, 'Chandigarh', '', 'woocommerce', 0),
(767, 'Dadra and Nagar Haveli', '', 'woocommerce', 0),
(768, 'Daman and Diu', '', 'woocommerce', 0),
(769, 'Delhi', '', 'woocommerce', 0),
(770, 'Lakshadeep', '', 'woocommerce', 0),
(771, 'Pondicherry (Puducherry)', '', 'woocommerce', 0),
(772, 'Khuzestan  (خوزستان)', '', 'woocommerce', 0),
(773, 'Tehran  (تهران)', '', 'woocommerce', 0),
(774, 'Ilaam (ایلام)', '', 'woocommerce', 0),
(775, 'Bushehr (بوشهر)', '', 'woocommerce', 0),
(776, 'Ardabil (اردبیل)', '', 'woocommerce', 0),
(777, 'Isfahan (اصفهان)', '', 'woocommerce', 0),
(778, 'Yazd (یزد)', '', 'woocommerce', 0),
(779, 'Kermanshah (کرمانشاه)', '', 'woocommerce', 0),
(780, 'Kerman (کرمان)', '', 'woocommerce', 0),
(781, 'Hamadan (همدان)', '', 'woocommerce', 0),
(782, 'Ghazvin (قزوین)', '', 'woocommerce', 0),
(783, 'Zanjan (زنجان)', '', 'woocommerce', 0),
(784, 'Luristan (لرستان)', '', 'woocommerce', 0),
(785, 'Alborz (البرز)', '', 'woocommerce', 0),
(786, 'East Azarbaijan (آذربایجان شرقی)', '', 'woocommerce', 0),
(787, 'West Azarbaijan (آذربایجان غربی)', '', 'woocommerce', 0),
(788, 'Chaharmahal and Bakhtiari (چهارمحال و بختیاری)', '', 'woocommerce', 0),
(789, 'South Khorasan (خراسان جنوبی)', '', 'woocommerce', 0),
(790, 'Razavi Khorasan (خراسان رضوی)', '', 'woocommerce', 0),
(791, 'North Khorasan (خراسان شمالی)', '', 'woocommerce', 0),
(792, 'Semnan (سمنان)', '', 'woocommerce', 0),
(793, 'Fars (فارس)', '', 'woocommerce', 0),
(794, 'Qom (قم)', '', 'woocommerce', 0),
(795, 'Kurdistan / کردستان)', '', 'woocommerce', 0),
(796, 'Kohgiluyeh and BoyerAhmad (کهگیلوییه و بویراحمد)', '', 'woocommerce', 0),
(797, 'Golestan (گلستان)', '', 'woocommerce', 0),
(798, 'Gilan (گیلان)', '', 'woocommerce', 0),
(799, 'Mazandaran (مازندران)', '', 'woocommerce', 0),
(800, 'Markazi (مرکزی)', '', 'woocommerce', 0),
(801, 'Hormozgan (هرمزگان)', '', 'woocommerce', 0),
(802, 'Sistan and Baluchestan (سیستان و بلوچستان)', '', 'woocommerce', 0),
(803, 'Agrigento', '', 'woocommerce', 0),
(804, 'Alessandria', '', 'woocommerce', 0),
(805, 'Ancona', '', 'woocommerce', 0),
(806, 'Aosta', '', 'woocommerce', 0),
(807, 'Arezzo', '', 'woocommerce', 0),
(808, 'Ascoli Piceno', '', 'woocommerce', 0),
(809, 'Asti', '', 'woocommerce', 0),
(810, 'Avellino', '', 'woocommerce', 0),
(811, 'Bari', '', 'woocommerce', 0),
(812, 'Barletta-Andria-Trani', '', 'woocommerce', 0),
(813, 'Belluno', '', 'woocommerce', 0),
(814, 'Benevento', '', 'woocommerce', 0),
(815, 'Bergamo', '', 'woocommerce', 0),
(816, 'Biella', '', 'woocommerce', 0),
(817, 'Bologna', '', 'woocommerce', 0),
(818, 'Bolzano', '', 'woocommerce', 0),
(819, 'Brescia', '', 'woocommerce', 0),
(820, 'Brindisi', '', 'woocommerce', 0),
(821, 'Cagliari', '', 'woocommerce', 0),
(822, 'Caltanissetta', '', 'woocommerce', 0),
(823, 'Campobasso', '', 'woocommerce', 0),
(824, 'Caserta', '', 'woocommerce', 0),
(825, 'Catania', '', 'woocommerce', 0),
(826, 'Catanzaro', '', 'woocommerce', 0),
(827, 'Chieti', '', 'woocommerce', 0),
(828, 'Como', '', 'woocommerce', 0),
(829, 'Cosenza', '', 'woocommerce', 0),
(830, 'Cremona', '', 'woocommerce', 0),
(831, 'Crotone', '', 'woocommerce', 0),
(832, 'Cuneo', '', 'woocommerce', 0),
(833, 'Enna', '', 'woocommerce', 0),
(834, 'Fermo', '', 'woocommerce', 0),
(835, 'Ferrara', '', 'woocommerce', 0),
(836, 'Firenze', '', 'woocommerce', 0),
(837, 'Foggia', '', 'woocommerce', 0),
(838, 'Forlì-Cesena', '', 'woocommerce', 0),
(839, 'Frosinone', '', 'woocommerce', 0),
(840, 'Genova', '', 'woocommerce', 0),
(841, 'Gorizia', '', 'woocommerce', 0),
(842, 'Grosseto', '', 'woocommerce', 0),
(843, 'Imperia', '', 'woocommerce', 0),
(844, 'Isernia', '', 'woocommerce', 0),
(845, 'La Spezia', '', 'woocommerce', 0),
(846, 'L\'Aquila', '', 'woocommerce', 0),
(847, 'Latina', '', 'woocommerce', 0),
(848, 'Lecce', '', 'woocommerce', 0),
(849, 'Lecco', '', 'woocommerce', 0),
(850, 'Livorno', '', 'woocommerce', 0),
(851, 'Lodi', '', 'woocommerce', 0),
(852, 'Lucca', '', 'woocommerce', 0),
(853, 'Macerata', '', 'woocommerce', 0),
(854, 'Mantova', '', 'woocommerce', 0),
(855, 'Massa-Carrara', '', 'woocommerce', 0),
(856, 'Matera', '', 'woocommerce', 0),
(857, 'Messina', '', 'woocommerce', 0),
(858, 'Milano', '', 'woocommerce', 0),
(859, 'Modena', '', 'woocommerce', 0),
(860, 'Monza e della Brianza', '', 'woocommerce', 0),
(861, 'Napoli', '', 'woocommerce', 0),
(862, 'Novara', '', 'woocommerce', 0),
(863, 'Nuoro', '', 'woocommerce', 0),
(864, 'Oristano', '', 'woocommerce', 0),
(865, 'Padova', '', 'woocommerce', 0),
(866, 'Palermo', '', 'woocommerce', 0),
(867, 'Parma', '', 'woocommerce', 0),
(868, 'Pavia', '', 'woocommerce', 0),
(869, 'Perugia', '', 'woocommerce', 0),
(870, 'Pesaro e Urbino', '', 'woocommerce', 0),
(871, 'Pescara', '', 'woocommerce', 0),
(872, 'Piacenza', '', 'woocommerce', 0),
(873, 'Pisa', '', 'woocommerce', 0),
(874, 'Pistoia', '', 'woocommerce', 0),
(875, 'Pordenone', '', 'woocommerce', 0),
(876, 'Potenza', '', 'woocommerce', 0),
(877, 'Prato', '', 'woocommerce', 0),
(878, 'Ragusa', '', 'woocommerce', 0),
(879, 'Ravenna', '', 'woocommerce', 0),
(880, 'Reggio Calabria', '', 'woocommerce', 0),
(881, 'Reggio Emilia', '', 'woocommerce', 0),
(882, 'Rieti', '', 'woocommerce', 0),
(883, 'Rimini', '', 'woocommerce', 0),
(884, 'Roma', '', 'woocommerce', 0),
(885, 'Rovigo', '', 'woocommerce', 0),
(886, 'Salerno', '', 'woocommerce', 0),
(887, 'Sassari', '', 'woocommerce', 0),
(888, 'Savona', '', 'woocommerce', 0),
(889, 'Siena', '', 'woocommerce', 0),
(890, 'Siracusa', '', 'woocommerce', 0),
(891, 'Sondrio', '', 'woocommerce', 0),
(892, 'Sud Sardegna', '', 'woocommerce', 0),
(893, 'Taranto', '', 'woocommerce', 0),
(894, 'Teramo', '', 'woocommerce', 0),
(895, 'Terni', '', 'woocommerce', 0),
(896, 'Torino', '', 'woocommerce', 0),
(897, 'Trapani', '', 'woocommerce', 0),
(898, 'Trento', '', 'woocommerce', 0),
(899, 'Treviso', '', 'woocommerce', 0),
(900, 'Trieste', '', 'woocommerce', 0),
(901, 'Udine', '', 'woocommerce', 0),
(902, 'Varese', '', 'woocommerce', 0),
(903, 'Venezia', '', 'woocommerce', 0),
(904, 'Verbano-Cusio-Ossola', '', 'woocommerce', 0),
(905, 'Vercelli', '', 'woocommerce', 0),
(906, 'Verona', '', 'woocommerce', 0),
(907, 'Vibo Valentia', '', 'woocommerce', 0),
(908, 'Vicenza', '', 'woocommerce', 0),
(909, 'Viterbo', '', 'woocommerce', 0),
(910, 'Hokkaido', '', 'woocommerce', 0),
(911, 'Aomori', '', 'woocommerce', 0),
(912, 'Iwate', '', 'woocommerce', 0),
(913, 'Miyagi', '', 'woocommerce', 0),
(914, 'Akita', '', 'woocommerce', 0),
(915, 'Yamagata', '', 'woocommerce', 0),
(916, 'Fukushima', '', 'woocommerce', 0),
(917, 'Ibaraki', '', 'woocommerce', 0),
(918, 'Tochigi', '', 'woocommerce', 0),
(919, 'Gunma', '', 'woocommerce', 0),
(920, 'Saitama', '', 'woocommerce', 0),
(921, 'Chiba', '', 'woocommerce', 0),
(922, 'Tokyo', '', 'woocommerce', 0),
(923, 'Kanagawa', '', 'woocommerce', 0),
(924, 'Niigata', '', 'woocommerce', 0),
(925, 'Toyama', '', 'woocommerce', 0),
(926, 'Ishikawa', '', 'woocommerce', 0),
(927, 'Fukui', '', 'woocommerce', 0),
(928, 'Yamanashi', '', 'woocommerce', 0),
(929, 'Nagano', '', 'woocommerce', 0),
(930, 'Gifu', '', 'woocommerce', 0),
(931, 'Shizuoka', '', 'woocommerce', 0),
(932, 'Aichi', '', 'woocommerce', 0),
(933, 'Mie', '', 'woocommerce', 0),
(934, 'Shiga', '', 'woocommerce', 0),
(935, 'Kyoto', '', 'woocommerce', 0),
(936, 'Osaka', '', 'woocommerce', 0),
(937, 'Hyogo', '', 'woocommerce', 0),
(938, 'Nara', '', 'woocommerce', 0),
(939, 'Wakayama', '', 'woocommerce', 0),
(940, 'Tottori', '', 'woocommerce', 0),
(941, 'Shimane', '', 'woocommerce', 0),
(942, 'Okayama', '', 'woocommerce', 0),
(943, 'Hiroshima', '', 'woocommerce', 0),
(944, 'Yamaguchi', '', 'woocommerce', 0),
(945, 'Tokushima', '', 'woocommerce', 0),
(946, 'Kagawa', '', 'woocommerce', 0),
(947, 'Ehime', '', 'woocommerce', 0),
(948, 'Kochi', '', 'woocommerce', 0),
(949, 'Fukuoka', '', 'woocommerce', 0),
(950, 'Saga', '', 'woocommerce', 0),
(951, 'Nagasaki', '', 'woocommerce', 0),
(952, 'Kumamoto', '', 'woocommerce', 0),
(953, 'Oita', '', 'woocommerce', 0),
(954, 'Miyazaki', '', 'woocommerce', 0),
(955, 'Kagoshima', '', 'woocommerce', 0),
(956, 'Okinawa', '', 'woocommerce', 0),
(957, 'Baringo', '', 'woocommerce', 0),
(958, 'Bomet', '', 'woocommerce', 0),
(959, 'Bungoma', '', 'woocommerce', 0),
(960, 'Busia', '', 'woocommerce', 0),
(961, 'Elgeyo-Marakwet', '', 'woocommerce', 0),
(962, 'Embu', '', 'woocommerce', 0),
(963, 'Garissa', '', 'woocommerce', 0),
(964, 'Homa Bay', '', 'woocommerce', 0),
(965, 'Isiolo', '', 'woocommerce', 0),
(966, 'Kajiado', '', 'woocommerce', 0),
(967, 'Kakamega', '', 'woocommerce', 0),
(968, 'Kericho', '', 'woocommerce', 0),
(969, 'Kiambu', '', 'woocommerce', 0),
(970, 'Kilifi', '', 'woocommerce', 0),
(971, 'Kirinyaga', '', 'woocommerce', 0),
(972, 'Kisii', '', 'woocommerce', 0),
(973, 'Kisumu', '', 'woocommerce', 0),
(974, 'Kitui', '', 'woocommerce', 0),
(975, 'Kwale', '', 'woocommerce', 0),
(976, 'Laikipia', '', 'woocommerce', 0),
(977, 'Lamu', '', 'woocommerce', 0),
(978, 'Machakos', '', 'woocommerce', 0),
(979, 'Makueni', '', 'woocommerce', 0),
(980, 'Mandera', '', 'woocommerce', 0),
(981, 'Marsabit', '', 'woocommerce', 0),
(982, 'Meru', '', 'woocommerce', 0),
(983, 'Migori', '', 'woocommerce', 0),
(984, 'Mombasa', '', 'woocommerce', 0),
(985, 'Murang’a', '', 'woocommerce', 0),
(986, 'Nairobi County', '', 'woocommerce', 0),
(987, 'Nakuru', '', 'woocommerce', 0),
(988, 'Nandi', '', 'woocommerce', 0),
(989, 'Narok', '', 'woocommerce', 0),
(990, 'Nyamira', '', 'woocommerce', 0),
(991, 'Nyandarua', '', 'woocommerce', 0),
(992, 'Nyeri', '', 'woocommerce', 0),
(993, 'Samburu', '', 'woocommerce', 0),
(994, 'Siaya', '', 'woocommerce', 0),
(995, 'Taita-Taveta', '', 'woocommerce', 0),
(996, 'Tana River', '', 'woocommerce', 0),
(997, 'Tharaka-Nithi', '', 'woocommerce', 0),
(998, 'Trans Nzoia', '', 'woocommerce', 0),
(999, 'Turkana', '', 'woocommerce', 0),
(1000, 'Uasin Gishu', '', 'woocommerce', 0),
(1001, 'Vihiga', '', 'woocommerce', 0),
(1002, 'Wajir', '', 'woocommerce', 0),
(1003, 'West Pokot', '', 'woocommerce', 0),
(1004, 'Attapeu', '', 'woocommerce', 0),
(1005, 'Bokeo', '', 'woocommerce', 0),
(1006, 'Bolikhamsai', '', 'woocommerce', 0),
(1007, 'Champasak', '', 'woocommerce', 0),
(1008, 'Houaphanh', '', 'woocommerce', 0),
(1009, 'Khammouane', '', 'woocommerce', 0),
(1010, 'Luang Namtha', '', 'woocommerce', 0),
(1011, 'Luang Prabang', '', 'woocommerce', 0),
(1012, 'Oudomxay', '', 'woocommerce', 0),
(1013, 'Phongsaly', '', 'woocommerce', 0),
(1014, 'Salavan', '', 'woocommerce', 0),
(1015, 'Savannakhet', '', 'woocommerce', 0),
(1016, 'Vientiane Province', '', 'woocommerce', 0),
(1017, 'Vientiane', '', 'woocommerce', 0),
(1018, 'Sainyabuli', '', 'woocommerce', 0),
(1019, 'Sekong', '', 'woocommerce', 0),
(1020, 'Xiangkhouang', '', 'woocommerce', 0),
(1021, 'Xaisomboun', '', 'woocommerce', 0),
(1022, 'Bomi', '', 'woocommerce', 0),
(1023, 'Bong', '', 'woocommerce', 0),
(1024, 'Gbarpolu', '', 'woocommerce', 0),
(1025, 'Grand Bassa', '', 'woocommerce', 0),
(1026, 'Grand Cape Mount', '', 'woocommerce', 0),
(1027, 'Grand Gedeh', '', 'woocommerce', 0),
(1028, 'Grand Kru', '', 'woocommerce', 0),
(1029, 'Lofa', '', 'woocommerce', 0),
(1030, 'Margibi', '', 'woocommerce', 0),
(1031, 'Maryland', '', 'woocommerce', 0),
(1032, 'Montserrado', '', 'woocommerce', 0),
(1033, 'Nimba', '', 'woocommerce', 0),
(1034, 'Rivercess', '', 'woocommerce', 0),
(1035, 'River Gee', '', 'woocommerce', 0),
(1036, 'Sinoe', '', 'woocommerce', 0),
(1037, 'Chi&#537;in&#259;u', '', 'woocommerce', 0),
(1038, 'B&#259;l&#539;i', '', 'woocommerce', 0),
(1039, 'Anenii Noi', '', 'woocommerce', 0),
(1040, 'Basarabeasca', '', 'woocommerce', 0),
(1041, 'Briceni', '', 'woocommerce', 0),
(1042, 'Cahul', '', 'woocommerce', 0),
(1043, 'Cantemir', '', 'woocommerce', 0),
(1044, 'C&#259;l&#259;ra&#537;i', '', 'woocommerce', 0),
(1045, 'C&#259;u&#537;eni', '', 'woocommerce', 0),
(1046, 'Cimi&#537;lia', '', 'woocommerce', 0);
INSERT INTO `wp_trp_gettext_en_us` (`id`, `original`, `translated`, `domain`, `status`) VALUES
(1047, 'Criuleni', '', 'woocommerce', 0),
(1048, 'Dondu&#537;eni', '', 'woocommerce', 0),
(1049, 'Drochia', '', 'woocommerce', 0),
(1050, 'Dub&#259;sari', '', 'woocommerce', 0),
(1051, 'Edine&#539;', '', 'woocommerce', 0),
(1052, 'F&#259;le&#537;ti', '', 'woocommerce', 0),
(1053, 'Flore&#537;ti', '', 'woocommerce', 0),
(1054, 'UTA G&#259;g&#259;uzia', '', 'woocommerce', 0),
(1055, 'Glodeni', '', 'woocommerce', 0),
(1056, 'H&icirc;nce&#537;ti', '', 'woocommerce', 0),
(1057, 'Ialoveni', '', 'woocommerce', 0),
(1058, 'Leova', '', 'woocommerce', 0),
(1059, 'Nisporeni', '', 'woocommerce', 0),
(1060, 'Ocni&#539;a', '', 'woocommerce', 0),
(1061, 'Orhei', '', 'woocommerce', 0),
(1062, 'Rezina', '', 'woocommerce', 0),
(1063, 'R&icirc;&#537;cani', '', 'woocommerce', 0),
(1064, 'S&icirc;ngerei', '', 'woocommerce', 0),
(1065, 'Soroca', '', 'woocommerce', 0),
(1066, 'Str&#259;&#537;eni', '', 'woocommerce', 0),
(1067, '&#536;old&#259;ne&#537;ti', '', 'woocommerce', 0),
(1068, '&#536;tefan Vod&#259;', '', 'woocommerce', 0),
(1069, 'Taraclia', '', 'woocommerce', 0),
(1070, 'Telene&#537;ti', '', 'woocommerce', 0),
(1071, 'Ungheni', '', 'woocommerce', 0),
(1072, 'Ciudad de M&eacute;xico', '', 'woocommerce', 0),
(1073, 'Jalisco', '', 'woocommerce', 0),
(1074, 'Nuevo Le&oacute;n', '', 'woocommerce', 0),
(1075, 'Aguascalientes', '', 'woocommerce', 0),
(1076, 'Baja California', '', 'woocommerce', 0),
(1077, 'Baja California Sur', '', 'woocommerce', 0),
(1078, 'Campeche', '', 'woocommerce', 0),
(1079, 'Chiapas', '', 'woocommerce', 0),
(1080, 'Chihuahua', '', 'woocommerce', 0),
(1081, 'Coahuila', '', 'woocommerce', 0),
(1082, 'Colima', '', 'woocommerce', 0),
(1083, 'Durango', '', 'woocommerce', 0),
(1084, 'Guanajuato', '', 'woocommerce', 0),
(1085, 'Guerrero', '', 'woocommerce', 0),
(1086, 'Hidalgo', '', 'woocommerce', 0),
(1087, 'Estado de M&eacute;xico', '', 'woocommerce', 0),
(1088, 'Michoac&aacute;n', '', 'woocommerce', 0),
(1089, 'Morelos', '', 'woocommerce', 0),
(1090, 'Nayarit', '', 'woocommerce', 0),
(1091, 'Oaxaca', '', 'woocommerce', 0),
(1092, 'Puebla', '', 'woocommerce', 0),
(1093, 'Quer&eacute;taro', '', 'woocommerce', 0),
(1094, 'Quintana Roo', '', 'woocommerce', 0),
(1095, 'San Luis Potos&iacute;', '', 'woocommerce', 0),
(1096, 'Sinaloa', '', 'woocommerce', 0),
(1097, 'Sonora', '', 'woocommerce', 0),
(1098, 'Tabasco', '', 'woocommerce', 0),
(1099, 'Tamaulipas', '', 'woocommerce', 0),
(1100, 'Tlaxcala', '', 'woocommerce', 0),
(1101, 'Veracruz', '', 'woocommerce', 0),
(1102, 'Yucat&aacute;n', '', 'woocommerce', 0),
(1103, 'Zacatecas', '', 'woocommerce', 0),
(1104, 'Johor', '', 'woocommerce', 0),
(1105, 'Kedah', '', 'woocommerce', 0),
(1106, 'Kelantan', '', 'woocommerce', 0),
(1107, 'Labuan', '', 'woocommerce', 0),
(1108, 'Malacca (Melaka)', '', 'woocommerce', 0),
(1109, 'Negeri Sembilan', '', 'woocommerce', 0),
(1110, 'Pahang', '', 'woocommerce', 0),
(1111, 'Penang (Pulau Pinang)', '', 'woocommerce', 0),
(1112, 'Perak', '', 'woocommerce', 0),
(1113, 'Perlis', '', 'woocommerce', 0),
(1114, 'Sabah', '', 'woocommerce', 0),
(1115, 'Sarawak', '', 'woocommerce', 0),
(1116, 'Selangor', '', 'woocommerce', 0),
(1117, 'Terengganu', '', 'woocommerce', 0),
(1118, 'Putrajaya', '', 'woocommerce', 0),
(1119, 'Kuala Lumpur', '', 'woocommerce', 0),
(1120, 'Cabo Delgado', '', 'woocommerce', 0),
(1121, 'Gaza', '', 'woocommerce', 0),
(1122, 'Inhambane', '', 'woocommerce', 0),
(1123, 'Manica', '', 'woocommerce', 0),
(1124, 'Maputo Province', '', 'woocommerce', 0),
(1125, 'Maputo', '', 'woocommerce', 0),
(1126, 'Nampula', '', 'woocommerce', 0),
(1127, 'Niassa', '', 'woocommerce', 0),
(1128, 'Sofala', '', 'woocommerce', 0),
(1129, 'Tete', '', 'woocommerce', 0),
(1130, 'Zambézia', '', 'woocommerce', 0),
(1131, 'Abia', '', 'woocommerce', 0),
(1132, 'Abuja', '', 'woocommerce', 0),
(1133, 'Adamawa', '', 'woocommerce', 0),
(1134, 'Akwa Ibom', '', 'woocommerce', 0),
(1135, 'Anambra', '', 'woocommerce', 0),
(1136, 'Bauchi', '', 'woocommerce', 0),
(1137, 'Bayelsa', '', 'woocommerce', 0),
(1138, 'Benue', '', 'woocommerce', 0),
(1139, 'Borno', '', 'woocommerce', 0),
(1140, 'Cross River', '', 'woocommerce', 0),
(1141, 'Delta', '', 'woocommerce', 0),
(1142, 'Ebonyi', '', 'woocommerce', 0),
(1143, 'Edo', '', 'woocommerce', 0),
(1144, 'Ekiti', '', 'woocommerce', 0),
(1145, 'Enugu', '', 'woocommerce', 0),
(1146, 'Gombe', '', 'woocommerce', 0),
(1147, 'Imo', '', 'woocommerce', 0),
(1148, 'Jigawa', '', 'woocommerce', 0),
(1149, 'Kaduna', '', 'woocommerce', 0),
(1150, 'Kano', '', 'woocommerce', 0),
(1151, 'Katsina', '', 'woocommerce', 0),
(1152, 'Kebbi', '', 'woocommerce', 0),
(1153, 'Kogi', '', 'woocommerce', 0),
(1154, 'Kwara', '', 'woocommerce', 0),
(1155, 'Lagos', '', 'woocommerce', 0),
(1156, 'Nasarawa', '', 'woocommerce', 0),
(1157, 'Niger', '', 'woocommerce', 0),
(1158, 'Ogun', '', 'woocommerce', 0),
(1159, 'Ondo', '', 'woocommerce', 0),
(1160, 'Osun', '', 'woocommerce', 0),
(1161, 'Oyo', '', 'woocommerce', 0),
(1162, 'Plateau', '', 'woocommerce', 0),
(1163, 'Rivers', '', 'woocommerce', 0),
(1164, 'Sokoto', '', 'woocommerce', 0),
(1165, 'Taraba', '', 'woocommerce', 0),
(1166, 'Yobe', '', 'woocommerce', 0),
(1167, 'Zamfara', '', 'woocommerce', 0),
(1168, 'Bagmati', '', 'woocommerce', 0),
(1169, 'Bheri', '', 'woocommerce', 0),
(1170, 'Dhaulagiri', '', 'woocommerce', 0),
(1171, 'Gandaki', '', 'woocommerce', 0),
(1172, 'Janakpur', '', 'woocommerce', 0),
(1173, 'Karnali', '', 'woocommerce', 0),
(1174, 'Koshi', '', 'woocommerce', 0),
(1175, 'Lumbini', '', 'woocommerce', 0),
(1176, 'Mahakali', '', 'woocommerce', 0),
(1177, 'Mechi', '', 'woocommerce', 0),
(1178, 'Narayani', '', 'woocommerce', 0),
(1179, 'Rapti', '', 'woocommerce', 0),
(1180, 'Sagarmatha', '', 'woocommerce', 0),
(1181, 'Seti', '', 'woocommerce', 0),
(1182, 'Northland', '', 'woocommerce', 0),
(1183, 'Auckland', '', 'woocommerce', 0),
(1184, 'Waikato', '', 'woocommerce', 0),
(1185, 'Bay of Plenty', '', 'woocommerce', 0),
(1186, 'Taranaki', '', 'woocommerce', 0),
(1187, 'Gisborne', '', 'woocommerce', 0),
(1188, 'Hawke&rsquo;s Bay', '', 'woocommerce', 0),
(1189, 'Manawatu-Wanganui', '', 'woocommerce', 0),
(1190, 'Wellington', '', 'woocommerce', 0),
(1191, 'Nelson', '', 'woocommerce', 0),
(1192, 'Marlborough', '', 'woocommerce', 0),
(1193, 'Tasman', '', 'woocommerce', 0),
(1194, 'West Coast', '', 'woocommerce', 0),
(1195, 'Canterbury', '', 'woocommerce', 0),
(1196, 'Otago', '', 'woocommerce', 0),
(1197, 'Southland', '', 'woocommerce', 0),
(1198, 'El Callao', '', 'woocommerce', 0),
(1199, 'Municipalidad Metropolitana de Lima', '', 'woocommerce', 0),
(1200, 'Ancash', '', 'woocommerce', 0),
(1201, 'Apur&iacute;mac', '', 'woocommerce', 0),
(1202, 'Arequipa', '', 'woocommerce', 0),
(1203, 'Ayacucho', '', 'woocommerce', 0),
(1204, 'Cajamarca', '', 'woocommerce', 0),
(1205, 'Cusco', '', 'woocommerce', 0),
(1206, 'Huancavelica', '', 'woocommerce', 0),
(1207, 'Hu&aacute;nuco', '', 'woocommerce', 0),
(1208, 'Ica', '', 'woocommerce', 0),
(1209, 'Jun&iacute;n', '', 'woocommerce', 0),
(1210, 'La Libertad', '', 'woocommerce', 0),
(1211, 'Lambayeque', '', 'woocommerce', 0),
(1212, 'Lima', '', 'woocommerce', 0),
(1213, 'Loreto', '', 'woocommerce', 0),
(1214, 'Madre de Dios', '', 'woocommerce', 0),
(1215, 'Moquegua', '', 'woocommerce', 0),
(1216, 'Pasco', '', 'woocommerce', 0),
(1217, 'Piura', '', 'woocommerce', 0),
(1218, 'Puno', '', 'woocommerce', 0),
(1219, 'San Mart&iacute;n', '', 'woocommerce', 0),
(1220, 'Tacna', '', 'woocommerce', 0),
(1221, 'Tumbes', '', 'woocommerce', 0),
(1222, 'Ucayali', '', 'woocommerce', 0),
(1223, 'Abra', '', 'woocommerce', 0),
(1224, 'Agusan del Norte', '', 'woocommerce', 0),
(1225, 'Agusan del Sur', '', 'woocommerce', 0),
(1226, 'Aklan', '', 'woocommerce', 0),
(1227, 'Albay', '', 'woocommerce', 0),
(1228, 'Antique', '', 'woocommerce', 0),
(1229, 'Apayao', '', 'woocommerce', 0),
(1230, 'Aurora', '', 'woocommerce', 0),
(1231, 'Basilan', '', 'woocommerce', 0),
(1232, 'Bataan', '', 'woocommerce', 0),
(1233, 'Batanes', '', 'woocommerce', 0),
(1234, 'Batangas', '', 'woocommerce', 0),
(1235, 'Benguet', '', 'woocommerce', 0),
(1236, 'Biliran', '', 'woocommerce', 0),
(1237, 'Bohol', '', 'woocommerce', 0),
(1238, 'Bukidnon', '', 'woocommerce', 0),
(1239, 'Bulacan', '', 'woocommerce', 0),
(1240, 'Cagayan', '', 'woocommerce', 0),
(1241, 'Camarines Norte', '', 'woocommerce', 0),
(1242, 'Camarines Sur', '', 'woocommerce', 0),
(1243, 'Camiguin', '', 'woocommerce', 0),
(1244, 'Capiz', '', 'woocommerce', 0),
(1245, 'Catanduanes', '', 'woocommerce', 0),
(1246, 'Cavite', '', 'woocommerce', 0),
(1247, 'Cebu', '', 'woocommerce', 0),
(1248, 'Compostela Valley', '', 'woocommerce', 0),
(1249, 'Cotabato', '', 'woocommerce', 0),
(1250, 'Davao del Norte', '', 'woocommerce', 0),
(1251, 'Davao del Sur', '', 'woocommerce', 0),
(1252, 'Davao Occidental', '', 'woocommerce', 0),
(1253, 'Davao Oriental', '', 'woocommerce', 0),
(1254, 'Dinagat Islands', '', 'woocommerce', 0),
(1255, 'Eastern Samar', '', 'woocommerce', 0),
(1256, 'Guimaras', '', 'woocommerce', 0),
(1257, 'Ifugao', '', 'woocommerce', 0),
(1258, 'Ilocos Norte', '', 'woocommerce', 0),
(1259, 'Ilocos Sur', '', 'woocommerce', 0),
(1260, 'Iloilo', '', 'woocommerce', 0),
(1261, 'Isabela', '', 'woocommerce', 0),
(1262, 'Kalinga', '', 'woocommerce', 0),
(1263, 'La Union', '', 'woocommerce', 0),
(1264, 'Laguna', '', 'woocommerce', 0),
(1265, 'Lanao del Norte', '', 'woocommerce', 0),
(1266, 'Lanao del Sur', '', 'woocommerce', 0),
(1267, 'Leyte', '', 'woocommerce', 0),
(1268, 'Maguindanao', '', 'woocommerce', 0),
(1269, 'Marinduque', '', 'woocommerce', 0),
(1270, 'Masbate', '', 'woocommerce', 0),
(1271, 'Misamis Occidental', '', 'woocommerce', 0),
(1272, 'Misamis Oriental', '', 'woocommerce', 0),
(1273, 'Mountain Province', '', 'woocommerce', 0),
(1274, 'Negros Occidental', '', 'woocommerce', 0),
(1275, 'Negros Oriental', '', 'woocommerce', 0),
(1276, 'Northern Samar', '', 'woocommerce', 0),
(1277, 'Nueva Ecija', '', 'woocommerce', 0),
(1278, 'Nueva Vizcaya', '', 'woocommerce', 0),
(1279, 'Occidental Mindoro', '', 'woocommerce', 0),
(1280, 'Oriental Mindoro', '', 'woocommerce', 0),
(1281, 'Palawan', '', 'woocommerce', 0),
(1282, 'Pampanga', '', 'woocommerce', 0),
(1283, 'Pangasinan', '', 'woocommerce', 0),
(1284, 'Quezon', '', 'woocommerce', 0),
(1285, 'Quirino', '', 'woocommerce', 0),
(1286, 'Rizal', '', 'woocommerce', 0),
(1287, 'Romblon', '', 'woocommerce', 0),
(1288, 'Samar', '', 'woocommerce', 0),
(1289, 'Sarangani', '', 'woocommerce', 0),
(1290, 'Siquijor', '', 'woocommerce', 0),
(1291, 'Sorsogon', '', 'woocommerce', 0),
(1292, 'South Cotabato', '', 'woocommerce', 0),
(1293, 'Southern Leyte', '', 'woocommerce', 0),
(1294, 'Sultan Kudarat', '', 'woocommerce', 0),
(1295, 'Sulu', '', 'woocommerce', 0),
(1296, 'Surigao del Norte', '', 'woocommerce', 0),
(1297, 'Surigao del Sur', '', 'woocommerce', 0),
(1298, 'Tarlac', '', 'woocommerce', 0),
(1299, 'Tawi-Tawi', '', 'woocommerce', 0),
(1300, 'Zambales', '', 'woocommerce', 0),
(1301, 'Zamboanga del Norte', '', 'woocommerce', 0),
(1302, 'Zamboanga del Sur', '', 'woocommerce', 0),
(1303, 'Zamboanga Sibugay', '', 'woocommerce', 0),
(1304, 'Metro Manila', '', 'woocommerce', 0),
(1305, 'Azad Kashmir', '', 'woocommerce', 0),
(1306, 'Balochistan', '', 'woocommerce', 0),
(1307, 'FATA', '', 'woocommerce', 0),
(1308, 'Gilgit Baltistan', '', 'woocommerce', 0),
(1309, 'Islamabad Capital Territory', '', 'woocommerce', 0),
(1310, 'Khyber Pakhtunkhwa', '', 'woocommerce', 0),
(1311, 'Sindh', '', 'woocommerce', 0),
(1312, 'Asunci&oacute;n', '', 'woocommerce', 0),
(1313, 'Concepci&oacute;n', '', 'woocommerce', 0),
(1314, 'San Pedro', '', 'woocommerce', 0),
(1315, 'Cordillera', '', 'woocommerce', 0),
(1316, 'Guair&aacute;', '', 'woocommerce', 0),
(1317, 'Caaguaz&uacute;', '', 'woocommerce', 0),
(1318, 'Caazap&aacute;', '', 'woocommerce', 0),
(1319, 'Itap&uacute;a', '', 'woocommerce', 0),
(1320, 'Paraguar&iacute;', '', 'woocommerce', 0),
(1321, 'Alto Paran&aacute;', '', 'woocommerce', 0),
(1322, '&Ntilde;eembuc&uacute;', '', 'woocommerce', 0),
(1323, 'Amambay', '', 'woocommerce', 0),
(1324, 'Canindey&uacute;', '', 'woocommerce', 0),
(1325, 'Presidente Hayes', '', 'woocommerce', 0),
(1326, 'Alto Paraguay', '', 'woocommerce', 0),
(1327, 'Boquer&oacute;n', '', 'woocommerce', 0),
(1328, 'Alba', '', 'woocommerce', 0),
(1329, 'Arad', '', 'woocommerce', 0),
(1330, 'Arge&#537;', '', 'woocommerce', 0),
(1331, 'Bac&#259;u', '', 'woocommerce', 0),
(1332, 'Bihor', '', 'woocommerce', 0),
(1333, 'Bistri&#539;a-N&#259;s&#259;ud', '', 'woocommerce', 0),
(1334, 'Boto&#537;ani', '', 'woocommerce', 0),
(1335, 'Br&#259;ila', '', 'woocommerce', 0),
(1336, 'Bra&#537;ov', '', 'woocommerce', 0),
(1337, 'Bucure&#537;ti', '', 'woocommerce', 0),
(1338, 'Buz&#259;u', '', 'woocommerce', 0),
(1339, 'Cara&#537;-Severin', '', 'woocommerce', 0),
(1340, 'Cluj', '', 'woocommerce', 0),
(1341, 'Constan&#539;a', '', 'woocommerce', 0),
(1342, 'Covasna', '', 'woocommerce', 0),
(1343, 'D&acirc;mbovi&#539;a', '', 'woocommerce', 0),
(1344, 'Dolj', '', 'woocommerce', 0),
(1345, 'Gala&#539;i', '', 'woocommerce', 0),
(1346, 'Giurgiu', '', 'woocommerce', 0),
(1347, 'Gorj', '', 'woocommerce', 0),
(1348, 'Harghita', '', 'woocommerce', 0),
(1349, 'Hunedoara', '', 'woocommerce', 0),
(1350, 'Ialomi&#539;a', '', 'woocommerce', 0),
(1351, 'Ia&#537;i', '', 'woocommerce', 0),
(1352, 'Ilfov', '', 'woocommerce', 0),
(1353, 'Maramure&#537;', '', 'woocommerce', 0),
(1354, 'Mehedin&#539;i', '', 'woocommerce', 0),
(1355, 'Mure&#537;', '', 'woocommerce', 0),
(1356, 'Neam&#539;', '', 'woocommerce', 0),
(1357, 'Olt', '', 'woocommerce', 0),
(1358, 'Prahova', '', 'woocommerce', 0),
(1359, 'S&#259;laj', '', 'woocommerce', 0),
(1360, 'Satu Mare', '', 'woocommerce', 0),
(1361, 'Sibiu', '', 'woocommerce', 0),
(1362, 'Suceava', '', 'woocommerce', 0),
(1363, 'Teleorman', '', 'woocommerce', 0),
(1364, 'Timi&#537;', '', 'woocommerce', 0),
(1365, 'Tulcea', '', 'woocommerce', 0),
(1366, 'V&acirc;lcea', '', 'woocommerce', 0),
(1367, 'Vaslui', '', 'woocommerce', 0),
(1368, 'Vrancea', '', 'woocommerce', 0),
(1369, 'Amnat Charoen', '', 'woocommerce', 0),
(1370, 'Ang Thong', '', 'woocommerce', 0),
(1371, 'Ayutthaya', '', 'woocommerce', 0),
(1372, 'Bangkok', '', 'woocommerce', 0),
(1373, 'Bueng Kan', '', 'woocommerce', 0),
(1374, 'Buri Ram', '', 'woocommerce', 0),
(1375, 'Chachoengsao', '', 'woocommerce', 0),
(1376, 'Chai Nat', '', 'woocommerce', 0),
(1377, 'Chaiyaphum', '', 'woocommerce', 0),
(1378, 'Chanthaburi', '', 'woocommerce', 0),
(1379, 'Chiang Mai', '', 'woocommerce', 0),
(1380, 'Chiang Rai', '', 'woocommerce', 0),
(1381, 'Chonburi', '', 'woocommerce', 0),
(1382, 'Chumphon', '', 'woocommerce', 0),
(1383, 'Kalasin', '', 'woocommerce', 0),
(1384, 'Kamphaeng Phet', '', 'woocommerce', 0),
(1385, 'Kanchanaburi', '', 'woocommerce', 0),
(1386, 'Khon Kaen', '', 'woocommerce', 0),
(1387, 'Krabi', '', 'woocommerce', 0),
(1388, 'Lampang', '', 'woocommerce', 0),
(1389, 'Lamphun', '', 'woocommerce', 0),
(1390, 'Loei', '', 'woocommerce', 0),
(1391, 'Lopburi', '', 'woocommerce', 0),
(1392, 'Mae Hong Son', '', 'woocommerce', 0),
(1393, 'Maha Sarakham', '', 'woocommerce', 0),
(1394, 'Mukdahan', '', 'woocommerce', 0),
(1395, 'Nakhon Nayok', '', 'woocommerce', 0),
(1396, 'Nakhon Pathom', '', 'woocommerce', 0),
(1397, 'Nakhon Phanom', '', 'woocommerce', 0),
(1398, 'Nakhon Ratchasima', '', 'woocommerce', 0),
(1399, 'Nakhon Sawan', '', 'woocommerce', 0),
(1400, 'Nakhon Si Thammarat', '', 'woocommerce', 0),
(1401, 'Nan', '', 'woocommerce', 0),
(1402, 'Narathiwat', '', 'woocommerce', 0),
(1403, 'Nong Bua Lam Phu', '', 'woocommerce', 0),
(1404, 'Nong Khai', '', 'woocommerce', 0),
(1405, 'Nonthaburi', '', 'woocommerce', 0),
(1406, 'Pathum Thani', '', 'woocommerce', 0),
(1407, 'Pattani', '', 'woocommerce', 0),
(1408, 'Phang Nga', '', 'woocommerce', 0),
(1409, 'Phatthalung', '', 'woocommerce', 0),
(1410, 'Phayao', '', 'woocommerce', 0),
(1411, 'Phetchabun', '', 'woocommerce', 0),
(1412, 'Phetchaburi', '', 'woocommerce', 0),
(1413, 'Phichit', '', 'woocommerce', 0),
(1414, 'Phitsanulok', '', 'woocommerce', 0),
(1415, 'Phrae', '', 'woocommerce', 0),
(1416, 'Phuket', '', 'woocommerce', 0),
(1417, 'Prachin Buri', '', 'woocommerce', 0),
(1418, 'Prachuap Khiri Khan', '', 'woocommerce', 0),
(1419, 'Ranong', '', 'woocommerce', 0),
(1420, 'Ratchaburi', '', 'woocommerce', 0),
(1421, 'Rayong', '', 'woocommerce', 0),
(1422, 'Roi Et', '', 'woocommerce', 0),
(1423, 'Sa Kaeo', '', 'woocommerce', 0),
(1424, 'Sakon Nakhon', '', 'woocommerce', 0),
(1425, 'Samut Prakan', '', 'woocommerce', 0),
(1426, 'Samut Sakhon', '', 'woocommerce', 0),
(1427, 'Samut Songkhram', '', 'woocommerce', 0),
(1428, 'Saraburi', '', 'woocommerce', 0),
(1429, 'Satun', '', 'woocommerce', 0),
(1430, 'Sing Buri', '', 'woocommerce', 0),
(1431, 'Sisaket', '', 'woocommerce', 0),
(1432, 'Songkhla', '', 'woocommerce', 0),
(1433, 'Sukhothai', '', 'woocommerce', 0),
(1434, 'Suphan Buri', '', 'woocommerce', 0),
(1435, 'Surat Thani', '', 'woocommerce', 0),
(1436, 'Surin', '', 'woocommerce', 0),
(1437, 'Tak', '', 'woocommerce', 0),
(1438, 'Trang', '', 'woocommerce', 0),
(1439, 'Trat', '', 'woocommerce', 0),
(1440, 'Ubon Ratchathani', '', 'woocommerce', 0),
(1441, 'Udon Thani', '', 'woocommerce', 0),
(1442, 'Uthai Thani', '', 'woocommerce', 0),
(1443, 'Uttaradit', '', 'woocommerce', 0),
(1444, 'Yala', '', 'woocommerce', 0),
(1445, 'Yasothon', '', 'woocommerce', 0),
(1446, 'Adana', '', 'woocommerce', 0),
(1447, 'Ad&#305;yaman', '', 'woocommerce', 0),
(1448, 'Afyon', '', 'woocommerce', 0),
(1449, 'A&#287;r&#305;', '', 'woocommerce', 0),
(1450, 'Amasya', '', 'woocommerce', 0),
(1451, 'Ankara', '', 'woocommerce', 0),
(1452, 'Antalya', '', 'woocommerce', 0),
(1453, 'Artvin', '', 'woocommerce', 0),
(1454, 'Ayd&#305;n', '', 'woocommerce', 0),
(1455, 'Bal&#305;kesir', '', 'woocommerce', 0),
(1456, 'Bilecik', '', 'woocommerce', 0),
(1457, 'Bing&#246;l', '', 'woocommerce', 0),
(1458, 'Bitlis', '', 'woocommerce', 0),
(1459, 'Bolu', '', 'woocommerce', 0),
(1460, 'Burdur', '', 'woocommerce', 0),
(1461, 'Bursa', '', 'woocommerce', 0),
(1462, '&#199;anakkale', '', 'woocommerce', 0),
(1463, '&#199;ank&#305;r&#305;', '', 'woocommerce', 0),
(1464, '&#199;orum', '', 'woocommerce', 0),
(1465, 'Denizli', '', 'woocommerce', 0),
(1466, 'Diyarbak&#305;r', '', 'woocommerce', 0),
(1467, 'Edirne', '', 'woocommerce', 0),
(1468, 'Elaz&#305;&#287;', '', 'woocommerce', 0),
(1469, 'Erzincan', '', 'woocommerce', 0),
(1470, 'Erzurum', '', 'woocommerce', 0),
(1471, 'Eski&#351;ehir', '', 'woocommerce', 0),
(1472, 'Gaziantep', '', 'woocommerce', 0),
(1473, 'Giresun', '', 'woocommerce', 0),
(1474, 'G&#252;m&#252;&#351;hane', '', 'woocommerce', 0),
(1475, 'Hakkari', '', 'woocommerce', 0),
(1476, 'Hatay', '', 'woocommerce', 0),
(1477, 'Isparta', '', 'woocommerce', 0),
(1478, '&#304;&#231;el', '', 'woocommerce', 0),
(1479, '&#304;stanbul', '', 'woocommerce', 0),
(1480, '&#304;zmir', '', 'woocommerce', 0),
(1481, 'Kars', '', 'woocommerce', 0),
(1482, 'Kastamonu', '', 'woocommerce', 0),
(1483, 'Kayseri', '', 'woocommerce', 0),
(1484, 'K&#305;rklareli', '', 'woocommerce', 0),
(1485, 'K&#305;r&#351;ehir', '', 'woocommerce', 0),
(1486, 'Kocaeli', '', 'woocommerce', 0),
(1487, 'Konya', '', 'woocommerce', 0),
(1488, 'K&#252;tahya', '', 'woocommerce', 0),
(1489, 'Malatya', '', 'woocommerce', 0),
(1490, 'Manisa', '', 'woocommerce', 0),
(1491, 'Kahramanmara&#351;', '', 'woocommerce', 0),
(1492, 'Mardin', '', 'woocommerce', 0),
(1493, 'Mu&#287;la', '', 'woocommerce', 0),
(1494, 'Mu&#351;', '', 'woocommerce', 0),
(1495, 'Nev&#351;ehir', '', 'woocommerce', 0),
(1496, 'Ni&#287;de', '', 'woocommerce', 0),
(1497, 'Ordu', '', 'woocommerce', 0),
(1498, 'Rize', '', 'woocommerce', 0),
(1499, 'Sakarya', '', 'woocommerce', 0),
(1500, 'Samsun', '', 'woocommerce', 0),
(1501, 'Siirt', '', 'woocommerce', 0),
(1502, 'Sinop', '', 'woocommerce', 0),
(1503, 'Sivas', '', 'woocommerce', 0),
(1504, 'Tekirda&#287;', '', 'woocommerce', 0),
(1505, 'Tokat', '', 'woocommerce', 0),
(1506, 'Trabzon', '', 'woocommerce', 0),
(1507, 'Tunceli', '', 'woocommerce', 0),
(1508, '&#350;anl&#305;urfa', '', 'woocommerce', 0),
(1509, 'U&#351;ak', '', 'woocommerce', 0),
(1510, 'Van', '', 'woocommerce', 0),
(1511, 'Yozgat', '', 'woocommerce', 0),
(1512, 'Zonguldak', '', 'woocommerce', 0),
(1513, 'Aksaray', '', 'woocommerce', 0),
(1514, 'Bayburt', '', 'woocommerce', 0),
(1515, 'Karaman', '', 'woocommerce', 0),
(1516, 'K&#305;r&#305;kkale', '', 'woocommerce', 0),
(1517, 'Batman', '', 'woocommerce', 0),
(1518, '&#350;&#305;rnak', '', 'woocommerce', 0),
(1519, 'Bart&#305;n', '', 'woocommerce', 0),
(1520, 'Ardahan', '', 'woocommerce', 0),
(1521, 'I&#287;d&#305;r', '', 'woocommerce', 0),
(1522, 'Yalova', '', 'woocommerce', 0),
(1523, 'Karab&#252;k', '', 'woocommerce', 0),
(1524, 'Kilis', '', 'woocommerce', 0),
(1525, 'Osmaniye', '', 'woocommerce', 0),
(1526, 'D&#252;zce', '', 'woocommerce', 0),
(1527, 'Arusha', '', 'woocommerce', 0),
(1528, 'Dar es Salaam', '', 'woocommerce', 0),
(1529, 'Dodoma', '', 'woocommerce', 0),
(1530, 'Iringa', '', 'woocommerce', 0),
(1531, 'Kagera', '', 'woocommerce', 0),
(1532, 'Pemba North', '', 'woocommerce', 0),
(1533, 'Zanzibar North', '', 'woocommerce', 0),
(1534, 'Kigoma', '', 'woocommerce', 0),
(1535, 'Kilimanjaro', '', 'woocommerce', 0),
(1536, 'Pemba South', '', 'woocommerce', 0),
(1537, 'Zanzibar South', '', 'woocommerce', 0),
(1538, 'Lindi', '', 'woocommerce', 0),
(1539, 'Mara', '', 'woocommerce', 0),
(1540, 'Mbeya', '', 'woocommerce', 0),
(1541, 'Zanzibar West', '', 'woocommerce', 0),
(1542, 'Morogoro', '', 'woocommerce', 0),
(1543, 'Mtwara', '', 'woocommerce', 0),
(1544, 'Mwanza', '', 'woocommerce', 0),
(1545, 'Coast', '', 'woocommerce', 0),
(1546, 'Rukwa', '', 'woocommerce', 0),
(1547, 'Ruvuma', '', 'woocommerce', 0),
(1548, 'Shinyanga', '', 'woocommerce', 0),
(1549, 'Singida', '', 'woocommerce', 0),
(1550, 'Tabora', '', 'woocommerce', 0),
(1551, 'Tanga', '', 'woocommerce', 0),
(1552, 'Manyara', '', 'woocommerce', 0),
(1553, 'Geita', '', 'woocommerce', 0),
(1554, 'Katavi', '', 'woocommerce', 0),
(1555, 'Njombe', '', 'woocommerce', 0),
(1556, 'Simiyu', '', 'woocommerce', 0),
(1557, 'Abim', '', 'woocommerce', 0),
(1558, 'Adjumani', '', 'woocommerce', 0),
(1559, 'Agago', '', 'woocommerce', 0),
(1560, 'Alebtong', '', 'woocommerce', 0),
(1561, 'Amolatar', '', 'woocommerce', 0),
(1562, 'Amudat', '', 'woocommerce', 0),
(1563, 'Amuria', '', 'woocommerce', 0),
(1564, 'Amuru', '', 'woocommerce', 0),
(1565, 'Apac', '', 'woocommerce', 0),
(1566, 'Arua', '', 'woocommerce', 0),
(1567, 'Budaka', '', 'woocommerce', 0),
(1568, 'Bududa', '', 'woocommerce', 0),
(1569, 'Bugiri', '', 'woocommerce', 0),
(1570, 'Bugweri', '', 'woocommerce', 0),
(1571, 'Buhweju', '', 'woocommerce', 0),
(1572, 'Buikwe', '', 'woocommerce', 0),
(1573, 'Bukedea', '', 'woocommerce', 0),
(1574, 'Bukomansimbi', '', 'woocommerce', 0),
(1575, 'Bukwa', '', 'woocommerce', 0),
(1576, 'Bulambuli', '', 'woocommerce', 0),
(1577, 'Buliisa', '', 'woocommerce', 0),
(1578, 'Bundibugyo', '', 'woocommerce', 0),
(1579, 'Bunyangabu', '', 'woocommerce', 0),
(1580, 'Bushenyi', '', 'woocommerce', 0),
(1581, 'Butaleja', '', 'woocommerce', 0),
(1582, 'Butambala', '', 'woocommerce', 0),
(1583, 'Butebo', '', 'woocommerce', 0),
(1584, 'Buvuma', '', 'woocommerce', 0),
(1585, 'Buyende', '', 'woocommerce', 0),
(1586, 'Dokolo', '', 'woocommerce', 0),
(1587, 'Gomba', '', 'woocommerce', 0),
(1588, 'Gulu', '', 'woocommerce', 0),
(1589, 'Hoima', '', 'woocommerce', 0),
(1590, 'Ibanda', '', 'woocommerce', 0),
(1591, 'Iganga', '', 'woocommerce', 0),
(1592, 'Isingiro', '', 'woocommerce', 0),
(1593, 'Jinja', '', 'woocommerce', 0),
(1594, 'Kaabong', '', 'woocommerce', 0),
(1595, 'Kabale', '', 'woocommerce', 0),
(1596, 'Kabarole', '', 'woocommerce', 0),
(1597, 'Kaberamaido', '', 'woocommerce', 0),
(1598, 'Kagadi', '', 'woocommerce', 0),
(1599, 'Kakumiro', '', 'woocommerce', 0),
(1600, 'Kalangala', '', 'woocommerce', 0),
(1601, 'Kaliro', '', 'woocommerce', 0),
(1602, 'Kalungu', '', 'woocommerce', 0),
(1603, 'Kampala', '', 'woocommerce', 0),
(1604, 'Kamuli', '', 'woocommerce', 0),
(1605, 'Kamwenge', '', 'woocommerce', 0),
(1606, 'Kanungu', '', 'woocommerce', 0),
(1607, 'Kapchorwa', '', 'woocommerce', 0),
(1608, 'Kapelebyong', '', 'woocommerce', 0),
(1609, 'Kasanda', '', 'woocommerce', 0),
(1610, 'Kasese', '', 'woocommerce', 0),
(1611, 'Katakwi', '', 'woocommerce', 0),
(1612, 'Kayunga', '', 'woocommerce', 0),
(1613, 'Kibaale', '', 'woocommerce', 0),
(1614, 'Kiboga', '', 'woocommerce', 0),
(1615, 'Kibuku', '', 'woocommerce', 0),
(1616, 'Kikuube', '', 'woocommerce', 0),
(1617, 'Kiruhura', '', 'woocommerce', 0),
(1618, 'Kiryandongo', '', 'woocommerce', 0),
(1619, 'Kisoro', '', 'woocommerce', 0),
(1620, 'Kitgum', '', 'woocommerce', 0),
(1621, 'Koboko', '', 'woocommerce', 0),
(1622, 'Kole', '', 'woocommerce', 0),
(1623, 'Kotido', '', 'woocommerce', 0),
(1624, 'Kumi', '', 'woocommerce', 0),
(1625, 'Kwania', '', 'woocommerce', 0),
(1626, 'Kween', '', 'woocommerce', 0),
(1627, 'Kyankwanzi', '', 'woocommerce', 0),
(1628, 'Kyegegwa', '', 'woocommerce', 0),
(1629, 'Kyenjojo', '', 'woocommerce', 0),
(1630, 'Kyotera', '', 'woocommerce', 0),
(1631, 'Lamwo', '', 'woocommerce', 0),
(1632, 'Lira', '', 'woocommerce', 0),
(1633, 'Luuka', '', 'woocommerce', 0),
(1634, 'Luwero', '', 'woocommerce', 0),
(1635, 'Lwengo', '', 'woocommerce', 0),
(1636, 'Lyantonde', '', 'woocommerce', 0),
(1637, 'Manafwa', '', 'woocommerce', 0),
(1638, 'Maracha', '', 'woocommerce', 0),
(1639, 'Masaka', '', 'woocommerce', 0),
(1640, 'Masindi', '', 'woocommerce', 0),
(1641, 'Mayuge', '', 'woocommerce', 0),
(1642, 'Mbale', '', 'woocommerce', 0),
(1643, 'Mbarara', '', 'woocommerce', 0),
(1644, 'Mitooma', '', 'woocommerce', 0),
(1645, 'Mityana', '', 'woocommerce', 0),
(1646, 'Moroto', '', 'woocommerce', 0),
(1647, 'Moyo', '', 'woocommerce', 0),
(1648, 'Mpigi', '', 'woocommerce', 0),
(1649, 'Mubende', '', 'woocommerce', 0),
(1650, 'Mukono', '', 'woocommerce', 0),
(1651, 'Nabilatuk', '', 'woocommerce', 0),
(1652, 'Nakapiripirit', '', 'woocommerce', 0),
(1653, 'Nakaseke', '', 'woocommerce', 0),
(1654, 'Nakasongola', '', 'woocommerce', 0),
(1655, 'Namayingo', '', 'woocommerce', 0),
(1656, 'Namisindwa', '', 'woocommerce', 0),
(1657, 'Namutumba', '', 'woocommerce', 0),
(1658, 'Napak', '', 'woocommerce', 0),
(1659, 'Nebbi', '', 'woocommerce', 0),
(1660, 'Ngora', '', 'woocommerce', 0),
(1661, 'Ntoroko', '', 'woocommerce', 0),
(1662, 'Ntungamo', '', 'woocommerce', 0),
(1663, 'Nwoya', '', 'woocommerce', 0),
(1664, 'Omoro', '', 'woocommerce', 0),
(1665, 'Otuke', '', 'woocommerce', 0),
(1666, 'Oyam', '', 'woocommerce', 0),
(1667, 'Pader', '', 'woocommerce', 0),
(1668, 'Pakwach', '', 'woocommerce', 0),
(1669, 'Pallisa', '', 'woocommerce', 0),
(1670, 'Rakai', '', 'woocommerce', 0),
(1671, 'Rubanda', '', 'woocommerce', 0),
(1672, 'Rubirizi', '', 'woocommerce', 0),
(1673, 'Rukiga', '', 'woocommerce', 0),
(1674, 'Rukungiri', '', 'woocommerce', 0),
(1675, 'Sembabule', '', 'woocommerce', 0),
(1676, 'Serere', '', 'woocommerce', 0),
(1677, 'Sheema', '', 'woocommerce', 0),
(1678, 'Sironko', '', 'woocommerce', 0),
(1679, 'Soroti', '', 'woocommerce', 0),
(1680, 'Tororo', '', 'woocommerce', 0),
(1681, 'Wakiso', '', 'woocommerce', 0),
(1682, 'Yumbe', '', 'woocommerce', 0),
(1683, 'Zombo', '', 'woocommerce', 0),
(1684, 'Baker Island', '', 'woocommerce', 0),
(1685, 'Howland Island', '', 'woocommerce', 0),
(1686, 'Jarvis Island', '', 'woocommerce', 0),
(1687, 'Johnston Atoll', '', 'woocommerce', 0),
(1688, 'Kingman Reef', '', 'woocommerce', 0),
(1689, 'Midway Atoll', '', 'woocommerce', 0),
(1690, 'Navassa Island', '', 'woocommerce', 0),
(1691, 'Palmyra Atoll', '', 'woocommerce', 0),
(1692, 'Wake Island', '', 'woocommerce', 0),
(1693, 'Alabama', '', 'woocommerce', 0),
(1694, 'Alaska', '', 'woocommerce', 0),
(1695, 'Arizona', '', 'woocommerce', 0),
(1696, 'Arkansas', '', 'woocommerce', 0),
(1697, 'California', '', 'woocommerce', 0),
(1698, 'Colorado', '', 'woocommerce', 0),
(1699, 'Connecticut', '', 'woocommerce', 0),
(1700, 'Delaware', '', 'woocommerce', 0),
(1701, 'District Of Columbia', '', 'woocommerce', 0),
(1702, 'Florida', '', 'woocommerce', 0),
(1703, 'Georgia', '', 'woocommerce', 0),
(1704, 'Hawaii', '', 'woocommerce', 0),
(1705, 'Idaho', '', 'woocommerce', 0),
(1706, 'Illinois', '', 'woocommerce', 0),
(1707, 'Indiana', '', 'woocommerce', 0),
(1708, 'Iowa', '', 'woocommerce', 0),
(1709, 'Kansas', '', 'woocommerce', 0),
(1710, 'Kentucky', '', 'woocommerce', 0),
(1711, 'Louisiana', '', 'woocommerce', 0),
(1712, 'Maine', '', 'woocommerce', 0),
(1713, 'Massachusetts', '', 'woocommerce', 0),
(1714, 'Michigan', '', 'woocommerce', 0),
(1715, 'Minnesota', '', 'woocommerce', 0),
(1716, 'Mississippi', '', 'woocommerce', 0),
(1717, 'Missouri', '', 'woocommerce', 0),
(1718, 'Nebraska', '', 'woocommerce', 0),
(1719, 'Nevada', '', 'woocommerce', 0),
(1720, 'New Hampshire', '', 'woocommerce', 0),
(1721, 'New Jersey', '', 'woocommerce', 0),
(1722, 'New Mexico', '', 'woocommerce', 0),
(1723, 'New York', '', 'woocommerce', 0),
(1724, 'North Carolina', '', 'woocommerce', 0),
(1725, 'North Dakota', '', 'woocommerce', 0),
(1726, 'Ohio', '', 'woocommerce', 0),
(1727, 'Oklahoma', '', 'woocommerce', 0),
(1728, 'Oregon', '', 'woocommerce', 0),
(1729, 'Pennsylvania', '', 'woocommerce', 0),
(1730, 'Rhode Island', '', 'woocommerce', 0),
(1731, 'South Carolina', '', 'woocommerce', 0),
(1732, 'South Dakota', '', 'woocommerce', 0),
(1733, 'Tennessee', '', 'woocommerce', 0),
(1734, 'Texas', '', 'woocommerce', 0),
(1735, 'Utah', '', 'woocommerce', 0),
(1736, 'Vermont', '', 'woocommerce', 0),
(1737, 'Virginia', '', 'woocommerce', 0),
(1738, 'Washington', '', 'woocommerce', 0),
(1739, 'West Virginia', '', 'woocommerce', 0),
(1740, 'Wisconsin', '', 'woocommerce', 0),
(1741, 'Wyoming', '', 'woocommerce', 0),
(1742, 'Armed Forces (AA)', '', 'woocommerce', 0),
(1743, 'Armed Forces (AE)', '', 'woocommerce', 0),
(1744, 'Armed Forces (AP)', '', 'woocommerce', 0),
(1745, 'Eastern Cape', '', 'woocommerce', 0),
(1746, 'Free State', '', 'woocommerce', 0),
(1747, 'Gauteng', '', 'woocommerce', 0),
(1748, 'KwaZulu-Natal', '', 'woocommerce', 0),
(1749, 'Limpopo', '', 'woocommerce', 0),
(1750, 'Mpumalanga', '', 'woocommerce', 0),
(1751, 'Northern Cape', '', 'woocommerce', 0),
(1752, 'North West', '', 'woocommerce', 0),
(1753, 'Western Cape', '', 'woocommerce', 0),
(1754, 'Luapula', '', 'woocommerce', 0),
(1755, 'North-Western', '', 'woocommerce', 0),
(1756, 'Southern', '', 'woocommerce', 0),
(1757, 'Copperbelt', '', 'woocommerce', 0),
(1758, 'Lusaka', '', 'woocommerce', 0),
(1759, 'Muchinga', '', 'woocommerce', 0),
(1760, 'Select an option&hellip;', '', 'woocommerce', 0),
(1761, 'No matches found', '', 'woocommerce', 0),
(1762, 'Loading failed', '', 'woocommerce', 0),
(1763, 'Please enter 1 or more characters', '', 'woocommerce', 0),
(1764, 'Please enter %qty% or more characters', '', 'woocommerce', 0),
(1765, 'Please delete 1 character', '', 'woocommerce', 0),
(1766, 'Please delete %qty% characters', '', 'woocommerce', 0),
(1767, 'You can only select 1 item', '', 'woocommerce', 0),
(1768, 'You can only select %qty% items', '', 'woocommerce', 0),
(1769, 'Loading more results&hellip;', '', 'woocommerce', 0),
(1770, 'Searching&hellip;', '', 'woocommerce', 0),
(1771, 'Please enter a stronger password.', '', 'woocommerce', 0),
(1772, 'Lost password', '', 'woocommerce', 0),
(1773, 'Lost your password? Please enter your username or email address. You will receive a link to create a new password via email.', '', 'woocommerce', 0),
(1774, 'Username or email', '', 'woocommerce', 0),
(1775, 'Someone has requested a new password for the following account on %s:', '', 'woocommerce', 0),
(1776, 'Username: %s', '', 'woocommerce', 0),
(1777, 'If you didn\'t make this request, just ignore this email. If you\'d like to proceed:', '', 'woocommerce', 0),
(1778, 'Click here to reset your password', '', 'woocommerce', 0),
(1779, 'Password reset email has been sent.', '', 'woocommerce', 0),
(1780, 'A password reset email has been sent to the email address on file for your account, but may take several minutes to show up in your inbox. Please wait at least 10 minutes before attempting another reset.', '', 'woocommerce', 0),
(1781, 'Display name', '', 'woocommerce', 0),
(1782, 'This will be how your name will be displayed in the account section and in reviews', '', 'woocommerce', 0),
(1783, 'Password change', '', 'woocommerce', 0),
(1784, 'Current password (leave blank to leave unchanged)', '', 'woocommerce', 0),
(1785, 'New password (leave blank to leave unchanged)', '', 'woocommerce', 0),
(1786, 'Confirm new password', '', 'woocommerce', 0),
(1787, 'Save changes', '', 'woocommerce', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_trp_original_meta`
--

CREATE TABLE `wp_trp_original_meta` (
  `meta_id` bigint(20) NOT NULL,
  `original_id` bigint(20) NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_trp_original_meta`
--

INSERT INTO `wp_trp_original_meta` (`meta_id`, `original_id`, `meta_key`, `meta_value`) VALUES
(1, 9, 'post_parent_id', '26'),
(2, 10, 'post_parent_id', '26'),
(3, 11, 'post_parent_id', '26'),
(4, 12, 'post_parent_id', '26'),
(5, 13, 'post_parent_id', '26'),
(6, 14, 'post_parent_id', '26'),
(7, 15, 'post_parent_id', '26'),
(8, 16, 'post_parent_id', '26'),
(9, 17, 'post_parent_id', '26'),
(10, 18, 'post_parent_id', '26'),
(11, 25, 'post_parent_id', '26'),
(12, 19, 'post_parent_id', '26'),
(13, 20, 'post_parent_id', '26'),
(14, 21, 'post_parent_id', '26'),
(15, 22, 'post_parent_id', '26'),
(16, 26, 'post_parent_id', '26'),
(17, 27, 'post_parent_id', '26'),
(18, 28, 'post_parent_id', '26'),
(19, 29, 'post_parent_id', '26'),
(20, 30, 'post_parent_id', '26'),
(21, 31, 'post_parent_id', '26'),
(22, 19, 'post_parent_id', '84'),
(23, 20, 'post_parent_id', '82'),
(24, 21, 'post_parent_id', '39'),
(25, 22, 'post_parent_id', '38'),
(26, 23, 'post_parent_id', '37'),
(27, 24, 'post_parent_id', '36');

-- --------------------------------------------------------

--
-- Table structure for table `wp_trp_original_strings`
--

CREATE TABLE `wp_trp_original_strings` (
  `id` bigint(20) NOT NULL,
  `original` text COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_trp_original_strings`
--

INSERT INTO `wp_trp_original_strings` (`id`, `original`) VALUES
(1, 'My Account'),
(2, 'Cart'),
(3, 'Blog'),
(4, 'Checkout'),
(5, 'My account'),
(6, 'Refer &#038; Earn'),
(7, 'Sample Page'),
(8, 'Shop'),
(9, 'SPEND MORE...'),
(10, 'SAVE MORE...'),
(11, '$100 OFF ON EVERY $950 SPENT'),
(12, 'SHOP BY CATEGORY'),
(13, 'test purpose only test purpose only test purpose only'),
(14, 'Comics'),
(15, 'Shop Astrology Art'),
(16, 'Shop Cultural Art'),
(17, 'Shop Motivational Art'),
(18, 'New Arrivals'),
(19, 'Elevate Yourself'),
(20, 'Mind your business'),
(21, 'Nipsey hussle'),
(22, 'Shine like a greek god'),
(23, 'Erotica &#8211; You&#8217;re Mine'),
(24, 'Separate Yourself to elevate yourself'),
(25, 'BEST SALES'),
(26, 'BUY NOW. PAY LATER WITH'),
(27, 'QUAD PAY'),
(28, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'),
(29, 'Already Sold'),
(30, 'Pre Orders'),
(31, 'Only for Order Over'),
(32, 'Links'),
(33, 'Contact Us'),
(34, 'Email: support@framehood.com'),
(35, 'Address: 3 Lylia Dr.'),
(36, 'West New York, NJ 07093'),
(37, '&copy; wallart 2020'),
(38, 'http://localhost/wallart/wp-content/uploads/2020/09/cropped-logo.jpg'),
(39, 'http://localhost/wallart/wp-content/uploads/2020/09/banner1-1.jpg'),
(40, 'http://localhost/wallart/wp-content/uploads/2020/09/category_1.jpg'),
(41, 'http://localhost/wallart/wp-content/uploads/2020/09/category_3.jpg'),
(42, 'http://localhost/wallart/wp-content/uploads/2020/09/category_2.jpg'),
(43, 'http://localhost/wallart/wp-content/uploads/2020/09/img_7.jpg'),
(44, 'http://localhost/wallart/wp-content/uploads/2020/09/img_8.jpg'),
(45, 'http://localhost/wallart/wp-content/uploads/2020/09/img_1.jpg'),
(46, 'http://localhost/wallart/wp-content/uploads/2020/09/img_2.jpg'),
(47, 'http://localhost/wallart/wp-content/uploads/2020/09/img_3.jpg'),
(48, 'http://localhost/wallart/wp-content/uploads/2020/09/img_4.jpg'),
(49, 'http://localhost/wallart/wp-content/uploads/2020/09/img_7-324x324.jpg'),
(50, 'http://localhost/wallart/wp-content/uploads/2020/09/img_8-324x324.jpg'),
(51, 'http://localhost/wallart/wp-content/uploads/2020/09/img_1-324x324.jpg'),
(52, 'http://localhost/wallart/wp-content/uploads/2020/09/img_2-324x324.jpg'),
(53, 'http://localhost/wallart/wp-content/uploads/2020/09/quad-pay-banner.jpg'),
(54, 'Share on Facebook'),
(55, 'Share on Twitter'),
(56, 'Share on Google+'),
(57, 'https://www.facebook.com/sharer/sharer.php?u=http://localhost/wallart/?trp-edit-translation=preview'),
(58, 'https://twitter.com/share?url=http://localhost/wallart/?trp-edit-translation=preview&text=Share Buttons Demo&via=sunnyismoi'),
(59, 'https://plus.google.com/share?url=http://localhost/wallart/?trp-edit-translation=preview'),
(60, '₹'),
(61, '₹'),
(62, 'https://www.facebook.com/sharer/sharer.php?u=http://localhost/wallart/ar/?trp-edit-translation=preview'),
(63, 'https://twitter.com/share?url=http://localhost/wallart/ar/?trp-edit-translation=preview&text=Share Buttons Demo&via=sunnyismoi'),
(64, 'https://plus.google.com/share?url=http://localhost/wallart/ar/?trp-edit-translation=preview'),
(65, 'https://www.facebook.com/sharer/sharer.php?u=http://localhost/wallart/ar/'),
(66, 'https://twitter.com/share?url=http://localhost/wallart/ar/&text=Share Buttons Demo&via=sunnyismoi'),
(67, 'https://plus.google.com/share?url=http://localhost/wallart/ar/'),
(68, 'This is an example page. It&#8217;s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:'),
(69, 'Hi there! I&#8217;m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin&#8217; caught in the rain.)'),
(70, '&#8230;or something like this:'),
(71, 'The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.'),
(72, 'As a new WordPress user, you should go to'),
(73, 'your dashboard'),
(74, 'to delete this page and create new pages for your content. Have fun!'),
(75, 'Hello world!'),
(76, 'Uncategorized'),
(77, 'Home'),
(78, 'FAQ'),
(79, 'Privacy Policy'),
(80, 'Shipping Policy'),
(81, 'Address:'),
(82, '3 Lylia Dr.'),
(83, 'West New York,'),
(84, 'NJ 07093'),
(85, '© 2020 - Wallart - All Rights Reserved');

-- --------------------------------------------------------

--
-- Table structure for table `wp_usermeta`
--

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'admin'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'text_widget_custom_html'),
(15, 1, 'show_welcome_panel', '1'),
(17, 1, 'wp_dashboard_quick_press_last_post_id', '54'),
(18, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:9:\"127.0.0.0\";}'),
(20, 1, '_woocommerce_tracks_anon_id', 'woo:V7BaiyHztJ7x3a3YCvJ9CjNw'),
(21, 1, 'last_update', '1599459273'),
(22, 1, 'woocommerce_admin_activity_panel_inbox_last_read', '1599459226703'),
(23, 1, 'wc_last_active', '1600300800'),
(24, 1, 'session_tokens', 'a:1:{s:64:\"9dc228779a1655afe28fcb3a8e75aaf252f0b45e37be3b0ec8435eee0a6410e9\";a:4:{s:10:\"expiration\";i:1600520171;s:2:\"ip\";s:9:\"127.0.0.1\";s:2:\"ua\";s:76:\"Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/70.0\";s:5:\"login\";i:1600347371;}}'),
(25, 1, '_order_count', '0'),
(27, 1, 'wp_user-settings', 'libraryContent=browse&editor=html'),
(28, 1, 'wp_user-settings-time', '1600261945'),
(30, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(31, 1, 'metaboxhidden_nav-menus', 'a:4:{i:0;s:21:\"add-post-type-product\";i:1;s:12:\"add-post_tag\";i:2;s:15:\"add-product_cat\";i:3;s:15:\"add-product_tag\";}'),
(33, 1, 'nav_menu_recently_edited', '24'),
(35, 1, 'dismissed_no_secure_connection_notice', '1'),
(36, 1, 'closedpostboxes_product', 'a:0:{}'),
(37, 1, 'metaboxhidden_product', 'a:2:{i:0;s:10:\"postcustom\";i:1;s:7:\"slugdiv\";}'),
(39, 2, 'nickname', 'pramodinidas100'),
(40, 2, 'first_name', 'Pramodini'),
(41, 2, 'last_name', 'Das'),
(42, 2, 'description', ''),
(43, 2, 'rich_editing', 'true'),
(44, 2, 'syntax_highlighting', 'true'),
(45, 2, 'comment_shortcuts', 'false'),
(46, 2, 'admin_color', 'fresh'),
(47, 2, 'use_ssl', '0'),
(48, 2, 'show_admin_bar_front', 'true'),
(49, 2, 'locale', 'en_US'),
(50, 2, 'wp_capabilities', 'a:1:{s:8:\"customer\";b:1;}'),
(51, 2, 'wp_user_level', '0'),
(52, 2, 'last_update', '1600262808'),
(54, 2, 'wc_last_active', '1600214400'),
(55, 3, 'nickname', 'manageadmin'),
(56, 3, 'first_name', 'Pramodini'),
(57, 3, 'last_name', 'Das'),
(58, 3, 'description', ''),
(59, 3, 'rich_editing', 'true'),
(60, 3, 'syntax_highlighting', 'true'),
(61, 3, 'comment_shortcuts', 'false'),
(62, 3, 'admin_color', 'fresh'),
(63, 3, 'use_ssl', '0'),
(64, 3, 'show_admin_bar_front', 'true'),
(65, 3, 'locale', 'en_US'),
(66, 3, 'wp_capabilities', 'a:1:{s:8:\"customer\";b:1;}'),
(67, 3, 'wp_user_level', '0'),
(68, 3, 'last_update', '1600263814'),
(70, 3, 'wc_last_active', '1600214400');

-- --------------------------------------------------------

--
-- Table structure for table `wp_users`
--

CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'admin', '$P$Bo/iEZOCYGdG4PwF.msiiIm3UWa3fN1', 'admin', 'pramodini.das@thoughtspheres.com', 'http://localhost/wallart', '2020-09-06 07:02:23', '', 0, 'admin'),
(2, 'pramodinidas100', '$P$BFRHMyc2.KcJQibGRj/biX4IVaxWx41', 'pramodinidas100', 'pramodinidas100@gmail.com', '', '2020-09-16 13:17:30', '1600262808:$P$BMHhLwHXn4ZAabv0WS0o7a8JaRXUVg.', 0, 'pramodinidas100'),
(3, 'manageadmin', '$P$BmQLOQ7jM2TVgsvYJJoqwo4U3V0rqQ1', 'manageadmin', 'manageadmin@gmail.com', '', '2020-09-16 13:43:32', '', 0, 'manageadmin');

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_admin_notes`
--

CREATE TABLE `wp_wc_admin_notes` (
  `note_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `locale` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `title` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `content_data` longtext COLLATE utf8mb4_unicode_520_ci,
  `status` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `source` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_reminder` datetime DEFAULT NULL,
  `is_snoozable` tinyint(1) NOT NULL DEFAULT '0',
  `layout` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `image` varchar(200) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `icon` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'info'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_wc_admin_notes`
--

INSERT INTO `wp_wc_admin_notes` (`note_id`, `name`, `type`, `locale`, `title`, `content`, `content_data`, `status`, `source`, `date_created`, `date_reminder`, `is_snoozable`, `layout`, `image`, `is_deleted`, `icon`) VALUES
(1, 'wc-admin-wc-helper-connection', 'info', 'en_US', 'Connect to WooCommerce.com', 'Connect to get important product notifications and updates.', '{}', 'unactioned', 'woocommerce-admin', '2020-09-07 06:09:59', NULL, 0, 'plain', '', 0, 'info'),
(2, 'wc-admin-onboarding-email-marketing', 'info', 'en_US', 'Tips, product updates, and inspiration', 'We\'re here for you - get tips, product updates and inspiration straight to your email box', '{}', 'unactioned', 'woocommerce-admin', '2020-09-07 06:10:00', NULL, 0, 'plain', '', 0, 'info'),
(3, 'wc-admin-marketing-intro', 'info', 'en_US', 'Connect with your audience', 'Grow your customer base and increase your sales with marketing tools built for WooCommerce.', '{}', 'unactioned', 'woocommerce-admin', '2020-09-07 06:10:00', NULL, 0, 'plain', '', 0, 'info'),
(4, 'wc-admin-onboarding-profiler-reminder', 'update', 'en_US', 'Welcome to WooCommerce! Set up your store and start selling', 'We\'re here to help you going through the most important steps to get your store up and running.', '{}', 'actioned', 'woocommerce-admin', '2020-09-07 06:11:13', NULL, 0, 'plain', '', 0, 'info'),
(5, 'wc-admin-coupon-page-moved', 'update', 'en_US', 'Coupon management has moved!', 'Coupons can now be managed from Marketing &gt; Coupons. Click the button below to remove the legacy WooCommerce &gt; Coupons menu item.', '{}', 'unactioned', 'woocommerce-admin', '2020-09-07 06:16:24', NULL, 0, 'plain', '', 0, 'info'),
(6, 'wc-admin-mobile-app', 'info', 'en_US', 'Install Woo mobile app', 'Install the WooCommerce mobile app to manage orders, receive sales notifications, and view key metrics — wherever you are.', '{}', 'unactioned', 'woocommerce-admin', '2020-09-09 07:02:32', NULL, 0, 'plain', '', 0, 'info'),
(7, 'wc-admin-choose-niche', 'info', 'en_US', 'How to choose a niche for your online store', 'Your niche defines the products and services you develop. It directs your marketing. It focuses your attention on specific problems facing your customers. It differentiates you from the competition. Learn more about the five guiding principles to define your niche.', '{}', 'unactioned', 'woocommerce-admin', '2020-09-09 07:02:33', NULL, 0, 'plain', '', 0, 'info'),
(8, 'wc-admin-learn-more-about-product-settings', 'info', 'en_US', 'Learn more about Product Settings', 'In this video you\'ll find information about configuring product settings, such as how they are displayed, editing inventory options and so on.', '{}', 'unactioned', 'woocommerce-admin', '2020-09-11 06:10:22', NULL, 0, 'plain', '', 0, 'info'),
(9, 'wc-admin-need-some-inspiration', 'info', 'en_US', 'Do you need some inspiration?', 'Check some of our favorite customer stories from entrepreneurs, agencies, and developers in our global community.', '{}', 'unactioned', 'woocommerce-admin', '2020-09-14 05:19:41', NULL, 0, 'plain', '', 0, 'info'),
(10, 'wc-admin-store-notice-giving-feedback-2', 'info', 'en_US', 'Give feedback', 'Now that you’ve chosen us as a partner, our goal is to make sure we\'re providing the right tools to meet your needs. We\'re looking forward to having your feedback on the store setup experience so we can improve it in the future.', '{}', 'unactioned', 'woocommerce-admin', '2020-09-15 06:10:07', NULL, 0, 'plain', '', 0, 'info');

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_admin_note_actions`
--

CREATE TABLE `wp_wc_admin_note_actions` (
  `action_id` bigint(20) UNSIGNED NOT NULL,
  `note_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `query` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `is_primary` tinyint(1) NOT NULL DEFAULT '0',
  `actioned_text` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_wc_admin_note_actions`
--

INSERT INTO `wp_wc_admin_note_actions` (`action_id`, `note_id`, `name`, `label`, `query`, `status`, `is_primary`, `actioned_text`) VALUES
(1, 1, 'connect', 'Connect', '?page=wc-addons&section=helper', 'unactioned', 0, ''),
(2, 2, 'yes-please', 'Yes please!', 'https://woocommerce.us8.list-manage.com/subscribe/post?u=2c1434dc56f9506bf3c3ecd21&amp;id=13860df971&amp;SIGNUPPAGE=plugin', 'actioned', 0, ''),
(3, 3, 'open-marketing-hub', 'Open marketing hub', 'http://localhost/wallart/wp-admin/admin.php?page=wc-admin&path=/marketing', 'actioned', 0, ''),
(4, 4, 'continue-profiler', 'Continue Store Setup', 'http://localhost/wallart/wp-admin/admin.php?page=wc-admin&enable_onboarding=1', 'unactioned', 1, ''),
(5, 4, 'skip-profiler', 'Skip Setup', 'http://localhost/wallart/wp-admin/admin.php?page=wc-admin&reset_profiler=0', 'actioned', 0, ''),
(6, 5, 'remove-legacy-coupon-menu', 'Remove legacy coupon menu', 'http://localhost/wallart/wp-admin/admin.php?page=wc-admin&action=remove-coupon-menu', 'actioned', 1, ''),
(7, 6, 'learn-more', 'Learn more', 'https://woocommerce.com/mobile/', 'actioned', 0, ''),
(8, 7, 'choose-niche', 'Learn more', 'https://woocommerce.com/posts/how-to-choose-a-niche-online-business/?utm_source=inbox', 'actioned', 1, ''),
(9, 8, 'learn-more-about-product-settings', 'Watch tutorial', 'https://www.youtube.com/watch?v=FEmwJsE8xDY&t=', 'actioned', 1, ''),
(10, 9, 'need-some-inspiration', 'See success stories', 'https://woocommerce.com/success-stories/?utm_source=inbox', 'actioned', 1, ''),
(11, 10, 'share-feedback', 'Share feedback', 'https://automattic.survey.fm/new-onboarding-survey', 'actioned', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_category_lookup`
--

CREATE TABLE `wp_wc_category_lookup` (
  `category_tree_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_wc_category_lookup`
--

INSERT INTO `wp_wc_category_lookup` (`category_tree_id`, `category_id`) VALUES
(15, 15);

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_customer_lookup`
--

CREATE TABLE `wp_wc_customer_lookup` (
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `username` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `first_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `date_last_active` timestamp NULL DEFAULT NULL,
  `date_registered` timestamp NULL DEFAULT NULL,
  `country` char(2) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `postcode` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `city` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `state` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_download_log`
--

CREATE TABLE `wp_wc_download_log` (
  `download_log_id` bigint(20) UNSIGNED NOT NULL,
  `timestamp` datetime NOT NULL,
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `user_ip_address` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_order_coupon_lookup`
--

CREATE TABLE `wp_wc_order_coupon_lookup` (
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `coupon_id` bigint(20) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `discount_amount` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_order_product_lookup`
--

CREATE TABLE `wp_wc_order_product_lookup` (
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `variation_id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `product_qty` int(11) NOT NULL,
  `product_net_revenue` double NOT NULL DEFAULT '0',
  `product_gross_revenue` double NOT NULL DEFAULT '0',
  `coupon_amount` double NOT NULL DEFAULT '0',
  `tax_amount` double NOT NULL DEFAULT '0',
  `shipping_amount` double NOT NULL DEFAULT '0',
  `shipping_tax_amount` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_order_stats`
--

CREATE TABLE `wp_wc_order_stats` (
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_created_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `num_items_sold` int(11) NOT NULL DEFAULT '0',
  `total_sales` double NOT NULL DEFAULT '0',
  `tax_total` double NOT NULL DEFAULT '0',
  `shipping_total` double NOT NULL DEFAULT '0',
  `net_total` double NOT NULL DEFAULT '0',
  `returning_customer` tinyint(1) DEFAULT NULL,
  `status` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_order_tax_lookup`
--

CREATE TABLE `wp_wc_order_tax_lookup` (
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `shipping_tax` double NOT NULL DEFAULT '0',
  `order_tax` double NOT NULL DEFAULT '0',
  `total_tax` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_product_meta_lookup`
--

CREATE TABLE `wp_wc_product_meta_lookup` (
  `product_id` bigint(20) NOT NULL,
  `sku` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT '',
  `virtual` tinyint(1) DEFAULT '0',
  `downloadable` tinyint(1) DEFAULT '0',
  `min_price` decimal(19,4) DEFAULT NULL,
  `max_price` decimal(19,4) DEFAULT NULL,
  `onsale` tinyint(1) DEFAULT '0',
  `stock_quantity` double DEFAULT NULL,
  `stock_status` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT 'instock',
  `rating_count` bigint(20) DEFAULT '0',
  `average_rating` decimal(3,2) DEFAULT '0.00',
  `total_sales` bigint(20) DEFAULT '0',
  `tax_status` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT 'taxable',
  `tax_class` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_wc_product_meta_lookup`
--

INSERT INTO `wp_wc_product_meta_lookup` (`product_id`, `sku`, `virtual`, `downloadable`, `min_price`, `max_price`, `onsale`, `stock_quantity`, `stock_status`, `rating_count`, `average_rating`, `total_sales`, `tax_status`, `tax_class`) VALUES
(28, '', 0, 0, '18.0000', '18.0000', 1, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(29, '', 0, 0, '55.0000', '55.0000', 1, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(30, '', 0, 0, '16.0000', '16.0000', 1, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(31, '', 0, 0, '90.0000', '90.0000', 0, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(32, '', 0, 0, '45.0000', '45.0000', 0, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(33, '', 0, 0, '35.0000', '35.0000', 1, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(34, '', 0, 0, '45.0000', '45.0000', 0, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(35, '', 0, 0, '42.0000', '42.0000', 1, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(36, '', 0, 0, '321.0000', '321.0000', 0, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(37, '', 0, 0, '122.0000', '122.0000', 0, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(38, '', 0, 0, '230.0000', '230.0000', 0, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(39, '', 0, 0, '120.0000', '120.0000', 1, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(82, '', 0, 0, '213.0000', '213.0000', 0, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(84, '', 0, 0, '213.0000', '213.0000', 0, NULL, 'instock', 0, '0.00', 0, 'taxable', '');

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_reserved_stock`
--

CREATE TABLE `wp_wc_reserved_stock` (
  `order_id` bigint(20) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `stock_quantity` double NOT NULL DEFAULT '0',
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `expires` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_tax_rate_classes`
--

CREATE TABLE `wp_wc_tax_rate_classes` (
  `tax_rate_class_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_wc_tax_rate_classes`
--

INSERT INTO `wp_wc_tax_rate_classes` (`tax_rate_class_id`, `name`, `slug`) VALUES
(1, 'Reduced rate', 'reduced-rate'),
(2, 'Zero rate', 'zero-rate');

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_webhooks`
--

CREATE TABLE `wp_wc_webhooks` (
  `webhook_id` bigint(20) UNSIGNED NOT NULL,
  `status` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `name` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `delivery_url` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `secret` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `topic` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_created_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `api_version` smallint(4) NOT NULL,
  `failure_count` smallint(10) NOT NULL DEFAULT '0',
  `pending_delivery` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_api_keys`
--

CREATE TABLE `wp_woocommerce_api_keys` (
  `key_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `description` varchar(200) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `permissions` varchar(10) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `consumer_key` char(64) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `consumer_secret` char(43) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `nonces` longtext COLLATE utf8mb4_unicode_520_ci,
  `truncated_key` char(7) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `last_access` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_attribute_taxonomies`
--

CREATE TABLE `wp_woocommerce_attribute_taxonomies` (
  `attribute_id` bigint(20) UNSIGNED NOT NULL,
  `attribute_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `attribute_label` varchar(200) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `attribute_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `attribute_orderby` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `attribute_public` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_downloadable_product_permissions`
--

CREATE TABLE `wp_woocommerce_downloadable_product_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `download_id` varchar(36) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `order_key` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_email` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `downloads_remaining` varchar(9) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `access_granted` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access_expires` datetime DEFAULT NULL,
  `download_count` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_log`
--

CREATE TABLE `wp_woocommerce_log` (
  `log_id` bigint(20) UNSIGNED NOT NULL,
  `timestamp` datetime NOT NULL,
  `level` smallint(4) NOT NULL,
  `source` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `context` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_order_itemmeta`
--

CREATE TABLE `wp_woocommerce_order_itemmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_order_items`
--

CREATE TABLE `wp_woocommerce_order_items` (
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `order_item_name` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `order_item_type` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `order_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_payment_tokenmeta`
--

CREATE TABLE `wp_woocommerce_payment_tokenmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `payment_token_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_payment_tokens`
--

CREATE TABLE `wp_woocommerce_payment_tokens` (
  `token_id` bigint(20) UNSIGNED NOT NULL,
  `gateway_id` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `token` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `type` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_sessions`
--

CREATE TABLE `wp_woocommerce_sessions` (
  `session_id` bigint(20) UNSIGNED NOT NULL,
  `session_key` char(32) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `session_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `session_expiry` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_woocommerce_sessions`
--

INSERT INTO `wp_woocommerce_sessions` (`session_id`, `session_key`, `session_value`, `session_expiry`) VALUES
(1, '1', 'a:7:{s:4:\"cart\";s:6:\"a:0:{}\";s:11:\"cart_totals\";s:367:\"a:15:{s:8:\"subtotal\";i:0;s:12:\"subtotal_tax\";i:0;s:14:\"shipping_total\";i:0;s:12:\"shipping_tax\";i:0;s:14:\"shipping_taxes\";a:0:{}s:14:\"discount_total\";i:0;s:12:\"discount_tax\";i:0;s:19:\"cart_contents_total\";i:0;s:17:\"cart_contents_tax\";i:0;s:19:\"cart_contents_taxes\";a:0:{}s:9:\"fee_total\";i:0;s:7:\"fee_tax\";i:0;s:9:\"fee_taxes\";a:0:{}s:5:\"total\";i:0;s:9:\"total_tax\";i:0;}\";s:15:\"applied_coupons\";s:6:\"a:0:{}\";s:22:\"coupon_discount_totals\";s:6:\"a:0:{}\";s:26:\"coupon_discount_tax_totals\";s:6:\"a:0:{}\";s:21:\"removed_cart_contents\";s:6:\"a:0:{}\";s:8:\"customer\";s:750:\"a:26:{s:2:\"id\";s:1:\"1\";s:13:\"date_modified\";s:25:\"2020-09-07T06:14:33+00:00\";s:8:\"postcode\";s:0:\"\";s:4:\"city\";s:0:\"\";s:9:\"address_1\";s:0:\"\";s:7:\"address\";s:0:\"\";s:9:\"address_2\";s:0:\"\";s:5:\"state\";s:2:\"OR\";s:7:\"country\";s:2:\"IN\";s:17:\"shipping_postcode\";s:0:\"\";s:13:\"shipping_city\";s:0:\"\";s:18:\"shipping_address_1\";s:0:\"\";s:16:\"shipping_address\";s:0:\"\";s:18:\"shipping_address_2\";s:0:\"\";s:14:\"shipping_state\";s:2:\"OR\";s:16:\"shipping_country\";s:2:\"IN\";s:13:\"is_vat_exempt\";s:0:\"\";s:19:\"calculated_shipping\";s:0:\"\";s:10:\"first_name\";s:0:\"\";s:9:\"last_name\";s:0:\"\";s:7:\"company\";s:0:\"\";s:5:\"phone\";s:0:\"\";s:5:\"email\";s:32:\"pramodini.das@thoughtspheres.com\";s:19:\"shipping_first_name\";s:0:\"\";s:18:\"shipping_last_name\";s:0:\"\";s:16:\"shipping_company\";s:0:\"\";}\";}', 1600520176);

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_shipping_zones`
--

CREATE TABLE `wp_woocommerce_shipping_zones` (
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `zone_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `zone_order` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_shipping_zone_locations`
--

CREATE TABLE `wp_woocommerce_shipping_zone_locations` (
  `location_id` bigint(20) UNSIGNED NOT NULL,
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `location_code` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `location_type` varchar(40) COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_shipping_zone_methods`
--

CREATE TABLE `wp_woocommerce_shipping_zone_methods` (
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `instance_id` bigint(20) UNSIGNED NOT NULL,
  `method_id` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `method_order` bigint(20) UNSIGNED NOT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_tax_rates`
--

CREATE TABLE `wp_woocommerce_tax_rates` (
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_country` varchar(2) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate_state` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate` varchar(8) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate_priority` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_compound` int(1) NOT NULL DEFAULT '0',
  `tax_rate_shipping` int(1) NOT NULL DEFAULT '1',
  `tax_rate_order` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_class` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_tax_rate_locations`
--

CREATE TABLE `wp_woocommerce_tax_rate_locations` (
  `location_id` bigint(20) UNSIGNED NOT NULL,
  `location_code` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `location_type` varchar(40) COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp_actionscheduler_actions`
--
ALTER TABLE `wp_actionscheduler_actions`
  ADD PRIMARY KEY (`action_id`),
  ADD KEY `hook` (`hook`),
  ADD KEY `status` (`status`),
  ADD KEY `scheduled_date_gmt` (`scheduled_date_gmt`),
  ADD KEY `args` (`args`),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `last_attempt_gmt` (`last_attempt_gmt`),
  ADD KEY `claim_id` (`claim_id`);

--
-- Indexes for table `wp_actionscheduler_claims`
--
ALTER TABLE `wp_actionscheduler_claims`
  ADD PRIMARY KEY (`claim_id`),
  ADD KEY `date_created_gmt` (`date_created_gmt`);

--
-- Indexes for table `wp_actionscheduler_groups`
--
ALTER TABLE `wp_actionscheduler_groups`
  ADD PRIMARY KEY (`group_id`),
  ADD KEY `slug` (`slug`(191));

--
-- Indexes for table `wp_actionscheduler_logs`
--
ALTER TABLE `wp_actionscheduler_logs`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `action_id` (`action_id`),
  ADD KEY `log_date_gmt` (`log_date_gmt`);

--
-- Indexes for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10)),
  ADD KEY `woo_idx_comment_type` (`comment_type`);

--
-- Indexes for table `wp_hugeit_slider_slide`
--
ALTER TABLE `wp_hugeit_slider_slide`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_hugeit_slider_slider`
--
ALTER TABLE `wp_hugeit_slider_slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `wp_mltlngg_terms_translate`
--
ALTER TABLE `wp_mltlngg_terms_translate`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `wp_mltlngg_translate`
--
ALTER TABLE `wp_mltlngg_translate`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`),
  ADD KEY `autoload` (`autoload`);

--
-- Indexes for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `wp_trp_dictionary_en_us_ar`
--
ALTER TABLE `wp_trp_dictionary_en_us_ar`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `index_name` (`original`(100));
ALTER TABLE `wp_trp_dictionary_en_us_ar` ADD FULLTEXT KEY `original_fulltext` (`original`);

--
-- Indexes for table `wp_trp_gettext_ar`
--
ALTER TABLE `wp_trp_gettext_ar`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `index_name` (`original`(100));
ALTER TABLE `wp_trp_gettext_ar` ADD FULLTEXT KEY `original_fulltext` (`original`);

--
-- Indexes for table `wp_trp_gettext_en_us`
--
ALTER TABLE `wp_trp_gettext_en_us`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `index_name` (`original`(100));
ALTER TABLE `wp_trp_gettext_en_us` ADD FULLTEXT KEY `original_fulltext` (`original`);

--
-- Indexes for table `wp_trp_original_meta`
--
ALTER TABLE `wp_trp_original_meta`
  ADD PRIMARY KEY (`meta_id`),
  ADD UNIQUE KEY `meta_id` (`meta_id`),
  ADD KEY `index_original_id` (`original_id`),
  ADD KEY `meta_key` (`meta_key`);

--
-- Indexes for table `wp_trp_original_strings`
--
ALTER TABLE `wp_trp_original_strings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_original` (`original`(100));

--
-- Indexes for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- Indexes for table `wp_wc_admin_notes`
--
ALTER TABLE `wp_wc_admin_notes`
  ADD PRIMARY KEY (`note_id`);

--
-- Indexes for table `wp_wc_admin_note_actions`
--
ALTER TABLE `wp_wc_admin_note_actions`
  ADD PRIMARY KEY (`action_id`),
  ADD KEY `note_id` (`note_id`);

--
-- Indexes for table `wp_wc_category_lookup`
--
ALTER TABLE `wp_wc_category_lookup`
  ADD PRIMARY KEY (`category_tree_id`,`category_id`);

--
-- Indexes for table `wp_wc_customer_lookup`
--
ALTER TABLE `wp_wc_customer_lookup`
  ADD PRIMARY KEY (`customer_id`),
  ADD UNIQUE KEY `user_id` (`user_id`),
  ADD KEY `email` (`email`);

--
-- Indexes for table `wp_wc_download_log`
--
ALTER TABLE `wp_wc_download_log`
  ADD PRIMARY KEY (`download_log_id`),
  ADD KEY `permission_id` (`permission_id`),
  ADD KEY `timestamp` (`timestamp`);

--
-- Indexes for table `wp_wc_order_coupon_lookup`
--
ALTER TABLE `wp_wc_order_coupon_lookup`
  ADD PRIMARY KEY (`order_id`,`coupon_id`),
  ADD KEY `coupon_id` (`coupon_id`),
  ADD KEY `date_created` (`date_created`);

--
-- Indexes for table `wp_wc_order_product_lookup`
--
ALTER TABLE `wp_wc_order_product_lookup`
  ADD PRIMARY KEY (`order_item_id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `date_created` (`date_created`);

--
-- Indexes for table `wp_wc_order_stats`
--
ALTER TABLE `wp_wc_order_stats`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `date_created` (`date_created`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `status` (`status`(191));

--
-- Indexes for table `wp_wc_order_tax_lookup`
--
ALTER TABLE `wp_wc_order_tax_lookup`
  ADD PRIMARY KEY (`order_id`,`tax_rate_id`),
  ADD KEY `tax_rate_id` (`tax_rate_id`),
  ADD KEY `date_created` (`date_created`);

--
-- Indexes for table `wp_wc_product_meta_lookup`
--
ALTER TABLE `wp_wc_product_meta_lookup`
  ADD PRIMARY KEY (`product_id`),
  ADD KEY `virtual` (`virtual`),
  ADD KEY `downloadable` (`downloadable`),
  ADD KEY `stock_status` (`stock_status`),
  ADD KEY `stock_quantity` (`stock_quantity`),
  ADD KEY `onsale` (`onsale`),
  ADD KEY `min_max_price` (`min_price`,`max_price`);

--
-- Indexes for table `wp_wc_reserved_stock`
--
ALTER TABLE `wp_wc_reserved_stock`
  ADD PRIMARY KEY (`order_id`,`product_id`);

--
-- Indexes for table `wp_wc_tax_rate_classes`
--
ALTER TABLE `wp_wc_tax_rate_classes`
  ADD PRIMARY KEY (`tax_rate_class_id`),
  ADD UNIQUE KEY `slug` (`slug`(191));

--
-- Indexes for table `wp_wc_webhooks`
--
ALTER TABLE `wp_wc_webhooks`
  ADD PRIMARY KEY (`webhook_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `wp_woocommerce_api_keys`
--
ALTER TABLE `wp_woocommerce_api_keys`
  ADD PRIMARY KEY (`key_id`),
  ADD KEY `consumer_key` (`consumer_key`),
  ADD KEY `consumer_secret` (`consumer_secret`);

--
-- Indexes for table `wp_woocommerce_attribute_taxonomies`
--
ALTER TABLE `wp_woocommerce_attribute_taxonomies`
  ADD PRIMARY KEY (`attribute_id`),
  ADD KEY `attribute_name` (`attribute_name`(20));

--
-- Indexes for table `wp_woocommerce_downloadable_product_permissions`
--
ALTER TABLE `wp_woocommerce_downloadable_product_permissions`
  ADD PRIMARY KEY (`permission_id`),
  ADD KEY `download_order_key_product` (`product_id`,`order_id`,`order_key`(16),`download_id`),
  ADD KEY `download_order_product` (`download_id`,`order_id`,`product_id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `user_order_remaining_expires` (`user_id`,`order_id`,`downloads_remaining`,`access_expires`);

--
-- Indexes for table `wp_woocommerce_log`
--
ALTER TABLE `wp_woocommerce_log`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `level` (`level`);

--
-- Indexes for table `wp_woocommerce_order_itemmeta`
--
ALTER TABLE `wp_woocommerce_order_itemmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `order_item_id` (`order_item_id`),
  ADD KEY `meta_key` (`meta_key`(32));

--
-- Indexes for table `wp_woocommerce_order_items`
--
ALTER TABLE `wp_woocommerce_order_items`
  ADD PRIMARY KEY (`order_item_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `wp_woocommerce_payment_tokenmeta`
--
ALTER TABLE `wp_woocommerce_payment_tokenmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `payment_token_id` (`payment_token_id`),
  ADD KEY `meta_key` (`meta_key`(32));

--
-- Indexes for table `wp_woocommerce_payment_tokens`
--
ALTER TABLE `wp_woocommerce_payment_tokens`
  ADD PRIMARY KEY (`token_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `wp_woocommerce_sessions`
--
ALTER TABLE `wp_woocommerce_sessions`
  ADD PRIMARY KEY (`session_id`),
  ADD UNIQUE KEY `session_key` (`session_key`);

--
-- Indexes for table `wp_woocommerce_shipping_zones`
--
ALTER TABLE `wp_woocommerce_shipping_zones`
  ADD PRIMARY KEY (`zone_id`);

--
-- Indexes for table `wp_woocommerce_shipping_zone_locations`
--
ALTER TABLE `wp_woocommerce_shipping_zone_locations`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `location_id` (`location_id`),
  ADD KEY `location_type_code` (`location_type`(10),`location_code`(20));

--
-- Indexes for table `wp_woocommerce_shipping_zone_methods`
--
ALTER TABLE `wp_woocommerce_shipping_zone_methods`
  ADD PRIMARY KEY (`instance_id`);

--
-- Indexes for table `wp_woocommerce_tax_rates`
--
ALTER TABLE `wp_woocommerce_tax_rates`
  ADD PRIMARY KEY (`tax_rate_id`),
  ADD KEY `tax_rate_country` (`tax_rate_country`),
  ADD KEY `tax_rate_state` (`tax_rate_state`(2)),
  ADD KEY `tax_rate_class` (`tax_rate_class`(10)),
  ADD KEY `tax_rate_priority` (`tax_rate_priority`);

--
-- Indexes for table `wp_woocommerce_tax_rate_locations`
--
ALTER TABLE `wp_woocommerce_tax_rate_locations`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `tax_rate_id` (`tax_rate_id`),
  ADD KEY `location_type_code` (`location_type`(10),`location_code`(20));

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wp_actionscheduler_actions`
--
ALTER TABLE `wp_actionscheduler_actions`
  MODIFY `action_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `wp_actionscheduler_claims`
--
ALTER TABLE `wp_actionscheduler_claims`
  MODIFY `claim_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `wp_actionscheduler_groups`
--
ALTER TABLE `wp_actionscheduler_groups`
  MODIFY `group_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wp_actionscheduler_logs`
--
ALTER TABLE `wp_actionscheduler_logs`
  MODIFY `log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;
--
-- AUTO_INCREMENT for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `wp_hugeit_slider_slide`
--
ALTER TABLE `wp_hugeit_slider_slide`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wp_hugeit_slider_slider`
--
ALTER TABLE `wp_hugeit_slider_slider`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_mltlngg_terms_translate`
--
ALTER TABLE `wp_mltlngg_terms_translate`
  MODIFY `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_mltlngg_translate`
--
ALTER TABLE `wp_mltlngg_translate`
  MODIFY `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2625;
--
-- AUTO_INCREMENT for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=636;
--
-- AUTO_INCREMENT for table `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=133;
--
-- AUTO_INCREMENT for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `wp_trp_dictionary_en_us_ar`
--
ALTER TABLE `wp_trp_dictionary_en_us_ar`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;
--
-- AUTO_INCREMENT for table `wp_trp_gettext_ar`
--
ALTER TABLE `wp_trp_gettext_ar`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;
--
-- AUTO_INCREMENT for table `wp_trp_gettext_en_us`
--
ALTER TABLE `wp_trp_gettext_en_us`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1788;
--
-- AUTO_INCREMENT for table `wp_trp_original_meta`
--
ALTER TABLE `wp_trp_original_meta`
  MODIFY `meta_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `wp_trp_original_strings`
--
ALTER TABLE `wp_trp_original_strings`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;
--
-- AUTO_INCREMENT for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;
--
-- AUTO_INCREMENT for table `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `wp_wc_admin_notes`
--
ALTER TABLE `wp_wc_admin_notes`
  MODIFY `note_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `wp_wc_admin_note_actions`
--
ALTER TABLE `wp_wc_admin_note_actions`
  MODIFY `action_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `wp_wc_customer_lookup`
--
ALTER TABLE `wp_wc_customer_lookup`
  MODIFY `customer_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_wc_download_log`
--
ALTER TABLE `wp_wc_download_log`
  MODIFY `download_log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_wc_tax_rate_classes`
--
ALTER TABLE `wp_wc_tax_rate_classes`
  MODIFY `tax_rate_class_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `wp_wc_webhooks`
--
ALTER TABLE `wp_wc_webhooks`
  MODIFY `webhook_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_api_keys`
--
ALTER TABLE `wp_woocommerce_api_keys`
  MODIFY `key_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_attribute_taxonomies`
--
ALTER TABLE `wp_woocommerce_attribute_taxonomies`
  MODIFY `attribute_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_downloadable_product_permissions`
--
ALTER TABLE `wp_woocommerce_downloadable_product_permissions`
  MODIFY `permission_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_log`
--
ALTER TABLE `wp_woocommerce_log`
  MODIFY `log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_order_itemmeta`
--
ALTER TABLE `wp_woocommerce_order_itemmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_order_items`
--
ALTER TABLE `wp_woocommerce_order_items`
  MODIFY `order_item_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_payment_tokenmeta`
--
ALTER TABLE `wp_woocommerce_payment_tokenmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_payment_tokens`
--
ALTER TABLE `wp_woocommerce_payment_tokens`
  MODIFY `token_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_sessions`
--
ALTER TABLE `wp_woocommerce_sessions`
  MODIFY `session_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wp_woocommerce_shipping_zones`
--
ALTER TABLE `wp_woocommerce_shipping_zones`
  MODIFY `zone_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_shipping_zone_locations`
--
ALTER TABLE `wp_woocommerce_shipping_zone_locations`
  MODIFY `location_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_shipping_zone_methods`
--
ALTER TABLE `wp_woocommerce_shipping_zone_methods`
  MODIFY `instance_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_tax_rates`
--
ALTER TABLE `wp_woocommerce_tax_rates`
  MODIFY `tax_rate_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_tax_rate_locations`
--
ALTER TABLE `wp_woocommerce_tax_rate_locations`
  MODIFY `location_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `wp_wc_download_log`
--
ALTER TABLE `wp_wc_download_log`
  ADD CONSTRAINT `fk_wp_wc_download_log_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `wp_woocommerce_downloadable_product_permissions` (`permission_id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
