<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package storefront
 */
?><!doctype html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2.0">
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

        <?php wp_head(); ?>
    </head>

    <body <?php body_class(); ?>>

        <?php wp_body_open(); ?>

        <?php do_action('storefront_before_site'); ?>

        <div id="page" class="hfeed site">
            <?php do_action('storefront_before_header'); ?>

            <header id="masthead" class="site-header" role="banner" style="<?php storefront_header_styles(); ?>">
                <div class="top-header-black">
                    <div class="col-full">
                        <div class="top-header-column">
                            <h5>
                                <i class="fa fa-phone"></i>&nbsp; (00) 000 111 222&nbsp;&nbsp;&nbsp;
                                <i class="fa fa-envelope-square"></i>&nbsp; (00) 000 111 222
                            </h5> 
                        </div>
                        <div class="top-header-column" style="text-align: right;">
                            <h5 style="margin-bottom: 0px;">
                                <span>
                                    <a style="color: #fff !important; font-weight: 600;" href="<?php echo home_url(); ?>/my-account/">My Account</a>
                                </span>
                                <span>
                                    <?php
                                    echo do_shortcode('[language-switcher]');
// if ( function_exists( 'mltlngg_display_switcher' ) ) mltlngg_display_switcher(); 
                                    ?> 
                                </span>
                                <span class="cart-count-header">
                                    <i class="fa fa-shopping-cart"></i>
                                    <a class="cart-contents">

                                    </a>
                                </span>
                            </h5>
                        </div>
                    </div>
                </div>
                <?php
                /**
                 * Functions hooked into storefront_header action
                 *
                 * @hooked storefront_header_container                 - 0
                 * @hooked storefront_skip_links                       - 5
                 * @hooked storefront_social_icons                     - 10
                 * @hooked storefront_site_branding                    - 20
                 * @hooked storefront_secondary_navigation             - 30
                 * @hooked storefront_product_search                   - 40
                 * @hooked storefront_header_container_close           - 41
                 * @hooked storefront_primary_navigation_wrapper       - 42
                 * @hooked storefront_primary_navigation               - 50
                 * @hooked storefront_header_cart                      - 60
                 * @hooked storefront_primary_navigation_wrapper_close - 68
                 */
                do_action('storefront_header');
                ?>

            </header><!-- #masthead -->

            <?php
            /**
             * Functions hooked in to storefront_before_content
             *
             * @hooked storefront_header_widget_region - 10
             * @hooked woocommerce_breadcrumb - 10
             */
            do_action('storefront_before_content');
            ?>

            <div id="content" class="site-content" tabindex="-1">
                <div class="col-full">

                    <?php
                    do_action('storefront_content_top');
                    