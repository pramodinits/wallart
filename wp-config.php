<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wallart' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'p455w0rd' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Vknf7j4%KtNKts~o>=F0A5+Ye)2UQ6Q1@2Q3HEyl9<|@5^Uh`X Fx>@RnCygs0P<' );
define( 'SECURE_AUTH_KEY',  'tyc?.6;(ty3K++b4|=#<mcflZ1mwit%}o+*ekp/Ww6.=#i%-EjHGD=/0O,0YLXn|' );
define( 'LOGGED_IN_KEY',    'lw6#.au#$ck{wQH4I&CaBOp 3ygYw:B1nT}[!D^%eess`?G((bnb[(?k?OhJn[,[' );
define( 'NONCE_KEY',        'Ckq8ta86{[uO?)45@BOq*dh#D8bURU^[d-=)h#W:os3&R_B)K #gDy+h~<GK60I3' );
define( 'AUTH_SALT',        '_B. GGTCBYeJ1<vx /[o{M![pwK>BDVy|&&m^d_f<ePSL5.Q4j3MnWFO2nARGa/d' );
define( 'SECURE_AUTH_SALT', 'S;#x6>H{G[tac!FW;w ET %bIN+%@p<.V HU_1lh?V,`hf-VFoK~@(dACZ*e$7sA' );
define( 'LOGGED_IN_SALT',   'EikPV~G-B7*Zk|/QEr4rrm_z3;hZ@g@T&lj)3L6Y1r!+@OvloV0hzW-3g)~*~xH$' );
define( 'NONCE_SALT',       '${nr%, 3LT2VnuxC]`$VCgA7BB=W1el(eHc`o4c.0~/mwHQv2Ma3(46XQdc{S.vg' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

define('FS_METHOD','direct');

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';

